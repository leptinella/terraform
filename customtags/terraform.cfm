<cfsetting enablecfoutputonly="yes">
<!--- 
cf_TerraForm forms management environment

Copyright 2001-2010 ESWsoftware
Visit us at http://www.eswsoftware.co.nz/products/terraform/

Version. . : 2.7 beta
Date . . . : 20100111
Recent changes:-

20100111 escape action attribute to prevent XSS 
20091209 remove dots from all IDs
20090622 added cross-site scripting protection to passthroughfieldlist values and to error messages.
20081204 allow empty strings for minlength and maxlength
20080703 was throwing error using terraRule on two fields with the same name 
20070114 work-arounds to manage the CF8.01 beta whitespace string comparison bug
20061103 added allowHtml boolean to help stop form spam
20060502 added version suffix to script path 
20060424 added inlineerrors attribute and substantial changes to support them
20060419 added timespanpicker
20060313 Added caption_error and and caption_label
20060126 Focusfield default failing as looking for fieldname instead of name 
20060116 Added accelerator style to access keys
20060111 Hashes now escaped in terrarule
20050727 terrarule now deduces fieldList attribute based on fields in rule 
20050629 added labelTransformRegex and labelTransformSubstring attributes
20050601 change \ in settings.cfm to / for Linux support
20040930 added name attribute back in for XHTML. It's deprecated but qForms needs it.
20040907 no longer throws an odd error when labels without associated fields 
         encountered
20040905 added support for multiple required fields messages
20040812 HTML label tag now wrapped around cf_terralabel labels
         added focusTo attribute
20040728 moved generation of required fields error down so that custom 
         validation may manipulate the list of missing required fields.
20040718 Blue Dragon compatibility: cfcatch/ -> cfcatch /
         Default is a reserved word
20040527 New JS function setDisabled() to be available for all fields
20040526 a unique ID is now assigned to each field unless the developer has 
         assigned one already. 
20040324 added datepicker
         if the url scope is used, then TerraForm now automatically uses the GET
		 method instead of POST.
20040316 added grouplist and groupcolumn attributes, to allow grouping of options, 
		 initially on select boxes only
20040315 fixed bug where " in a field processed by cf_terrarule threw an error 
20040301 added stars format
20040227 added FCKEditor support
20040220 enhancedfile nows cleans up bad characters in file names
20040206 Form was not submitting in Netscape 4 due to hidden field being wrapped 
         in style display:none
20040205 bugfix: qForms validation failing with timepicker 
20040128 changed cgi.path_info to cgi.script_name (path_info is blank in apache)
20040104 added missing pageLocale definition
20031203 2.6
20031202 Bugfix: colorpicker failing in Netscape 4
20031201 htmlhead data being repeated - fixed
20031127 renamed terraform_core to terraform_udfs
	     added form locking for embedded distribution.
		 only the first required flag in a field is used. The rest are discarded. 
		 You can now add #chr(27)#* to the caption of grouped radio buttons -- nice
		 effect
20031124 relative settings path is now relative to page
		 datatypeattributes now ignored
20031119 caption first letter now capitalised where necessary in error messages.
20031102 moved hidebuttons() into standard.js
		 extensive small changes to improve XHTML support. New XHTML attribute
20031101 replaced attributeslist (a list) with ignoredAttributes (a struct) to improve speed
		 Addressed Mac issue with checkbox grids
		 Added form id attribute to match name attribute for XHTML compliance
20030927 debugging mentioned FORM scope when it should be the scope specified
20030923 Added a missing cfbreak at line 636 - after successfully inserting data from XML
		 scope, and added a cfif to check that there actually is content in the array returned
		 by xmlsearch()
20030913 2.5.1
20030913 Tag context bug introduced by a fault in 6.1 -- fixed
20030908 textarea bugfix (thanks Greg Walker)
		 added detection of form state tampering that could bypass validation
		 (thanks Matt Smith)
20030812 bugfix accesskey labelling
20030806 Now added option of inserting flag markers (#ESC#*) into formats
20030805 tweaked debugging info
20030729 2.5
20030716 improved terrarule - each {} value is evaluated individually before insertion 
		 in the rule, which is subsequently evaluated
20030704 improved disabled field support
20030703 improved debugging output
20030702 added multilingual captions
20030618 automatic access key flagging in labels
20030604 added multi-language support for terralabel
20030527 added jsMask
20030510 & is no longer replaced with &amp; when inserted in a field. This means 
         codes such as &#8212; will appear as the symbol itself rather than the 
		 markup code. 
		 Added the following keys to request.terraform.locale: locale.language,
		 locale.country, locale.javaLocaleId
20030418 real numbers being rounded off by localisation bug resolved
20030405 2.1 
20030329 fixed leaking internal enhancedFile fields, and enhancedFile support
		 for uppercase extensions
20030326 full variable name now allowed in inputscopelist
20030320 bug: < not being handled correctly
20030319 added workaround where relative path detection not working
		 fixed two bugs: commas in spinedit field, textarea counter not updating
		 added validation rules via cf_terrarule
20030313 fixed reversed cfcatch.tagcontext array ordering issue for CF5
20030310 2.1 beta
		 Improved whitespace management
		 increased XHTML compliance
		 added qForms n selects related compatibility
		 added extra passthrough attributes to formats
		 added automatic required field flagging
		 redesigned the settings file
		 added relative paths for custom validation and settings files
		 added formats: EnhancedFile, TimePicker, Null 
		 added preset: USState 
		 added datatype: UUID 
20030209 added formatClean functions
20030128 replaced the "this" variables with "thisField" to improve CFMX compatibility
		 removed errors and form defaults, and shifted them to the settings file
20030127 changed the terraform_format_ file prefix to format_ , changed the default
         settings file from terraform_settings.cfm to settings.cfm
20030123 Shifted InputFieldFormat() from rawvalue to the individual formats.
		 This was because some formats (WYSIWYG editors such as ActivEdit and
		 soEditor) should not have their data preprocessed like thisField. Now the rawvalue
		 really is raw.
20021222 added link to evaluation warning
20021218 added "if ( objForm.#thisField.Name# )" to qForms code as qForms
		 doesn't seem to generate an object for submit buttons, causing a
		 JS error
20021205 CSE HTML validator compliance tweaks
20021130 Added support for the undocumented locales: Chinese (China), Chinese (Hong Kong),
         Chinese (Taiwan)
20021010 put some evaluate()s back for CF5 compatibility
20020921 added request.terraform.null
20020916 replaced placeholder tags with ESC characters. Added security
20020914 added i18m and l10n functions to terraform_core
20020911 Tweaked around lne 525, no longer tries to validate prompt text
20020907 Now allows placeholders in custom error messages. This is useful where a preset
		 declares the error attribute.
20020829 Replaced SettingsDirectory attribute with SettingsPath attribute
		 Improved handling of UDFs for validation etc - removed cftry and used
		 isCustomFunction() instead
20020820 Adjusted so that a developer can force enctype="application/x-www-form-urlencoded"
		 even if a file field is used
20020813 Added request.terraform.FormFoot
20020713 Improved "Datatype (Maxlength)" handling
		 Can now specify CustomValidationPath as a full path or relative to the
		 location of the TerraForm custom tag. CF sometimes fails on long CFINCLUDE paths.
		 Using UDF InputFieldFormat() instead of HTMLEditFormat(). This was interfering with
		 double-byte languages. 
20020629 Added ResourcePath attribute
20020627 Added support for Japanese and Korean, the new CFMX locales
		 Added Focus (boolean) and FocusField (name of field to send initial 
		 focus to) attributes, and automatic send focus to error fields
20020626 Now can specify Datatype="dddddd (nn)" where dddddd is the datatype and 
		 nn is the MaxLength.
20020616 Improved handling of text prompts
20020615 Form. now works properly for boolean fields
20020613 Datatypes can now declare attributes using 
		 DataTypeAttributes.[Datatype] = "[attribute1],[attribute2],..."
		 Added request.terraform.HTMLHead - formats can dump JS in here to go
		 into the page head. This is preferable to using CFHTMLHead as it isn't
		 used if the form is valid.
		 Improved time handling
		 Added date range error messages
20020610 Bug in button hiding script
20020608 Added Min and Max attributes - equivalent to range
20020524 Added Submit format
		 Added HideButtonsOnSubmit attribute
		 Added onSubmit attribute to form
		 New Value attribute overrides dynamic computation of value 
         using defaults and input scopes
20020522 Now allows forms with no fields
20020521 Added better error trapping where ValueColumn not specified
20020424 Fix bug inferring datatypes from formats and formats from datatypes
		 Fix bug cf_terrafield InputScopeList 
		 Fix bug request.terraform.datemask not being set
20020418 Now uses data from all rows if you specify a query
		 as an inputscope
		 Fixed bug re passing HTML attributes to checkboxes
		 Added query error msg
20020415 Fixed handling of quotation marks in fields
	 	 Changed Implication to preset
20020407 Added language filtering
20020406 Added client locale detection
		 Improved date handling
20020403 Added further qForms validation for datatypes.
20020324 Added request.terraform.SettingsDirectory as default SettingsDirectory.
20020322 Stopped error repetition when multiple fields
		 with the same name are set required
20020320 qForms support: qForms and qFormsPath attributes
		 repair bug when AlwaysDisplayForm="YES"
--->





<cfswitch expression="#ThisTag.ExecutionMode#">
	
	<cfcase value="start">
	
<!--- 
************************************************************
**  PART 1: Check for correct cf_terraform tag use and       **
**  initialise attributes                                 **
************************************************************
--->
		
		<cfif NOT ThisTag.HasEndTag>
			<cfabort showerror="TerraForm implementation error: cf_terraform must have an end tag.">
		</cfif>
		
		<cftry>
			<cfinclude template="terraform_udfs.cfm">
			<cfcatch type="MissingInclude">
				<cfabort showerror="TerraForm implementation error: terraform_udfs.cfm should be placed in the same folder as terraform.cfm .">
			</cfcatch>
		</cftry>
		<cfparam name="request.eswsoftware" default="#structNew()#">
		<cfset request.eswsoftware.terraform = "2.7 beta">
		
<!--- 
************************************************************
** PART 2: Initialise working variables and structures    **
** and read the settings file                             **
************************************************************
--->

		<cfscript>
		
			evaluationFields = 0;
			locked = false;
			keys = "";
			
			pageLocale = getLocale();
			Presets = StructNew();
			attributeslist = ""; // provided for backward compatibility with old formats
			qFormsValidate = StructNew();			
			RangeValidationStyle = StructNew();
			DataTypeAttributes = StructNew();
			StatusTagCreatedManually = FALSE;
			qFormsScript = "";
			if ( NOT StructKeyExists(Request, "TerraForm") )
				Request.TerraForm = StructNew();	
			if ( NOT StructKeyExists(Request.TerraForm, "instance") )
				Request.TerraForm.instance = 0;	
			Request.TerraForm.instance = Request.TerraForm.instance + 1;	
			request.terraform.CurrentFieldNum = 0;
			request.terraform.CurrentLabelNum = 0;
			request.terraform.CurrentRuleNum = 0;
			if ( NOT StructKeyExists(Request.TerraForm, "HTMLHead") )
				request.terraform.HTMLHead = "";
			request.terraform.FormFoot = "";
			request.terraform.null = structNew();
			request.terraform.cfsqltype = structNew();
			request.terraform.flags = structNew();
			request.terraform.fields = structnew();
			if ( NOT StructKeyExists(Request.TerraForm, "SettingsPath") )
				request.terraform.SettingsPath = "terraform/settings.cfm";
			if ( NOT IsDefined("Attributes.SettingsPath") )
				Attributes.SettingsPath = request.terraform.SettingsPath;
			ESC = Chr(27); // Escape
			SUB = Chr(26); // Substitute, we'll use it as a placeholder
			PASSTHROUGH = SUB;
			CUSTOM1 = ""; // CUSTOM1 and CUSTOM2 are used to pass
			CUSTOM2 = ""; // miscellaneous pieces of data into error messages
			RS = Chr(30); // Record separator, we'll use it as a delimiter
			errors = structNew();
			foundARequiredField = false;
		</cfscript>
		
	
		
		
		<!--- calculate path back to calling template. This will be used for intelligently processing paths supplied by the developer. We have one report of this not working on a CF5 platform. It seems as if there is nothing returned in the cfcatch.tagcontext array. If this is the case, relative paths will not work. --->
		<cfset parentTemplate = "">
 		<cftry>
			<cfthrow message="No error. Just examining tag context.">
			<cfcatch type="Any">
				<!--- the cfcatch.tagcontext array is in reverse order on the CFMX server --->
				<cfif listfirst(server.coldfusion.productversion) eq 5>
					<!--- CF5 server --->
					<cfloop index="i" from="#arraylen(cfcatch.tagcontext)#" to="1" step="-1">
						<cfif cfcatch.tagcontext[i].template neq getcurrenttemplatepath()>
							<cfset parentTemplate = cfcatch.tagcontext[i].template>
							<cfbreak>
						</cfif>
					</cfloop>
				<cfelse>
					<!--- CFMX server --->
					<cfloop index="i" from="1" to="#arraylen(cfcatch.tagcontext)#">
						<cfif cfcatch.tagcontext[i].template neq getcurrenttemplatepath() and (len(cfcatch.tagcontext[i].template) gt 1) and not find("(", cfcatch.tagcontext[i].template)> <!--- bugfix for 6.1: 6.1 spits out "template paths" of just one letter (apparently the drive letter) sometimes. Skip these. ---> <!--- bugfix for 7.02 --->
							<cfset parentTemplate = cfcatch.tagcontext[i].template>
							<cfbreak>
						</cfif>
					</cfloop>				
				</cfif>
			</cfcatch>
		</cftry>
		<cfset currenttemplatefolder = getdirectoryfrompath(getcurrenttemplatepath())>
		<cfif len(parentTemplate)> <!--- if the code above did not fail... --->
			<cfset parentTemplateFolder = getdirectoryfrompath(parentTemplate)>
			<cfloop index="i" from="1" to="#min(listlen(parentTemplateFolder, "/\"), listlen(currenttemplatefolder, "/\"))#">
				<cfif listGetAt(parentTemplateFolder, i, "/\") eq listGetAt(currenttemplatefolder, i, "/\")>
					<cfset currenttemplatefolder = listSetAt(currenttemplatefolder, i, SUB, "/\")>
					<cfset parentTemplateFolder = listSetAt(parentTemplateFolder, i, SUB, "/\")>
				<cfelse>	
					<cfbreak>	
				</cfif>
			</cfloop>
			<cfset relativePathToParentTemplateFolder = "">
			<cfloop index="i" from="#listLen(currenttemplatefolder, "/\")#" to="1" step="-1">
				<cfset folder = listGetAt(currenttemplatefolder, i, "/\")>
				<cfif not Compare(folder, SUB)>
					<cfbreak>
				</cfif>
				<cfset relativePathToParentTemplateFolder = listPrepend(relativePathToParentTemplateFolder, "..", "\")>
			</cfloop>
			<cfloop index="i" from="1" to="#listLen(parentTemplateFolder, "/\")#">
				<cfset folder = listGetAt(parentTemplateFolder, i, "/\")>
				<cfif Compare(folder, SUB)>
					<cfset relativePathToParentTemplateFolder = listAppend(relativePathToParentTemplateFolder, folder, "\")>
				</cfif>
			</cfloop>	
			<cfif len(relativePathToParentTemplateFolder)>
				<cfset relativePathToParentTemplateFolder = relativePathToParentTemplateFolder & "\">
			</cfif>
		</cfif>
		
	
		<cfset request.terraform.browser = getBrowser()>

		<!--- Check if path is absolute or relative. If relative, first attempt to incorporate settings file relative to parent template. If this fails, 
		try to incorporate settings file relative to this template. --->
		
		<cfif listFindNoCase("/,\", left(Attributes.SettingsPath,1))>
		
			<!--- it's a mapped path. Use as is. --->
			<cftry>
				<cfinclude template="#Attributes.SettingsPath#">
				<cfset pathToSettingsFolder = getDirectoryFromPath(Attributes.SettingsPath)>
				<cfcatch type="MissingInclude">
					<cfabort showerror="TerraForm implementation error: Problem with custom file: ""#Attributes.SettingsPath#"" - not found.">
				</cfcatch>
			</cftry>
			
		<cfelseif find(":", Attributes.SettingsPath)>
		
			<!--- it's an absolute path. Turn it into a relative path. --->
			<cfset currenttemplatefolder = getdirectoryfrompath(getcurrenttemplatepath())>
			<cfset settingsFolder = getdirectoryfrompath(Attributes.SettingsPath)>
			<cfloop index="i" from="1" to="#min(listlen(settingsFolder, "/\"), listlen(currenttemplatefolder, "/\"))#">
				<cfif listGetAt(settingsFolder, i, "/\") eq listGetAt(currenttemplatefolder, i, "/\")>
					<cfset currenttemplatefolder = listSetAt(currenttemplatefolder, i, SUB, "/\")>
					<cfset settingsFolder = listSetAt(settingsFolder, i, SUB, "/\")>
				<cfelse>	
					<cfbreak>	
				</cfif>
			</cfloop>
			<cfset relativePathToSettingsFolder = "">
			<cfloop index="i" from="#listLen(currenttemplatefolder, "/\")#" to="1" step="-1">
				<cfset folder = listGetAt(currenttemplatefolder, i, "/\")>
				<cfif folder eq SUB>
					<cfbreak>
				</cfif>
				<cfset relativePathToSettingsFolder = listPrepend(relativePathToSettingsFolder, "..", "\")>
			</cfloop>
			<cfloop index="i" from="1" to="#listLen(settingsFolder, "/\")#">
				<cfset folder = listGetAt(settingsFolder, i, "/\")>
				<cfif folder neq SUB>
					<cfset relativePathToSettingsFolder = listAppend(relativePathToSettingsFolder, folder, "\")>
				</cfif>
			</cfloop>	
			<cfset relativePathToSettingsFile = relativePathToSettingsFolder & "\" & getfilefrompath(Attributes.SettingsPath)>
			<cfset pathToSettingsFolder = relativePathToSettingsFolder & "\">
			
			<cftry>
				<cfinclude template="#relativePathToSettingsFile#">
				<cfcatch type="MissingInclude">
					<cfabort showerror="TerraForm implementation error: Problem with custom file: ""#Attributes.SettingsPath#"" - not found.">
				</cfcatch>
			</cftry>
			
		<cfelse>
			<!--- it's a relative path. Try to include file relative to the calling template. --->
			<!--- <cfif IsDefined("relativePathToParentTemplateFolder") and fileExists(expandPath("#relativePathToParentTemplateFolder##Attributes.SettingsPath#"))> ---><cfif IsDefined("variables.relativePathToParentTemplateFolder") and fileExists(expandPath(Attributes.SettingsPath))>
				<cfinclude template="#relativePathToParentTemplateFolder##Attributes.SettingsPath#">
				<!--- <cfset pathToSettingsFolder = getDirectoryFromPath("#relativePathToParentTemplateFolder##Attributes.SettingsPath#")> ---><cfset pathToSettingsFolder = getDirectoryFromPath(expandPath(Attributes.SettingsPath))>
			<cfelse>
				<cftry>
					<!--- Try to include file relative to this template. --->
					<cfinclude template="#Attributes.SettingsPath#">
					<cfset pathToSettingsFolder = getDirectoryFromPath(Attributes.SettingsPath)>
					<cfcatch type="MissingInclude">
						<cfif IsDefined("variables.relativePathToParentTemplateFolder")>
							<cfabort showerror="TerraForm implementation error: Problem with custom file: ""#Attributes.SettingsPath#"" - not found relative to calling template or terraform.cfm custom tag.">
						<cfelse>
							<cfabort showerror="TerraForm implementation error: Problem with custom file: ""#Attributes.SettingsPath#"" - not found relative to terraform.cfm custom tag. Note that paths relative to the calling template are not supported on your ColdFusion platform. If your settings file is located in the same folder as your form, consider writing your relative path as follows: SettingsPath=""##GetDirectoryFromPath(GetCurrentTemplatePath())##[mysettingsfile].cfm"") .">
						</cfif>
					</cfcatch>
				</cftry>
			</cfif>

		</cfif>
		
		<!--- remainder of declarations --->
		<cfparam name="Attributes.FormName" default="">
		
		<cfparam name="Attributes.Name" default="#Attributes.FormName#">
		<cfif NOT Len(Attributes.Name)>
			<cfabort showerror="TerraForm implementation error: cf_terraform requires a NAME / FORMNAME attribute.">
		</cfif>
		
		<cfif Request.TerraForm.instance eq 1>
			<cfset request.terraform.htmlhead = request.terraform.htmlhead & "<script src=""#attributes.resourcepath#javascript/standard.js"" type=""text/javascript""></script><link rel=""stylesheet"" href=""#attributes.resourcepath#style.css"" type=""text/css""#closeTag#">
		</cfif>
		
		<cfif Attributes.HideButtonsOnSubmit>
			<cfset Attributes.onSubmit = ListAppend(Attributes.onSubmit, "hidebuttons(this)", ";")>
		</cfif>

		<cfset Scope = "Caller.#Attributes.Scope#">
				
		<cfparam name="attributes.xml" default="">
		<cfif len(attributes.xml)>
			<cfset Attributes.InputScopeList = listprepend(Attributes.InputScopeList, attributes.xml)>
		</cfif>
		
		<cfloop index="i" from="1" to="#ListLen(Attributes.InputScopeList)#">
			<cfset Attributes.InputScopeList = ListSetAt(Attributes.InputScopeList, i, "Caller.#ListGetAt(Attributes.InputScopeList, i)#")>
		</cfloop>
		
		<!--- The form has a hidden control field with the same name as the form. This field records the form's current state. --->
		<cfparam name="#Scope#.#Attributes.Name#" default="NEW">
		<cfset State = Evaluate("#Scope#.#Attributes.Name#")>
		<cfif not compareNoCase(state, "valid")>
			<!--- Should never be valid at this stage. This line prevents a malicious user altering the value of the state field to bypss validation. --->
			<cfset state = "form">
			<cfset "#Scope#.#Attributes.Name#" = "form">
		</cfif>
		<cfset request.terraform.DateMask = request.terraform.Locale.shortDateMask>
		<cfset request.terraform.DateHint = request.terraform.Locale.shortDateHint>
		
	</cfcase>
	
	<cfcase value="end">
		
		<cfif locked>
			<cfwddx action="CFML2WDDX" input="#thisTag.fields#" output="packet" usetimezoneinfo="No">
			<cfset key = hash(packet)>
			<cfif not listFind(keys, key)>
				<cfabort showerror="This form [#key#] is unauthorised. This distribution of TerraForm may not be reused or used with modified forms. However you may replace this version with a licenced version available from http://www.eswsoftware.com/.">
			</cfif>
		</cfif>
		
<!--- 
************************************************************
**  PART 3: Now at the closing tag, initialise the        **
**  for all the form fields. The nested cf_terrafield tag **
**  has loaded all the field attributes into the          **
**  ThisTag.Fields array. This is scanned for missing     **
**  required attributes. If a query is specified for a    **
**  field then this is translated into a ValueList and    **
**  DisplayList.                                          **
************************************************************
--->

		<cfif NOT StructKeyExists(ThisTag, "Fields")>
			<cfset ThisTag.Fields = ArrayNew(1)>
		</cfif>
		<cfset CountFields = ArrayLen(ThisTag.Fields)>
		<cfif NOT StructKeyExists(ThisTag, "Labels")>
			<cfset ThisTag.Labels = ArrayNew(1)>
		</cfif>		
		<cfif NOT StructKeyExists(ThisTag, "Rules")>
			<cfset ThisTag.Rules = ArrayNew(1)>
		</cfif>		
		<cfset CountLabels = ArrayLen(ThisTag.Labels)>
		<cfset CountRules = ArrayLen(ThisTag.Rules)>
		<cfif evaluationFields>
			<cfset CountFields = min(CountFields, evaluationFields)>
			<cfloop index="i" from="#incrementValue(CountFields)#" to="#ArrayLen(ThisTag.Fields)#">
				<cfset ThisTag.GeneratedContent = Replace(ThisTag.GeneratedContent, "#ESC#terrafield #i##ESC#", "<div style=""color : white; background-color : ##990000""><strong>[TerraForm evaluation limited to #evaluationFields# fields. Please <a href=""http://www.eswsoftware.com/products/terraform/purchase/"">purchase a TerraForm licence</a>]</strong></div>")>
			</cfloop>
		</cfif>

		<cfloop index="i" from="1" to="#CountFields#">
			<cfset ThisField = ThisTag.Fields[i]>
			
			<cfif StructKeyExists(ThisField, "Preset")>
				<!--- <cfinclude template="#RelativePathToSettingsDirectory#/presets/#thisField.Preset#.cfm"> --->
				<!--- change February 2003 --->
				<cftry>
					<cfloop collection="#Presets[thisField.Preset]#" item="ThisAttribute">
						<cfparam name="thisField.#ThisAttribute#" default="#Presets[thisField.Preset][ThisAttribute]#">
					</cfloop>
					<cfcatch>
						<cfabort showerror="TerraForm implementation error: Unrecognised preset: ""#thisField.Preset#"" - check that it is spelled correctly and that it is found in your settings file (usually presets.cfm).">
					</cfcatch>
				</cftry>
			</cfif>
			
			
			
			<cfif StructKeyExists(ThisField, "Datatype")>
				<cfscript>
			 		// alternative datatype formatting, like "varchar (50)"
			 		OpenBracket = Find("(", thisField.DataType);
			 		if ( OpenBracket ) {
			  			CloseBracket = Find(")", thisField.DataType, OpenBracket);
			  			if ( OpenBracket AND CloseBracket ) {
			   				if ( NOT StructKeyExists(ThisField, "MaxLength") )
								thisField.MaxLength = Mid(thisField.DataType, OpenBracket+1, CloseBracket
		- OpenBracket - 1);
			   				thisField.DataType = Trim(Left(thisField.DataType, OpenBracket-1));
			  			}
			 		}
				</cfscript>
				<cfset PresetName="Datatype_#thisField.Datatype#">
				<cfif StructKeyExists(Presets, PresetName) AND StructKeyExists(Presets[PresetName], "Format")>
			 		<cfparam name="thisField.Format" type="string" default="#Presets[PresetName].Format#">
				</cfif>
		   </cfif>   


			<cfparam name="thisField.Format" type="string" default="text">	
			<cfif attributes.smartdegrade>
				<cftry>
					<cfset thisField.Format = evaluate("#thisField.Format#FormatDegrade()")>
					<cfcatch />
				</cftry>
			</cfif>
						
			<cfset PresetName="Format_#thisField.Format#">
			<cfif StructKeyExists(Presets, PresetName) AND StructKeyExists(Presets[PresetName], "Datatype")>
				<cfparam name="thisField.DataType" default="#Presets[PresetName].Datatype#">
			</cfif>			
			
			<cfparam name="thisField.DataType" default="default">
			
			<cfscript>
				// alternative datatype formatting, like "varchar (50)"
				OpenBracket = Find("(", thisField.DataType);
				if ( OpenBracket ) {
					CloseBracket = Find(")", thisField.DataType, OpenBracket);
					if ( OpenBracket AND CloseBracket ) {
						if ( NOT StructKeyExists(ThisField, "MaxLength") )
							thisField.MaxLength = Mid(thisField.DataType, OpenBracket+1, CloseBracket - OpenBracket - 1);
						thisField.DataType = Trim(Left(thisField.DataType, OpenBracket-1));
					}
				}
			</cfscript>
			
			<cfset PresetName="DataType_#thisField.DataType#">
			<cfif StructKeyExists(Presets, PresetName)>
				<cfloop collection="#Presets[PresetName]#" item="ThisAttribute">
					<cfparam name="thisField.#ThisAttribute#" default="#Presets[PresetName][ThisAttribute]#">
				</cfloop>
			</cfif>
			
			<cfset PresetName="Format_#thisField.Format#">
			<cfif StructKeyExists(Presets, PresetName)>
				<cfloop collection="#Presets[PresetName]#" item="ThisAttribute">
					<cfparam name="thisField.#ThisAttribute#" default="#Presets[PresetName][ThisAttribute]#">
				</cfloop>
			</cfif>
				
			<cfparam name="thisField.Required" type="boolean" default="NO">
			<cfif thisField.Required>
				<cfset foundARequiredField = true>
			</cfif>
			
			<cfif structkeyexists(thisfield, "disabled") and isboolean(thisfield.disabled) and (not thisfield.disabled)>
				<cfset structdelete(thisfield, "disabled")>
			</cfif>
			
			<cfparam name="thisField.FieldName" default="">
			<cfparam name="thisField.Name" default="#thisField.FieldName#">
			<cfif NOT Len("thisField.Name")>
				<cfabort showerror="TerraForm implementation error: CF_terrafield ###i# in your form has a missing NAME / FIELDNAME attribute.">
			</cfif>

			<cfif structKeyExists(request.terraform.fields, thisField.name)>
				<cfif isarray(request.terraform.fields[thisField.name])>
					<cfset arrayappend(request.terraform.fields[thisField.name], thisfield)>
				<cfelse>
					<cfset temp = request.terraform.fields[thisField.name]>
					<cfset request.terraform.fields[thisField.name] = arraynew(1)>
					<cfset request.terraform.fields[thisField.name][1] = temp>
					<cfset request.terraform.fields[thisField.name][2] = thisfield>
				</cfif>
				<cfparam name="thisField.id" type="string" default="terraForm_#attributes.name#_#thisField.name##arrayLen(request.terraform.fields[thisField.name])#">	
			<cfelse>
				<cfset request.terraform.fields[thisField.name] = thisfield>
				<cfparam name="thisField.id" type="string" default="terraForm_#attributes.name#_#thisField.name#">	
			</cfif>
			
			<cfset thisField.ErrorCode = "OK">
			<cfset thisfield.debug_validationApplied = "">			
			
			<cfif structkeyexists(thisfield, "caption_" & request.terraform.locale.javaLocaleId)>
				<cfset thisField.Caption = thisfield["caption_" & request.terraform.locale.javaLocaleId]>
			<cfelseif structkeyexists(thisField, "caption_" & request.terraform.locale.languageCode)>
				<cfset thisField.Caption = thisfield["caption_" & request.terraform.locale.languageCode]>
			<cfelseif structkeyexists(thisField, "caption_" & request.terraform.locale.countryCode)>
				<cfset thisField.Caption = thisfield["caption_" & request.terraform.locale.countryCode]>
			<cfelseif structkeyexists(thisField, "caption_" & request.terraform.locale.language)>
				<cfset thisField.Caption = thisfield["caption_" & request.terraform.locale.language]>
			<cfelse>
				<cfparam name="thisField.Caption" type="string" default="#thisField.Name#">
			</cfif>
			<cfset thisField.caption_label = reReplace(thisField.Caption, "^\([^)]*\)", "")>
			<cfset thisField.caption_error = reReplace(thisField.Caption, "^\(([^)]*)\)", "\1")> 

						
			<cfparam name="thisField.allowHtml" type="boolean" default="Yes">
			<cfparam name="thisField.ValueList" default="">
			<cfparam name="thisField.groupList" default="">
			<cfparam name="thisField.Pattern" default="">
			<cfparam name="thisField.DisplayList" default="#thisField.ValueList#">		
			<cfparam name="thisField.Multiple" type="boolean" default="NO">
			<cfparam name="thisField.Min" default="">
			<cfparam name="thisField.Max" default=""> 	
			<cfparam name="thisField.Value" default="">
			<cfparam name="thisField.Error" default="">
			<cfparam name="thisField.LanguageFilter" type="boolean" default="NO">
			<cfparam name="thisField.default" default="">
		
			<cfparam name="thisField.InputScopeList" default="">
			<cfif Len(thisField.InputScopeList)>
				<cfloop index="i" from="1" to="#ListLen(thisField.InputScopeList)#">
					<cfset thisField.InputScopeList = ListSetAt(thisField.InputScopeList, i, "Caller.#ListGetAt(thisField.InputScopeList, i)#")>
				</cfloop>
			</cfif>
			
			<cfif IsDefined("variables.thisField.Query")>
				<cfif NOT IsDefined("Caller.#thisField.Query#") OR NOT IsQuery(Evaluate("Caller.#thisField.Query#"))>
					<cfabort showerror="TerraForm implementation error: the query ""#thisField.Query#"" required by the #thisField.Name# field does not exist.">
				</cfif>
				<cfparam name="thisField.Delimiters" default="#RS#">
				<cfif NOT IsDefined("variables.thisField.ValueColumn")>
					<cfabort showerror="TerraForm implementation error: You have used the Query attribute for field #thisField.Name#. You must also specify the ValueColumn.">
				</cfif>
				<cfset thisField.ValueList=Evaluate("ValueList(Caller.#thisField.Query#.#thisField.ValueColumn#,thisField.Delimiters)")>

				<cfparam name="thisField.DisplayColumn" default="#thisField.ValueColumn#">
				<cfset thisField.DisplayList=Evaluate("ValueList(Caller.#thisField.Query#.#thisField.DisplayColumn#,thisField.Delimiters)")>
				
				<cfparam name="thisField.groupColumn" default="">
				<cfif len(thisField.groupColumn)>
					<cfset thisField.groupList=Evaluate("ValueList(Caller.#thisField.Query#.#thisField.groupColumn#,thisField.Delimiters)")>
				<cfelse>
					<cfset thisField.groupList = "">
				</cfif>

			</cfif>
			<cfparam name="thisField.Delimiters" default=",">
			<!--- fix up any empty list elements --->
			<cfscript>
				thisField.ValueList = Replace(thisField.ValueList, "#thisField.Delimiters##thisField.Delimiters#", "#thisField.Delimiters# #thisField.Delimiters#", "ALL");
				thisField.ValueList = Replace(thisField.ValueList, "#thisField.Delimiters##thisField.Delimiters#", "#thisField.Delimiters# #thisField.Delimiters#", "ALL");
				if ( not Compare(Left(thisField.ValueList, 1), thisField.Delimiters) )
					thisField.ValueList = " #thisField.ValueList#";
				if ( not Compare(Right(thisField.ValueList, 1), thisField.Delimiters) )
					thisField.ValueList = "#thisField.ValueList# ";
				
				thisField.DisplayList = Replace(thisField.DisplayList, "#thisField.Delimiters##thisField.Delimiters#", "#thisField.Delimiters# #thisField.Delimiters#", "ALL");
				thisField.DisplayList = Replace(thisField.DisplayList, "#thisField.Delimiters##thisField.Delimiters#", "#thisField.Delimiters# #thisField.Delimiters#", "ALL");
				if ( not Compare(Left(thisField.DisplayList, 1), thisField.Delimiters) )
					thisField.DisplayList = " #thisField.DisplayList#";
				if ( not Compare(Right(thisField.DisplayList, 1), thisField.Delimiters) )
					thisField.DisplayList = "#thisField.DisplayList# ";
					
				thisField.groupList = Replace(thisField.groupList, "#thisField.Delimiters##thisField.Delimiters#", "#thisField.Delimiters# #thisField.Delimiters#", "ALL");
				thisField.groupList = Replace(thisField.groupList, "#thisField.Delimiters##thisField.Delimiters#", "#thisField.Delimiters# #thisField.Delimiters#", "ALL");
				if ( not Compare(Left(thisField.groupList, 1), thisField.Delimiters) )
					thisField.groupList = " #thisField.groupList#";
				if ( not Compare(Right(thisField.groupList, 1), thisField.Delimiters) )
					thisField.groupList = "#thisField.groupList# ";	
					
			</cfscript>
			<cfparam name="thisField.MinLength" default="" type="string">
			<cfparam name="thisField.MaxLength" default="" type="string">
			<cfset thisField.MinLength = val(thisField.MinLength)>
			<cfset thisField.MaxLength = val(thisField.MaxLength)>
			<cfparam name="thisField.xpath" type="string" default="">
			
			<cfif not len(thisfield.name)>
				<cfabort showerror="TerraForm implementation error: Form field ###i# is missing a NAME or FIELDNAME attribute. This attribute is required.">
			</cfif>
			
			<cfif thisField.Name EQ Attributes.Name>
				<cfset StatusTagCreatedManually = TRUE>
			</cfif>
			
		</cfloop>

		<cfif CountFields>
  			<cfparam name="Attributes.FocusField" default="#ThisTag.Fields[1].Name#">
  		</cfif>
		
		
		
<!--- 
************************************************************
**  PART 4. Load the value for each field depending on    **
**  circumstances:                                        **
**   - If it's a new form, look for a value in all
**     applicable scopes or choose the default and        **
**     pre-process it.                                    **
**   - If it's a posted form, load the form values        **
************************************************************
--->
		
		<cfloop index="i" from="1" to="#CountFields#">
			<cfset ThisField = ThisTag.Fields[i]>
			<cfset Debug_ValidationApplied = "">
			
			<cfif NOT Len(thisField.Value)>
			
				<cfif not compareNoCase(state, "new")><!---  or structkeyexists(thisfield, "disabled") --->
					<!--- If it's a new form, pre-process and load the defaults --->
					<cfset Done = FALSE>
					<cfset ScopesForThisField = ListAppend(ListAppend(Scope, thisField.InputScopeList), Attributes.InputScopeList)>
					<cfloop index="ThisScope" list="#ScopesForThisField#">
					
						<cftry>
							<cfset isSimple = evaluate("issimplevalue(#ThisScope#)")>
							<cfcatch>
								<cfset isSimple = false>
							</cfcatch>
						</cftry>
						
						<cfif isSimple>
							<cfset thisField.Value = Evaluate(ThisScope)>
							<cfset Done = TRUE>
							<cfset thisfield.debug_found = """#thisField.Value#"" as #ThisScope#">
							<cfbreak>
						</cfif> 

						<cfif len(thisField.xpath) and IsDefined(ThisScope) and evaluate("IsXmlDoc(#ThisScope#)")>
							<!--- search for the data in the xml --->
							<cftry>
								<cfset temp = xmlsearch(evaluate(ThisScope), thisField.xpath)>
								<cfif arraylen(temp)>
									<cfloop from="1" to="#arraylen(temp)#" index="xmlCounter">
										<cfset thisField.Value = listAppend(thisField.Value, temp[xmlCounter].xmlText)>
									</cfloop>
									<cfset Done = TRUE>
									<cfset thisfield.debug_found = """#thisField.Value#"" in XML: #ThisScope#">
									<cfbreak>
								</cfif>
								<cfcatch />
							</cftry>
						</cfif>
												
						<cftry>
							<cfif IsDefined(ThisScope) and IsDefined("#ThisScope#.#thisField.Name#")>
								<cfif IsDefined(ThisScope) AND Evaluate("IsQuery(#ThisScope#)") AND thisField.Multiple>
									<cfset thisField.Value = Evaluate("ValueList(#ThisScope#.#thisField.Name#)")>
								<cfelse>
									<cfset thisField.Value = Evaluate("#ThisScope#.#thisField.Name#")>
								</cfif>
								<cfset Done = TRUE>
								<cfset thisfield.debug_found = """#thisField.Value#"" in #ThisScope#">
								<cfbreak>
							</cfif>
							<cfcatch />
						</cftry>
						
					</cfloop>
					
					<cfif NOT Done>
						<cfset thisField.Value=thisField.Default>	
						<cfset thisfield.debug_found = """#thisField.Value#"" in DEFAULT">
					</cfif>
					
					<!--- Try to preprocess data --->
					<!---
        			this works in cfmx but is not backwards compatible
     				<cfif IsDefined("#thisField.DataType#In") AND IsCustomFunction(variables["#thisField.DataType#In"])>
    				--->
     				<cfif IsDefined("variables.#thisField.DataType#In") AND IsCustomFunction(evaluate("#thisField.DataType#In"))>
						<cfset thisField.Value = Evaluate("#thisField.DataType#In(thisField.Value)")>
					</cfif>
				<cfelse>
					<!--- formatclean here allows multi-HTML field form fields to be reassembled into one field --->
					<cfif IsDefined("variables.#thisField.format#FormatClean") and IsCustomFunction(evaluate("#thisField.format#FormatClean"))>
						<cfset thisField.Value = evaluate("#thisField.format#FormatClean(thisField.Value)")>
						<cfset "#scope#.#thisField.name#" = thisField.Value>
					</cfif>
					
					<cfif IsDefined("#Scope#.#thisField.Name#")>
						<cfset thisField.Value = Evaluate("#Scope#.#thisField.Name#")>
						<cfset thisfield.debug_found = """#thisField.Value#"" in #Scope#">
					<cfelse>
						<cfset thisfield.debug_found = "NOT FOUND">
					</cfif>
				</cfif>
				
			<cfelse>
				<cfset thisfield.debug_found = """#thisField.Value#"" in VALUE">
			</cfif>
			<cfset thisField.RawValue = thisField.Value>

		</cfloop>	

<!--- 
************************************************************
**  PART 5. If it's a posted form, validate it            **
************************************************************
--->

		<cfset ErrorFieldList = "">
		<cfset ErrorRequiredFieldList = "">
		<cfset ErrorMessageList = "">
		
		<cfif not compareNoCase(state, "form")>
			
			<!--- Tidy up raw values and check Required fields first --->
			<cfset MissingRequiredFields = "">
			<cfloop index="i" from="1" to="#CountFields#">
				<cfset ThisField = ThisTag.Fields[i]>
				<!---
       			this works in cfmx but is not backwards compatible
    			<cfif isDefined("#thisField.DataType#Clean") and IsCustomFunction(variables["#thisField.DataType#Clean"])>
				--->
				<cfif IsDefined("variables.#thisField.Format#FormatClean") and IsCustomFunction(evaluate("#thisField.Format#FormatClean"))>
					<cfset thisField.Value = evaluate("#thisField.Format#FormatClean(thisField.Value)")>
				</cfif>
				<cfif IsDefined("variables.#thisField.DataType#Clean") and IsCustomFunction(evaluate("#thisField.DataType#Clean"))>
					<cfset thisField.Value = evaluate("#thisField.DataType#Clean(thisField.Value)")>
				</cfif>
					
				<cfif StructKeyExists(ThisField, "Prompt") AND (thisField.Value EQ thisField.Prompt)>
					<cfset thisField.Value = "">
				</cfif>		
				
				<cfif	
					thisField.Required
					AND (NOT ListFindNoCase(MissingRequiredFields, thisField.Caption_error, RS))
					AND (
						NOT Len(thisField.Value)
						OR (
							StructKeyExists(ThisField, "PromptValue")
							AND (thisField.Value EQ thisField.PromptValue)
						)	
					)
				>	
					<cfset MissingRequiredFields = ListAppend(MissingRequiredFields, thisField.Caption_error, RS)>
					<cfset ErrorRequiredFieldList = ListAppend(ErrorRequiredFieldList, thisField.name)>
					<cfset ErrorFieldList = ListAppend(ErrorFieldList, thisField.Name)>
				</cfif>
			</cfloop>
			
			
			
		
			
			<cfset MovedFocus = "No">
			<cfloop index="i" from="1" to="#CountFields#">
				<cfset ThisField = ThisTag.Fields[i]>
				
				<cfif Len(thisField.Value) AND NOT ListFindNoCase(ErrorFieldList, thisField.Name)>	
	
					<!--- Value list validation --->
					<cfif Len(thisField.ValueList) AND ((NOT IsDefined("variables.thisField.EnforceValueList")) OR thisField.EnforceValueList)>
						<cfset thisfield.debug_validationApplied = ListAppend(thisfield.debug_validationApplied, "ValueList")>
						<cfif IsDefined("variables.thisField.Multiple") AND thisField.Multiple>
							<cfloop index="ThisValue" list="#thisField.Value#">
								<cfif NOT ListFindNoCase(thisField.ValueList, ThisValue, thisField.Delimiters)>
									<cfif IsDefined("variables.thisField.PromptValue")>
										<cfif ThisValue NEQ thisField.PromptValue>
											<cfset thisField.ErrorCode = "NotInList">
										</cfif>
									<cfelse>
										<cfset thisField.ErrorCode = "NotInList">
									</cfif>
								</cfif>
							</cfloop>
						<cfelse>
							<!--- <cfdump var="#thisField#"> ---><cfif NOT ListFindNoCase(thisField.ValueList, thisField.Value, thisField.Delimiters)>
								<cfif IsDefined("variables.thisField.PromptValue")>
									<cfif thisField.Value NEQ thisField.PromptValue>
										<cfset thisField.ErrorCode = "NotInList">
									</cfif>
								<cfelse>
									<cfset thisField.ErrorCode = "NotInList">
								</cfif>
							</cfif>
						</cfif>
					</cfif>
	
					<!--- Datatype validation --->
					<cfif not compareNoCase(thisField.ErrorCode, "OK")>
						<!---
         				this works in cfmx but is not backwards compatible
      					<cfif isDefined("#thisField.DataType#Validate") and
isCustomFunction(variables["#thisField.DataType#Validate"])>
      					--->
      					<cfif IsDefined("variables.#thisField.DataType#Validate") and
isCustomFunction(evaluate("#thisField.DataType#Validate"))>

							<!--- <cftry> ---><cfset thisField.ErrorCode = evaluate("#thisField.DataType#Validate(thisField.Value)")><!--- <cfcatch><cfdump var="#thisfield#"></cfcatch></cftry> --->
							<cfset thisfield.debug_validationApplied = ListAppend(thisfield.debug_validationApplied, "#thisField.DataType#Validate()")>	
						</cfif>
					</cfif>
					
					<!--- Coarse language validation --->
					<cfif (not compareNoCase(thisField.ErrorCode, "OK")) AND thisField.LanguageFilter>
						<cfset FoundWordList = "">
						<cfloop index="Word" list="#Attributes.LanguageList#">
							<cfif REFindNoCase("(^|[[:punct:][:space:]])#Word#($|[[:punct:][:space:]])", thisField.Value)>
								<cfset FoundWordList = ListAppend(FoundWordList, Word)>
							</cfif>	
						</cfloop>
						<cfif Len(FoundWordList)>
							<cfif ListLen(FoundWordList) GT 1>
								<cfset thisField.ErrorCode = "ProhibitedWords">
							<cfelse>	
								<cfset thisField.ErrorCode = "ProhibitedWord">
							</cfif>
						</cfif>
						<cfset thisfield.debug_validationApplied = ListAppend(thisfield.debug_validationApplied, "LanguageFilter")>
					</cfif>
					
					<!--- HTML content validation --->
					<cfif (not compareNoCase(thisField.ErrorCode, "OK")) AND not thisField.allowHtml>
						<cfif reFind("<[/[:alnum:]]+[^>]*>", thisField.Value)>
							<cfset thisField.ErrorCode = "ProhibitedHtml">
						</cfif>	
						<cfset thisfield.debug_validationApplied = ListAppend(thisfield.debug_validationApplied, "HtmlFilter")>
					</cfif>
					
					<cfif not compareNoCase(thisField.ErrorCode, "OK")>
						<!--- try length validation --->
						<cfif thisField.MinLength>
							<cfif thisField.MaxLength>
								<cfif thisField.MinLength EQ thisField.MaxLength>
									<!--- exact length --->
									<cfif Len(thisField.Value) LT thisField.MinLength>
										<cfset thisField.ErrorCode = "TooShort_Exact">
									<cfelseif Len(thisField.Value) GT thisField.MaxLength>
										<cfset thisField.ErrorCode = "TooLong_Exact">	
									</cfif>
								<cfelse>
									<!--- min and maxlength --->
									<cfif Len(thisField.Value) LT thisField.MinLength>
										<cfset thisField.ErrorCode = "TooShort_MinMax">
									<cfelseif Len(thisField.Value) GT thisField.MaxLength>
										<cfset thisField.ErrorCode = "TooLong_MinMax">	
									</cfif>
								</cfif>
							<cfelse>
								<!--- minlength --->
								<cfif Len(thisField.Value) LT thisField.MinLength>
									<cfset thisField.ErrorCode = "TooShort_Min"> 
								</cfif>
							</cfif>
							<cfset thisfield.debug_validationApplied = ListAppend(thisfield.debug_validationApplied, "Min/MaxLength")>
						<cfelseif thisField.MaxLength>
							<!--- maxlength --->
							<cfif Len(thisField.Value) GT thisField.MaxLength>
								<cfset thisField.ErrorCode = "TooLong_Max"> 
							</cfif>
							<cfset thisfield.debug_validationApplied = ListAppend(thisfield.debug_validationApplied, "Min/MaxLength")>
						</cfif>
					</cfif>
						
					<cfif (not compareNoCase(thisField.ErrorCode, "OK")) AND Len(thisField.Pattern)>
						<!--- Attempt regular expression validation --->
						<cfset thisfield.debug_validationApplied = ListAppend(thisfield.debug_validationApplied, "Pattern")>
						<cfset Result = REFindNoCase(thisField.Pattern, thisField.Value, 1, "YES")>
						<cfif (Result.Pos[1] NEQ 1) OR (Result.Len[1] NEQ Len(thisField.Value))>
							<!--- fail --->
							<cfset thisField.ErrorCode = "Pattern"> 
						</cfif>
					</cfif>
	
					<cfif not compareNoCase(thisField.ErrorCode, "OK")>
						<!--- try range validation
						There are three forms of range validation, depending on the type of data: date, numeric, and string. The RangeValidationStyle struct holds entries for each data type - the default is string. 
						* String validation truncates the entered value to the
						  length of the range maximum. In this way, "Cottage"
						  would fit within the range, "A~C", and "A~CO", but
						  not "A-CL".
						* Numeric validation simply compares the values.
						* Date validation uses date comparison functions. --->
						
						<cfif StructKeyExists(RangeValidationStyle, thisField.DataType)>
							<cfset ThisRangeValidationStyle = RangeValidationStyle[thisField.DataType]>
						<cfelse>	
							<cfset ThisRangeValidationStyle = "string">
						</cfif>
						
						<cfif Len(thisField.Min)>
							<cfset thisfield.debug_validationApplied = ListAppend(thisfield.debug_validationApplied, "Min")>

							<cfswitch expression="#ThisRangeValidationStyle#">
								
								<cfcase value="Numeric,String">
									<cfif thisField.Value LT thisField.Min>
										<cfif Len(thisField.Max)>
											<cfif thisField.Max EQ thisField.Min>
												<cfset thisField.ErrorCode = "OutOfRange_Exact">
											<cfelse>
												<cfset thisField.ErrorCode = "OutOfRange_MinMax">
											</cfif>
										<cfelse>
											<cfset thisField.ErrorCode = "OutOfRange_Min">
										</cfif>
									</cfif>
								</cfcase>
								
								<cfcase value="Date">
									<cfset DateToConsider = thisField.Value>
									<cfif Len(DateToConsider)>
										<cfset DateToConsider = CreateDate(Year(DateToConsider), Month(DateToConsider), Day(DateToConsider))>
										<cfif Len(thisField.Min)>
											<cfset thisField.Min = CreateDate(Year(thisField.Min), Month(thisField.Min), Day(thisField.Min))>
										</cfif>
										<!--- <cfif LSIsDate(thisField.Min)>
											<cfset thisField.Min = LSParseDateTime(thisField.Min)>
										<cfelseif IsDate(thisField.Min)>
											<cfset thisField.Min = ParseDateTime(thisField.Min)>
										<cfelse>
											<cfset thisField.Min = "">
										</cfif> --->
										<cfif Len(thisField.Min) AND (DateCompare(thisField.Min, DateToConsider) GT 0)>
											<cfif Len(thisField.Max)>
												<cfif thisField.Max EQ thisField.Min>
													<cfset thisField.ErrorCode = "OutOfRange_Exact_#ThisRangeValidationStyle#">
												<cfelse>
													<cfset thisField.ErrorCode = "OutOfRange_MinMax_#ThisRangeValidationStyle#">
												</cfif>
											<cfelse>
												<cfset thisField.ErrorCode = "OutOfRange_Min_#ThisRangeValidationStyle#">
											</cfif>
										</cfif>
									</cfif>
								</cfcase>
								<cfcase value="Time">
									<cfset TimeToConsider = thisField.Value>
									<cfif Len(TimeToConsider)>
										<cfset TimeToConsider = CreateTime(Hour(TimeToConsider), Minute(TimeToConsider), Second(TimeToConsider))>
										<cfif Len(thisField.Min)>
											<cfset thisField.Min = CreateTime(Hour(thisField.Min), Minute(thisField.Min), Second(thisField.Min))>
										</cfif>
										<!--- <cfif LSIsDate(thisField.Min)>
											<cfset thisField.Min = LSParseDateTime(thisField.Min)>
										<cfelseif IsDate(thisField.Min)>
											<cfset thisField.Min = ParseDateTime(thisField.Min)>
										<cfelse>
											<cfset thisField.Min = "">
										</cfif> --->
										<cfif Len(thisField.Min) AND (DateCompare(thisField.Min, TimeToConsider) GT 0)>
											<cfif Len(thisField.Max)>
												<cfif thisField.Max EQ thisField.Min>
													<cfset thisField.ErrorCode = "OutOfRange_Exact_#ThisRangeValidationStyle#">
												<cfelse>
													<cfset thisField.ErrorCode = "OutOfRange_MinMax_#ThisRangeValidationStyle#">
												</cfif>
											<cfelse>
												<cfset thisField.ErrorCode = "OutOfRange_Min_#ThisRangeValidationStyle#">
											</cfif>
										</cfif>
									</cfif>
								</cfcase>
								
							</cfswitch>
						</cfif>
						
						<cfif Len(thisField.Max)>
							<cfset thisfield.debug_validationApplied = ListAppend(thisfield.debug_validationApplied, "Max")>

							<cfswitch expression="#ThisRangeValidationStyle#">
								
								<cfcase value="Numeric">
									<cfif thisField.Value GT thisField.Max>
										<cfif Len(thisField.Min)>
											<cfif thisField.Max EQ thisField.Min>
												<cfset thisField.ErrorCode = "OutOfRange_Exact">
											<cfelse>
												<cfset thisField.ErrorCode = "OutOfRange_MinMax">
											</cfif>
										<cfelse>
											<cfset thisField.ErrorCode = "OutOfRange_Max">
										</cfif>									
									</cfif>
								</cfcase>
								
								<cfcase value="String">
									<cfset MaxTestValue = Mid(thisField.Value, 1, Len(thisField.Max))>
									<cfif MaxTestValue GT thisField.Max>
										<cfif Len(thisField.Min)>
											<cfif thisField.Max EQ thisField.Min>
												<cfset thisField.ErrorCode = "OutOfRange_Exact">
											<cfelse>
												<cfset thisField.ErrorCode = "OutOfRange_MinMax">
											</cfif>
										<cfelse>
											<cfset thisField.ErrorCode = "OutOfRange_Max">
										</cfif>		
									</cfif>
								</cfcase>
								
								<cfcase value="Date">
									<cfset DateToConsider = thisField.Value>
									<cfif Len(DateToConsider)>
										<cfset DateToConsider = CreateDate(Year(DateToConsider), Month(DateToConsider), Day(DateToConsider))>
										<cfif Len(thisField.Max)>
											<cfset thisField.Max = CreateDate(Year(thisField.Max), Month(thisField.Max), Day(thisField.Max))>
										</cfif>
										<!--- <cfif LSIsDate(thisField.Max)>
											<cfset thisField.Max = LSParseDateTime(thisField.Max)>
										<cfelseif IsDate(thisField.Max)>
											<cfset thisField.Max = ParseDateTime(thisField.Max)>
										<cfelse>
											<cfset thisField.Max = "">
										</cfif> --->
										<cfif Len(thisField.Max) AND (DateCompare(DateToConsider, thisField.Max) GT 0)>
											<cfif Len(thisField.Min)>
												<cfif thisField.Max EQ thisField.Min>
													<cfset thisField.ErrorCode = "OutOfRange_Exact_#ThisRangeValidationStyle#">
												<cfelse>
													<cfset thisField.ErrorCode = "OutOfRange_MinMax_#ThisRangeValidationStyle#">
												</cfif>
											<cfelse>
												<cfset thisField.ErrorCode = "OutOfRange_Max_#ThisRangeValidationStyle#">
											</cfif>		
										</cfif>
									</cfif>
								</cfcase>

								<cfcase value="Time">
									<cfset TimeToConsider = thisField.Value>
									<cfif Len(TimeToConsider)>
										<cfset TimeToConsider = CreateTime(Hour(TimeToConsider), Minute(TimeToConsider), Second(TimeToConsider))>
										<cfif Len(thisField.Max)>
											<cfset thisField.Max = CreateTime(Hour(thisField.Max), Minute(thisField.Max), Second(thisField.Max))>
										</cfif>
										<!--- <cfif LSIsDate(thisField.Max)>
											<cfset thisField.Max = LSParseDateTime(thisField.Max)>
										<cfelseif IsDate(thisField.Max)>
											<cfset thisField.Max = ParseDateTime(thisField.Max)>
										<cfelse>
											<cfset thisField.Max = "">
										</cfif> --->
										<cfif Len(thisField.Max) AND (DateCompare(TimeToConsider, thisField.Max) GT 0)>
											<cfif Len(thisField.Min)>
												<cfif thisField.Max EQ thisField.Min>
													<cfset thisField.ErrorCode = "OutOfRange_Exact_#ThisRangeValidationStyle#">
												<cfelse>
													<cfset thisField.ErrorCode = "OutOfRange_MinMax_#ThisRangeValidationStyle#">
												</cfif>
											<cfelse>
												<cfset thisField.ErrorCode = "OutOfRange_Max_#ThisRangeValidationStyle#">
											</cfif>		
										</cfif>
									</cfif>
								</cfcase>
								
							</cfswitch>
						</cfif>
						
					</cfif>
				</cfif>	

				<cfif thisField.ErrorCode EQ "OK">
					<cfset thisField.generated_error = "">
				<cfelse>
					<cfset ErrorFieldList = ListAppend(ErrorFieldList, thisField.Name)>
					<cfif Len(thisField.Error)>
						<cfset thisField.generated_error = BuildError("", thisField.Error)>
					<cfelse>
						<cfset thisField.generated_error = BuildError(thisField.ErrorCode)>	
					</cfif>	
					<cfset ErrorMessageList = ListAppend(ErrorMessageList, thisField.generated_error, RS)>
					<cfif Attributes.Focus AND NOT MovedFocus>
						<cfset MovedFocus = "Yes">
						<cfset Attributes.FocusField = thisField.Name>
					</cfif>	
					
				</cfif>

			</cfloop>
			
			
			
	

			
			<!--- handle rules --->
			<!--- To use the cleaned value of a field in a rule, simply refer to the field as follows: {fieldname}. You can also use other attributes of a field by writing: request.terraform.fields.fieldname.attribute where fieldname is the name of your field and attribute may be any attribute of your field. If there are multiple fields with the same name then request.terraform.fields.fieldname will be an array. Refer to each field as request.terraform.fields.fieldname[i] where i is the ordinal value of the field. For example: request.terraform.fields.address[2].value is the cleaned value of the second field on the form called "address". --->

			<cfif structKeyExists(thistag, "rules")>
				<cfloop index="i" from="1" to="#arraylen(thistag.rules)#">
					<cfset thisrule = thistag.rules[i]>
					<cfif thisrule.applyiferrors or not len(ErrorMessageList & errorfieldlist)>
					
						<!--- handle {} substitutions ---> 
						<cfset substitutions = arraynew(1)>
						<cfset pos = 1>
						<cfloop condition="#pos#">
							<cfset nextSubstitution = refindnocase("\{([[:alnum:]_]+)\}", thisrule.rule, pos, true)>
							<cfset pos = nextSubstitution.pos[1] + nextSubstitution.len[1]>	
							<cfif pos>
								<cfset substitution = structnew()>
								<cfset substitution.pos = nextSubstitution.pos[1]>
								<cfset substitution.len = nextSubstitution.len[1]>
								<cfset substitution.var = mid(thisrule.rule, nextSubstitution.pos[2], nextSubstitution.len[2])>
								<cfset arrayappend(substitutions, substitution)>
							</cfif>
						</cfloop>
						<cfloop index="j" from="#arraylen(substitutions)#" to="1" step="-1">
							<cfif isArray(request.terraform.fields[substitutions[j].var])>
								<cfset thisrule.rule = insert("""" & replace(replace(request.terraform.fields[substitutions[j].var][1].value, "##", "####", "all"), """", """""", "all") & """", removechars(thisrule.rule, substitutions[j].pos, substitutions[j].len), substitutions[j].pos-1)>
							<cfelse>
								<cfset thisrule.rule = insert("""" & replace(replace(request.terraform.fields[substitutions[j].var].value, "##", "####", "all"), """", """""", "all") & """", removechars(thisrule.rule, substitutions[j].pos, substitutions[j].len), substitutions[j].pos-1)>
							</cfif>
						</cfloop>

						<cftry>
							<cfset thisrule.result = evaluate(thisrule.rule)>
							<cfcatch>
								<cfabort showerror="TerraForm implementation error: Rule ###i# (#thisrule.rule#) does not evaluate. #cfcatch.message# [Tip: Check that curly braces {} are only wrapped around form field names, not around functions, operators, or other characters.]">
							</cfcatch>
						</cftry>
						<cfif isboolean(thisrule.result)>
							<cfif not thisrule.result>
								<cfset ErrorMessageList = ListAppend(ErrorMessageList, thisrule.error, RS)>
								<cfset ErrorFieldList = ListAppend(ErrorFieldList, thisrule.fieldlist)>
							</cfif>		
						<cfelse>	
							<cfabort showerror="TerraForm implementation error: Rule ###i# (#thistag.rules[i].rule#) evaluates to ""#thisrule.result#"" which cannot be converted to a boolean value.">				
						</cfif>

					</cfif>
				</cfloop>
			</cfif>


			
			


			
			
			
		
			
			
			<cfif IsDefined("Attributes.CustomValidationPath") and len(Attributes.CustomValidationPath)>		
				<!--- Attempt to insert custom validation file. --->
				
				<cfif listFindNoCase("/,\", left(Attributes.CustomValidationPath,1))>
				
					<!--- it's a mapped path. Use as is. --->
					<cftry>
						<cfinclude template="#Attributes.CustomValidationPath#">
						<cfcatch>
							<cfoutput><p><strong>TerraForm implementation error: Problem processing custom validation file: "#Attributes.CustomValidationPath#". #cfcatch.message#</strong></p></cfoutput><cfdump var="#cfcatch#" expand="no"><cfrethrow>
						</cfcatch>
					</cftry>
					
				<cfelseif find(":", Attributes.CustomValidationPath)>
				
					<!--- it's an absolute path. Turn it into a relative path. --->
					<cfset currenttemplatefolder = getdirectoryfrompath(getcurrenttemplatepath())>
					<cfset CustomValidationFolder = getdirectoryfrompath(Attributes.CustomValidationPath)>
					<cfloop index="i" from="1" to="#min(listlen(CustomValidationFolder, "/\"), listlen(currenttemplatefolder, "/\"))#">
						<cfif not Compare(listGetAt(CustomValidationFolder, i, "/\"), listGetAt(currenttemplatefolder, i, "/\"))>
							<cfset currenttemplatefolder = listSetAt(currenttemplatefolder, i, SUB, "/\")>
							<cfset CustomValidationFolder = listSetAt(CustomValidationFolder, i, SUB, "/\")>
						<cfelse>	
							<cfbreak>	
						</cfif>
					</cfloop>
					<cfset relativePathToCustomValidationFolder = "">
					<cfloop index="i" from="#listLen(currenttemplatefolder, "/\")#" to="1" step="-1">
						<cfset folder = listGetAt(currenttemplatefolder, i, "/\")>
						<cfif not Compare(folder, SUB)>
							<cfbreak>
						</cfif>
						<cfset relativePathToCustomValidationFolder = listPrepend(relativePathToCustomValidationFolder, "..", "\")>
					</cfloop>
					<cfloop index="i" from="1" to="#listLen(CustomValidationFolder, "/\")#">
						<cfset folder = listGetAt(CustomValidationFolder, i, "/\")>
						<cfif Compare(folder, SUB)>
							<cfset relativePathToCustomValidationFolder = listAppend(relativePathToCustomValidationFolder, folder, "\")>
						</cfif>
					</cfloop>	
					<cfset relativePathToCustomValidationFile = relativePathToCustomValidationFolder & "\" & getfilefrompath(Attributes.CustomValidationPath)>
					
					<cftry>
						<cfinclude template="#relativePathToCustomValidationFile#">
						<cfcatch>
							<cfoutput><p><strong>TerraForm implementation error: Problem processing custom validation file: "#Attributes.CustomValidationPath#". #cfcatch.message#</strong></p></cfoutput><cfdump var="#cfcatch#" expand="no"><cfrethrow>
						</cfcatch>
					</cftry>
					
				<cfelse>
				
					<!--- it's a relative path. Try to include file relative to the calling template. --->
					<cfif IsDefined("variables.relativePathToParentTemplateFolder")>
						<cftry>
							<cfinclude template="#relativePathToParentTemplateFolder##Attributes.CustomValidationPath#">
							<cfcatch>
								<cfoutput><p><strong>TerraForm implementation error: Problem processing custom validation file: "#Attributes.CustomValidationPath#". #cfcatch.message#</strong></p></cfoutput><cfdump var="#cfcatch#" expand="no"><cfrethrow>
							</cfcatch>
						</cftry>
					<cfelse>
						<cfabort showerror="TerraForm implementation error: Relative custom validation paths not supported on your ColdFusion platform. If your custom validation file is located in the same folder as your form, consider writing your relative path as follows: CustomValidationPath=""##GetDirectoryFromPath(GetCurrentTemplatePath())##[mycustomvalidationfile].cfm"") .">
					</cfif>	
							
				</cfif>
			</cfif>	

			
			
						
			<cfswitch expression="#ListLen(MissingRequiredFields, RS)#">
				<cfcase value="0"/>
				<cfcase value="1">
					<cfset ErrorMessageList = listPrepend(ErrorMessageList, BuildError("RequiredFieldMissing"), RS)>
				</cfcase>
				<cfdefaultcase>
					<cfset ErrorMessageList = listPrepend(ErrorMessageList, BuildError("RequiredFieldsMissing"), RS)>
				</cfdefaultcase>
			</cfswitch>
			
			

					
		</cfif>

<!--- 
************************************************************
**  PART 6. If it's a new form OR it failed validation    **
**  OR it's set to always display OR its state is         **
**  "Refresh" then display it                             **
************************************************************
--->	

		<cfif (not compareNoCase(State, "new")) OR (not compareNoCase(State, "refresh")) OR Len(ErrorFieldList) OR Len(ErrorMessageList) OR Attributes.AlwaysDisplayForm>
						
			<!--- build the HTML for each tag and insert it into the form --->
			<cfloop index="i" from="1" to="#CountFields#">
				<cfset ThisField = ThisTag.Fields[i]>
				
				<cfscript>
					ignoreAttributes = structNew();
					ignoreAttributes.ALLOWHTML = "";
					ignoreAttributes.CAPTION = "";
					ignoreAttributes.CAPTION_label = "";
					ignoreAttributes.CAPTION_error = "";
					ignoreAttributes.DATATYPE = "";
					ignoreAttributes["DEFAULT"] = "";
					ignoreAttributes.DEBUG_FOUND = "";
					ignoreAttributes.DEBUG_VALIDATIONAPPLIED = "";
					ignoreAttributes.DELIMITERS = "";
					ignoreAttributes.DISPLAYCOLUMN = "";
					ignoreAttributes.DISPLAYLIST = "";
					ignoreAttributes.EnforceValueList = "";
					ignoreAttributes.Error = "";
					ignoreAttributes.ERRORCODE = "";
					ignoreAttributes.FIELDNAME = "";
					ignoreAttributes.FOCUSTO = "";
					ignoreAttributes.FORMAT = "";
					ignoreAttributes.GENERATED_ERROR = "";
					ignoreAttributes.GROUPCOLUMN = "";
					ignoreAttributes.GROUPLIST = "";
					ignoreAttributes.INPUTSCOPELIST = "";
					ignoreAttributes.jsMask = "";
					ignoreAttributes.labelTransformSubstring = "";
					ignoreAttributes.labelTransformRegex = "";
					ignoreAttributes.LANGUAGEFILTER = "";
					ignoreAttributes.MAX = "";
					ignoreAttributes.MAXLENGTH = "";
					ignoreAttributes.MIN = "";
					ignoreAttributes.MINLENGTH = "";
					ignoreAttributes.MULTIPLE = "";
					ignoreAttributes.NAME = "";
					ignoreAttributes.NOSYNONYMS = "";
					ignoreAttributes.PATTERN = "";
					ignoreAttributes.PRESET = "";
					ignoreAttributes.Prompt = "";
					ignoreAttributes.PromptValue = "";
					ignoreAttributes.QUERY = "";
					ignoreAttributes.RANGE = "";
					ignoreAttributes.RAWVALUE = "";
					ignoreAttributes.REQUIRED = "";
					ignoreAttributes.VALUE = "";
					ignoreAttributes.VALUECOLUMN = "";
					ignoreAttributes.VALUELIST = "";
					ignoreAttributes.XPATH = "";
					ignoreAttributes.YESSYNONYMS = "";
				</cfscript>
				
				<cfif StructKeyExists(DataTypeAttributes, thisField.Datatype)>
					<cfloop index="key" list="#DataTypeAttributes[thisField.Datatype]#">
						<cfset ignoreAttributes[key]="">
					</cfloop>
				</cfif>

				<!---
    			this works in cfmx but is not backwards compatible
    			<cfif IsDefined("#thisField.Format#Format") AND IsCustomFunction(variables["#thisField.Format#Format"])>
   				--->
    			<cfif IsDefined("variables.#thisField.Format#Format") AND IsCustomFunction(evaluate("#thisField.Format#Format"))>
				
					<cfset HTMLBlock = Evaluate("#thisField.Format#Format(thisField)")>
				<cfelse>
					<cfsavecontent variable="HTMLBlock">
						<cfset FormatPath = ListAppend(pathToSettingsFolder, "formats/includes/#lCase(thisField.Format)#.cfm", "\/")>
						<cftry>
							<cfinclude template="#FormatPath#">
							<cfcatch>
								<cfoutput><p><strong>TerraForm implementation error: Problem processing format file: "#FormatPath#" for field: #thisField.Name#. #cfcatch.message#</strong></p></cfoutput><cfdump var="#cfcatch#" expand="no"><cfrethrow>
							</cfcatch>
						</cftry>
					</cfsavecontent>				
				</cfif>
		
				<!--- add the hidden field for disabled field data passthrough --->
				<!--- <cfif not listFindNoCase("HTMLArea,select,soeditor,submit,submitbutton", thisField.format)>
					<cfif listFindNoCase("MSSlider", thisField.format)>
						<cfset nameBlock = "">
					<cfelse>
						<cfif structKeyExists(thisfield, "disabled")>
							<cfset nameBlock = "name=""#thisField.name#""">
						<cfelse>	
							<cfset nameBlock = "name=""#thisField.name#.hidden""">
						</cfif>
					</cfif>
					<cfif structKeyExists(thisfield, "disabled")>
						<cfset HTMLBlock = htmlBlock & "<input #nameBlock# value=""#InputFieldFormat(ThisField.RawValue)#""  id=""#ThisField.id#_hidden"" #closeTag#"><!--- type=""hidden"" --->
					<cfelse>
						<cfset HTMLBlock = htmlBlock & "<input #nameBlock# value=""#InputFieldFormat(ThisField.RawValue)#""  id=""#ThisField.id#_hidden"" disabled=""disabled"" #closeTag#"><!--- type=""hidden"" --->
					</cfif>
				</cfif> --->
			
		
				<cfif Attributes.qForms>
					<cfset qFormsScript = ListAppend(qFormsScript, "if ( objForm.#thisField.Name# ) objForm.#thisField.Name#.description = '#JSStringFormat(thisField.Caption_error)#'", ";")>
					<cfif thisField.Required>
						<cfset qFormsScript = ListAppend(qFormsScript, "if ( objForm.#thisField.Name# ) objForm.#thisField.Name#.required = true", ";")>
					</cfif>
					<cfif StructKeyExists(qFormsValidate, thisField.DataType) and not listFindNoCase("timespanpicker", thisField.format)>
						<cfset qFormsScript = ListAppend(qFormsScript, Replace(qFormsValidate[thisField.DataType], SUB, thisField.Name), ";")>
					</cfif>
				</cfif>	
				
				<!--- All unrecognised attributes are passed through --->
				<cfset PassedThrough = "">
				<cfloop collection="#thisField#" item="ThisAttribute">
					<cfif 
						not structKeyExists(ignoreAttributes, ThisAttribute) 
						and (listfirst(ThisAttribute, "_") neq "caption")
						and isSimpleValue(ThisField[ThisAttribute])
					>
						<cfset Passedthrough = ListAppend(Passedthrough, "#lcase(ThisAttribute)#=""#ThisField[ThisAttribute]#""", " ")>
					</cfif>
				</cfloop>
				
				<cfset HTMLBlock = Replace(HTMLBlock, Passthrough, Passedthrough, "ALL")>

				<cfif attributes.flagRequiredFields and not attributes.flaglabels and thisField.required>
					<cfif find("#ESC#*", HTMLBlock)>
						<cfset HTMLBlock = replace(HTMLBlock, "#ESC#*", attributes.requiredFlag)>
						<cfset HTMLBlock = replace(HTMLBlock, "#ESC#*", "", "all")>
					<cfelse>
						<cfset HTMLBlock = HTMLBlock & attributes.requiredFlag>
					</cfif>
					<!--- <cfset thistag.generatedcontent = rereplacenocase(thistag.generatedcontent, "<\*([[:alnum:]]+,)*#thisfield.name#(,[[:alnum:]]+)*\*>", attributes.requiredFlag, "ALL")> --->
				<cfelse>
					<cfset HTMLBlock = replace(HTMLBlock, "#ESC#*", "", "ALL")>
				</cfif>

				<cfif Attributes.Debug>
					<!--- Add debugging information --->
					<cfset debug = structnew()>
					<cfset debug["1) Name"] = thisField.Name>
					<cfset debug["2) Data"] = thisfield.debug_found>
					<cfset debug["3) Validation"] = "#thisField.DataType# #thisField.Min#-#thisField.Max# #thisField.Pattern#">
					<cfif thisField.Required>
						<cfset debug["3) Validation"] = "Required " & debug["3) validation"]>
					</cfif>
					<cfif not compareNoCase(thisField.ErrorCode, "OK")>
						<cfset debug["4) Result"] = "No error">
					<cfelse>
						<cfset debug["4) Result"] = thisField.ErrorCode>
					</cfif>
					<cfif Len(thisfield.debug_validationApplied)>
						<cfset debug["4) Result"] = debug["4) Result"] & " (validated with #thisfield.debug_validationApplied#)">
					</cfif>
					<cfsavecontent variable="DebugInfo"><cfdump var="#debug#" label="Debugging"></cfsavecontent>
					<cfset HTMLBlock = HTMLBlock & DebugInfo>
				</cfif>
				
				<!--- add inline error if appropriate --->
				<cfif attributes.inlineErrors and (Len(ErrorFieldList) OR Len(ErrorMessageList))>
					<cfif len(thisField.generated_error)>
						<cfset HTMLBlock = HTMLBlock & Attributes.inlineErrorsTagStart & thisField.generated_error & Attributes.inlineErrorsTagEnd>
					<cfelseif listFindNoCase(ErrorRequiredFieldList, thisField.name)>
						<cfset HTMLBlock = HTMLBlock & Attributes.inlineErrorsTagStart & buildError("InlineRequiredFieldMissing") & Attributes.inlineErrorsTagEnd>
					</cfif>
				</cfif>
				
				<cfset ThisTag.GeneratedContent = Replace(ThisTag.GeneratedContent, "#ESC#terrafield #i##ESC#", HTMLBlock)>
			</cfloop>

			<!--- add any inline rule errors into the form --->
			<cfif attributes.inlineErrors>
				<cfloop index="i" from="1" to="#CountRules#">
					<cfset ThisRule = ThisTag.Rules[i]>
					<cfif structKeyExists(thisRule, "result") and not thisRule.result>
						<cfset HTMLBlock = Attributes.inlineErrorsTagStart & ThisRule.error & Attributes.inlineErrorsTagEnd>
						<cfset ThisTag.GeneratedContent = Replace(ThisTag.GeneratedContent, "#ESC#rule #i##ESC#", HTMLBlock)>
					</cfif>			
				</cfloop>
			</cfif>
			<cfif CountRules>
				<cfset ThisTag.GeneratedContent = reReplace(ThisTag.GeneratedContent, "#ESC#rule [0-9]+#ESC#", "", "all")>
			</cfif>
			
			<!--- build the HTML for each label and insert it into the form --->
			<cfloop index="i" from="1" to="#CountLabels#">
				
				<cfset ThisLabel = ThisTag.Labels[i]>
				<cfset AttributesList="FOR">

				<cfset HTMLBlock = "">
				<cfif ThisLabel.nolabel>
					<cfset htmlBlock = "">
					
				<cfelseif structkeyexists(ThisLabel, "label_" &  request.terraform.locale.javaLocaleId)>
					<cfset htmlBlock = ThisLabel["label_" & request.terraform.locale.javaLocaleId]>		
				<cfelseif structkeyexists(ThisLabel, "label_" &  request.terraform.locale.languageCode)>
					<cfset htmlBlock = ThisLabel["label_" & request.terraform.locale.languageCode]>		
				<cfelseif structkeyexists(ThisLabel, "label_" &  request.terraform.locale.countryCode)>
					<cfset htmlBlock = ThisLabel["label_" & request.terraform.locale.countryCode]>
				<cfelseif structkeyexists(ThisLabel, "label_" & request.terraform.locale.language)>
					<cfset htmlBlock = ThisLabel["label_" & request.terraform.locale.language]>			
				<cfelseif len(ThisLabel.label)>
					<cfset htmlBlock = ThisLabel.label>			
				<cfelse>
					<cfloop index="forfield" list="#ThisLabel.for#">
						<cfif structkeyexists(request.terraform.fields, forfield)>
							<cfif isarray(request.terraform.fields[forfield])>
								<cfset HTMLBlock = listappend(HTMLBlock, request.terraform.fields[forfield][1].caption_label)>
                             <cfelse>
                             	<cfset HTMLBlock = listappend(HTMLBlock, request.terraform.fields[forfield].caption_label)>
                             </cfif>
						</cfif>
					</cfloop>
					<cfset HTMLBlock = listchangedelims(HTMLBlock, ", ")>
					<cfset htmlBlock = reReplace(htmlBlock, attributes.labelTransformRegex, attributes.labelTransformSubstring, "all")>
				</cfif>
				
				<!--- Try to mark the access key --->
				<cfif ThisLabel.indicateAccessKey and (listlen(ThisLabel.for) eq 1) and structkeyexists(request.terraform.fields, ThisLabel.for) and not isarray(request.terraform.fields[ThisLabel.for]) and structkeyexists(request.terraform.fields[ThisLabel.for], "accesskey") and len(request.terraform.fields[ThisLabel.for].accesskey)>
					<cfset HTMLBlock = rereplacenocase(HTMLBlock, "(" & request.terraform.fields[ThisLabel.for].accesskey & ")", "<span style=""accelerator:true;text-decoration:underline"">\1</span>")>
				</cfif>
				
				<!--- handle flagging of required fields --->
				<cfif attributes.flagRequiredFields and attributes.flaglabels>
					<cfset flag = false>
					<cfloop index="forfield" list="#ThisLabel.for#">
						<cfif structKeyExists(request.terraform.fields, forfield)>
							<cfif isarray(request.terraform.fields[forfield])>
								<cfset flag = flag or request.terraform.fields[forfield][1].required>
							<cfelse>	
								<cfset flag = flag or request.terraform.fields[forfield].required>
							</cfif>
						</cfif>
					</cfloop>
					<cfif flag>
						<cfset HTMLBlock = HTMLBlock & attributes.requiredFlag>
					</cfif>
				</cfif>
				
				<cfset focusField = listFirst(thisLabel.for)>
				<cfif structKeyExists(request.terraform.fields, focusField)>
					<cfif isArray(request.terraform.fields[focusField])>
						<cfset theField = request.terraform.fields[focusField][1]>
					<cfelse>	
						<cfset theField = request.terraform.fields[focusField]>
					</cfif>
					
					<cfif structKeyExists(theField, "focusTo")>
						<cfset HTMLBlock = "<label for=""#theField.focusTo#"">" & HTMLBlock & "</label>">
					<cfelse>
						<cfset HTMLBlock = "<label for=""#theField.id#"">" & HTMLBlock & "</label>">
					</cfif>
				</cfif>
			
				<!--- handle highlighting of error fields --->
				<cfset highlight = false>
				<cfloop index="forfield" list="#ThisLabel.for#">
					<cfif structKeyExists(request.terraform.fields, forfield)>				
						<cfif isarray(request.terraform.fields[forfield])>
							<cfset highlight = highlight or listfindnocase(ErrorFieldList, request.terraform.fields[forfield][1].name)>
						<cfelse>	
							<cfset highlight = highlight or listfindnocase(ErrorFieldList, request.terraform.fields[forfield].name)>
						</cfif>
					</cfif>
				</cfloop>
				<cfif highlight>
					<cfset HTMLBlock = Attributes.HighlightLabelTagStart & HTMLBlock & Attributes.HighlightLabelTagEnd>
				</cfif>	
				
				<cfset ThisTag.GeneratedContent = Replace(ThisTag.GeneratedContent, "#ESC#label #i##ESC#", HTMLBlock)>
			</cfloop>
			

			<!--- Highlight error fields --->
			<cfloop index="thisField" list="#ErrorFieldList#">
				<cfset ThisTag.GeneratedContent = REReplaceNoCase(ThisTag.GeneratedContent, "LabelFor=""([^,""]*,)*#thisField#(,[^,""]*)*""", Attributes.Highlight, "ALL")>
			</cfloop>
			<cfset ThisTag.GeneratedContent = REReplaceNoCase(ThisTag.GeneratedContent, " LabelFor=""[^""]*""", "", "ALL")>		

			<!--- Insert error messages --->
			<cfif Len(ErrorMessageList)>
				<cfset ThisTag.GeneratedContent = ReplaceNoCase(ThisTag.GeneratedContent, "<Messages>", "<ul><li #Attributes.Highlight#>#ListChangeDelims(ErrorMessageList, "</li><li #Attributes.Highlight#>", RS)#</li></ul>")>
				<cfset ThisTag.GeneratedContent =  ReplaceNoCase(ReplaceNoCase(ThisTag.GeneratedContent, "</MessagesBlock>", ""), "<MessagesBlock>", "")>
			<cfelse>
				<cfset MessagesBlockStart = FindNoCase("<MessagesBlock>", ThisTag.GeneratedContent)>
				<cfif MessagesBlockStart>
					<cfset MessagesBlockEnd = FindNoCase("</MessagesBlock>", ThisTag.GeneratedContent, MessagesBlockStart)>
				</cfif>
				<cfif MessagesBlockStart AND MessagesBlockEnd>
					<cfset ThisTag.GeneratedContent=RemoveChars(ThisTag.GeneratedContent, MessagesBlockStart, MessagesBlockEnd + 16 - MessagesBlockStart)>
				</cfif>
			</cfif>	

			<cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & "<div style=""display : none"">">
			<!--- Add PassthroughFields, control field, and wrap FORM tags around the form--->
			<cfset PassthroughFields = "">
			<cfloop index="ThisPassthroughField" list="#Attributes.PassthroughFieldList#">
				<cfif IsDefined("#Scope#.#ThisPassthroughField#")>
					<cfset PassthroughFields = PassthroughFields & "<input type=""hidden"" name=""#ThisPassthroughField#"" value=""#htmlEditFormat(evaluate("#Scope#.#ThisPassthroughField#"))#""#closeTag#">
				</cfif>
			</cfloop>
			<cfif len(PassthroughFields)>
				<cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & PassthroughFields>
			</cfif>
 
			<cfif NOT StatusTagCreatedManually>
				<!--- This doesn't work in Netscape 4 <cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & "<div style=""display : none""><input type=""hidden"" name=""#Attributes.Name#"" value=""Form""#closeTag#</div>"> --->
				<cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & "<input type=""hidden"" name=""#Attributes.Name#"" value=""Form""#closeTag#">
			</cfif>
 			<cfif Attributes.qForms>
				<cfhtmlhead text="
					<script type=""text/javascript"" src=""#Attributes.qFormsPath#qforms.js?v=#URLEncodedFormat(request.eswsoftware.terraform)#""></script>
					<script type=""text/javascript"">
					<!--// 
						qFormAPI.setLibraryPath(""#JSStringFormat(Attributes.qFormsPath)#"");
						//qFormAPI.include(""validation"");
						//qFormAPI.include(""functions"", null, ""12"");
						qFormAPI.include(""*"");
					//-->
					</script>
				">
			</cfif>
			<cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & "</div>">
			
			<cfif Attributes.BuildFormTags>
				<!--- <cfif Attributes.Focus AND Len(Attributes.FocusField)>
					<cfset Attributes.OnLoad = ListAppend(Attributes.OnLoad, "setTimeout('document.#Attributes.Name#.#Attributes.FocusField#.focus()', 1000)", ";")>
				</cfif> --->
				<cfif not len(Attributes.EncType)>
					<cfset Attributes.EncType = "application/x-www-form-urlencoded">
				</cfif>
				
				<!--- <cfif Attributes.xhtml>
					<cfset nameAttribute = "">
				<cfelse>
					 ---><cfset nameAttribute = " name=""#Attributes.Name#"""><!--- 
				</cfif> --->
				<cfif Attributes.scope eq "url">
					<cfset method = "get">
				<cfelse>
					<cfset method = "post">
				</cfif>
				<cfset ThisTag.GeneratedContent = "<form onsubmit=""#Attributes.onSubmit#""#nameAttribute# id=""#Attributes.Name#"" enctype=""#Attributes.EncType#"" action=""#htmlEditFormat(Attributes.Action)#"" method=""#method#"">#ThisTag.GeneratedContent#</form>">
			</cfif>
				
			<!--- insert required fields message --->
			<cfif Attributes.flagRequiredFields and len(attributes.requiredfieldsmessage) and foundARequiredField>
				<cfset message = replaceNoCase(attributes.requiredfieldsmessage, "#ESC#REQUIREDFLAG", Attributes.requiredFlag)>
				<cfif refindNoCase("<requiredMessage/?>", ThisTag.GeneratedContent)>
					<cfset ThisTag.GeneratedContent = reReplaceNoCase(ThisTag.GeneratedContent, "<requiredMessage/?>", message, "all")>
				<cfelse>
					<cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & message>
				</cfif>
			<cfelse>
				<cfset ThisTag.GeneratedContent = reReplaceNoCase(ThisTag.GeneratedContent, "<requiredMessage/?>", "")>
			</cfif>
			
			<cfif Attributes.Focus AND Len(Attributes.FocusField)>
				<!--- <cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & "<script type=""text/javascript"">SafeAddOnload(document.#Attributes.Name#.#Attributes.FocusField#.focus);</script>"> ---><cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & "<script type=""text/javascript"" for=""onload"">document.#Attributes.Name#.#Attributes.FocusField#.focus();</script>">
			</cfif>

			<cfif Len(qFormsScript)>
				<cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & "<script type=""text/javascript"">objForm = new qForm(""#Attributes.Name#"");#qFormsScript#</script>">
			</cfif>
			
			
			
			
			
			
<!--- 			
			<!--- set up JS API --->
			<cfsavecontent variable="script">
				<cfoutput>
					<script type="text/javascript">
						if(!document.terraForm)
							document.terraForm = new Object();
						if(!document.terraForm.forms)
							document.terraForm.forms = new Object();	
						document.terraForm.forms["#attributes.name#"] = new Object();
						document.terraForm.forms["#attributes.name#"].fields = new Object();
						<cfloop index="i" from="1" to="#CountFields#">
							<cfset thisField = thisTag.fields[i]>
							document.terraForm.forms["#attributes.name#"].fields["#thisField.name#"] = new Object();
							<cfif listFindNoCase("text,checkbox", thisField.format)>
								document.terraForm.forms["#attributes.name#"].fields["#thisField.name#"].toggleDisabled = #lCase(thisField.format)#ToggleDisabled;
								document.terraForm.forms["#attributes.name#"].fields["#thisField.name#"].setDisabled = #lCase(thisField.format)#SetDisabled;
							</cfif>
						</cfloop>					
					</script>
				</cfoutput>
			</cfsavecontent>
			<cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & script>
			 --->
					
			<cfsavecontent variable="script">
				<cfoutput>
					<script type="text/javascript">
					<!--// 
						<cfloop index="i" from="1" to="#CountFields#">
							<cfset thisField = thisTag.fields[i]>
							<!--- try {  --->terraFormApi.fields['#thisField.id#'] = new terraFormApi.formatPrototypes['#lCase(thisField.format)#']('#thisField.id#');<!---  } catch(err) {} --->
						</cfloop>
					//-->
					</script>
				</cfoutput>
			</cfsavecontent>
				
			<cfset ThisTag.GeneratedContent = ThisTag.GeneratedContent & request.terraform.FormFoot & script>
			
			<cfhtmlHead text="#request.terraform.HTMLHead#">
			<cfset request.terraform.HTMLHead = "">

		</cfif>
		
		
		
		
		
		
		
		<cfif (not compareNoCase(state, "form")) AND NOT Len(ErrorFieldList) AND NOT Len(ErrorMessageList)>
		
			<!--- 
			*****************************************************************
			** WRAP UP                                                     **
			** It passed validation, so post-process the field values, and **
			** shift into the output. Flag the form data as valid          **
			*****************************************************************
			--->
			
			<cfloop index="ThisPassthroughField" list="#Attributes.PassthroughFieldList#">
				<cfif IsDefined("#Scope#.#ThisPassthroughField#")>
					<cfset value = Evaluate("#Scope#.#ThisPassthroughField#")>
					<cfif structKeyExists(request.terraform.null, ThisPassthroughField)>
						<cfset request.terraform.null[ThisPassthroughField] = request.terraform.null[ThisPassthroughField] and (value eq "")>	
					<cfelse>
						<cfset request.terraform.null[ThisPassthroughField] = (value eq "")>
					</cfif>		
				</cfif>
			</cfloop>
			
			<cfloop index="i" from="1" to="#CountFields#">
				<cfset ThisField = ThisTag.Fields[i]>
				<cfif StructKeyExists(ThisField, "Prompt") AND (thisField.Value EQ thisField.Prompt)>
					<cfif StructKeyExists(ThisField, "PromptValue")>
						<cfset thisField.Value = thisField.PromptValue>
					<cfelse>
						<cfset thisField.Value = "">	
					</cfif>
				</cfif>
				<!---
          		this works in cfmx but is not backwards compatible
    			<cfif isDefined("#thisField.DataType#Out") and IsCustomFunction(variables["#thisField.DataType#Out"])>
    			--->
    			<cfif IsDefined("variables.#thisField.DataType#Out") and IsCustomFunction(evaluate("#thisField.DataType#Out"))>
					<cfset thisField.Value = evaluate("#thisField.DataType#Out(thisField.Value)")>
				</cfif>
				
				<cfif structKeyExists(request.terraform.null, thisField.name)>
					<cfset request.terraform.null[thisField.name] = request.terraform.null[thisField.name] and (thisField.value eq "")>	
				<cfelse>
					<cfset request.terraform.null[thisField.name] = (thisField.value eq "")>
				</cfif>
				
				<cfif IsDefined("variables.#thisField.DataType#cfsqltype") and IsCustomFunction(evaluate("#thisField.DataType#cfsqltype"))>
					<cfset request.terraform.cfsqltype[thisField.name] = evaluate("#thisField.DataType#cfsqltype()")>
				<cfelse>
					<cfset request.terraform.cfsqltype[thisField.name] = "cf_sql_varchar">
				</cfif>

				<cfif not compareNoCase(Scope, "Caller.Form")>
					<cfparam name="Form.FieldNames" default="">
					<cfif NOT ListFindNoCase(Form.FieldNames, thisField.Name)>
						<cfset Form.FieldNames = ListAppend(Form.FieldNames, UCase(thisField.Name))>
					</cfif>
				</cfif>
				
				<cfif StructKeyExists(ThisField, "NoProcessing") AND IsBoolean(thisField.NoProcessing) AND thisField.NoProcessing>
					<!--- do nothing --->
				<cfelse>
					<cfset "#Scope#.#thisField.Name#" = thisField.Value>
				</cfif>
				
				<!--- generate XML output if required --->
				<cfif len(attributes.xml) and len(thisField.xpath)>
					<!--- try to find the location of the data in the XML --->
					<cftry>
					 	<cfset temp = xmlFind(caller[attributes.xml], thisField.xpath)>
						<cfset temp.xmlText = thisField.Value>
						<cfcatch />
					</cftry>
				</cfif>
						
			</cfloop>	
			
			<cfif NOT Attributes.AlwaysDisplayForm>
				<cfset ThisTag.GeneratedContent = "">
			</cfif>
			
			<cfset "#Scope#.#Attributes.Name#" = "Valid">
			
		</cfif>

		<cfif Attributes.Debug>
			<!--- dump Fields array for debugging --->
			<cfset debug = structNew()>
			
			<cfset debug["01) Browser [request.terraform.browser]"] = request.terraform.browser>
			<cfset debug["02) cf_terraform tag attributes"] = attributes>
			<cfset debug["03) Settings path [request.terraform.settingspath]"] = request.terraform.settingspath>
			<cfset debug["04) Localization [request.terraform.locale]"] = request.terraform.locale>
			<cfset debug["05) #attributes.scope# scope"] = Evaluate(Scope)>
			<cfset debug["06) Form state [#attributes.scope#.#attributes.name#]"] = state>
			<cfset debug["07) HTML to insert into the HTML head [request.terraform.HTMLHead]"] = request.terraform.HTMLHead>
			<cfset debug["08) Fields (array by ordinal) [ThisTag.Fields]"] = ThisTag.Fields>
			<cfset debug["09) Fields (struct keyed by name) [request.terraform.fields]"] = request.terraform.fields>
			<cfif structKeyExists(thistag, "labels")>
				<cfset debug["10) Labels"] = ThisTag.labels>
			<cfelse>	
				<cfset debug["11) Labels"] = "">
			</cfif>
			<cfif structKeyExists(thistag, "rules")>
				<cfset debug["12) Rules"] = ThisTag.rules>
			<cfelse>
				<cfset debug["13) Rules"] = "">
			</cfif>
			<cfset debug["14) HTML to insert at the form foot [request.terraform.FormFoot]"] = request.terraform.FormFoot>
			<cfset debug["15) Error fields [errorFieldList]"] = errorFieldList>
			<cfset debug["16) Missing required fields [ErrorRequiredFieldList]"] = ErrorRequiredFieldList>
			<cfset debug["17) Error messages [errorMessageList]"] = errorMessageList>
			<cfset debug["18) CFSQLType (if form valid) [request.terraform.CFSQLType]"] = request.terraform.CFSQLType>
			<cfset debug["19) Null (if form valid) [request.terraform.null]"] = request.terraform.null>
			<cfdump var="#debug#" label="Debugging information" expand="no">
		</cfif>

		
		
		
		<cfif Attributes.DynamicLocale>
			<cfset SetLocale(PageLocale)>
		</cfif>
		

		
		
	</cfcase>
	
</cfswitch>
<cfsetting enablecfoutputonly="no">