<cfsilent>
	<cfif not compareNoCase(ThisTag.ExecutionMode, "end")>
	
		<!--- correct tag usage checks --->
		<cfif NOT ListFind(GetBaseTagList(),"CF_TERRAFORM")>
			<cfabort showerror="TerraForm implementation error: CF_terrarule must be nested inside a CF_terraform tag pair.">
		</cfif>
		
		<cfparam name="attributes.rule" type="string">
		<cfparam name="attributes.error" type="string">
		
		<cfif not structKeyExists(attributes, "fieldlist")>
			<!--- search the rule for fields --->
			<cfset attributes.fieldList = "">
			<cfset pos = 1>
			<cfloop condition="#pos#">
				<cfset nextField = refindnocase("\{([[:alnum:]_]+)\}", attributes.rule, pos, true)>
				<cfset pos = nextField.pos[1] + nextField.len[1]>	
				<cfif pos>
					<cfset attributes.fieldList = listAppend(attributes.fieldList, mid(attributes.rule, nextField.pos[2], nextField.len[2]))>
				</cfif>
			</cfloop>
		</cfif>
		
		<cfparam name="attributes.applyiferrors" type="boolean" default="false">
		
		<!--- shift this tag's attributes into the parent CF_TERRAFORM tag --->
		<cfassociate basetag="CF_terraform" datacollection="rules">
		
		<!--- insert a placeholder so we know where to substitute any inline errors block for this field  --->
		<cfset request.terraform.CurrentRuleNum = request.terraform.CurrentRuleNum + 1>
		<cfset ThisTag.GeneratedContent = "#chr(27)#rule #request.terraform.CurrentRuleNum##chr(27)#">

	</cfif>
</cfsilent>