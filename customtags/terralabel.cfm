<cfsilent>
	<cfswitch expression="#ThisTag.ExecutionMode#">
		
		<cfcase value="start">
		
			<!--- correct tag usage checks --->
			<cfif NOT ListFind(GetBaseTagList(),"CF_TERRAFORM")>
				<cfabort showerror="TerraForm implementation error: CF_terralabel must be nested inside a CF_terraform tag pair.">
			</cfif>
			<cfif NOT ThisTag.HasEndTag>
				<cfabort showerror="TerraForm implementation error: CF_terralabel must have an end tag.">
			</cfif>
			
			<!--- shift this tag's attributes into the parent CF_TERRAFORM tag --->
			<cfassociate basetag="CF_terraform" datacollection="labels">
			
		</cfcase>
		
		<cfcase value="end">
		
			<cfif NOT structkeyexists(attributes, "for")>
				<cfabort showerror="TerraForm implementation error: cf_terralabel must have a FOR attribute listing the field name or names that this label applies to.">
			</cfif>
			<cfset attributes.nolabel = structkeyexists(attributes, "label") and not len(attributes.label)>
			<cfparam name="attributes.label" default = "#ThisTag.GeneratedContent#">
			<cfparam name="attributes.indicateAccessKey" default = true>
			<!--- insert a placeholder so we know where to substitute the HTML
			block for this field --->
			<cfset request.terraform.CurrentLabelNum = request.terraform.CurrentLabelNum + 1>
			<cfset ThisTag.GeneratedContent = "#chr(27)#label #request.terraform.CurrentLabelNum##chr(27)#">
			
		</cfcase>
		
	</cfswitch>
</cfsilent>