<cfsilent>

	<cfscript>
	
		function StripHTML(value) {
			return rereplacenocase(value,"(<[^>]*>)|(&[^[:space:];];)","","all");
		}


		/**
		 * Pass in a value in bytes, and this function converts it to a human-readable format of bytes, KB, MB, or GB.
		 * Updated from Nat Papovich's version.
		 * 01/2002 - Optional Units added by Sierra Bufe (sierra@brighterfusion.com)
		 * 
		 * @param size 	 Size to convert. 
		 * @param unit 	 Unit to return results in.  Valid options are bytes,KB,MB,GB. 
		 * @return Returns a string. 
		 * @author Paul Mone (paul@ninthlink.com) 
		 * @version 2.1, January 7, 2002 
		 */
		function byteConvert(num) {
			var result = 0;
			var unit = "";
			
			// Set unit variables for convenience
			var bytes = 1;
			var kb = 1024;
			var mb = 1048576;
			var gb = 1073741824;
		
			// Check for non-numeric or negative num argument
			if (not isNumeric(num) OR num LT 0)
				return "Invalid size argument";
			
			// Check to see if unit was passed in, and if it is valid
			if ((ArrayLen(Arguments) GT 1)
				AND ("bytes,KB,MB,GB" contains Arguments[2]))
			{
				unit = Arguments[2];
			// If not, set unit depending on the size of num
			} else {
				  if      (num lt kb) {	unit ="bytes";
				} else if (num lt mb) {	unit ="KB";
				} else if (num lt gb) {	unit ="MB";
				} else                {	unit ="GB";
				}		
			}
			
			// Find the result by dividing num by the number represented by the unit
			result = num / Evaluate(unit);
			
			// Format the result
			if (result lt 10)
			{
				result = NumberFormat(Round(result * 100) / 100,"0.00");
			} else if (result lt 100) {
				result = NumberFormat(Round(result * 10) / 10,"90.0");
			} else {
				result = Round(result);
			}
			// Concatenate result and unit together for the return value
			return (result & " " & unit);
		}

		function internationaliseDate(date) {
			date = trim(date);
			if ( LSIsDate(date) )
				return LSParseDateTime(date);		
			if ( IsDate(date) )
				return ParseDateTime(date);
			return date;
		}
		function localiseDate(date) {
			if ( IsDate(date) )
				return LSDateFormat(date, ShortDateMask());
			return date;
		}

		function internationaliseNumber(number) {
			number = trim(number);
			if ( LSIsNumeric(number) )
				return LSParseNumber(number);		
			return number;
		}
		function localiseNumber(number) {
			var wholepart = 0;
			var decimalpath = 0;
			if ( IsNumeric(number) ) {
				wholepart = listfirst(number, ".");
				decimalpart = listrest(number, ".");
				decimalpart = rereplace(decimalpart, "0+$", "");
				if ( not len(decimalpart) )
					return wholepart;
				return wholepart & request.terraform.locale.decimalSymbol & decimalpart;
			}	
			return number;
		}

		function internationaliseTime(time) {
			time = replaceNoCase(replaceNoCase(trim(time), request.terraform.locale.am, "AM"), request.terraform.locale.pm, "PM");
			if ( LSIsDate(time) )
				return LSParseDateTime(time);		
			if ( IsDate(time) )
				return ParseDateTime(time);
			return time;
		}
		function localiseTime(time) {
			var result = "";
			if ( IsDate(time) ) {
				result = LSTimeFormat(time, shortTimeMask());
				return replaceNoCase(replaceNoCase(trim(result), "AM", request.terraform.locale.am), "PM", request.terraform.locale.pm);
			}	
			return time;
		}
		
		function internationaliseCurrency(currency) {
			currency = trim(currency);	
			if ( len(currency) and LSIsCurrency(currency) )
				return LSParseEuroCurrency(currency);		
			return currency;
		}
		function localiseCurrency(currency) {
			if ( IsNumeric(currency) )
				return LSEuroCurrencyFormat(currency);
			return currency;
		}
			
		function InputFieldFormat(string) {
			// this function no longer filters ampersands (&) in order that markup such as &#32080; IS translated into the appropriate Unicode character. Previously, the markup would be displayed. Here is how the function used to look:
			// return ReplaceList(string, ""&,",<,>", "&amp;,&quot;,&lt;,&gt;");
			return ReplaceList(string, """,<,>", "&quot;,&lt;,&gt;");
		}
		
		function ClientLocale() {
			var FirstLocale = "";
			var LanguageCode = "";
			var CountryCode = "";
			if ( StructKeyExists(CGI, "HTTP_ACCEPT_LANGUAGE") ) {
				FirstLocale = SpanExcluding(CGI.HTTP_ACCEPT_LANGUAGE, ",;");
				LanguageCode = ListFirst(FirstLocale, "-");
				if ( ListLen(FirstLocale, "-") EQ 2 )
					CountryCode = ListGetAt(FirstLocale, 2, "-");
			}
			switch(LanguageCode){
				case "nl":
					switch(CountryCode){
						case "be":
							return "Dutch (Belgian)";
						default:
							return "Dutch (Standard)";
					}		
				case "en":
					switch(CountryCode){
						case "au":
							return "English (Australian)";
						case "ca":
							return "English (Canadian)";
						case "nz":
							return "English (New Zealand)";
						case "us":
							return "English (US)";
						default:
							return "English (UK)";
					}		
				case "fr":
					switch(CountryCode){
						case "be":
							return "French (Belgian)";
						case "ca":
							return "French (Canadian)";
						case "ch":
							return "French (Swiss)";
						default:
							return "French (Standard)";
					}
				case "de":
					switch(CountryCode){
						case "at":
							return "German (Austrian)";
						case "ch":
							return "German (Swiss)";
						default:
							return "German (Standard)";
					}		
				case "it":
					switch(CountryCode){
						case "ch":
							return "Italian (Swiss)";
						default:
							return "Italian (Standard)";
					}
				case "ja":
					return "Japanese";	
				case "ko":
					return "Korean";						
				case "nb":
					return "Norwegian (Bokmal)";	
				case "nn":
					return "Norwegian (Nynorsk)";
				case "pt":
					switch(CountryCode){
						case "br":
							return "Portuguese (Brazilian)";
						default:
							return "Portuguese (Standard)";
					}	
				case "es":
					switch(CountryCode){
						case "mx":
							return "Spanish (Mexican)";
						default:
							return "Spanish (Modern)";
					}
				case "zh":
					switch(CountryCode){
						case "tw":
							return "Chinese (Taiwan)";
						case "hk":
							return "Chinese (Hong Kong)";
						default:
							return "Chinese (China)";
					}							
				default:
					return GetLocale();
			}		
		}	
		
		function LSShortNumberFormat(number) {
			return Replace(number, ".", request.terraform.locale.decimalSymbol);
		}
		
		function ShortDateMask() {
			var locale = getLocale();
			if ( ArrayLen(Arguments) )
				locale = Arguments[1]; 
			return request.terraform.locale.shortDateMask;
		}
		
		function ShortTimeMask() {
			var locale = getLocale();
			if ( ArrayLen(Arguments) )
				locale = Arguments[1]; 
			return request.terraform.locale.shortTimeMask;
		}			
			
		function HumanShortDateMask() {
			var locale = getLocale();
			if ( ArrayLen(Arguments) )
				locale = Arguments[1]; 
			return request.terraform.locale.shortDateHint;		
		}
		
		function DeclareError(ErrorCode, ErrorMessage) {
			if ( NOT StructKeyExists(Errors, ErrorCode) )
				Errors[ErrorCode] = ErrorMessage;
			return TRUE;	
		}

		function BuildError(ErrorCode) {
			var firstLetterOfCaption = "";
			var restOfCaption = "";
			var capitalisedCaption = "";
			
			if ( len(thisField.caption_error) )
				firstLetterOfCaption = uCase(mid(thisField.caption_error, 1, 1));
			if ( len(thisField.caption_error) gt 1 )
				restOfCaption = mid(thisField.caption_error, 2, len(thisField.caption_error) - 1);

			capitalisedCaption = firstLetterOfCaption & restOfCaption;
				
			if ( len(errorCode) )
				theError = Errors[errorCode];
			else
				theError = arguments[2];	
				
			// it's important to replace the shorter substitutes last so e.g.
			// MAXDATE doesn't get interpreted as MAX.
			theError = Replace(theError, "#ESC#CAPTION", thisField.caption_error);
			theError = Replace(theError, "#ESC#InitialCapsCAPTION", capitalisedCaption);
			theError = Replace(theError, "#ESC#CUSTOM2", CUSTOM2);
			theError = Replace(theError, "#ESC#VALUE", htmlEditFormat(thisField.Value));
			theError = Replace(theError, "#ESC#MINLENGTH", thisField.MinLength);
			theError = Replace(theError, "#ESC#MAXLENGTH", thisField.MaxLength);	
			theError = Replace(theError, "#ESC#DATEHINT", request.terraform.locale.shortDateHint);
			theError = Replace(theError, "#ESC#TIMEHINT", request.terraform.locale.shortTimeHint);
			theError = Replace(theError, "#ESC#CUSTOM1", CUSTOM1);
			theError = Replace(theError, "#ESC#CUSTOM2", CUSTOM2);
	
			switch(ErrorCode) {
				case "ENHANCEDFILE_ExtensionNotAllowed": {
					theError = Replace(theError, "#ESC#EXTENSIONLIST", listChangeDelims(thisField.extensionList, ", "));
					break;
				}
				case "ENHANCEDFILE_FileTooLarge": {
					theError = Replace(theError, "#ESC#SIZE", byteConvert(thisField.filesize));
					theError = Replace(theError, "#ESC#MAXSIZE", byteConvert(thisField.maxSize));
					break;
				}
				case "ENHANCEDFILE_MimetypeNotAllowed": {
					theError = Replace(theError, "#ESC#MIMETYPE", thisField.mimetype);
					theError = Replace(theError, "#ESC#MIMETYPELIST", listchangedelims(thisField.mimetypelist, ", "));
					break;
				}										
				case "OutOfRange_Min_Date": {
					theError = Replace(theError, "#ESC#MINDATE", LSDateFormat(parseDateTime(thisField.Min), ShortDateMask()));
					break;
				}
				case "OutOfRange_MinMax_Date": {
					theError = Replace(theError, "#ESC#MINDATE", LSDateFormat(parseDateTime(thisField.Min), ShortDateMask()));
					theError = Replace(theError, "#ESC#MAXDATE", LSDateFormat(parseDateTime(thisField.Max), ShortDateMask()));
					break;
				}
				case "OutOfRange_Max_Date": {
					theError = Replace(theError, "#ESC#MAXDATE", LSDateFormat(parseDateTime(thisField.Max), ShortDateMask()));
					break;
				}
				case "OutOfRange_Exact_Date": {
					theError = Replace(theError, "#ESC#MINDATE", LSDateFormat(parseDateTime(thisField.Min), ShortDateMask()));
					theError = Replace(theError, "#ESC#MAXDATE", LSDateFormat(parseDateTime(thisField.Max), ShortDateMask()));
					break;
				}
				case "OutOfRange_Min_Time": {
					theError = Replace(theError, "#ESC#MINTIME", LSTimeFormat(parseDateTime(thisField.Min)));
					break;
				}
				case "OutOfRange_MinMax_Time": {
					theError = Replace(theError, "#ESC#MINTIME", LSTimeFormat(parseDateTime(thisField.Min)));
					theError = Replace(theError, "#ESC#MAXTIME", LSTimeFormat(parseDateTime(thisField.Max)));
					break;
				}
				case "OutOfRange_Max_Time": {
					theError = Replace(theError, "#ESC#MAXTIME", LSTimeFormat(parseDateTime(thisField.Max)));
					break;
				}
				case "OutOfRange_Exact_Time": {
					theError = Replace(theError, "#ESC#MINTIME", LSTimeFormat(parseDateTime(thisField.Min)));
					theError = Replace(theError, "#ESC#MAXTIME", LSTimeFormat(parseDateTime(thisField.Max)));
					break;
				}
				case "Pattern": {
					theError = Replace(theError, "#ESC#PATTERN", thisField.Pattern);
					break;
				}								
				case "RequiredFieldsMissing": {
					temp = ListChangeDelims(MissingRequiredFields, ", ", RS);
					theError = Replace(theError, "#ESC#REQUIREDFIELDS", temp);	
					break;
				}
				case "RequiredFieldMissing": {
					theError = Replace(theError, "#ESC#REQUIREDFIELDS", MissingRequiredFields);	
					break;
				}	
				case "ProhibitedWords": {
					temp = ListChangeDelims(ListQualify(FoundWordList, """"), ", ");
					theError = Replace(theError, "#ESC#PROHIBITEDWORDS", temp);	
					break;
				}
				case "ProhibitedWord": {
					theError = Replace(theError, "#ESC#PROHIBITEDWORDS", FoundWordList);	
					break;
				}
			}	
			theError = Replace(theError, "#ESC#MIN", thisField.Min);
			theError = Replace(theError, "#ESC#MAX", thisField.Max);
				
			return theError;
		}
		
		function ApplyDataTypeMinMax(Min, Max) {
			if ( (NOT Len(thisField.Min)) OR (Min GT thisField.Min) )
				thisField.Min = Min;
			if ( (NOT Len(thisField.Max)) OR (Max LT thisField.Max) )
				thisField.Max = Max;
		}
	
		function ApplyDataTypeMaxLength(MaxLength) {
			if ( (NOT Len(thisField.MaxLength)) OR (MaxLength LT thisField.MaxLength) )
				thisField.MaxLength = MaxLength;
		}	
	
		function Preset(PresetName, Name, Value) {
			if ( NOT StructKeyExists(Presets, PresetName) )
				Presets[PresetName] = StructNew();
			if ( NOT StructKeyExists(Presets[PresetName], Name) )
				Presets[PresetName][Name] = Value;
		}


		function getBrowser() {
			
			/* Sample user agents:
			MSIE 6.0/PC				Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)
			MSIE 5.0/Mac			Mozilla/4.0 (compatible; MSIE 5.0; Mac_PowerPC)
			Netscape 4.7/PC			Mozilla/4.7 [en] (WinNT; I)
			Netscape 6.01/PC 		Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; m18) Gecko/20010131 Netscape6/6.01
			Netscape 6.01/Mac 		Mozilla/5.0 (Macintosh; N; PPC; en-US; m18) Gecko/20010131 Netscape6/6.01
			Opera 7.11/PC 			Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1) Opera 7.11  [en]
			
			See http://www.opera.com/support/search/supsearch.dml?index=570
			*/
			
			var browser = StructNew();
			var foundPlatform = false;
			var foundEngine = false;
			var match = "";
			var openingSpace	 = "([(;[:space:]]|^)";
			var closingSpace	 = "([);[:space:]]|$)";
			var version			 = "([.0-9]+[+.[:alnum:]]*)";
			
			browser.engine 		 = "[Unknown]";
			browser.engineversion= 0;
			browser.platform 	 = "[Unknown]";

			if ( not foundEngine ) {
				// Search for Konqueror
				match = refind("#openingSpace#Konqueror/#version##closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.engine = "Konqueror";
					browser.engineversion = mid(cgi.http_user_agent, match.pos[3], match.len[3]);
					foundEngine = true;
				}
			}
			
			if ( not foundEngine ) {
				// Search for Safari
				match = refind("#openingSpace#Safari/#version##closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.engine = "Konqueror";
					foundEngine = true;
				}
			}
			
			if ( not foundEngine ) {
				// Search for Opera
				match = refind("#openingSpace#Opera[/[:space:]]#version##closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.engine = "Opera";
					browser.engineversion = mid(cgi.http_user_agent, match.pos[3], match.len[3]);
					foundEngine = true;
				}
			}

			if ( not foundEngine ) {
				// search for Web TV
				match = refind("#openingSpace#WebTV[/[:space:]]#version##closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.engine = "WebTV";
					browser.engineversion = mid(cgi.http_user_agent, match.pos[3], match.len[3]);
					foundEngine = true;
				}
			}
				
			if ( not foundEngine ) {
				// search for MSIE
				match = refind("#openingSpace#MSIE[[:space:]]#version##closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.engine = "Internet Explorer";
					browser.engineversion = mid(cgi.http_user_agent, match.pos[3], match.len[3]);
					foundEngine = true;
				}
			}
						
			if ( not foundEngine ) {
				// search for Gecko
				match = refind("#openingSpace#Gecko/#version##closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.engine = "Gecko";
					browser.engineversion = mid(cgi.http_user_agent, match.pos[3], match.len[3]);
					foundEngine = true;
				}
			}

			if ( not foundEngine ) {
				// search for Mozilla
				match = refind("#openingSpace#Mozilla/#version##closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.engine = "Netscape";
					browser.engineversion = mid(cgi.http_user_agent, match.pos[3], match.len[3]);
					foundEngine = true;
				}
			}

			if ( not foundEngine ) {
				// search for Lynx
				match = refind("#openingSpace#Lynx/#version##closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.engine = "Lynx";
					browser.engineversion = mid(cgi.http_user_agent, match.pos[3], match.len[3]);
					foundEngine = true;
				}
			}
			
			browser.intEngineVersion = int(val(browser.engineVersion));
						

			if ( not foundPlatform ) {
				// search for Windows
				match = refind("#openingSpace#Win[^[:space:]]*#closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.platform = "Windows";
					foundPlatform = true;
				}
			}

			if ( not foundPlatform ) {
				// search for Mac
				match = refind("#openingSpace#Mac(_PowerPC|intosh)#closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.platform = "Mac";
					foundPlatform = true;
				}
			}

			if ( not foundPlatform ) {
				// search for Linux
				match = refind("#openingSpace#Linux#closingSpace#", cgi.http_user_agent, 1, true);
				if ( match.pos[1] ) {
					browser.platform = "Linux";
					foundPlatform = true;
				}
			}
			
			return browser;
		}
		
		function getLocaleData(localeName) {
			var locale = structNew();
			
			switch(localeName) {
			
				case "Dutch (Belgian)": {
					locale.language 		 = "Dutch";
					locale.languageCode		 = "nl";
					locale.country			 = "Belgium";
					locale.countryCode		 = "BE";
					locale.javaLocaleId		 = "nl_BE";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "d/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "dag / maand / jaar";
					locale.shortTimeHint 	 = "uur:minuut";
					break;
				}
				
				case "Dutch (Standard)": {
					locale.language 		 = "Dutch";
					locale.languageCode		 = "nl";
					locale.country			 = "Netherlands";
					locale.countryCode		 = "NL";
					locale.javaLocaleId		 = "nl_NL";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "d-m-yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "dag-maand-jaar";
					locale.shortTimeHint 	 = "uur:minuut";
					break;
				}
				
				case "English (Australian)": {
					locale.language 		 = "English";
					locale.languageCode		 = "en";
					locale.country			 = "Australia";
					locale.countryCode		 = "AU";
					locale.javaLocaleId		 = "en_AU";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = false;
					locale.shortDateMask	 = "d/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "h:mm:ss tt";
					locale.shortDateHint 	 = "day / month / year";
					locale.shortTimeHint 	 = "HH:MM AM|PM";
					break;
				}
				
				case "English (Canadian)": {
					locale.language 		 = "English";
					locale.languageCode		 = "en";
					locale.country			 = "Canada";
					locale.countryCode		 = "CA";
					locale.javaLocaleId		 = "en_CA";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = false;
					locale.shortDateMask	 = "dd/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "h:mm:ss tt";
					locale.shortDateHint 	 = "day / month / year";
					locale.shortTimeHint 	 = "HH:MM AM|PM";
					break;
				}
				
				case "English (New Zealand)": {
					locale.language 		 = "English";
					locale.languageCode		 = "en";
					locale.country			 = "New Zealand";
					locale.countryCode		 = "NZ";
					locale.javaLocaleId		 = "en_NZ";
					locale.am				 = "a.m.";
					locale.pm				 = "p.m.";
					locale.twentyFourHour	 = false;
					locale.shortDateMask	 = "d/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "h:mm:ss tt";
					locale.shortDateHint 	 = "day / month / year";
					locale.shortTimeHint 	 = "HH:MM a.m.|p.m.";
					break;
				}
				
				case "English (UK)": {
					locale.language 		 = "English";
					locale.languageCode		 = "en";
					locale.country			 = "United Kingdom";
					locale.countryCode		 = "GB";
					locale.javaLocaleId		 = "en_GB";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "day / month / year";
					locale.shortTimeHint 	 = "HH:MM";
					break;
				}
				
				case "English (US)": {
					locale.language 		 = "English";
					locale.languageCode		 = "en";
					locale.country			 = "United States";
					locale.countryCode		 = "US";
					locale.javaLocaleId		 = "en_US";
					locale.am 				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = false;
					locale.shortDateMask	 = "m/d/yyyy";
					locale.dateOrder		 = "m,d,y";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "h:mm:ss tt";
					locale.shortDateHint 	 = "month / day / year";
					locale.shortTimeHint 	 = "HH:MM AM|PM";
					break;
				}
				
				case "French (Belgian)": {
					locale.language 		 = "French";
					locale.languageCode		 = "fr";
					locale.country			 = "Belgium";
					locale.countryCode		 = "BE";
					locale.javaLocaleId		 = "fr_BE";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "d/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "jour / mois / ann&eacute;e";
					locale.shortTimeHint 	 = "heure:minute";
					break;
				}
				
				case "French (Canadian)": {
					locale.language 		 = "French";
					locale.languageCode		 = "fr";
					locale.country			 = "Canada";
					locale.countryCode		 = "CA";
					locale.javaLocaleId		 = "fr_CA";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "yyyy-mm-dd";
					locale.dateOrder		 = "y,m,d";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = " ";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "ann&eacute;e-mois-jour";
					locale.shortTimeHint 	 = "heure:minute";
					break;
				}
				
				case "French (Standard)": {
					locale.language 		 = "French";
					locale.languageCode		 = "fr";
					locale.country			 = "France";
					locale.countryCode		 = "FR";
					locale.javaLocaleId		 = "fr_FR";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = " ";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "jour / mois /ann&eacute;e";
					locale.shortTimeHint 	 = "heure:minute";
					break;
				}
				
				case "French (Swiss)": {
					locale.language 		 = "French";
					locale.languageCode		 = "fr";
					locale.country			 = "Switzerland";
					locale.countryCode		 = "CH";
					locale.javaLocaleId		 = "fr_CH";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd.mm.yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = "'";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "jour.mois.ann&eacute;e";
					locale.shortTimeHint 	 = "heure:minute";
					break;
				}
				
				case "German (Austrian)": {
					locale.language 		 = "German";
					locale.languageCode		 = "de";
					locale.country			 = "Austria";
					locale.countryCode		 = "AT";
					locale.javaLocaleId		 = "de_AT";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd.mm.yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "Tag.Monat.Jahr";
					locale.shortTimeHint 	 = "Stunde:Minute";
					break;
				}
				
				case "German (Standard)": {
					locale.language 		 = "German";
					locale.languageCode		 = "de";
					locale.country			 = "Germany";
					locale.countryCode		 = "DE";
					locale.javaLocaleId		 = "de_DE";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd.mm.yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "Tag.Monat.Jahr";
					locale.shortTimeHint 	 = "Stunde:Minute";
					break;
				}
				
				case "German (Swiss)": {
					locale.language 		 = "German";
					locale.languageCode		 = "de";
					locale.country			 = "Switzerland";
					locale.countryCode		 = "CH";
					locale.javaLocaleId		 = "de_CH";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd.mm.yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = "'";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "Tag.Monat.Jahr";
					locale.shortTimeHint 	 = "Stunde:Minute";
					break;
				}
				
				case "Italian (Standard)": {
					locale.language 		 = "Italian";
					locale.languageCode		 = "it";
					locale.country			 = "Italy";
					locale.countryCode		 = "IT";
					locale.javaLocaleId		 = "it_IT";
					locale.am				 = "AM";
					locale.pm 				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "H.mm.ss";
					locale.shortDateHint 	 = "giorno / mese / anno";
					locale.shortTimeHint 	 = "ora.minuto";
					break;
				}
				
				case "Italian (Swiss)": {
					locale.language 		 = "Italian";
					locale.languageCode		 = "it";
					locale.country			 = "Switzerland";
					locale.countryCode		 = "CH";
					locale.javaLocaleId		 = "it_CH";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd.mm.yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = "'";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "giorno.mese.anno";
					locale.shortTimeHint 	 = "ora:minuto";
					break;
				}
				
				case "Norwegian (Bokmal)": {
					locale.language 		 = "Norwegian (Bokmal)";
					locale.languageCode		 = "no";
					locale.country			 = "Norway";
					locale.countryCode		 = "NO";
					locale.javaLocaleId		 = "no_NO";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd.mm.yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = " ";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "dag.m&aring;ned.&aring;r";
					locale.shortTimeHint 	 = "time:minutt";
					break;
				}
				
				case "Norwegian (Nynorsk)": {
					locale.language 		 = "Norwegian (Nynorsk)";
					locale.languageCode		 = "no";
					locale.country			 = "Norway";
					locale.countryCode		 = "NO";
					locale.javaLocaleId		 = "no_NO_nynorsk";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd.mm.yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = " ";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "dag.m&aring;ned.&aring;r";
					locale.shortTimeHint 	 = "time:minutt";
					break;
				}
				
				case "Portuguese (Brazilian)": {
					locale.language 		 = "Portuguese";
					locale.languageCode		 = "pt";
					locale.country			 = "Brazil";
					locale.countryCode		 = "BR";
					locale.javaLocaleId		 = "pt_BR";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "d/m/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "HH:mm:ss";
					locale.shortDateHint 	 = "dia / m&ecirc;s / ano";
					locale.shortTimeHint 	 = "hora:minuto";
					break;
				}
				
				case "Portuguese (Standard)": {
					locale.language 		 = "Portuguese";
					locale.languageCode		 = "pt";
					locale.country			 = "Portugal";
					locale.countryCode		 = "PT";
					locale.javaLocaleId		 = "pt_PT";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd-mm-yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "dia-m&ecirc;s-ano";
					locale.shortTimeHint 	 = "hora:minuto";
					break;
				}
				
				case "Spanish (Mexican)": {
					locale.language 		 = "Spanish";
					locale.languageCode		 = "es";
					locale.country			 = "Mexico";
					locale.countryCode		 = "MX";
					locale.javaLocaleId		 = "es_MX";
					locale.am				 = "a.m.";
					locale.pm				 = "p.m.";
					locale.twentyFourHour	 = false;
					locale.shortDateMask	 = "dd/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "hh:mm:ss tt";
					locale.shortDateHint 	 = "d&iacute;a / mes / a&ntilde;o";
					locale.shortTimeHint 	 = "hora:minuto a.m.|p.m.";
					break;
				}
				
				case "Spanish (Modern)": {
					locale.language 		 = "Spanish";
					locale.languageCode		 = "es";
					locale.country			 = "Spain";
					locale.countryCode		 = "ES";
					locale.javaLocaleId		 = "es_ES";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "d&iacute;a / mes / a&ntilde;o";
					locale.shortTimeHint 	 = "hora:minuto";
					break;
				}
				
				case "Spanish (Standard)": {
					locale.language 		 = "Spanish";
					locale.languageCode		 = "es";
					locale.country			 = "Spain";
					locale.countryCode		 = "ES";
					locale.javaLocaleId		 = "es_ES";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "dd/mm/yyyy";
					locale.dateOrder		 = "d,m,y";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "d&iacute;a / mes / a&ntilde;o";
					locale.shortTimeHint 	 = "hora:minuto";
					break;
				}
				
				case "Swedish": {
					locale.language 		 = "Swedish";
					locale.languageCode		 = "sv";
					locale.country			 = "Sweden";
					locale.countryCode		 = "SE";
					locale.javaLocaleId		 = "sv_SE";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.shortDateMask	 = "yyyy-mm-dd";
					locale.dateOrder		 = "y,m,d";
					locale.decimalSymbol	 = ",";
					locale.digitGroupingSymbol
											 = ".";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "&aring;r-m&aring;nad-dag";
					locale.shortTimeHint 	 = "timma:minut";
					break;
				}
				
				case "Japanese": {
					locale.language 		 = "Japanese";
					locale.languageCode		 = "ja";
					locale.country			 = "Japan";
					locale.countryCode		 = "JP";
					locale.javaLocaleId		 = "ja_JP";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.dateOrder		 = "y,m,d";
					locale.shortDateMask	 = "yyyy/mm/dd";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "&##24180; / &##26376; / &##26085;";
					locale.shortTimeHint 	 = "&##26178;&##38291;:&##20998;";
					break;
				}
				
				case "Korean": {
					locale.language 		 = "Korean";
					locale.languageCode		 = "ko";
					locale.country			 = "Korea";
					locale.countryCode		 = "KR";
					locale.javaLocaleId		 = "ko_KR";
					locale.am = "&##50724;&##51204;";
					locale.pm = "&##50724;&##54980;";
					locale.twentyFourHour	 = false;
					locale.dateOrder		 = "y,m,d";
					locale.shortDateMask	 = "yyyy-mm-dd";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "tt h:mm:ss";
					locale.shortDateHint 	 = "&##45380;-&##44060;&##50900;-&##51068;";
					locale.shortTimeHint 	 = "&##50724;&##51204;|&##50724;&##54980;";
					break;
				}
				
				case "Chinese (China)": {
					locale.language 		 = "Chinese";
					locale.languageCode		 = "zh";
					locale.country			 = "China";
					locale.countryCode		 = "CN";
					locale.javaLocaleId		 = "zh_CN";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.dateOrder		 = "y,m,d";
					locale.shortDateMask	 = "yyyy-m-d";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "&##24180;-&##26376;-&##22825;";
					locale.shortTimeHint 	 = "&##23567;&##26102;:&##20998;&##38047;";
					break;
				}
				
				case "Chinese (Hong Kong)": {
					locale.language 		 = "Chinese";
					locale.languageCode		 = "zh";
					locale.country			 = "Hong Kong";
					locale.countryCode		 = "HK";
					locale.javaLocaleId		 = "zh_HK";
					locale.am				 = "AM";
					locale.pm				 = "PM";
					locale.twentyFourHour	 = true;
					locale.dateOrder		 = "d,m,y";
					locale.shortDateMask	 = "d/m/yyyy";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "H:mm:ss";
					locale.shortDateHint 	 = "&##22825;/&##26376;/&##24180;";
					locale.shortTimeHint 	 = "&##23567;&##26102;:&##20998;&##38047;";
					break;
				}
				
				case "Chinese (Taiwan)": {
					locale.language 		 = "Chinese";
					locale.languageCode		 = "zh";
					locale.country			 = "Taiwan";
					locale.countryCode		 = "TW";
					locale.javaLocaleId		 = "zh_TW";
					locale.am				 = "&##19978;&##21320;";
					locale.pm				 = "&##19979;&##21320;";
					locale.twentyFourHour	 = false;
					locale.dateOrder		 = "y,m,d";
					locale.shortDateMask	 = "yyyy-m-d";
					locale.decimalSymbol	 = ".";
					locale.digitGroupingSymbol
											 = ",";
					locale.shortTimeMask	 = "tt hh:mm:ss";
					locale.shortDateHint 	 = "&##24180;-&##26376;-&##22825;";
					locale.shortTimeHint 	 = "&##19978;&##21320;|&##19979;&##21320; &##23567;&##26102;:&##20998;&##38047;";
					break;
				}
		
			}
			return locale;
		}
		
		function xmlFind(xmlDoc, xPath) {
			if ( not refind("^(/[[:alpha:]][[:alnum:]]*)+$", arguments.xPath) )
				return false; //can't parse this - it might be a valid xPath but this is just a very basic parser :-( 	
			return evaluate("arguments.xmlDoc#replace(arguments.xPath, "/", ".", "all")#");
		}		

	</cfscript>

</cfsilent>