<!--- ---------------------------------------------------------------------
TerraForm Settings File
---------------------------------------------------------------------------

This file controls your TerraForm functionality by declaring all the presets, datatypes, formats, errors, and default values you may want to use. For tidyness and modularity it is recommended that most declarations are CFINCLUDEd rather than placed directly in this file. Note that you can use multiple settings files on a site or server to customise your TerraForm setup for different situations. For example, you may have one settings file for public pages and another for administration pages. 

This file now includes language evaluation so that TerraForm can incorporate the appropriate language pack automatically.

This file comprises 8 sections:
	1) Language evaluation
	2) Default cf_terraform attributes
	3) Conditional code for XHTML 
	4) Error messages
	5) Presets
	6) Datatypes
	7) Formats
	8) Misc
---------------------------------------------------------------------- --->


<!--- ---------------------------------------------------------------------
1) Language evaluation
---------------------------------------------------------------------- --->

<!--- Feel free to alter the following two settings . . . --->
<cfparam name="Attributes.DynamicLocale" default="NO" type="boolean">

<!--- this defines which language to use if the appropriate language pack is not available --->
<cfset defaultLanguageCode = "en">


<!--- . . . but please don't alter the following language evaluation lines unless you're sure you know what you're doing! These lines evaluate the appropriate language pack to use based on those available, and shouldn't need to be altered. --->
<cfif Attributes.DynamicLocale>
	<cfset SetLocale(ClientLocale())>
</cfif>
<cfset request.terraform.locale = getLocaleData(getLocale())>

<cfif directoryexists("#getdirectoryfrompath(getcurrenttemplatepath())##request.terraform.locale.languagecode#/")>
	<cfset languageCode = request.terraform.locale.languagecode>
<cfelse>	
	<cfset languageCode = defaultLanguageCode>
</cfif>

<cfif directoryexists("#getdirectoryfrompath(getcurrenttemplatepath())##request.terraform.locale.languagecode#\#request.terraform.locale.countrycode#/")>
	<cfset countrycode = request.terraform.locale.countrycode>
<cfelse>
	<cfinclude template="#languageCode#/defaultCountryCode.cfm">
</cfif>


<!--- ---------------------------------------------------------------------
2) Default cf_terraform attributes
---------------------------------------------------------------------- --->

<cfinclude template="#languageCode#/#countryCode#/defaults.cfm">



<!--- ---------------------------------------------------------------------
3) Conditional code for XHTML 
---------------------------------------------------------------------- --->

<cfinclude template="xhtml.cfm">




<!--- ---------------------------------------------------------------------
4) Error messages
---------------------------------------------------------------------- --->

<cfinclude template="#languageCode#/#countryCode#/errors.cfm">



<!--- ---------------------------------------------------------------------
5) Presets
---------------------------------------------------------------------- --->

<cfinclude template="#languageCode#/#countryCode#/presets.cfm">



<!--- ---------------------------------------------------------------------
6) Datatypes
---------------------------------------------------------------------- --->

<cfinclude template="datatypes/common.cfm">
<!--- cfinclude these datatype packs if you want to use them.  --->
<!--- <cfinclude template="datatypes/msaccess.cfm"> --->
<cfinclude template="datatypes/mssqlserver.cfm"><!---  --->
<!--- <cfinclude template="datatypes/mysql.cfm"> --->




<!--- ---------------------------------------------------------------------
7) Formats
---------------------------------------------------------------------- --->

<cfset Preset("Format_FCKEditor", "config.CustomConfigurationsPath", "#application.environment.resourcePath#settings/fckeditor/fckconfig.js")>
<cfinclude template="formats/common.cfm">
<cfif listFirst(server.coldfusion.productversion) gte 6>
	<!--- These formats are only supported by CFMX. --->
	<cfinclude template="formats/cfmx.cfm">
</cfif>



<!--- ---------------------------------------------------------------------
8) Misc
---------------------------------------------------------------------- --->

<cfinclude template="#languageCode#/#countryCode#/misc.cfm">



<!--- ---------------------------------------------------------------------
End
---------------------------------------------------------------------- --->
