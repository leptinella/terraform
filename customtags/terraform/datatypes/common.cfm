<cfscript>
	
	// *************************************************************************
	// DataType:	 BOOLEAN 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Build:		 20040718
	// Dependencies: String	
	// Notes:-
	// Boolean accepts any values that CF can evaluate as boolean ("TRUE", "NO"
	// etc.) and returns "YES" or "NO". "" will evaluate as "NO". If YesSynonyms 
	// and NoSynonyms are set, TerraForm will use these to translate first. You could
	// for example set YesSynonyms="ACCEPT" and NoSynonyms="DECLINE" or add support for
	// a foreign language. If you are using YesSynonyms and NoSynonyms, TerraForm will
	// use the first item in each list when displaying existing data.
	//						 
	// History:		 20040718 Blue Dragon compatibility: boolean is a reserved word
	// 				 20030708 added DataTypeAttributes
	//				 20030619 booleanclean() now returns "" as false
	//				 20030605 added BooleanCfsqltype
	//				 20021214 added YesSynonyms and NoSynonyms 
	// 				 20020914 added booleanIn()
	//				 20020531 Fixed preset bug
	//				 20020223 Created
	// Customisations:-
	//
	
	Preset("Datatype_Boolean", "Format", "Boolean");
	Preset("Datatype_Boolean", "YesSynonyms", "");
	Preset("Datatype_Boolean", "NoSynonyms", "");
	DataTypeAttributes["Boolean"] = "YesSynonyms,NoSynonyms";

	BooleanCfsqltype = stringCfsqltype;
	
	function BooleanIn(value) {
		value = trim(value);
		if ( isboolean(value) )
			if ( value ) {
				if ( len(thisField.YesSynonyms) )
					value = listFirst(thisField.YesSynonyms);
			}
			else {
				if ( len(thisField.NoSynonyms) )
					value = listFirst(thisField.NoSynonyms);
			}	
		return value;
	}
		
	function BooleanClean(value) {
		value = trim(value);
		if ( not len(value) ) 
			return "no";	
		if ( listFindNoCase(thisField.YesSynonyms, value) )
			value = "yes";
		else
			if ( listFindNoCase(thisField.NoSynonyms, value) )
				value = "no";
		return value;
	}

	function BooleanValidate(value) {
		if ( NOT IsBoolean(value) )
			return "NotBoolean";
		return "OK";
	}
	
	function BooleanOut(value) {
		return YesNoFormat(value);
	} 
	
	// End DataType: BOOLEAN 
	// *************************************************************************



	// *************************************************************************
	// DataType:	 CREDITCARDNUMBER 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20080707
	// Dependencies: 
	// Notes:-
	// CreditCardNumber performs the Luhn Formula (MOD 10) validation. ThisField is 
	// a useful preliminary check but does not guarantee the card is valid.
	// cf. http://www.beachnet.com/~hstiles/cardtype.html
	// History:		 20080707 removed Posix from regex
	//				 20030708 added DataTypeAttributes
	//				 20030605 added CreditCardNumberCfsqltype
	//				 20030527 added jsMask
	//				 20020620 Added default format
	//				 20020524 Turn autocomplete off
	//				 20020522 Added size preset
	//				 20020403 added qForms validation	
	// Customisations:-
	//
	
	Preset("Datatype_CreditCardNumber", "Format", "text");
	Preset("Datatype_CreditCardNumber", "MinLength", 14);
	Preset("Datatype_CreditCardNumber", "MaxLength", 16);
	Preset("Datatype_CreditCardNumber", "Size", 16);
	Preset("Datatype_CreditCardNumber", "AutoComplete", "off");
	Preset("Datatype_CreditCardNumber", "jsMask", "/[-0-9 ]/");
	DataTypeAttributes.CreditCardNumber = "jsMask";
	
	qFormsValidate.CreditCardNumber = "objForm.#SUB#.validateCreditCard()";

	function CreditCardNumberCfsqltype() {
		return "cf_sql_varchar";
	}
	
	function CreditCardNumberIn(value) {
		return ReplaceList(value, " ,-", ",");
	}
	CreditCardNumberClean = CreditCardNumberIn;
	
	function CreditCardNumberValidate(value) {
		var i = 0;
		var sum = 0;
		if ( REFind("[^0-9]", value) )
			return "CreditCardNumber_InvalidCharacters";
		value = REReplace(value, "(.)", "\1,", "ALL");
		for ( i = ListLen(value) - 1; i GTE 1; i = i - 2 )
			value = ListSetAt(value, i, ListGetAt(value, i) * 2);
		value = REReplace(value, "(.)", "\1,", "ALL");
		for ( i = 1; i LTE ListLen(value); i = i + 1 )
			sum = sum + ListGetAt(value, i);
		if ( sum MOD 10 )
			return "CreditCardNumber_InvalidNumber";
		return "OK";
	}
	
	// End DataType: CREDITCARDNUMBER 
	// *************************************************************************
	

	
	// *************************************************************************
	// DataType:	 DATE 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20080707
	// Dependencies: 
	// Notes:-
	// Date attempts to parse a date value in a locale-dependent format. 
	// History:		 20080707 handles disabledWeekdays, disabledDates
	//				 20030605 added DateCfsqltype
	//				 20020914 removed dateOut() and rewrote dateIn() and dateClean()
	//				 20020620 Added default format
	//				 20020406 Now using ShortDateMask() function
	// Customisations:-
	//
	
	Preset("Datatype_Date", "Format", "Calendar");
	RangeValidationStyle.Date = "Date";
		
	function DateCfsqltype() {
		return "cf_sql_date";
	}
		
	function DateIn(value) {
		return localiseDate(value);	
	}	

	function DateClean(value) {
		return internationaliseDate(value);
	}	
		// <!--- <cffunction name="DateValidate"><cfdump var="#thisField#"><cfabort></cffunction> --->
	function DateValidate(value) {
		var dow = 0;
		var i = 0;
		
		if ( NOT IsDate(value) )
			return "NotADate";
			
		if ( structKeyExists(thisField, "disabledWeekdays") and len(thisField.disabledWeekdays) ) {
			dow = dayOfWeek(value);
			// use backward compatibility for calendar format
			if ( thisField.format eq "calendar" ) {
				// convert dow into old system 0-6
				dow = (dow + 5) mod 7;
			}
			if ( listFind(thisField.disabledWeekdays, dow) )
				return "dateNotAvailable";
		}

		if ( structKeyExists(thisField, "disabledDates") and isArray(thisField.disabledDates) and arrayLen(thisField.disabledDates) ) {
			for ( i = 1; i lte arrayLen(thisField.disabledDates); i = i + 1 ) {
				if ( isSimpleValue(thisField.disabledDates[i]) ) {
					if ( value eq thisField.disabledDates[i] )
						return "dateNotAvailable";
				}
				else {
					if ( value gte thisField.disabledDates[i].from and value lte thisField.disabledDates[i].to )
						return "dateNotAvailable";
				}
			}	
		}
	
		Preset("Format_DatePicker", "disabledWeekdays", "");
		Preset("Format_DatePicker", "disabledDates", arrayNew(1));	
			
		return "OK";
	}
	
	// End DataType: DATE 
	// *************************************************************************


	
	// *************************************************************************
	// DataType:	 DEFAULT
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020620
	// Dependencies: 			
	// Notes:-
	// Default is the default datatype. It has no characteristics, performs
	// no transformations or validation.
	// History:		 20020620 Created	
	// Customisations:-
	//	
	
	// End DataType: DEFAULT 
	// *************************************************************************



	// *************************************************************************
	// DataType:	 EMAIL 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20100120
	// Dependencies: String		
	// Notes:-
	// Email accepts any value matching a regular expression. It does not 
	// verify that the value represents an actual email address.
	// History:		20100120 Stop allowing spaces
	//				20091210 Made validation far more general
	// 				20080331 changed expression to allow more characters in username
	// 				20061206 updated regex to support diverse TLDs
	//				20030605 added EmailCfsqltype
	//				20030514 added more insightful errors and "&"
	//				20021013 Updated regex based on UDF at
	//						 http://www.cflib.org/udf.cfm?ID=216
	//				20020620 Added default format
	//				20020403 added qForms validation
	//				20020401 moved "\-" to end of character class in regex.
	// 						 Wasn't working in middle.
	// Customisations:-
	//
	
	Preset("Datatype_Email", "Format", "text");
	qFormsValidate.Email = "objForm.#SUB#.validateEmail(""Please enter your e-mail address."")";
	
	EmailCfsqltype = stringCfsqltype;
	EmailClean = StringClean;
	
	function EmailValidate(value) {
		if ( NOT REFindNoCase("^[^@.\s]+(\.[^@.\s]+)*@[^@.\s]+(\.[^@.\s]+)+$", value, 1) ) {
			// if the email address is invalid, we will attempt to provide a more insightful error
			// by analysing it for specific faults.
			if ( not find("@", value) )
				return "NotAnEmailAddress_MissingAt";
			if ( not REFindNoCase("@[^@.\s]+(\.[^@.\s]+)+$", value) )
				return "NotAnEmailAddress_MissingDomain";
			return "NotAnEmailAddress";
		}
		return "OK";	
	}
	
	// End DataType: EMAIL 
	// *************************************************************************



	// *************************************************************************
	// DataType:	 INTEGER 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030605
	// Dependencies: 
	// Notes:-
	// Integer verifies that the value is a number, and that it is an integer
	// using locale-dependent number handling.	
	// History:		 20030605 added IntegerCfSqltype
	//				 20030527 added jsMask
	//				 20030319 commented out integerIn() 
	//				 		  - more trouble than it's worth
	//				 20020914 rewrote IntegerIn() and IntegerClean()
	//				 20020627 removed IntegerOut processing. It was putting commas
	//						  in numbers at the thousands position
	// 				 20020403 added qForms validation
	// Customisations:-
	//
	
	qFormsValidate.Integer = "objForm.#SUB#.validateNumeric()";
	
	Preset("Datatype_Integer", "Format", "Numeric");
	Preset("Datatype_Integer", "jsMask", "/[-+0-9]/");
	
	RangeValidationStyle.Integer = "Numeric";

	//function IntegerIn(value) {
	//	return localiseNumber(value);	
	//}	

	function IntegerCfSqltype() {
		return "cf_sql_integer";
	}	
	
	function IntegerClean(value) {
		return internationaliseNumber(value);
	}	
	
	function IntegerValidate(value) {
		if ( NOT IsNumeric(value) )
			return "NotANumber";
		if ( value NEQ Int(value) )
			return "NotAnInteger";
		return "OK";
	}
		
	// End DataType: INTEGER 
	// *************************************************************************



	// *************************************************************************
	// DataType:	 INTEGERLIST
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020912
	// Dependencies: 			
	// Notes:-
	// IntegerList validates a comma-delimited list. It verifies each item is a
	// number, and that it is an integer. Note that this datatype DOES NOT handle
	// locale specific numbers. Localised integers could legally contain commas.
	// However HTML forms use commas as delimiters.
	// History:		 20020912 Fixed validation bug
	//				 20020620 Added format and multiple presets
	//				 20020408 Locale handling was halfway there. Chose to remove 
	//				 		  it for speed rather than finish it and make the
	//						  handler slower.
	// Customisations:-
	//	
	
	Preset("Datatype_IntegerList", "Format", "Checkbox");
	Preset("Datatype_IntegerList", "Multiple", "Yes");
	
	function IntegerListValidate(value) {
		var ListLength = ListLen(value);
		var i = 1;
		for ( ; i LTE ListLength; i = i + 1 ) {
			ThisFieldInteger = ListGetAt(value, i);
			if ( NOT IsNumeric(ThisFieldInteger) )
				return "NotANumber";
			if ( thisFieldInteger NEQ Int(ThisFieldInteger) )
				return "NotAnInteger";
		}
		return "OK";
	}
	
	// End DataType: INTEGERLIST 
	// *************************************************************************

	
	
	// *************************************************************************
	// DataType:	 PRICE
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030605
	// Dependencies: 			
	// Notes:-
	// Price attempts to parse a currency value in a locale-dependent format. 
	// History:		 20030605 added PriceCfSqltype
	//				 20020914 rewrote PriceIn() and PriceClean()
	//				 20020409 Added preset format
	// Customisations:-
	//	
	
	Preset("Datatype_Price", "Format", "Numeric");

	function PriceCfSqltype() {
		return "cf_sql_money";
	}	
	
	function PriceIn(value) {
		return localiseCurrency(value);	
	}	

	function PriceClean(value) {
		return internationaliseCurrency(value);
	}	
	
	function PriceValidate(value) {
		if ( NOT IsNumeric(value) )
			return "NotAPrice";
		return "OK";
	}
	
	// End DataType: PRICE 
	// *************************************************************************
	

	
	// *************************************************************************
	// DataType:	 REAL
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20080707
	// Dependencies: 			
	// Notes:-
	// Real verifies that the value is a number using locale-dependent number
	// handling.	
	// History:		 20080707 added jsMask
	//				 20030605 added realCfSqltype
	//				 20020628 fixed RealIn
	// 				 20020403 added qForms validation	
	// Customisations:-
	//	
	
	qFormsValidate.Real = "objForm.#SUB#.validateNumeric()";
		
	Preset("Datatype_Real", "Format", "Numeric");
	Preset("Datatype_Real", "jsMask", "/[-+0-9\.]/");
	
	RangeValidationStyle.Real = "Numeric";

	function realCfSqltype() {
		return "cf_sql_real";
	}	
	
	function RealIn(value) {
		return localiseNumber(value);	
	}	

	function RealClean(value) {
		return internationaliseNumber(trim(value));
	}	
	
	function RealValidate(value) {
		if ( NOT LSIsNumeric(value) )
			return "NotANumber";
		return "OK";
	}
	
	// End DataType: REAL 
	// *************************************************************************



	// *************************************************************************
	// DataType:	 RGBTRIPLET 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Dependencies: string	
	// Notes:-
	// RGBTriplet accepts an RGB colour triplet. It optionally accepts only
	// "Web-safe" colours.
	// History:		20030708 added DataTypeAttributes
	//				20030605 added rgbtripletCfSqltype
	//				20030527 added jsMask
	//				20020816 Created
	// Customisations:-
	//
	
	Preset("Datatype_RGBTriplet", "Format", "ColorPicker");
	Preset("Datatype_RGBTriplet", "WebSafe", false);
	Preset("Datatype_RGBTriplet", "jsMask", "/[##0-9a-f]/i");
	DataTypeAttributes.RGBTriplet = "jsMask,WebSafe";
		
	rgbtripletCfSqltype = stringCfSqltype;
	RGBTripletClean = StringClean;	
		
	function RGBTripletOut(value) {
		if ( Len(value) and (Left(value, 1) neq "##") )
			return "##" & value; 
		return value;	
	}
	
	function RGBTripletValidate(value) {
		if ( NOT REFindNoCase("^##?[0-9a-f]{6}$", value, 1) )
			return "NotAColorTriplet";
		if ( thisField.WebSafe and ( NOT REFindNoCase("^##?(00|33|66|99|cc|ff){3}$", value, 1) ) )
			return "NotAWebsafeColor";	
		return "OK";	
	}
	
	// End DataType: RGBTRIPLET 
	// *************************************************************************
	

	
	// *************************************************************************
	// DataType:	 STRING
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030605
	// Dependencies: 			
	// Notes:-
	// String performs no validation. White space is trimmed.
	// History:		 20030605 added StringCfSqltype
	//				 20020913 added StringIn()
	//				 20020318 Created	
	// Customisations:-
	//	
	
	Preset("Datatype_String", "Format", "text");
	
	function StringCfSqltype() {
		return "cf_sql_varchar";
	}
	
	function StringClean(value) {
		return Trim(value);
	}
	StringIn = StringClean;

	// End DataType: STRING 
	// *************************************************************************


	
	// *************************************************************************
	// DataType:	 TIME
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20050412
	// Dependencies: Date			
	// Notes:-
	// Time attempts to parse a time value in a locale-dependent format. Since
	// it is locale-dependent, the error message is not very helpful. It is
	// recommended you declare an error in Section 2, outlining the correct
	// time format.
	// History:		 20050412 added createOdbcTime
	//				 20030605 added timeCfSqltype
	//				 20020914 rewrote timein() and timeclean()
	//				 20020620 Added format preset 
	//				 20020613 Fixed handling of min / max
	//				 20020318 Created	
	// Customisations:-
	//	
	
	Preset("Datatype_Time", "Format", "Timepicker");
	RangeValidationStyle.Time = "Time";	
	DataTypeAttributes.Time = "Date";

	function timeCfSqltype() {
		return "cf_sql_time";
	}
	
	function TimeIn(value) {
		return localiseTime(value);	
	}	

	function TimeClean(value) {
		value = internationaliseTime(value);
		if ( IsDate(value) ) {
			if ( StructKeyExists(thisField, "Date") AND Len(thisField.Date) )
				return CreateDateTime(Year(thisField.Date), Month(thisField.Date), Day(thisField.Date), Hour(value), Minute(value), Second(value));	
			return createOdbcTime(CreateTime(Hour(value), Minute(value), Second(value)));
		}		
		return value;
	}	
	
	function TimeValidate(value) {
		if ( NOT IsDate(value) )
			return "NotATime";
		else
			return "OK";
	}
	
	// End DataType: TIME 
	// *************************************************************************	



	// *************************************************************************
	// DataType:	 UUID 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Dependencies: 		
	// Notes:-
	// UUID accepts a universally unique identifier (UUID). A UUID is a 
	// 35-character string representation of a unique 128-bit integer. 
	// History:		20030708 added DataTypeAttributes
	//				20030605 added UUIDCfSqltype
	//				20030527 added jsMask
	//				20021108 Created
	// Customisations:-
	//
	
	Preset("Datatype_UUID", "Format", "text");
	Preset("Datatype_UUID", "minlength", 35);
	Preset("Datatype_UUID", "maxlength", 35);
	Preset("Datatype_UUID", "size", 40);
	Preset("Datatype_UUID", "jsMask", "/[-0-9a-f]/i");
	DataTypeAttributes.UUID = "jsMask";
	
	UUIDCfSqltype = StringCfSqltype;	
	UUIDClean = StringClean;	

	function UUIDValidate(value) {
		if ( NOT REFindNoCase("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{16}$", value, 1) )
			return "NotAUUID";
		return "OK";	
	}
	
	// End DataType: UUID 
	// *************************************************************************

</cfscript>








