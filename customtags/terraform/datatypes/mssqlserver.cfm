<!--- 
*****************************************************************
**  MICROSOFT SQL SERVER DATATYPE PACK  *************************
**             5 April 2004             *************************
*****************************************************************

This file includes definitions for SQL Server data types. You should cfinclude this file at the bottom of section 5 in your settings.cfm file. All these datatypes are prefixed with "sq", for example: "sqNText". You may prefer to remove these prefixes, but beware that if you do, some datatypes will clash with the standard TerraForm datatypes or datatypes from other datatype packs. 

When you create a TerraForm field, you can declare the datatype to match the field in your database table. For example, if you have a bit field in your table, you can specify datatype="sqbit"; if you have an nvarchar field in your table, you can specify datatype="sqNVarChar". Sometimes, you may be better to use a standard TerraForm datatype. For example, an email address would be better declared using datatype="email" rather than, say, datatype="sqVarChar". Generally though, you should find these datatypes reduce the time involved and errors made in assembling a form. Try them out and decide for yourself!

Note that many definitions here are depepndent on the standard TerraForm datatypes. If you have removed or altered these datatypes, some of the following datatypes may not function correctly. Dependencies are noted for each datatype.
--->

<cfscript>



	// *************************************************************************
	// DataType:	 sqBIGINT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: INTEGER Datatype
	// Notes:-
	// sqBigInt supports the SQL Server BigInt data type
	// History:		 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqBigInt", "Format", "Numeric");
	
	qFormsValidate.sqBigInt = qFormsValidate.Integer;
	RangeValidationStyle.sqBigInt = "Numeric";
	//sqBigIntIn = IntegerIn;
	sqBigIntClean = IntegerClean;
		
	function sqBigIntValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-9223372036854775808, 9223372036854775807);
		return result;
	}	

	// End DataType: sqBIGINT 
	// *************************************************************************



	// *************************************************************************
	// DataType:	 sqBIT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020318
	// Distribution: MICROSOFT SQL SERVER DataType Pack		
	// Dependencies: Boolean, String
	// Notes:-
	// sqBIT accepts any values that CF can evaluate as boolean ("TRUE", "NO"
	// etc.) and returns 1 or 0. "" will evaluate as 0.
	// History:		 20020318 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqBit", "Format", "Boolean");	
	sqBitClean = StringClean;
	sqBitValidate = BooleanValidate;

	function sqBitOut(value) {
		if ( YesNoFormat(value) )
			return 1;
		return 0;
	}
	
	// End DataType: sqBIT 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqCHAR 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020614
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: STRING Datatype
	// Notes:-
	// sqChar supports the SQL Server Char data type
	// History:		 20020614 Created
	// Customisations:-
	//

	Preset("Datatype_sqChar", "Format", "Text");
	sqCharClean = StringClean;
	
	function sqCharValidate(value) {
		ApplyDatatypeMaxLength(8000);	
		return "OK";
	}
	
	// End DataType: sqCHAR 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqDATETIME
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT SQL SERVER DataType Pack
	// Dependencies: DATE Datatype
	// Notes:-
	// sqDateTime supports the Microsoft SQL Server DateTime data type
	// History:		 20020614 Created
	// Customisations:-
	//

	RangeValidationStyle.sqDateTime = "Date";
	Preset("Datatype_sqDateTime", "Format", "Calendar");

	function sqDateTimeIn(value) {
		return "#localiseDate(value)# #localiseTime(value)#";	
	}	

	function sqDateTimeClean(value) {
		return internationaliseDate(value);
	}	
		
	function sqDateTimeValidate(value) {
		if ( NOT IsDate(value) )
			return "NotADate";
		ApplyDatatypeMinMax("{ts '1753-01-01 00:00:00'}", "{ts '9999-12-31 23:59:59'}");			
		return "OK";
	}
	
	// End DataType: sqDATETIME
	// *************************************************************************

	

	// *************************************************************************
	// DataType:	 sqDecimal 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20040405
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: REAL Datatype
	// Notes:-
	// sqDecimal supports the SQL Server Decimal data type
	// History:		 20040405 bugfix
	//				 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqDecimal", "Format", "Numeric");
	
	qFormsValidate.sqDecimal = qFormsValidate.Real;
	RangeValidationStyle.sqDecimal = "Numeric";
	sqDecimalIn = RealIn;
	sqDecimalClean = RealClean;

	function sqDecimalValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-1E+39+1, 1E+39-1);
		return result;
	}
	
	// End DataType: sqDecimal 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqFloat 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: REAL Datatype
	// Notes:-
	// sqFloat supports the SQL Server Float data type
	// History:		 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqFloat", "Format", "Numeric");
	
	qFormsValidate.sqFloat = qFormsValidate.Real;
	RangeValidationStyle.sqFloat = "Numeric";
	sqFloatIn = RealIn;
	sqFloatClean = RealClean;
	
	function sqFloatValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-1.79E+308, 1.79E+308);
		return result;
	}

	// End DataType: sqFloat 
	// *************************************************************************
	
	

	// *************************************************************************
	// DataType:	 sqInt 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: INTEGER Datatype
	// Notes:-
	// sqInt supports the SQL Server Int data type
	// History:		 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqInt", "Format", "Numeric");
	
	qFormsValidate.sqInt = qFormsValidate.Integer;
	RangeValidationStyle.sqInt = "Numeric";
	//sqIntIn = IntegerIn;
	sqIntClean = IntegerClean;
	
	function sqIntValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-2147483648, 2147483647);
		return result;
	}
	
	// End DataType: sqInt 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqMoney 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: REAL Datatype
	// Notes:-
	// sqMoney supports the SQL Server Money data type
	// History:		 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqMoney", "Format", "Numeric");
	
	qFormsValidate.sqMoney = qFormsValidate.Real;
	RangeValidationStyle.sqMoney = "Numeric";
	sqMoneyIn = RealIn;
	sqMoneyClean = RealClean;
		
	function sqMoneyValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-9223372036854775808, 9223372036854775807);
		return result;
	}
	
	// End DataType: sqMoney 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqNChar 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020614
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: STRING Datatype
	// Notes:-
	// sqNChar supports the SQL Server NChar data type
	// History:		 20020614 Created
	// Customisations:-
	//

	Preset("Datatype_sqNChar", "Format", "Text");
	sqNCharClean = StringClean;
	
	function sqNCharValidate(value) {
		ApplyDatatypeMaxLength(4000);	
		return "OK";
	}
	
	// End DataType: sqNChar 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqNText
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: SQL SERVER DataType Pack	
	// Dependencies: STRING Datatype
	// Notes:-
	// sqNText supports the SQL Server NText data type
	// History:		 20020607 Created
	// Customisations:-
	//

	Preset("Datatype_sqNText", "Format", "textarea");
	sqNText = StringClean;
	
	function sqNTextValidate(value) {
		ApplyDatatypeMaxLength(1073741823);
		return "OK";
	}
	
	// End DataType: sqNText 
	// *************************************************************************	
		
	
	
	// *************************************************************************
	// DataType:	 sqNVarChar 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020614
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: sqNChar Datatype
	// Notes:-
	// sqNVarChar supports the SQL Server NVarChar data type
	// History:		 20020614 Created
	// Customisations:-
	//

	Preset("Datatype_sqNVarChar", "Format", "Text");
	sqNVarCharClean = sqNCharClean;
	
	sqNVarCharValidate = sqNCharValidate;
	
	// End DataType: sqNVarChar 
	// *************************************************************************		
	
	
	
	// *************************************************************************
	// DataType:	 sqNumeric 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: sqDecimal Datatype
	// Notes:-
	// sqNumeric supports the SQL Server Numeric data type, a synonym for Decimal
	// History:		 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqNumeric", "Format", "Numeric");
	
	qFormsValidate.sqNumeric = qFormsValidate.sqDecimal;
	RangeValidationStyle.sqNumeric = "Numeric";
	sqNumericIn = sqDecimalIn;
	sqNumericClean = sqDecimalClean;
	sqNumericValidate = sqDecimalValidate;

	// End DataType: sqNumeric 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqReal 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: REAL Datatype
	// Notes:-
	// sqReal supports the SQL Server Real data type
	// History:		 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqReal", "Format", "Numeric");
	
	qFormsValidate.sqReal = qFormsValidate.Real;
	RangeValidationStyle.sqReal = "Numeric";
	sqRealIn = RealIn;
	sqRealClean = RealClean;	
		
	function sqRealValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-3.40E+38, 3.40E+38);
		return result;
	}
	
	// End DataType: sqReal 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqSmallDateTime
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT SQL SERVER DataType Pack
	// Dependencies: sqDATETIME, DATE Datatypes
	// Notes:-
	// sqSmallDateTime supports the Microsoft SQL Server SmallDateTime data type
	// History:		 20020614 Created
	// Customisations:-
	//

	RangeValidationStyle.sqSmallDateTime = "Date";
	Preset("Datatype_sqSmallDateTime", "Format", "Calendar");
	
	sqSmallDateTimeIn = sqDateTimeIn;
	sqSmallDateTimeClean = sqDateTimeClean;	
	
	function sqSmallDateTimeValidate(value) {
		if ( NOT IsDate(value) )
			return "NotADate";
		ApplyDatatypeMinMax("{ts '1900-01-01 00:00:00'}", "{ts '2079-06-06 23:59:00'}");
		return "OK";
	}

	// End DataType: sqSmallDateTime
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqSmallInt 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: MICROSOFT SQL SERVER DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// sqSmallInt supports the SQL Server SmallInt data type
	// History:		 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqSmallInt", "Format", "Numeric");
	
	qFormsValidate.sqSmallInt = qFormsValidate.Integer;
	RangeValidationStyle.sqSmallInt = "Numeric";
	//sqSmallIntIn = IntegerIn;
	sqSmallIntClean = IntegerClean;

	function sqSmallIntValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-32768, 32767);
		return result;
	}
	
	// End DataType: sqSmallInt 
	// *************************************************************************



	// *************************************************************************
	// DataType:	 sqSmallMoney 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: REAL Datatype
	// Notes:-
	// sqSmallMoney supports the SQL Server SmallMoney data type
	// History:		 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqSmallMoney", "Format", "Numeric");
	
	qFormsValidate.sqSmallMoney = qFormsValidate.Real;
	RangeValidationStyle.sqSmallMoney = "Numeric";
	sqSmallMoneyIn = RealIn;
	sqSmallMoneyClean = RealClean;
	
	function sqSmallMoneyValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-214748.3648, 214748.3647);
		return result;
	}
	
	// End DataType: sqSmallMoney 
	// *************************************************************************
	
	

	// *************************************************************************
	// DataType:	 sqText
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020614
	// Distribution: SQL SERVER DataType Pack	
	// Dependencies: STRING Datatype
	// Notes:-
	// sqText supports the SQL Server Text data type
	// History:		 20020614 Created
	// Customisations:-
	//

	Preset("Datatype_sqText", "Format", "textarea");
	sqTextClean = StringClean;
	
	function sqTextValidate(value) {
		ApplyDatatypeMaxLength(2147483647);
		return "OK";
	}
	
	// End DataType: sqText 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 sqTinyInt 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020607
	// Distribution: MICROSOFT SQL SERVER DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// sqTinyInt supports the SQL Server TinyInt data type
	// History:		 20020607 Created
	// Customisations:-
	//
	
	Preset("Datatype_sqTinyInt", "Format", "Numeric");
	Preset("Datatype_sqTinyInt", "maxlength", "3");
	
	qFormsValidate.sqTinyInt = qFormsValidate.Integer;
	RangeValidationStyle.sqTinyInt = "Numeric";
	//sqTinyIntIn = IntegerIn;
	sqTinyIntClean = IntegerClean;
	
	function sqTinyIntValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(0, 255);
		return result;
	}
	
	// End DataType: sqTinyInt 
	// *************************************************************************
	

	
	// *************************************************************************
	// DataType:	 sqVarChar 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020614
	// Distribution: MICROSOFT SQL SERVER DataType Pack	
	// Dependencies: sqCHAR Datatype
	// Notes:-
	// sqVarChar supports the SQL Server VarChar data type
	// History:		 20020614 Created
	// Customisations:-
	//

	Preset("Datatype_sqVarChar", "Format", "Text");
	sqVarCharClean = sqCharClean;
	
	sqVarCharValidate = sqCharValidate;
	
	// End DataType: sqVarChar 
	// *************************************************************************
	
	

</cfscript>	