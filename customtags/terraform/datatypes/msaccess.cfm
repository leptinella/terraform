<!--- 
*****************************************************************
**  MICROSOFT ACCESS DATATYPE PACK      *************************
**             5 April 2004             *************************
*****************************************************************

This file includes definitions for Access data types. You should cfinclude this file at the bottom of section 5 in your settings.cfm file. All these datatypes are prefixed with "ac", for example: "acByte". You may prefer to remove these prefixes, but beware that if you do, some datatypes will clash with the standard TerraForm datatypes or datatypes from other datatype packs. 

When you create a TerraForm field, you can declare the datatype to match the field in your database table. For example, if you have a memo field in your table, you can specify datatype="acmemo"; if you have a LongInteger field in your table, you can specify datatype="aclonginteger". Sometimes, you may be better to use a standard TerraForm datatype. For example, an email address would be better declared using datatype="email" rather than, say, datatype="actext". Generally though, you should find these datatypes reduce the time involved and errors made in assembling a form. Try them out and decide for yourself!

Note that many definitions here are depepndent on the standard TerraForm datatypes. If you have removed or altered these datatypes, some of the following datatypes may not function correctly. Dependencies are noted for each datatype.
--->

<cfscript>



	// *************************************************************************
	// DataType:	 acBYTE
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// acByte supports the Microsoft Access Number (Byte) data type
	// History:		 20020613 Created
	// Customisations:-
	//
	
	Preset("Datatype_acByte", "Format", "Numeric");
	Preset("Datatype_acByte", "maxlength", "3");
	
	qFormsValidate.acByte = qFormsValidate.Integer;
	RangeValidationStyle.acByte = RangeValidationStyle.Integer;
	//acByteIn = IntegerIn;
	acByteClean = IntegerClean;
	
	function acByteValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(0, 255);
		return result;
	}	

	// End DataType: acBYTE
	// *************************************************************************



	// *************************************************************************
	// DataType:	 acCURRENCY
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: REAL Datatype
	// Notes:-
	// acCurrency supports the Microsoft Access Currency data type
	// History:		 20020613 Created
	// Customisations:-
	//
	
	Preset("Datatype_acCurrency", "Format", "Numeric");
	
	qFormsValidate.acCurrency = qFormsValidate.Real;
	RangeValidationStyle.acCurrency = "Numeric";
	acCurrencyIn = RealIn;
	acCurrencyClean = RealClean;
	
	function acCurrencyValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-999999999999999.9999,999999999999999.9999);
		return result;
	}	

	// End DataType: acCURRENCY
	// *************************************************************************	



	// *************************************************************************
	// DataType:	 acDATETIME
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: DATE Datatype
	// Notes:-
	// acDateTime supports the Microsoft Access Date/Time data type
	// History:		 20020613 Created
	// Customisations:-
	//

	Preset("Datatype_acDateTime", "Format", "Calendar");
	RangeValidationStyle.acDateTime = "Date";

	function acDateTimeIn(value) {
		return "#localiseDate(value)# #localiseTime(value)#";	
	}	

	function acDateTimeClean(value) {
		return internationaliseDate(value);
	}	
		
	function acDateTimeValidate(value) {
		if ( NOT IsDate(value) )
			return "NotADate";
		return "OK";
	}
	
	// End DataType: acDATETIME
	// *************************************************************************
	
	

	// *************************************************************************
	// DataType:	 acDECIMAL 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20040405
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: REAL Datatype
	// Notes:-
	// acDecimal supports the Microsoft Access Number (Decimal) data type
	// History:		 20040405 bugfix
	//				 20020613 Created
	// Customisations:-
	//
	
	Preset("Datatype_acDecimal", "Format", "Numeric");
	
	qFormsValidate.acDecimal = qFormsValidate.Real;
	RangeValidationStyle.acDecimal = "Numeric";
	acDecimalIn = RealIn;
	acDecimalClean = RealClean;
	
	function acDecimalValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-1E+29-1,1E+29-1);
		return result;
	}
	
	// End DataType: acDECIMAL 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 acDOUBLE
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: REAL Datatype
	// Notes:-
	// acDouble supports the Microsoft Access Number (Double) data type
	// History:		 20020613 Created
	// Customisations:-
	//
	
	Preset("Datatype_acDouble", "Format", "Numeric");
	
	qFormsValidate.acDouble = qFormsValidate.Real;
	RangeValidationStyle.acDouble = "Numeric";
	acDoubleIn = RealIn;
	acDoubleClean = RealClean;
	
	function acDoubleValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-1.79769313486231E308,1.79769313486231E308);
		return result;
	}
	
	// End DataType: acDOUBLE
	// *************************************************************************



	// *************************************************************************
	// DataType:	 acINTEGER
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// acInteger supports the Microsoft Access Number (Integer) data type
	// History:		 20020613 Created
	// Customisations:-
	//
	
	Preset("Datatype_acInteger", "Format", "Numeric");
	Preset("Datatype_acInteger", "maxlength", "6");
	
	qFormsValidate.acInteger = qFormsValidate.Integer;
	RangeValidationStyle.acInteger = "Numeric";
	//acIntegerIn = IntegerIn;
	acIntegerClean = IntegerClean;
	
	function acIntegerValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-32768, 32767);
		return result;
	}
	
	
	// End DataType: acINTEGER
	// *************************************************************************



	// *************************************************************************
	// DataType:	 acLONGINTEGER
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// acLongInteger supports the Microsoft Access Number (LongInteger) data type
	// History:		 20020613 Created
	// Customisations:-
	//
	
	Preset("Datatype_acLongInteger", "Format", "Numeric");
	
	qFormsValidate.acLongInteger = qFormsValidate.Integer;
	RangeValidationStyle.acLongInteger = "Numeric";
	//acLongIntegerIn = IntegerIn;
	acLongIntegerClean = IntegerClean;
		
	function acLongIntegerValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-2147483648, 2147483647);
		return result;
	}

	
	// End DataType: acLONGINTEGER
	// *************************************************************************



	// *************************************************************************
	// DataType:	 acMEMO
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack	
	// Dependencies: STRING Datatype, TEXTAREA Format
	// Notes:-
	// acMEMO supports the Microsoft Access Memo data type
	// History:		 20020613 Created
	// Customisations:-
	//

	Preset("Datatype_acMemo", "Format", "TextArea");
	acMemoClean = StringClean;
	
	// End DataType: acMEMO
	// *************************************************************************



	// *************************************************************************
	// DataType:	 acNUMBER
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// acNumber supports the Microsoft Access Number data type
	// History:		 20020613 Created
	// Customisations:-
	//
	
	Preset("Datatype_acNumber", "Format", "Numeric");
	
	qFormsValidate.acNumber = qFormsValidate.Integer;
	RangeValidationStyle.acNumber = "Numeric";
	//acNumberIn = IntegerIn;
	acNumberClean = IntegerClean;
	acNumberValidate = IntegerValidate;
	
	// End DataType: acNUMBER
	// *************************************************************************



	// *************************************************************************
	// DataType:	 acSINGLE
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: REAL Datatype
	// Notes:-
	// acSingle supports the Microsoft Access Number (Single) data type
	// History:		 20020613 Created
	// Customisations:-
	//
	
	Preset("Datatype_acSingle", "Format", "Numeric");
	
	qFormsValidate.acSingle = qFormsValidate.Real;
	RangeValidationStyle.acSingle = "Numeric";
	acSingleIn = RealIn;
	acSingleClean = RealClean;
	function acSingleValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-3.402823E38,3.402823E38);
		return result;
	}	
	
	// End DataType: acSINGLE 
	// *************************************************************************	



	// *************************************************************************
	// DataType:	 acTEXT
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack	
	// Dependencies: STRING Datatype
	// Notes:-
	// acText supports the Microsoft Access Text data type
	// History:		 20020613 Created
	// Customisations:-
	//

	Preset("Datatype_acText", "Format", "Text");
	acTextClean = StringClean;
	
	function acTextValidate(value) {
		ApplyDatatypeMaxLength(255);	
		return "OK";
	}
	
	// End DataType: acTEXT
	// *************************************************************************
	

	
	// *************************************************************************
	// DataType:	 acYESNO
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020613
	// Distribution: MICROSOFT ACCESS DataType Pack
	// Dependencies: BOOLEAN Datatype, BOOLEAN Format
	// Notes:-
	// acYesNo supports the Microsoft Access Yes/No data type
	// History:		 20030305 Added yesSynonyms, noSynonyms,
	//				 20020613 Created
	// Customisations:-
	//
	
	Preset("Datatype_acYesNo", "Format", "Boolean");
    Preset("Datatype_acYesNo", "YesSynonyms", "");
    Preset("Datatype_acYesNo", "NoSynonyms", "");	
	acYesNoClean = BooleanClean;
	acYesNoValidate = BooleanValidate;
	acYesNoOut = BooleanOut;
	
	// End DataType: acYESNO
	// *************************************************************************
	
	
</cfscript>	