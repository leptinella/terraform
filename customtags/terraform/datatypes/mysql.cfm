<!--- 
*****************************************************************
**  mySQL DATATYPE PACK   ***************************************
**  Created: 8 July 2003  ***************************************
**  Updated: 6 April 2004 ***************************************
*****************************************************************

This file includes definitions for SQL Server data types. You should cfinclude this file at the bottom of section 5 in your settings.cfm file. All these datatypes are prefixed with "sq", for example: "sqNText". You may prefer to remove these prefixes, but beware that if you do, some datatypes will clash with the standard TerraForm datatypes or datatypes from other datatype packs. 

When you create a TerraForm field, you can declare the datatype to match the field in your database table. For example, if you have a bit field in your table, you can specify datatype="sqbit"; if you have an nvarchar field in your table, you can specify datatype="sqNVarChar". Sometimes, you may be better to use a standard TerraForm datatype. For example, an email address would be better declared using datatype="email" rather than, say, datatype="sqVarChar". Generally though, you should find these datatypes reduce the time involved and errors made in assembling a form. Try them out and decide for yourself!

Note that many definitions here are depepndent on the standard TerraForm datatypes. If you have removed or altered these datatypes, some of the following datatypes may not function correctly. Dependencies are noted for each datatype.
--->

<cfscript>



	// *************************************************************************
	// DataType:	 myBigInt
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// myBigInt supports the mySQL BigInt data type
	// History:		 20031015 removed myBigIntIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myBigInt", "Format", "Numeric");
	Preset("Datatype_myBigInt", "maxlength", 20);
	
	qFormsValidate.myBigInt = qFormsValidate.Integer;
	RangeValidationStyle.myBigInt = "Numeric";
	//myBigIntIn = IntegerIn;
	myBigIntClean = IntegerClean;
	
	function myBigIntValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-9223372036854775808, 9223372036854775807);
		return result;
	}
	
	// End DataType: myBigInt 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myBigInt_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// myBigInt_unsigned supports the mySQL BigInt data type with type unsigned
	// History:		 20031015 removed myBigInt_unsignedIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myBigInt_unsigned", "Format", "Numeric");
	Preset("Datatype_myBigInt_unsigned", "maxlength", 20);
	
	qFormsValidate.myBigInt_unsigned = qFormsValidate.Integer;
	RangeValidationStyle.myBigInt_unsigned = "Numeric";
	// myBigInt_unsignedIn = IntegerIn;
	myBigInt_unsignedClean = IntegerClean;
	
	function myBigInt_unsignedValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(0, 18446744073709551615);
		return result;
	}
	
	// End DataType: myBigInt_unsigned
	// *************************************************************************

	
	
	// *************************************************************************
	// DataType:	 myBIT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack
	// Dependencies: Boolean, String
	// Notes:-
	// myBIT accepts any values that CF can evaluate as boolean ("TRUE", "NO"
	// etc.) and returns 1 or 0. "" will evaluate as 0.
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myBIT", "Format", "Boolean");	
	myBitClean = StringClean;
	myBitValidate = BooleanValidate;

	function myBitOut(value) {
		if ( YesNoFormat(value) )
			return 1;
		return 0;
	}
	
	// End DataType: myBIT 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myBlob
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: STRING Datatype
	// Notes:-
	// myBlob supports the mySQL Blob data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myBlob", "Format", "TextArea");
	myBlobClean = StringClean;
	function myBlobValidate(value) {
		ApplyDatatypeMaxLength(65535);	
		return "OK";
	}
	
	// End DataType: myBlob
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myBOOL
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack
	// Dependencies: myBit
	// Notes:-
	// myBool is a synonym for myBit.
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myBool", "Format", "Boolean");	
	myBoolClean = myBitClean;
	myBoolValidate = myBitValidate;
	myBoolOut = myBitOut;
	
	// End DataType: myBOOL 
	// *************************************************************************

	
	
	// *************************************************************************
	// DataType:	 myChar 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: STRING Datatype
	// Notes:-
	// myChar supports the mySQL Char data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myChar", "Format", "Text");
	myCharClean = StringClean;
	
	function myCharValidate(value) {
		ApplyDatatypeMaxLength(255);	
		return "OK";
	}
	
	// End DataType: myChar 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myDate
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20040406
	// Distribution: mySQL DataType Pack
	// Dependencies: DATE Datatype
	// Notes:-
	// myDate supports the mySQL date data type
	// History:		 20040406 bugfix
	//				 20030708 Created
	// Customisations:-
	//

	RangeValidationStyle.myDate = "Date";
	Preset("Datatype_myDate", "Format", "Calendar");

	function myDateIn(value) {
		return localiseDate(value);	
	}	

	function myDateClean(value) {
		return internationaliseDate(value);
	}	
		
	function myDateValidate(value) {
		if ( NOT IsDate(value) )
			return "NotADate";
		ApplyDatatypeMinMax("{ts '1000-01-01 00:00:00'}", "{ts '9999-12-31 23:59:59'}");			
		return "OK";
	}
	
	// End DataType: myDate
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myDateTime
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack
	// Dependencies: DATE Datatype
	// Notes:-
	// myDateTime supports the mySQL datetime data type
	// History:		 20030708 Created
	// Customisations:-
	//

	RangeValidationStyle.myDateTime = "Date";
	Preset("Datatype_myDateTime", "Format", "Calendar");

	function myDateTimeIn(value) {
		return localiseDate(value);	
	}	

	function myDateTimeClean(value) {
		return internationaliseDate(value);
	}	
		
	function myDateTimeValidate(value) {
		if ( NOT IsDate(value) )
			return "NotADate";
		ApplyDatatypeMinMax("{ts '1000-01-01 00:00:00'}", "{ts '9999-12-31 23:59:59'}");			
		return "OK";
	}
	
	// End DataType: myDateTime
	// *************************************************************************	


	
	// *************************************************************************
	// DataType:	 myDouble
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: REAL Datatype
	// Notes:-
	// myDouble supports the mySQL Double data type
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myDouble", "Format", "Numeric");
	
	qFormsValidate.myDouble = qFormsValidate.Real;
	RangeValidationStyle.myDouble = "Numeric";
	myDoubleIn = RealIn;
	myDoubleClean = RealClean;
	
	function myDoubleValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-1.7976931348623157E+308, 1.7976931348623157E+308);
		return result;
	}

	// End DataType: myDouble
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myDouble_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: REAL Datatype
	// Notes:-
	// myDouble supports the mySQL Double data type with type unsigned
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myDouble_unsigned", "Format", "Numeric");
	
	qFormsValidate.myDouble_unsigned = qFormsValidate.Real;
	RangeValidationStyle.myDouble_unsigned = "Numeric";
	myDouble_unsignedIn = RealIn;
	myDouble_unsignedClean = RealClean;
	
	function myDouble_unsignedValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(0, 1.7976931348623157E+308);
		return result;
	}

	// End DataType: myDouble_unsigned 
	// *************************************************************************	

	
	
	// *************************************************************************
	// DataType:	 myDec
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble Datatype
	// Notes:-
	// myDec is a synonym for myDouble
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myDec", "Format", "Numeric");
	
	qFormsValidate.myDec = qFormsValidate.myDouble;
	RangeValidationStyle.myDec = "Numeric";
	myDecIn = myDoubleIn;
	myDecClean = myDoubleClean;
	myDecValidate = myDoubleValidate;

	// End DataType: myDec
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myDec_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble_unsigned Datatype
	// Notes:-
	// myDec_unsigned is a synonym for myDouble_unsigned
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myDec_unsigned", "Format", "Numeric");
	
	qFormsValidate.myDec_unsigned = qFormsValidate.myDouble_unsigned;
	RangeValidationStyle.myDec_unsigned = "Numeric";
	myDec_unsignedIn = myDouble_unsignedIn;
	myDec_unsignedClean = myDouble_unsignedClean;
	myDec_unsignedValidate = myDouble_unsignedValidate;

	// End DataType: myDec_unsigned 
	// *************************************************************************	
	
	
	
	// *************************************************************************
	// DataType:	 myDecimal
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble Datatype
	// Notes:-
	// myDecimal is a synonym for myDouble
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myDecimal", "Format", "Numeric");
	
	qFormsValidate.myDecimal = qFormsValidate.myDouble;
	RangeValidationStyle.myDecimal = "Numeric";
	myDecimalIn = myDoubleIn;
	myDecimalClean = myDoubleClean;
	myDecimalValidate = myDoubleValidate;

	// End DataType: myDecimal
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myDecimal_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble_unsigned Datatype
	// Notes:-
	// myDecimal_unsigned is a synonym for myDouble_unsigned
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myDecimal_unsigned", "Format", "Numeric");
	
	qFormsValidate.myDecimal_unsigned = qFormsValidate.myDouble_unsigned;
	RangeValidationStyle.myDecimal_unsigned = "Numeric";
	myDecimal_unsignedIn = myDouble_unsignedIn;
	myDecimal_unsignedClean = myDouble_unsignedClean;
	myDecimal_unsignedValidate = myDouble_unsignedValidate;

	// End DataType: myDecimal_unsigned 
	// *************************************************************************	
	

	
	// *************************************************************************
	// DataType:	 myDoublePrecision
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble Datatype
	// Notes:-
	// myDoublePrecision is a synonym for myDouble
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myDoublePrecision", "Format", "Numeric");
	
	qFormsValidate.myDoublePrecision = qFormsValidate.myDouble;
	RangeValidationStyle.myDoublePrecision = "Numeric";
	myDoublePrecisionIn = myDoubleIn;
	myDoublePrecisionClean = myDoubleClean;
	myDoublePrecisionValidate = myDoubleValidate;

	// End DataType: myDoublePrecision
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myDoublePrecision_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble_unsigned Datatype
	// Notes:-
	// myDoublePrecision_unsigned is a synonym for myDouble_unsigned
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myDoublePrecision_unsigned", "Format", "Numeric");
	
	qFormsValidate.myDoublePrecision_unsigned = qFormsValidate.myDouble_unsigned;
	RangeValidationStyle.myDoublePrecision_unsigned = "Numeric";
	myDoublePrecision_unsignedIn = myDouble_unsignedIn;
	myDoublePrecision_unsignedClean = myDouble_unsignedClean;
	myDoublePrecision_unsignedValidate = myDouble_unsignedValidate;

	// End DataType: myDoublePrecision_unsigned 
	// *************************************************************************	
	
	
	
	// *************************************************************************
	// DataType:	 myEnum
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: 
	// Notes:-
	// myEnum supports the mySQL Enum data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myEnum", "Format", "radio");
	
	// End DataType: myEnum
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myFloat 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: REAL Datatype
	// Notes:-
	// myFloat supports the mySQL Float data type
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myFloat", "Format", "Numeric");
	
	qFormsValidate.myFloat = qFormsValidate.Real;
	RangeValidationStyle.myFloat = "Numeric";
	myFloatIn = RealIn;
	myFloatClean = RealClean;
	
	function myFloatValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(-3.402823466E+38, 3.402823466E+38);
		return result;
	}

	// End DataType: myFloat 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myFloat_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: REAL Datatype
	// Notes:-
	// myFloat supports the mySQL Float data type with type unsigned
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myFloat_unsigned", "Format", "Numeric");
	
	qFormsValidate.myFloat_unsigned = qFormsValidate.Real;
	RangeValidationStyle.myFloat_unsigned = "Numeric";
	myFloat_unsignedIn = RealIn;
	myFloat_unsignedClean = RealClean;
	
	function myFloat_unsignedValidate(value) {
		var result = RealValidate(value);
		ApplyDatatypeMinMax(0, 3.402823466E+38);
		return result;
	}

	// End DataType: myFloat_unsigned 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myInt
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// myInt supports the mySQL Int data type
	// History:		 20031015 removed myIntIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myInt", "Format", "Numeric");
	Preset("Datatype_myInt", "maxlength", 11);
	
	qFormsValidate.myInt = qFormsValidate.Integer;
	RangeValidationStyle.myInt = "Numeric";
	// myIntIn = IntegerIn;
	myIntClean = IntegerClean;
	
	function myIntValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-2147483648, 2147483647);
		return result;
	}
	
	// End DataType: myInt 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myInt_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// myInt supports the mySQL Int data type with type unsigned
	// History:		 20031015 removed myInt_unsignedIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myInt_unsigned", "Format", "Numeric");
	Preset("Datatype_myInt_unsigned", "maxlength", 10);
	
	qFormsValidate.myInt_unsigned = qFormsValidate.Integer;
	RangeValidationStyle.myInt_unsigned = "Numeric";
	// myInt_unsignedIn = IntegerIn;
	myInt_unsignedClean = IntegerClean;
	
	function myInt_unsignedValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(0, 4294967295);
		return result;
	}
	
	// End DataType: myInt_unsigned 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myInteger
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: myInt Datatype
	// Notes:-
	// myInteger is a synonym for myInt
	// History:		 20031015 removed myIntegerIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myInteger", "Format", "Numeric");
	Preset("Datatype_myInteger", "maxlength", 11);
	
	qFormsValidate.myInteger = qFormsValidate.myInt;
	RangeValidationStyle.myInteger = "Numeric";
	// myIntegerIn = myIntIn;
	myIntegerClean = myIntClean;
	myIntegerValidate = myIntValidate;
	
	// End DataType: myInteger 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myInteger_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: myInt_unsigned Datatype
	// Notes:-
	// myInteger_unsigned is a synonym for myInt_unsigned
	// History:		 20031015 removed myInteger_unsignedIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myInteger_unsigned", "Format", "Numeric");
	Preset("Datatype_myInteger_unsigned", "maxlength", 10);
	
	qFormsValidate.myInteger_unsigned = qFormsValidate.myInt_unsigned;
	RangeValidationStyle.myInteger_unsigned = "Numeric";
	// myInteger_unsignedIn = myInt_unsignedIn;
	myInteger_unsignedClean = myInt_unsignedClean;
	myInteger_unsignedValidate = myInt_unsignedValidate;
	
	// End DataType: myInteger_unsigned
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myLongBlob
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: STRING Datatype
	// Notes:-
	// myLongBlob supports the mySQL LongBlob data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myLongBlob", "Format", "TextArea");
	myLongBlobClean = StringClean;
	function myLongBlobValidate(value) {
		ApplyDatatypeMaxLength(4294967295);	
		return "OK";
	}
	
	// End DataType: myLongBlob
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myLongText
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: myLongBlob Datatype
	// Notes:-
	// myLongText supports the mySQL LongText data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myLongText", "Format", "TextArea");
	myLongTextClean = myLongBlobClean;
	myLongTextValidate = myLongBlobValidate;
	
	// End DataType: myLongText
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myMediumBlob
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: STRING Datatype
	// Notes:-
	// myMediumBlob supports the mySQL MediumBlob data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myMediumBlob", "Format", "TextArea");
	myMediumBlobClean = StringClean;
	function myMediumBlobValidate(value) {
		ApplyDatatypeMaxLength(16777215);	
		return "OK";
	}
	
	// End DataType: myMediumBlob
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myMediumInt
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// myMediumInt supports the mySQL MediumInt data type
	// History:		 20031015 removed myMediumIntIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myMediumInt", "Format", "Numeric");
	Preset("Datatype_myMediumInt", "maxlength", 8);
	
	qFormsValidate.myMediumInt = qFormsValidate.Integer;
	RangeValidationStyle.myMediumInt = "Numeric";
	// myMediumIntIn = IntegerIn;
	myMediumIntClean = IntegerClean;
	
	function myMediumIntValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-8388608, 8388607);
		return result;
	}
	
	// End DataType: myMediumInt 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myMediumInt_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// myMediumInt_unsigned supports the mySQL MediumInt data type with type unsigned
	// History:		 20031015 removed myMediumInt_unsignedIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myMediumInt_unsigned", "Format", "Numeric");
	Preset("Datatype_myMediumInt_unsigned", "maxlength", 8);
	
	qFormsValidate.myMediumInt_unsigned = qFormsValidate.Integer;
	RangeValidationStyle.myMediumInt_unsigned = "Numeric";
	// myMediumInt_unsignedIn = IntegerIn;
	myMediumInt_unsignedClean = IntegerClean;
	
	function myMediumInt_unsignedValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(0, 16777215);
		return result;
	}
	
	// End DataType: myMediumInt_unsigned
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myMediumText
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: myMediumBlob Datatype
	// Notes:-
	// myMediumText supports the mySQL MediumText data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myMediumText", "Format", "TextArea");
	myMediumTextClean = myMediumBlobClean;
	myMediumTextValidate = myMediumBlobValidate;
	
	// End DataType: myMediumText
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myNChar 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: myChar Datatype
	// Notes:-
	// myNChar supports the mySQL NChar data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myNChar", "Format", "Text");
	myNCharClean = myCharClean;
	myNCharValidate = myCharValidate;
	
	// End DataType: myNChar 
	// *************************************************************************


	
	// *************************************************************************
	// DataType:	 myNumeric
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble Datatype
	// Notes:-
	// myNumeric is a synonym for myDouble
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myNumeric", "Format", "Numeric");
	
	qFormsValidate.myNumeric = qFormsValidate.myDouble;
	RangeValidationStyle.myNumeric = "Numeric";
	myNumericIn = myDoubleIn;
	myNumericClean = myDoubleClean;
	myNumericValidate = myDoubleValidate;

	// End DataType: myNumeric
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myNumeric_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble_unsigned Datatype
	// Notes:-
	// myNumeric_unsigned is a synonym for myDouble_unsigned
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myNumeric_unsigned", "Format", "Numeric");
	
	qFormsValidate.myNumeric_unsigned = qFormsValidate.myDouble_unsigned;
	RangeValidationStyle.myNumeric_unsigned = "Numeric";
	myNumeric_unsignedIn = myDouble_unsignedIn;
	myNumeric_unsignedClean = myDouble_unsignedClean;
	myNumeric_unsignedValidate = myDouble_unsignedValidate;

	// End DataType: myNumeric_unsigned 
	// *************************************************************************	
	
	
	
	// *************************************************************************
	// DataType:	 myNVarChar 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: myChar Datatype
	// Notes:-
	// myNVarChar supports the mySQL NVarChar data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myNVarChar", "Format", "Text");
	myNVarCharClean = myCharClean;
	myNVarCharValidate = myCharValidate;
	
	// End DataType: myNVarChar 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myReal
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble Datatype
	// Notes:-
	// myReal is a synonym for myDouble
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myReal", "Format", "Numeric");
	
	qFormsValidate.myReal = qFormsValidate.myDouble;
	RangeValidationStyle.myReal = "Numeric";
	myRealIn = myDoubleIn;
	myRealClean = myDoubleClean;
	myRealValidate = myDoubleValidate;

	// End DataType: myReal
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myReal_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack	
	// Dependencies: myDouble_unsigned Datatype
	// Notes:-
	// myReal_unsigned is a synonym for myDouble_unsigned
	// History:		 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myReal_unsigned", "Format", "Numeric");
	
	qFormsValidate.myReal_unsigned = qFormsValidate.myDouble_unsigned;
	RangeValidationStyle.myReal_unsigned = "Numeric";
	myReal_unsignedIn = myDouble_unsignedIn;
	myReal_unsignedClean = myDouble_unsignedClean;
	myReal_unsignedValidate = myDouble_unsignedValidate;

	// End DataType: myReal_unsigned 
	// *************************************************************************	
	
	
	
	// *************************************************************************
	// DataType:	 mySet
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies:
	// Notes:-
	// mySet supports the mySQL Set data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_mySet", "Format", "checkbox");
	
	// End DataType: mySet
	// *************************************************************************

	
	
	// *************************************************************************
	// DataType:	 mySmallInt
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// mySmallInt supports the mySQL SmallInt data type
	// History:		 20031015 removed mySmallIntIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_mySmallInt", "Format", "Numeric");
	Preset("Datatype_mySmallInt", "maxlength", 6);
	
	qFormsValidate.mySmallInt = qFormsValidate.Integer;
	RangeValidationStyle.mySmallInt = "Numeric";
	// mySmallIntIn = IntegerIn;
	mySmallIntClean = IntegerClean;
	
	function mySmallIntValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-32768, 32767);
		return result;
	}
	
	// End DataType: mySmallInt 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 mySmallInt_unsigned
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// mySmallInt_unsigned supports the mySQL SmallInt data type with type unsigned
	// History:		 20031015 removed mySmallInt_unsignedIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_mySmallInt_unsigned", "Format", "Numeric");
	Preset("Datatype_mySmallInt_unsigned", "maxlength", 5);
	
	qFormsValidate.mySmallInt_unsigned = qFormsValidate.Integer;
	RangeValidationStyle.mySmallInt_unsigned = "Numeric";
	// mySmallInt_unsignedIn = IntegerIn;
	mySmallInt_unsignedClean = IntegerClean;
	
	function mySmallInt_unsignedValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(0, 65535);
		return result;
	}
	
	// End DataType: mySmallInt_unsigned
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myText
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: myBlob Datatype
	// Notes:-
	// myText supports the mySQL Text data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myText", "Format", "TextArea");
	myTextClean = myBlobClean;
	myTextValidate = myBlobValidate;
	
	// End DataType: myText
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myTime
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack
	// Dependencies: TIME Datatype
	// Notes:-
	// myTime supports the mySQL Time data type
	// History:		 20030708 Created
	// Customisations:-
	//

	Preset("Datatype_myTime", "Format", "Timepicker");
	RangeValidationStyle.myTime = "Time";	
	DataTypeAttributes.myTime = "Date";

	myTimeIn = TimeIn;
	myTimeClean = TimeClean;
	myTimeValidate = timeValidate;
	
	// End DataType: myTime
	// *************************************************************************	
	
	
	
	// *************************************************************************
	// DataType:	 myTimeStamp
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030708
	// Distribution: mySQL DataType Pack
	// Dependencies: DATE Datatype
	// Notes:-
	// myTimeStamp supports the mySQL TimeStamp data type
	// History:		 20030708 Created
	// Customisations:-
	//

	RangeValidationStyle.myTimeStamp = "Date";
	Preset("Datatype_myTimeStamp", "Format", "Calendar");

	function myTimeStampIn(value) {
		return localiseDate(value);	
	}	

	function myTimeStampClean(value) {
		return internationaliseDate(value);
	}	
		
	function myTimeStampValidate(value) {
		if ( NOT IsDate(value) )
			return "NotADate";
		ApplyDatatypeMinMax("{ts '1970-01-01 00:00:00'}", "{ts '2037-12-31 23:59:59'}");			
		return "OK";
	}
	
	// End DataType: myTimeStamp
	// *************************************************************************	
	
	
	
	// *************************************************************************
	// DataType:	 myTinyBlob
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: myChar Datatype
	// Notes:-
	// myTinyBlob supports the mySQL TinyBlob data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myTinyBlob", "Format", "Text");
	myTinyBlobClean = myCharClean;
	myTinyBlobValidate = myCharValidate;
	
	// End DataType: myTinyBlob 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myTinyInt 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// myTinyInt supports the mySQL TinyInt data type
	// History:		 20031015 removed myTinyIntIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myTinyInt", "Format", "spinedit");
	Preset("Datatype_myTinyInt", "maxlength", 4);
	
	qFormsValidate.myTinyInt = qFormsValidate.Integer;
	RangeValidationStyle.myTinyInt = "Numeric";
	// myTinyIntIn = IntegerIn;
	myTinyIntClean = IntegerClean;
	
	function myTinyIntValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(-128, 127);
		return result;
	}
	
	// End DataType: myTinyInt 
	// *************************************************************************
	
	

	// *************************************************************************
	// DataType:	 myTinyInt_unsigned 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// myTinyInt_unsigned supports the mySQL TinyInt data type with type unsigned
	// History:		 20031015 removed myTinyInt_unsignedIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myTinyInt_unsigned", "Format", "spinedit");
	Preset("Datatype_myTinyInt_unsigned", "maxlength", 3);
	
	qFormsValidate.myTinyInt_unsigned = qFormsValidate.Integer;
	RangeValidationStyle.myTinyInt_unsigned = "Numeric";
	// myTinyInt_unsignedIn = IntegerIn;
	myTinyInt_unsignedClean = IntegerClean;
	
	function myTinyInt_unsignedValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(0, 256);
		return result;
	}
	
	// End DataType: myTinyInt_unsigned 
	// *************************************************************************

	
	
	// *************************************************************************
	// DataType:	 myTinyText
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: myChar Datatype
	// Notes:-
	// myTinyText supports the mySQL TinyText data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myTinyText", "Format", "Text");
	myTinyTextClean = myCharClean;
	myTinyTextValidate = myCharValidate;
	
	// End DataType: myTinyText
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myVarChar 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030711
	// Distribution: mySQL DataType Pack
	// Dependencies: myChar Datatype
	// Notes:-
	// myVarChar supports the mySQL VarChar data type
	// History:		 20030711 Created
	// Customisations:-
	//

	Preset("Datatype_myVarChar", "Format", "Text");
	myVarCharClean = myCharClean;
	myVarCharValidate = myCharValidate;
	
	// End DataType: myVarChar 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// DataType:	 myYear
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031015
	// Distribution: mySQL DataType Pack
	// Dependencies: INTEGER Datatype
	// Notes:-
	// myYear supports the mySQL Year data type
	// History:		 20031015 removed myYearIn
	//				 20030708 Created
	// Customisations:-
	//
	
	Preset("Datatype_myYear", "Format", "spinedit");
	Preset("Datatype_myYear", "maxlength", 4);
	
	qFormsValidate.myYear = qFormsValidate.Integer;
	RangeValidationStyle.myYear = "Numeric";
	// myYearIn = IntegerIn;
	myYearClean = IntegerClean;
	
	function myYearValidate(value) {
		var result = IntegerValidate(value);
		ApplyDatatypeMinMax(0, 2155);
		return result;
	}
	
	// End DataType: myYear 
	// *************************************************************************
	
	

</cfscript>	