<cffunction name="createUniqueFileName" returntype="string">

	<cfargument name="folder" type="string" required="yes">
	<cfargument name="file" type="string" required="yes">
	
	<cfset var extension = listlast(arguments.file, ".")>
	<cfset var filename = listdeleteat(arguments.file, listlen(arguments.file, "."), ".")>
	<cfset var files = "">
	<cfset var foundSuffix = 0>
	<cfset var lenFile = 0>
	<cfset var maxSuffix = 0>
	<cfset var offset = 0>
	
	<cfset filename = replace(filename, " ", "_", "all")>
	<cfset filename = reReplace(filename, "[^_[:alnum:]]+", "", "all")>
	<cfset offset = len(filename) + 1>
	<cfset lenFile = len("#filename#.#extension#")>
	
	<cfif fileexists("#arguments.folder##filename#.#extension#")>
	
		<!--- the requested file is not available. Find an alternative --->
		<cfdirectory action="LIST" directory="#arguments.Folder#" name="files" filter="#filename#*.#extension#">
		<cfloop query="files">
			<cfset foundSuffix = mid(name, offset, len(name) - lenFile)>
			<cfif isNumeric(foundSuffix)>
				<cfset maxSuffix = max(maxSuffix,foundSuffix)>
			</cfif>
		</cfloop>
		<cfreturn "#filename##maxSuffix + 1#.#extension#">
		
	<cfelse>
	
		<!--- the requested file is available. Simply return it --->
		<cfreturn "#filename#.#extension#">	
		
	</cfif>
	
</cffunction>








<cfscript>

	// *************************************************************************
	// Format/datatype:
	//				 ENHANCEDFILE 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20090209
	// Dependencies: 
	// Notes:-
	// ENHANCEDFILE builds a file upload field with numerous enhancements over
	// regular HTML file fields. The enhancedFile format must be used with the
	// enhancedFile datatype and vice versa.
	// History:		20090209 duplicate id attributes
	//				20090209 removed hspace, vspace and border for HTML strict support
	//				20090209 UUIDs were causing problems as ids starting with digits are not allowed. 
	//				20060412 increased security in browsers means the thumbnail was broken
	//				20060114 changed extension alert to use a placeholder
	//				20060114 form refresh was throwing an error -- added a call 
	//						 to the clean function if the form has been refreshed 
	//				20050616 catches erroneous CF error when destination 
	//						 folder non-existent
	//				20040220 createUniqueFilename() nows cleans up bad
	//						 characters in file names
	//				20031119 bugfix for MSIE/Mac -- display:inline not supported
	//				20031102 made XHTML tag closure dependent on XHTML attribute
	//				20031031 switched from attributesList to ignoreAttributes
	//				20030807 fixed issue with thumbnail of current image failing to show 
	//						 now checks for existence of temp folder.
	//						 added required flag placeholder
	//				20030704 added disabled support
	//				20030604 caught several issues with validation failing on 
	//				previously uploaded files due to cffile attributes not being
	//				available - thanks Florian Carstens for spotting this
	//				20030529 disabled thumbnail viewing for Opera - as it didn't work
	//				20030426 now uses the createUniqueFileName() UDF to resolve duplicate
	//						 file name issues elegantly
	//				20030225 created
	// Customisations:-
	//

	Preset("Datatype_EnhancedFile", "Format", "EnhancedFile");
	Preset("Format_EnhancedFile", "Datatype", "EnhancedFile");
	
	Preset("Format_EnhancedFile", "maxsize", 0); 				 // maximum size of file in bytes. 0 if no maximum		
	Preset("Format_EnhancedFile", "thumbnail", "yes"); 			 // display a thumbnail if file is an image
	Preset("Format_EnhancedFile", "extensionlist", "");			 // list of allowed file extensions, e.g. "jpg,gif,png"
	Preset("Format_EnhancedFile", "mimetypelist", "");			 // list of allowed MIME types. These can be type (e.g. "image") or type/subtype (e.g. "image/jpeg,image/pjpeg,image/gif"). See RFC 2046.
	Preset("Format_EnhancedFile", "folder", "");				 // location file is to be stored. If blank, it is up to the developer to delete the old file, and copy the new file here from the temp folder.
	Preset("Format_EnhancedFile", "webfolder", "");				 // web path to storage location, if thumbnails required.
	Preset("Format_EnhancedFile", "option1", "Remove file");	 // Caption for choice 1
	Preset("Format_EnhancedFile", "option2", "Revert to");		 // Caption for choice 2
	Preset("Format_EnhancedFile", "option3", "Keep current file:"); // Caption for choice 3
	Preset("Format_EnhancedFile", "option4", "Use this file:");	 // Caption for choice 4
	Preset("Format_EnhancedFile", "option5", "Upload a new file:"); // Caption for choice 5
	Preset("Format_EnhancedFile", "thumbnailHeight", 60);	 	 // Height of thumbnail image
	Preset("Format_EnhancedFile", "hourstokeeptempfile", 24);	 // Number of hours to keep temp files. If 0 it is up to the developer to clear abandoned temp files from the temp folder
	Preset("Format_EnhancedFile", "extensionmessage", "Note: that file type is not allowed. Allowed file extensions are: #ESC#EXTENSIONS.");										 // JavaScript popup alert used when the extension is not allowed

	function enhancedFileFormatDegrade() {
		return "enhancedFile";
	}
	
</cfscript>





<cffunction name="EnhancedFileFormat">
	
	<cfargument name="ThisField" required="true">
	<cfset var thumbnailId = "id" & createUUID()>
	<cfset var captionId = "id" & createUUID()>
	<cfset var HTMLBlock = "">
	<cfset var HTMLBlockAdd = "">
	<cfset var clientThumbnail = false>
	<cfset var serverThumbnail = false>
	<cfset var optionPassthrough = "">
	<cfset var tableStyle = "">
	
	<cfif thisField.datatype neq "enhancedFile">
		<cfabort showerror="TerraForm implementation error: enhancedFile format must be used with enhancedFile datatype only.">
	</cfif>
	
	<cfif state eq "refresh">
		<cfset EnhancedFileClean()>
	</cfif>
	
	<!--- <cfif (request.terraform.browser.engine neq "Gecko") and ((request.terraform.browser.engine neq "Internet Explorer") or (request.terraform.browser.platform neq "Windows"))>
		<cfset thisfield.thumbnail = false>
	</cfif> --->
	
	<cfscript>
		ignoreAttributes.id = "";
		ignoreAttributes.action = "";
		ignoreAttributes.clientfile = "";
		ignoreAttributes.clientfolder = "";
		ignoreAttributes.extensionlist = "";
		ignoreAttributes.extensionmessage = "";
		ignoreAttributes.folder = "";
		ignoreAttributes.hourstokeeptempfile = "";
		ignoreAttributes.maxsize = "";
		ignoreAttributes.mimetypelist = "";
		ignoreAttributes.NoProcessing = "";
		ignoreAttributes.option1 = "";
		ignoreAttributes.option2 = "";
		ignoreAttributes.option3 = "";
		ignoreAttributes.option4 = "";
		ignoreAttributes.option5 = "";
		ignoreAttributes.serverfile = "";
		ignoreAttributes.tempfile = "";
		ignoreAttributes.tempfolder = "";
		ignoreAttributes.thumbnail = "";
		ignoreAttributes.thumbnailheight = "";
		ignoreAttributes.webfolder = "";
	</cfscript>

	<cfif not structKeyExists(thisField, "id") or not len(thisField.id)>
		<cfset thisField.id = thisField.name>
	</cfif>
	<cfif not directoryexists(thisfield.tempfolder)>
		<cfabort showerror="TerraForm implementation error: enhancedFile temp folder: #thisfield.tempfolder# does not exist. Please create this empty folder or correct the path.">
	</cfif>
	
	<!--- clear out temp folder --->
	<cfif thisfield.hourstokeeptempfile>
		<cfdirectory action="LIST" directory="#thisfield.tempfolder#" name="tempdata" sort="datelastmodified">
		<cfloop query="tempdata">
			<cfif datediff("h", parsedatetime(datelastmodified), now()) gte thisfield.hourstokeeptempfile>
				<cfif not compareNoCase(type, "file")>
					<cffile action="DELETE" file="#thisfield.tempfolder##name#">
				</cfif>
			<cfelse>
				<cfbreak>	
			</cfif>
		</cfloop>
	</cfif>
 
 	<cfif structkeyexists(thisfield, "disabled")>
		<cfset optionPassthrough = " disabled=""disabled""">
	</cfif>
	
 	<cfif not structKeyExists(thisField, "onChange")>
		<cfset thisField.onChange = "">
	</cfif>
	
	<cfif len(thisfield.clientfile) or len(thisfield.serverfile)>
		<cfset thisField.onchange = "document.getElementById('#ThisField.Name#_action_new').click();" & thisField.onchange>
	</cfif>
	
	<cfif thisField.thumbnail>
		<!--- <cfset thisField.onchange = "var image = document.getElementById('#thumbnailId#'); if ((extension=='jpg')||(extension=='gif')||(extension=='png')||(extension=='bmp')) {image.src = 'file://' + this.value;image.style.visibility = 'visible';image.style.display='inline';} else {image.style.visibility = 'hidden';image.src = '';};" & thisField.onchange> --->	
		<cfif len(thisfield.serverfile) and listFindNoCase("bmp,gif,jpg,png", listLast(thisfield.serverfile, "."))>
			<cfset serverThumbnail = true>
		</cfif>
		<cfif len(thisfield.clientfile) and listFindNoCase("bmp,gif,jpg,png", listLast(thisfield.clientfile, "."))>
			<cfset clientThumbnail = true>
		</cfif>
		<cfsavecontent variable="HTMLBlockAdd"><cfoutput>
			<a href="##" onclick="var image = document.getElementById('#thumbnailId#'); if ( image.style.visibility != 'hidden' ) window.open(image.src, '_blank', 'left=50, top=50, width=300, height=300,resizable=1,scrollbars=1'); return false"><img src="#thisfield.webfolder##thisfield.serverfile#" style="border-width:1px;margin:3px;<cfif not serverThumbnail or (thisfield.action neq "revert")>display : none</cfif>" id="#thumbnailId#" alt="Preview" height="#thisfield.thumbnailHeight#"#closeTag#</a>
		</cfoutput></cfsavecontent>
		<cfset HTMLBlock = HTMLBlock & HTMLBlockAdd>
	</cfif>
	
	<cfsavecontent variable="HTMLBlockAdd"><cfoutput>
		<div>
			<cfif (len(thisfield.tempfile) or len(thisfield.serverfile)) and not thisfield.required>
				<label #optionPassthrough# for="#ThisField.Name#_action_clear"><input #optionPassthrough# type="radio" <cfif thisField.thumbnail>onclick="var image = document.getElementById('#thumbnailId#'); image.style.display = 'none';"</cfif> name="#ThisField.Name#_action" id="#ThisField.Name#_action_clear" value="clear" <cfif not compareNoCase(thisfield.action, "clear")>checked</cfif>#closeTag##thisfield.option1#</label><br>
			</cfif>
			<cfif len(thisfield.serverfile)>
				<label #optionPassthrough# for="#ThisField.Name#_action_revert"><input #optionPassthrough# <cfif thisField.thumbnail>onclick="var image = document.getElementById('#thumbnailId#');<cfif serverThumbnail>image.style.display='inline';<cfelse>image.style.display = 'none';</cfif>"</cfif> type="radio" name="#ThisField.Name#_action" id="#ThisField.Name#_action_revert" value="revert" <cfif (not compareNoCase(thisfield.action, "revert")) or ((not compareNoCase(thisfield.action, "uploaded")) and not len(thisfield.tempfile))>checked</cfif>#closeTag#<cfif len(thisfield.tempfile)>#thisfield.option2#<cfelse>#thisfield.option3#</cfif> <code>#thisfield.serverfile#</code></label><!---  <cfif len(thisfield.webfolder)><a href="#thisfield.webfolder##thisfield.serverfile#" target="_blank">(preview)</a></cfif> ---><br>
			</cfif>
			<cfif len(thisfield.clientfile)>
				<label #optionPassthrough# for="#ThisField.Name#_action_uploaded"><input #optionPassthrough# type="radio" <cfif thisField.thumbnail>onclick="var image = document.getElementById('#thumbnailId#');<!--- following doesn't work anymore due to increased browser security...  <cfif clientThumbnail>image.src='#jsStringFormat(thisfield.clientFolder & thisfield.clientfile)#';image.style.visibility = 'visible';image.style.display='inline'<cfelse>image.style.visibility = 'hidden';image.src = '';</cfif> --->image.style.display = 'none';"</cfif> name="#ThisField.Name#_action" id="#ThisField.Name#_action_uploaded" value="uploaded" checked#closeTag##thisfield.option4# <code>#thisfield.clientfile#</code></label><!---  <a href="#thisfield.clientfolder##thisfield.clientfile#" target="_blank">(preview)</a> ---><br>
			</cfif>
			<cfif len(thisfield.clientfile) or len(thisfield.serverfile)>
				<cfif not structKeyExists(ThisField, "onFocus")>
					<cfset thisField.onFocus = "">
				</cfif>
				<cfset thisField.onFocus = listAppend(thisField.onFocus, "document.getElementById('#ThisField.Name#_action_new').click()", ";")>
				<label #optionPassthrough# for="#ThisField.Name#_action_new"><input #optionPassthrough# type="radio" <cfif thisField.thumbnail>onclick="var image = document.getElementById('#thumbnailId#');<!--- following doesn't work anymore due to increased browser security... var extension = document.getElementById('#ThisField.Name#').value.toLowerCase().match('[/-~,!--]*$');if ((extension=='jpg')||(extension=='gif')||(extension=='png')||(extension=='bmp')) {image.src = document.getElementById('#ThisField.Name#').value;image.style.visibility = 'visible';image.style.display='inline'} else {image.style.visibility = 'hidden';image.src = '';} --->image.style.display = 'none';"</cfif> name="#ThisField.Name#_action" id="#ThisField.Name#_action_new" value="new" <cfif not compareNoCase(thisfield.action, "new")>checked</cfif>#closeTag##thisfield.option5#</label><br>
			</cfif>
			<input type="hidden" name="#ThisField.Name#_tempfile" value="#thisfield.tempfile#"#closeTag#<input type="hidden" name="#ThisField.Name#_serverfile" value="#thisField.serverfile#"#closeTag#<input type="hidden" name="#ThisField.Name#_clientfile" value="#thisField.clientfile#"#closeTag#<input type="hidden" name="#ThisField.Name#_clientFolder" value="#thisField.clientFolder#"#closeTag#<input #PASSTHROUGH# type="file" id="#ThisField.id#" name="#ThisField.Name#"#closeTag#
		</div>
	</cfoutput></cfsavecontent>
	<cfset HTMLBlock = HTMLBlock & HTMLBlockAdd>
 
	<cfif len(thisField.extensionlist)>
		<cfset thisField.onchange = "var re = new RegExp('(^|,)'+extension+'($|,)','i');if('#thisField.extensionlist#'.search(re)==-1) {alert('#jsStringFormat(replace(thisField.extensionmessage, "#ESC#EXTENSIONS", listChangeDelims(thisField.extensionlist, ", ")))#');} #thisField.onchange#">
	</cfif>
	
	<cfif thisField.thumbnail or len(thisField.extensionlist)>
		<cfset thisField.onchange = "var extension = (this.value.toLowerCase().match('[/-~,!--]*$'));" & thisField.onchange>
	</cfif>						
	
	<cfif not len(Attributes.EncType)>
		<cfset Attributes.EncType = "multipart/form-data">
	</cfif>
	
	<cfif not ( (not compareNoCase(request.terraform.browser.platform, "Mac")) and (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )>
		<cfset tableStyle = "display: inline;">	
	</cfif>
		
	<cfset HTMLBlock = "<table class=""terraFormLayoutElement"" style=""#tableStyle#"" border=""0"" cellspacing=""0"" cellpadding=""0""><tr><td>#HTMLBlock#</td><td>#ESC#*</td></tr></table>">
	
	<cfreturn HTMLBlock>
	
</cffunction>
		






<cffunction name="EnhancedFileIn">
	<cfargument name="value" required="true">

	<cfif thisField.datatype neq "enhancedFile">
		<cfabort showerror="TerraForm implementation error: ENHANCEDFILE datatype must be used with ENHANCEDFILE format only.">
	</cfif>
	<cfif not structKeyExists(thisfield, "tempfolder")>
		<cfabort showerror="TerraForm implementation error: TEMPFOLDER attribute is required for ENHANCEDFILE field.">
	</cfif>
	<cfset "form.#thisfield.name#_clientfile" = "">
	<cfset "form.#thisfield.name#_clientFolder" = "">
	<cfset "form.#thisfield.name#_serverfile" = value>
	<cfset "form.#thisfield.name#_tempfile" = "">
	<cfset thisfield.clientfile = "">
	<cfset thisfield.clientFolder = "">
	<cfset thisfield.serverfile = value>
	<cfset thisfield.tempfile = "">
	<cfif len(thisfield.serverfile)>
		<cfset thisfield.action = "revert">
	<cfelse>	
		<cfset thisfield.action = "upload">
	</cfif>
	<cfset "form.#thisfield.name#_action" = thisfield.action>
	<cfreturn value>
</cffunction>










<cffunction name="EnhancedFileClean">
	<cfparam name="form.#thisfield.name#_clientfile" default="">
	<cfparam name="form.#thisfield.name#_clientFolder" default="">
	<cfparam name="form.#thisfield.name#_serverfile" default="">
	<cfparam name="form.#thisfield.name#_tempfile" default="">
	<cfparam name="form.#thisfield.name#_action" default="">
	<cfparam name="form.#thisfield.name#" default="">
	<cfset thisfield.clientfile = form["#thisfield.name#_clientfile"]>
	<cfset thisfield.clientFolder = form["#thisfield.name#_clientFolder"]>
	<cfset thisfield.serverfile = form["#thisfield.name#_serverfile"]>
	<cfset thisfield.tempfile = form["#thisfield.name#_tempfile"]>
	<cfset thisfield.temppath = thisfield.tempfolder & listlast(thisfield.tempfile, "/\")>
	<!--- the listlast() is there to stop malicious users posting a path in the tempfile field --->
	<cfset thisfield.action = form["#thisfield.name#_action"]>
	
	<cfswitch expression="#thisfield.action#">
		
		<cfcase value="clear,revert">
			<cfif fileExists(thisfield.temppath)>
				<cffile action="DELETE" file="#thisfield.temppath#">
			</cfif>
			<cfset thisfield.tempfile = "">
			<cfset thisfield.clientfile = "">
			<cfset thisfield.clientFolder = "">
			<cfif thisfield.action eq "revert">
				<cfset thisfield.value = thisfield.serverfile>
			<cfelse>	
				<cfset thisfield.value = "">	
			</cfif>
		</cfcase>
		
		<cfcase value="uploaded">
			<cfset thisfield.value = thisfield.tempfile>	
			<cfset thisfield.extension = listlast(thisfield.clientfile, ".")>
		</cfcase>
		
		<cfdefaultcase>
			<cfif structKeyExists(form, thisfield.name) and len(form[thisfield.name])>
				<cftry>
					<cffile action="upload" filefield="#thisfield.name#" destination="#thisfield.tempfolder#" nameconflict="makeunique">
					<cfset thisfield.clientfile = cffile.clientfile>
					<cfset thisfield.clientFolder = cffile.clientdirectory & "\">
					<cfset thisfield.tempfile = cffile.serverfile>
					<cfset thisfield.action = "uploaded">
					<cfset thisField.filesize = cffile.filesize>
					<cfset thisField.extension = cffile.clientfileext>
					<cfset thisField.mimetype = cffile.contenttype>
					<cfset thisField.mimesubtype = cffile.contentsubtype>				
					<cfcatch>
						<cfset thisfield.uploaderror = true>
					</cfcatch>
				</cftry>
			</cfif>	
		</cfdefaultcase>
			
	</cfswitch>

	<cfreturn thisfield.value>
</cffunction>		


		 
		 
		 

<cffunction name="EnhancedFileValidate">

	<cfargument name="value" required="true">
	<cfset var thefile = "">
	<cfset var pathToDelete = "">
	<cfset var error = "OK">
	<cfset var thismimetype = "">
	<cfset foundmimetype = false>

	<cfif structkeyexists(thisfield, "uploaderror")>
		<cfset error = "ENHANCEDFILE_Upload">
	</cfif>
	
	<cfif len(thisfield.tempfile) and len(thisField.clientFile)>
	
		<!--- validate the file extension --->
		<cfif len(thisField.extensionlist) and not listFindNoCase(thisField.extensionlist, thisfield.extension)>
			<cfset error = "ENHANCEDFILE_ExtensionNotAllowed">
		</cfif>	
		
		<cfif error eq "OK">
			<!--- validate the MIME type - don't run if the MIME type is not defined, i.e. if the image was uploaded earlier --->
			<cfif structkeyexists(thisfield, "mimetype") and len(thisField.mimetypelist)>
				<cfloop index="thisMimetype" list="#thisField.mimetypelist#">
					<cfif listLen(thismimetype, "/") gt 1>
						<!--- type/subtype format --->
						<cfif thismimetype eq "#thisfield.mimetype#/#thisfield.mimesubtype#">
							<cfset foundmimetype = true>
							<cfbreak>
						</cfif>		
					<cfelse>	
						<!--- type format --->
						<cfif thisfield.mimetype eq thismimetype>
							<cfset foundmimetype = true>
							<cfbreak>
						</cfif>
					</cfif>
				</cfloop>
				<cfif not foundmimetype>
					<cfset error = "ENHANCEDFILE_MimeTypeNotAllowed">
				</cfif>
			</cfif>		
		</cfif>
		
		<cfif error eq "OK">
			<!--- validate the size - don't run if the size is not defined, i.e. if the image was uploaded earlier --->
			<cfif structkeyexists(thisfield, "filesize") and thisField.maxsize>
				<cfif thisField.filesize gt thisField.maxsize>	
					<cfset error = "ENHANCEDFILE_FileTooLarge">
				</cfif>
			</cfif>
		</cfif>
		
		<cfif error neq "OK">
			<!--- delete the temp file --->
			<cfset pathToDelete = thisfield.tempfolder & listlast(thisfield.tempfile, "/\")>
			<!--- the listlast() is there to stop malicious users posting a path in the tempfile field --->
			<cfif fileExists(pathToDelete)>
				<cffile action="delete" file="#pathToDelete#">
			</cfif>		
			<cfset thisField.tempfile = "">
			<cfset thisField.clientFile = "">
			<cfset thisField.clientFolder = "">	
		</cfif>
		
	</cfif>
	
	<!--- update action based on result of validation --->
	<cfif thisfield.action eq "uploaded" and not len(thisfield.tempfile)>
		<cfif len(thisfield.serverfile)>
			<cfset thisfield.action = "revert">
		<cfelse>
			<cfset thisfield.action = "new">		
		</cfif>
	</cfif>
	
	<cfswitch expression="#thisfield.action#">
		<cfcase value="uploaded">
			<cfset thisfield.value = thisfield.tempfile>
		</cfcase>
		<cfcase value="new">
			<cfset thisfield.value = "">
		</cfcase>
		<cfcase value="revert">
			<cfset thisfield.value = thisfield.serverfile>
		</cfcase>		
	</cfswitch> 	
	
	<cfset form["#thisfield.name#_clientfile"] = thisfield.clientfile>
	<cfset form["#thisfield.name#_clientFolder"] = thisfield.clientFolder>
	<cfset form["#thisfield.name#_serverfile"] = thisfield.serverfile>
	<cfset form["#thisfield.name#_tempfile"] = thisfield.tempfile>
	<cfset form["#thisfield.name#_action"] = thisfield.action>		
	<cfset form[thisfield.name] = thisfield.value>		
	<cfreturn error>
</cffunction>





<cffunction name="EnhancedFileOut">
	<cfset var newfilename = "">
	<cfset var extension = "">
	<cfset var pathToDelete = "">
	<cfparam name="thisfield.action" default="">
	<cfparam name="thisfield.serverfile" default="">

	<cfswitch expression="#thisfield.action#">
		<cfcase value="uploaded">
			<cfset thisfield.value = thisfield.tempfile>
		</cfcase>
		<cfcase value="new">
			<cfset thisfield.value = "">
		</cfcase>
		<cfcase value="revert">
			<cfset thisfield.value = thisfield.serverfile>
		</cfcase>		
	</cfswitch> 	
	
	<cfset form["#thisfield.name#_clientfile"] = thisfield.clientfile>
	<cfset form["#thisfield.name#_clientFolder"] = thisfield.clientFolder>
	<cfset form["#thisfield.name#_serverfile"] = thisfield.serverfile>
	<cfset form["#thisfield.name#_tempfile"] = thisfield.tempfile>
	<cfset form["#thisfield.name#_action"] = thisfield.action>		
	<cfset form[thisfield.name] = thisfield.value>
		
	<cfif listFindNoCase("clear,uploaded", thisfield.action)>
		<cfif len(thisfield.folder)>
			<!--- delete the old file --->
			<cfset pathToDelete = thisfield.Folder & listlast(thisfield.serverfile, "/\")> <!--- the listlast() is there to stop malicious users posting a path in the tempfile field --->
			<cfif fileExists(pathToDelete)>
				<cffile action="delete" file="#pathToDelete#">
			</cfif>		
			
			<cfif thisfield.action eq "uploaded">
				<cflock timeout="5" throwontimeout="Yes" type="EXCLUSIVE" scope="SERVER">			
					<!--- create a unique name for the new file if necessary --->
					<cfset newfilename = createUniqueFileName(thisfield.Folder, thisfield.clientfile)>
					<!--- <cfset newfilename = listDeleteAt(thisfield.clientfile, listLen(thisfield.clientfile, "."), ".")>
					<cfset extension = listLast(thisfield.clientfile, ".")>
					<cfif fileExists("#thisfield.Folder##thisfield.clientfile#")>
						<cfset newfilename = createUUID()>
					</cfif>
					<cfset newfilename = newfilename & "." & extension> --->
					<!--- move in the new file --->
					<cftry>
						<cffile action="MOVE" source="#thisfield.tempFolder##thisfield.tempfile#" destination="#thisfield.Folder##newfilename#">
						<cfcatch type="Application">
							<cfoutput><p><strong>Note that some versions of ColdFusion incorrectly report an error 'The value of the attribute source, which is currently "XXX", is invalid.' when in fact the problem is that the destination directory does not exist. Please check that the destination directory you have specified (#thisfield.Folder#) does exist. Error data as reported by ColdFusion is dumped below.</strong></p></cfoutput><cfdump var="#cfcatch#"><cfabort>
						</cfcatch>
					</cftry>
				</cflock>
				<cfreturn newfilename>
			<cfelse>
				<cfreturn "">	
			</cfif>		
		</cfif>
		<cfreturn thisfield.tempfile>
	<cfelse>
		<cfreturn thisfield.serverfile>
	</cfif>	
</cffunction>






<cfscript>
	// End Format / datatype: ENHANCEDFILE 
	// *************************************************************************
</cfscript>





<cfscript>
	// *************************************************************************
	// Format/datatype:
	//				RECAPTCHA 
	// Author:		Matthew Walker (esw@eswsoftware.com)
	// Version:		20080717
	// Dependencies: 
	// Notes:-		See http://recaptcha.net/apidocs/captcha/client.html for attributes
	// History:		20080717  Created

	Preset("Format_reCAPTCHA", "Datatype", "reCAPTCHA");
	Preset("Format_reCAPTCHA", "required", true);
	Preset("Datatype_reCAPTCHA", "Format", "reCAPTCHA");
	Preset("Datatype_reCAPTCHA", "theme", "light");

	function reCAPTCHAFormatDegrade(){
		return "reCAPTCHA";
	}
</cfscript>



<cffunction name="reCAPTCHAFormat">
	<cfargument name="thisField" required="true">
	<cfset var htmlBlock = "">
	<cfset var address = "http://api.recaptcha.net">
	
	<cfif thisField.datatype neq "reCAPTCHA">
		<cfabort showerror="TerraForm implementation error: reCAPTCHA format must be used with reCAPTCHA datatype only.">
	</cfif>
	<cfif not structKeyExists(thisField, "publicKey") or not structKeyExists(thisField, "privateKey")>
		<cfabort showerror="TerraForm implementation error: reCAPTCHA requires publicKey and privateKey attributes. Sign up here: http://recaptcha.net/api/getkey?domain=#cgi.SERVER_NAME#&app=TerraForm .">
	</cfif>
	
	<cfsavecontent variable="htmlBlock">
		<cfoutput>
			<script src="https://www.google.com/recaptcha/api.js?hl=#ListFirst(request.terraform.locale.javaLocaleID, '_')#"></script>
			<div class="g-recaptcha" data-sitekey="#thisField.publicKey#" data-theme="#thisField.theme#"<cfif structKeyExists(thisField, "tabIndex")> data-tabindex="#thisField.tabIndex#"</cfif>></div>
		</cfoutput>	
	</cfsavecontent>
	
	<cfreturn htmlBlock>
</cffunction>



<cffunction name="reCAPTCHAclean">
	<cfargument name="value" required="true">
	<cfif structKeyExists(form, "g-recaptcha-response")>
		<cfreturn form["g-recaptcha-response"]>
	</cfif>
	<cfreturn value>
</cffunction>



<cffunction name="reCAPTCHAValidate">
	<cfargument name="value" required="true">
	<cfset var response = "">

	<cftry>
		<cfhttp url="https://www.google.com/recaptcha/api/siteverify" method="post" timeout="5" throwonerror="true">
			<cfhttpparam type="formfield" name="secret" value="#thisField.privateKey#">
			<cfhttpparam type="formfield" name="remoteip" value="#cgi.REMOTE_ADDR#">
			<cfhttpparam type="formfield" name="response" value="#form["g-recaptcha-response"]#">
		</cfhttp>
		<cfcatch>
			<!--- if the service cannot be contacted, then the verification is passed. --->
			<cfreturn "ok">
		</cfcatch>
	</cftry>

	<cfset response = deserializeJson(cfhttp.fileContent)>
	<cfset form.recaptcha = response.success>
	<cfset structDelete(form, "g-recaptcha-response")>
	<cfif response.success>
		<cfreturn "ok">
	</cfif>
	
	<cfreturn "CAPTCHAIncorrect">	
</cffunction>



<cfscript>

	// End Format / datatype: reCAPTCHA 
	// *************************************************************************

</cfscript>

