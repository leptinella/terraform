<cfscript>





	// *************************************************************************
	// Format:		 ACTIVEDIT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030806
	// Dependencies: 
	// Notes:-
	// ACTIVEDIT creates an ActivEdit WYSIWYG editor field. Place ActivEdit.cfm in
	// [customtags]\terraform\formats\includes\activedit\ .
	// Place the ActivEdit inc folder in [ResourcePath]/formats/activedit/
	// where [ResourcePath] is the ResourcePath attribute supplied in your cf_terraform tag or 
	// set up in your settings file. Regular ActivEdit attributes apply although the inc 
	// attribute is not used. See cfdev.com for details.
	// History:		 20030806 added required flag placeholder
	//				 20030724 added activeditformatdegrade()
	//				 20030611 added cfsetting tags
	//				 20030331 changed path to activedit.cfm
	//				 20030128 changed this to thisField
	//				 20020914 changed this.value to this.rawvalue
	//				 20020813 shifted inc files
	// 				 20020319 Created
	// Customisations:-
	//
	
	function activEditFormatDegrade() {
		if ( 
			( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
			or ( request.terraform.browser.engine eq "Gecko" )
		)
			return "activedit";
		return textareaFormatDegrade();
	}
	
	// End Format: ACTIVEDIT
	// *************************************************************************
	
	

	// *************************************************************************
	// Format:		 BOOLEAN 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20060313
	// Dependencies: 
	// Notes:-
	// BOOLEAN builds a checkbox labelled using the Caption attribute. It
	// coordinates well with the BOOLEAN datatype.
	// History:		20060313 switched to caption_label
	//				20040526 now uses supplied id
	//				20031102 made XHTML tag closure dependent on XHTML attribute
	//						 added value to checked attribute
	//				20031031 switched from attributesList to ignoreAttributes
	//				20030704 added disabled support
	//				20030605 added Mac label workaound
	//				20030511 added booleanFormatDegrade()
	//				20030302 bugfix for CFMX
	//				20020607 added labelPassthrough
	//				20020318 Created
	// Customisations:-
	//
	
	Preset("Format_Boolean", "Datatype", "Boolean");
	Preset("Format_Boolean", "labelPassthrough", "");

	function booleanFormatDegrade() {
		return "boolean";
	}
			
	function BooleanFormat(ThisField) {
		var HTMLBlock = "#PASSTHROUGH# type=""checkbox"" name=""#ThisField.Name#"" value=""yes""";
		
		if ( structKeyExists(thisfield, "disabled") )
			thisField.labelPassthrough = thisField.labelPassthrough & " disabled=""disabled""";
		
		ignoreAttributes.labelPassthrough = "";
		
		if ( IsBoolean(ThisField.Value) AND ThisField.Value )
			HTMLBlock = HTMLBlock & " checked=""checked""";
		if ( (not compareNoCase(request.terraform.browser.platform, "Mac")) and (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
			HTMLBlock = "<label id=""#ThisField.id#_label"" #thisField.labelPassthrough#><input #HTMLBlock##closeTag##ThisField.caption_label#</label>";
		else
			HTMLBlock = "<label id=""#ThisField.id#_label"" for=""#ThisField.id#"" #thisField.labelPassthrough#><input #HTMLBlock##closeTag##ThisField.caption_label#</label>";
		return HTMLBlock;
	}
	
	// End Format: BOOLEAN 
	// *************************************************************************
	


	// *************************************************************************
	// Format:		 BUTTON 
	// Author:  	 Matthew Walker ( esw@eswsoftware.com )
	// Version:		 20060313
	// Dependencies: Submit
	// Notes:-
	//
	// History:  	 20060313 switched to caption_label
	//				 20030730 added disabled support
	//               20030511 added buttonFormatDegrade()
	//				 20020901 Added graceful degrade in browsers that don't
	//						  support button
	// 				 20020701 Created
	// Customisations:-
	//
 
	Preset("Format_Button", "Datatype", "Default");

	function buttonFormatDegrade() {
		if ( 
			( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
			or ( request.terraform.browser.engine eq "Opera" )
			or ( request.terraform.browser.engine eq "Gecko" )
		)
			return "button";
		ThisField.caption_label = stripHtml(ThisField.caption_label);
		return submitFormatDegrade();
	}
	 
	function ButtonFormat(ThisField) {
		return "<button #PASSTHROUGH# type=""button"" name=""#ThisField.Name#"">#ThisField.caption_label#</button>";
	}
 
	// End Format: BUTTON 
	// ************************************************************************* 



// *************************************************************************
	// Format:		 CALENDAR
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20090522
	// Dependencies: 
	// Notes:-		 CALENDAR generates a popup calendar based on the 
	//				 current locale setting. This format is supported
	//			 	 via the external file: calendar.cfm
	//				 along with some images and Javascript files.
	//				 See readme.txt.
	// History:		 20090522 added inline attribute to disable positioning of popup calendar
	//				 20040730 added disabled support
	//				 20040528 now uses supplied id
	//				 20031125 Updated to Matt Kruse's 19 Aug 2003
	//						  release. Now selects default date.
	//				 20031119 added Safari support
	//				 20031102 made XHTML tag closure dependent on XHTML attribute
	//				 20030909 added offsetx and offsety attributes
	//				 20030806 added required flag placeholder
	//				 20030704 added disabled support
	//				 20030625 replaced poupcalendarnumber with calendarid - a uuid based id
	//						  to resolve bug with two forms on one page
	//				 20030527 tweaked for Netscape and Opera
	//				 20030521 added styles to tweak alignment in Netscape and Opera
	//				 20030519 implemented updated code from mattkruse.com. dates outside
	//						  min and max now disabled. Added disabledDates attribute.
	//				 20030511 added calendarFormatDegrade()
	//				 20030129 added buttonPassthrough
	//				 20030128 changed this to thisField
	//				 20030123 Added InputFieldFormat()
	//				 20021205 CSE HTML validator compliance tweaks
	//				 20021130 added support for the undocumented locales: Chinese (China), 
	// 				 Chinese (Hong Kong), Chinese (Taiwan)
	//				 20021020 degrades gracefully in Netscape 3
	//				 20020914 changed this.value to this.rawvalue
	//				 20020627 Added support for Japanese and Korean, the new CFMX locales
	//				 20020415 Created
	// Customisations:-
	//

	Preset("Format_Calendar", "Datatype", "Date");
	Preset("Format_Calendar", "inline", false);
	Preset("Format_Calendar", "offsetx", -152);
	Preset("Format_Calendar", "offsety", 25);
	Preset("Format_Calendar", "Size", 10);
	Preset("Format_Calendar", "ButtonCaption", "");
	Preset("Format_Calendar", "ButtonPassthrough", "style=""font: 10px Verdana, Geneva, Arial, Helvetica, sans-serif;""");
	Preset("Format_Calendar", "PopupWindow", "FALSE");
	Preset("Format_Calendar", "ShowYearNavigation", "FALSE");
	Preset("Format_Calendar", "DisabledWeekdays", "");
	
	function calendarFormatDegrade() {
		if ( 
			( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
			or ( request.terraform.browser.engine eq "Opera" )
			or ( request.terraform.browser.engine eq "Gecko" )
			or ( request.terraform.browser.engine eq "Konqueror" )
		)
			return "calendar";
		if ( not structKeyExists(thisField, "datatype") )
			thisField.datatype = "date";	
		return textFormatDegrade();
	}
	
	// End Format: CALENDAR
	// *************************************************************************
	


	// *************************************************************************
	// Format:		 CHECKBOX 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20091127
	// Dependencies: 
	// Notes:-
	// CHECKBOX builds a set of checkboxes or radio buttons from the ValueList
	// and DisplayList attributes (which may be generated internally from a
	// Query attribute).  Set the Multiple attribute to control whether radio 
	// buttons or checkboxes are generated. The checkboxes are laid out in a
	// column format. The number of columns is adjusted using the Cols
	// attribute (default is 2). If desired, you can use multiple 
	// cf_terrafield references using the same fieldname.
	//
	// Note that the checkboxes are now wrapped in two tables by default. The 
	// inner provides the grid for the checkboxes, while the outer provides 
	// positioning for the required field flag. If you alter gridstart, be 
	// sure to open two tables as demonstrated in the comment below.
	
	// History:		20091127 added padding cells
	//				20091006 handle empty items better
	//				20060313 switched to caption_label
	//				20040812 added focusTo support
	//				20040803 added disabled support
	//				20040526 now uses variations of supplied id
	//						 guaranteeuniqueid is now meaningless and deprecated
	//				20031126 legend now highlights if error
	//				20031119 bugfix for MSIE/Mac -- display:inline not supported
	//				20031102 made XHTML tag closure dependent on XHTML attribute
	//					     added value to checked attribute
	//				20031031 switched from attributesList to ignoreAttributes
	//						 removed style="display : inline" -- failed in MSIE/Mac
	//				20030806 added required flag placeholder
	//				20030704 added disabled support
	//				20030601 now specify cols=0 to have just one row
	//						 added gridStart/gridEnd
	//						 - note these override tablePassthrough
	//						 added cellStart/cellEnd 
	//						 - note these override tdPassthrough
	//						 added rowStart/rowEnd 	
	//				20030511 added checkboxFormatDegrade()
	//				20030329 no longer produces extra <tr> - thanks Nathan Mische!
	//				20030321 replaced list with array for performance gain
	//				20030129 added tablePassthrough and tdPassthrough
	//				20020903 Added group attribute
	//				20020620 added default datatype
	//				20020607 bugfix for CFMX
	//				20020418 Added Preset multiple -> true
	//				Added Cols to AttributesList 
	// Customisations:-
	//
	
	Preset("Format_Checkbox", "Datatype", "Default");
	Preset("Format_Checkbox", "Group", "no");
	Preset("Format_Checkbox", "Multiple", "Yes");
	Preset("Format_Checkbox", "tablePassthrough", "class=""terraFormLayoutElement""");
	Preset("Format_Checkbox", "tdPassthrough", "");
	Preset("Format_Checkbox", "gridEnd", "</table></td><td>#ESC#*</td></tr></table>");
	Preset("Format_Checkbox", "rowStart", "<tr>");
	Preset("Format_Checkbox", "rowEnd", "</tr>");
	Preset("Format_Checkbox", "cellEnd", "");
	
	function checkboxFormatDegrade() {
		return "checkbox";
	}
	
	function CheckboxFormat(ThisField) {
		var HTMLBlock = "";
		var Checked = "";
		var theFormat = "";
		var i = 0;
		var j = 0;
		var ThisFieldElement = "";
		var arrValue = "";
		var arrDisplay = "";		
		var labelpassthrough = "";
		var fieldSetStyle = "";
		var script = "";
		var paddingCells = 0;
		var paddingCellMarkup = "";
			
		ignoreAttributes.Cols = "";
		ignoreAttributes.group = "";
		ignoreAttributes.tablePassthrough = "";
		ignoreAttributes.id = "";
		ignoreAttributes.tdPassthrough = "";
		ignoreAttributes.gridStart = "";
		ignoreAttributes.gridEnd = "";
		ignoreAttributes.rowStart = "";
		ignoreAttributes.rowEnd = "";
		ignoreAttributes.cellStart = "";
		ignoreAttributes.cellEnd = "";

		thisField.focusTo = "#thisField.id#_value_0";
		
		if ( structkeyexists(thisfield, "disabled") )
			labelPassthrough = " disabled=""disabled""";
		if ( NOT IsDefined("variables.ThisField.Cols") )
			ThisField.Cols = 2;
		if ( IsDefined("variables.ThisField.ValueList") AND Len(ThisField.ValueList) ) {
			arrValue = listtoarray(ThisField.ValueList, ThisField.Delimiters, true);
			arrDisplay = listtoarray(ThisField.DisplayList, ThisField.Delimiters, true);
			
			HTMLBlock = HTMLBlock & "<span id=""#ThisField.id#"" title=""#arrayLen(arrValue)#""></span>";

			if ( IsDefined("variables.ThisField.Multiple") AND ThisField.Multiple )
				theFormat = "checkbox";
			else {
				theFormat = "radio";
				ThisField.Multiple = FALSE;
			}
			for ( i = 1; i LTE arraylen(arrValue); i = i + 1 ) {
			
				j = i - 1;
			
				if ( ThisField.Multiple )
					if ( ListFindNoCase(ThisField.Value, arrValue[i]) )
						Checked = "checked=""checked""";
					else 
						Checked = "";	
				else
					if ( ThisField.Value EQ arrValue[i] )
						Checked = "checked=""checked""";
					else 
						Checked = "";	
				
				if ( (not compareNoCase(request.terraform.browser.platform, "Mac")) and (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
					ThisFieldElement = "<label id=""#ThisField.id#_label_#j#"" #labelPassthrough#><input #PASSTHROUGH# type=""#theFormat#"" name=""#ThisField.Name#"" id=""#ThisField.id#_value_#j#"" value=""#arrValue[i]#"" #Checked##closeTag##arrDisplay[i]#</label>";
				else
					ThisFieldElement = "<label #labelPassthrough# id=""#ThisField.id#_label_#j#"" for=""#ThisField.id#_value_#j#""><input #PASSTHROUGH# type=""#theFormat#"" name=""#ThisField.Name#"" id=""#ThisField.id#_value_#j#"" value=""#arrValue[i]#"" #Checked##closeTag##arrDisplay[i]#</label>";

				if ( structkeyexists(thisfield, "cellstart") )
					HTMLBlock = HTMLBlock & thisfield.cellstart & ThisFieldElement & thisfield.cellend;
				else
					HTMLBlock = HTMLBlock & "<td #ThisField.tdPassthrough#>#ThisFieldElement#</td>";
				if ( thisField.cols AND NOT (i MOD ThisField.Cols) AND (i NEQ arraylen(arrValue)) ) 
					HTMLBlock = HTMLBlock & "</tr><tr>";
			}
			
			if ( thisField.cols gt 1 ) {
				if ( structkeyexists(thisfield, "cellstart") )
					paddingCellMarkup = thisfield.cellstart & "&nbsp;" & thisfield.cellend;
				else
					paddingCellMarkup = "<td #ThisField.tdPassthrough#>&nbsp;</td>";
				paddingCells = arraylen(arrValue) mod thisField.cols;
				HTMLBlock = HTMLBlock & repeatString(paddingCellMarkup, paddingCells);
			}
		
		}
		
		HTMLBlock = thisfield.rowstart & HTMLBlock & thisfield.rowend;
		if ( structkeyexists(thisfield, "gridstart") )
			HTMLBlock = ThisField.gridstart & HTMLBlock & thisfield.gridend;
		else
			HTMLBlock = "<table border=""0"" cellspacing=""0"" cellpadding=""0""><tr valign=""top""><td><table #ThisField.tablePassthrough#>#HTMLBlock#</table></td><td>#ESC#*</td></tr></table>";
		if ( thisField.group ) {
			if ( not ( (not compareNoCase(request.terraform.browser.platform, "Mac")) and (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) ) )
				fieldSetStyle = "display: inline;";
			HTMLBlock = "<fieldset id=""#ThisField.id#_fieldset"" style=""#fieldSetStyle#""><legend labelfor=""#thisField.name#"">#ThisField.caption_label#</legend>#HTMLBlock#</fieldset>";
		}
		return HTMLBlock;
	}
	
	// End Format: CHECKBOX 
	// *************************************************************************



	// *************************************************************************
	// Format:    COLORPICKER
	// Author:    Matthew Walker ( esw@eswsoftware.com )
	// Version:   20090209
	// Dependencies: text
	// Notes:-   
	// COLORPICKER implements the color picker written by Brent V. Ertz
	// ( brent80301@yahoo.com ). This color picker allows for multiple
	// customisable swatches similar in appearance to those found in Macromedia
	// software.
	// History:   20090209 removed align, hspace, vspace and border for HTML strict support
	//		      20060502 added version suffix to script path
	//			  20040526 now uses supplied id
	//			  20031202 In adding XHTML support broke Netscape 4 support
	//			  20031119 added Safari support
	//			  20031102 made XHTML tag closure dependent on XHTML attribute
	//					   replace & in URL with &amp;
	//			  20031031 switched from attributesList to ignoreAttributes
	//			  20030704 added disabled support
	//			  20030511 added colorpickerFormatDegrade()
	//			  20030123 Added InputFieldFormat()
	//			  20021215 added buttoncaption
	// 			  20020815 Created
	// Customisations:-
	//
 
	Preset("Format_ColorPicker", "datatype", "rgbtriplet");
	Preset("Format_ColorPicker", "colorgrid", "continuous");
	Preset("Format_ColorPicker", "disabledgrids", "");
	Preset("Format_ColorPicker", "size", "7");
 	Preset("Format_ColorPicker", "buttoncaption", "Color Picker");
	
	function colorPickerFormatDegrade() {
		if (
			( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
			or ( request.terraform.browser.engine eq "Opera" )
			or ( request.terraform.browser.engine eq "Gecko" )
			or ( request.terraform.browser.engine eq "Konqueror" )
			or (( request.terraform.browser.engine eq "netscape" ) and ( request.terraform.browser.intEngineVersion gte 5 ))
		)
			return "colorpicker";
		return textFormatDegrade();
	}
	
	function ColorPickerFormat(ThisField) {
		var HTMLBlock = "
			<input type=""text"" name=""#ThisField.Name#"" value=""#InputFieldFormat(ThisField.RawValue)#"" #PASSTHROUGH##closeTag#
			<a id=""#ThisField.id#_anchor"" href=""javascript:void(0);"" onclick=""if ( !this.disabled ) initColorPicker(event,'#Attributes.ResourcePath#formats/colorpicker/colorpicker.cfm?id=#thisfield.id#&amp;inc=#Attributes.ResourcePath#formats/colorpicker/&amp;colorgrid=#ThisField.ColorGrid#&amp;disabledgrids=#ThisField.DisabledGrids#&amp;title=#urlencodedformat(thisfield.buttoncaption)#');""><img style=""vertical-align:middle"" src=""#Attributes.ResourcePath#formats/colorpicker/colorselect.gif"" width=""18"" height=""18"" alt=""#ThisField.ButtonCaption#"" border=""0""#closeTag#</a>
		";
  		if ( NOT structkeyexists(request.terraform.flags, "ColorPicker") ) {
			request.terraform.flags.ColorPicker = true;
			request.terraform.HTMLHead = request.terraform.HTMLHead & "<script src=""#Attributes.ResourcePath#formats/colorpicker/colorpicker.js?#request.eswsoftware.terraform#"" type=""text/javascript""></script>";
		} 

		ignoreAttributes.buttoncaption = "";
		ignoreAttributes.disabledgrids = "";
		ignoreAttributes.colorgrid = "";
		ignoreAttributes.websafe = "";
		
		return HTMLBlock;
	} 

	// End Format: COLORPICKER
	// *************************************************************************
	


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// *************************************************************************
	// Format:		 COMBO 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20091006
	// Dependencies: 
	// Notes:-
	// A Microsoft-style combo box that behaves like a select box and a text field.
	// Choose an existing value or enter a new value directly.
	// 
	// History:		 20091006 handle empty items better
	//				 20090303 removed negative tabindex
	//				 20060119 was not handling case where first item selected properly
	//				 20040812 Created
	// Customisations:-
	//
	
	Preset("Format_Combo", "Datatype", "Default");
	Preset("Format_Combo", "width", 100);
	Preset("Format_Combo", "enforceValueList", false);
	
	function comboFormatDegrade() {
		return "combo";
	}
	
	function comboFormat(ThisField) {
		var HTMLBlock = "";
		var pos = 0;
		var arrValue = "";
		var arrDisplay = "";
		var arrGroup = "";
		var ThisFieldHTMLTag = "";
		var foundValue = false;
		var matchedSelectDisplay = ThisField.Value;
		
		ignoreAttributes.width = "";
		
		ThisField.enforceValueList = false;
		
		if ( not structKeyExists(thisField, "style") )
			thisField.style = "";
		
		thisField.style = listAppend(thisField.style, "z-index : 99; position : absolute; width : #int(ThisField.width+10)#px; clip : rect(0 #ThisField.width#px auto 0px)", ";");

		if ( Len(ThisField.ValueList) ) {
		
			arrValue = listtoarray(ThisField.ValueList, ThisField.Delimiters, true);
			arrDisplay = listtoarray(ThisField.DisplayList, ThisField.Delimiters, true);
			arrGroup = listtoarray(ThisField.groupList, ThisField.Delimiters, true);
			
			for ( pos = 1; pos LTE arraylen(arrValue); Pos = pos + 1 ) {
				ThisFieldHTMLTag = " value=""#HTMLEditFormat(arrValue[pos])#""";
				if ( ThisField.Value eq arrValue[pos] ) {
					foundValue = true;
					matchedSelectDisplay = arrDisplay[pos];
					if ( attributes.xhtml )
						ThisFieldHTMLTag = "#ThisFieldHTMLTag# selected=""selected""";
					else
						ThisFieldHTMLTag = "#ThisFieldHTMLTag# selected";
				}
				ThisFieldHTMLTag = "<option #ThisFieldHTMLTag#>#HTMLEditFormat(arrDisplay[pos])#</option>";
				
				// handle optgroup functionality
				if ( len(ThisField.groupList) ) {
					if ( (pos eq 1) or (arrGroup[pos] neq arrGroup[pos - 1]) ) 
						ThisFieldHTMLTag = "<optgroup label=""#HTMLEditFormat(arrGroup[pos])#"">" & ThisFieldHTMLTag;
					if ( (pos eq arraylen(arrValue)) or (arrGroup[pos] neq arrGroup[pos + 1]) )
						ThisFieldHTMLTag = ThisFieldHTMLTag & "</optgroup>";
				}
								
				HTMLBlock = HTMLBlock & ThisFieldHTMLTag;
			}
			if ( not foundValue )
				HTMLBlock = "<option value=""#HTMLEditFormat(ThisField.Value)#"">#HTMLEditFormat(ThisField.Value)#</option>" & HTMLBlock;
		}	
		else
			HTMLBlock = "";
		HTMLBlock = "<div style=""height : 22px; position : relative""><input #Passthrough# value=""#matchedSelectDisplay#"" type=""text"" name=""#ThisField.Name#"" onkeyup=""comboLookup('#thisField.id#')""#closeTag#<select onselect=""alert()"" onchange=""document.getElementById('#thisField.id#').value=this.options[this.selectedIndex].innerHTML; document.getElementById('#thisField.id#').select()"" id=""#ThisField.id#_select""  style=""position: absolute; width : #int(ThisField.width+20)#px; clip : rect(0 #int(ThisField.width+20)#px auto #int(ThisField.width)#px)"" tabindex=""0"">#HTMLBlock#</select></div>";
		if ( structKeyExists(thisfield, "qformsrelatedselect") and isBoolean(thisfield.qformsrelatedselect) and thisfield.qformsrelatedselect )
			qFormsScript = listAppend(qFormsScript, "objForm.#thisField.name#.setValue(""#ThisField.Value#"")", ";");
		return HTMLBlock;
	}

	function comboFormatClean() {
		var thescope = evaluate(scope);
		var pos = 0;
		var value = "";
		if ( structKeyExists(thescope, thisfield.name) ) {
			value = theScope[thisField.name];
			pos = listFindNoCase(ThisField.displayList, theScope[thisField.name], thisField.delimiters);
			if ( pos )
				theScope[thisfield.name & "_select"] = listGetAt(thisField.valueList, pos, thisField.delimiters);
			else
				 theScope[thisfield.name & "_select"] = "";
		}	
		return value;
	}

	// End Format: COMBO 
	// *************************************************************************
	
	
	


	
	// *************************************************************************
    // Format:        DATEPICKER 
    // Author:        Matthew Walker (esw@eswsoftware.com)
    // Version:       20100515
    // Dependencies: 
    // Notes: 		  the calendar displays in the wrong position if wrapped in any position css.
    // History:		  20100515 added showYear, showMonth, showDay
	// 				  20090523 added calBlock so that calendar can be added inline rather than in footer when inline is true
	//				  20090522 added inline attribute 
	//				  20090303 moved calendar div into form foot  to alleviate calendar position issue.
	//				  20090209 added PopupWindow to ignore list
	//				  20090209 removed align, hspace, vspace and border for HTML strict support
	//				  20060810 added calendar button and a range of attributes    
	//				  20060602 added masks to ignoreAttributes
	//				  20060502 added version suffix to script path
	//				  20060424 datePickerFormatClean() was screwed up
	//				  20060201 wasn�t setting correct month length when first loaded
	//         		  20050506 added dayMask, monthMask, yearMask (warning:
    //                  		some possible masks will generate misleading results.
    //                        	Useful options: 
    //                            dayMask - d,dd
    //                            monthMask - m,mm,mmm,mmmm
    //                            yearMask - yy,yyyy                            
    //                20040812 Added focusTo support
    //                20040611 added column prompt values
    //                20040526 now uses supplied id
    //                20040323 Created    
    // Customisations:-
    //

	Preset("Format_DatePicker", "Datatype", "Date");
	Preset("Format_DatePicker", "min", now());
 	Preset("Format_DatePicker", "max", createDate(year(now())+9,12,31));
 	Preset("Format_DatePicker", "dayPrompt", "D");
	Preset("Format_DatePicker", "monthPrompt", "M");
	Preset("Format_DatePicker", "yearPrompt", "Y");
	Preset("Format_DatePicker", "showDay", true);
	Preset("Format_DatePicker", "showMonth", true);
	Preset("Format_DatePicker", "showYear", true);
	Preset("Format_DatePicker", "dayMask", "d");
	Preset("Format_DatePicker", "monthMask", "mmmm");
	Preset("Format_DatePicker", "yearMask", "yyyy");
	Preset("Format_DatePicker", "calendar", false);
	Preset("Format_DatePicker", "showYearNavigation", false);
	Preset("Format_DatePicker", "popupWindow", false);
	Preset("Format_DatePicker", "months", "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec");
	Preset("Format_DatePicker", "days", "S,M,T,W,T,F,S");
	Preset("Format_DatePicker", "today", "Today");
	Preset("Format_DatePicker", "firstDayOfWeek", 1);
	Preset("Format_DatePicker", "offsetX", -152);
	Preset("Format_DatePicker", "offsetY", 25);
	Preset("Format_DatePicker", "yearCharacter", "");
	Preset("Format_DatePicker", "disabledWeekdays", "");
	Preset("Format_DatePicker", "disabledDates", arrayNew(1));
	Preset("Format_DatePicker", "inline", false);
		
	function datePickerFormatDegrade() {
		return "datePicker";
	}
	
	function datePickerFormat(ThisField) {
	
		var htmlBlock = "";
		var dayBlock = "";
		var monthBlock = "";
		var yearBlock = "";
		var i = 0;
		var startYear = year(thisField.min);
		var endYear = year(thisField.max);
		var internationalisedValue = internationaliseDate(thisField.value);
		var valueDay = "";
		var valueMonth = "";
		var valueYear = "";
		var thescope = evaluate(scope);
		var monthLength = 31;
		var calendarId=replace(thisField.id, ".", "_", "all");
		var jsDisabledWeekdays = "";
		var disabledDatesBlock = "";
		var divIdentifier = "";
		var onChange = "";
		var inline = false;
		var calBlock = "";
		var f = "";
		
		ignoreAttributes.id = "";
		ignoreAttributes.dayPrompt = "";
		ignoreAttributes.monthPrompt = "";
		ignoreAttributes.yearPrompt = "";
		ignoreAttributes.dayMask = "";
		ignoreAttributes.monthMask = "";
		ignoreAttributes.yearMask = "";
		ignoreAttributes.calendar = "";
		ignoreAttributes.PopupWindow = "";
		ignoreAttributes.showYearNavigation = "";
		ignoreAttributes.months = "";
		ignoreAttributes.days = "";
		ignoreAttributes.today = "";
		ignoreAttributes.firstDayOfWeek = "";
		ignoreAttributes.offsetX = "";
		ignoreAttributes.offsetY = "";
		ignoreAttributes.yearCharacter = "";
		ignoreAttributes.disabledWeekdays = "";
		ignoreAttributes.disabledDates = "";
		ignoreAttributes.inline = "";

		if ( structKeyExists(thisField, "onChange") )
			onChange = thisField.onChange;
		if ( structKeyExists(thescope, "#thisfield.name#_day") )
			valueDay = thescope["#thisfield.name#_day"];
		if ( structKeyExists(thescope, "#thisfield.name#_month") )
			valueMonth = thescope["#thisfield.name#_month"];
		if ( structKeyExists(thescope, "#thisfield.name#_year") )
			valueYear = thescope["#thisfield.name#_year"];
		
		if ( isDate(internationalisedValue) ) {
			valueDay = day(internationalisedValue);
			valueMonth = month(internationalisedValue);
			valueYear = year(internationalisedValue);
			monthLength = daysInMonth(internationalisedValue);
		}
		
		HTMLBlock = HTMLBlock & "<span id=""#ThisField.id#""></span>";
		
		//if ( not isDate(thisField.value) )
		//	thisField.value = thisField.min;
			
		//defaultDay = day(thisField.value);
		//defaultMonth = month(thisField.value);
		//defaultYear = year(thisField.value);
		
		if ( thisField.showYear and len(thisField.yearPrompt) )
			yearBlock = "<option value="""">#thisField.yearPrompt#</option>";

		if ( thisField.showDay ) {
			if ( len(thisField.dayPrompt) )
				dayBlock = "<option value="""">#thisField.dayPrompt#</option>";
			for ( i = 1; i lte monthLength; i = i + 1 ) {
				dayBlock = dayBlock & "<option value=""#i#""";
				if ( i eq valueDay )
					if ( attributes.xhtml )
						dayBlock = dayBlock & " selected=""selected""";
					else
						dayBlock = dayBlock & " selected";
				dayBlock = dayBlock & ">#lsDateFormat(createDate(2000, 1, i), thisField.dayMask)#</option>";
			}
			dayBlock = "<select name=""#thisfield.name#_day"" id=""#ThisField.id#_day"" #PASSTHROUGH#>" & dayBlock & "</select>";
		}
		
		if ( thisField.showMonth ) {
			if ( len(thisField.monthPrompt) )
				monthBlock = "<option value="""">#thisField.monthPrompt#</option>";
			for ( i = 1; i lte 12; i = i + 1 ) {
				monthBlock = monthBlock & "<option value=""#i#""";
				if ( i eq valueMonth )
					if ( attributes.xhtml )
						monthBlock = monthBlock & " selected=""selected""";
					else
						monthBlock = monthBlock & " selected";
				monthBlock = monthBlock & ">#lsDateFormat(createDate(2000, i, 1), thisField.monthMask)#</option>";
			}
			monthBlock = "<select name=""#thisfield.name#_month"" id=""#ThisField.id#_month""  #PASSTHROUGH# onchange=""datePickerUpdateDays('#thisField.id#_year', '#thisField.id#_month', '#thisField.id#_day')"">" & monthBlock & "</select>";
		}	

		if ( thisField.showYear ) {
			if ( len(thisField.yearPrompt) )
				yearBlock = "<option value="""">#thisField.yearPrompt#</option>";
			for ( i = startYear; i lte endYear; i = i + 1 ) {
				yearBlock = yearBlock & "<option value=""#i#""";
				if ( i eq valueYear )
					if ( attributes.xhtml )
						yearBlock = yearBlock & " selected=""selected""";
					else
						yearBlock = yearBlock & " selected";
				yearBlock = yearBlock & ">#lsDateFormat(createDate(i, 1, 1), thisField.yearMask)#</option>";
			}
			yearBlock = "<select name=""#thisfield.name#_year"" id=""#ThisField.id#_year"" #PASSTHROUGH#  onchange=""datePickerUpdateDays('#thisField.id#_year', '#thisField.id#_month', '#thisField.id#_day')"">" & yearBlock & "</select>";
		}	
		
		for ( i = 1; i lte 3; i++ ) {
			f = listGetAt(request.terraform.locale.dateOrder, i);
			if ( 
				( f eq 'd' and thisField.showDay )
				or ( f eq 'm' and thisField.showMonth )
				or ( f eq 'y' and thisField.showYear )
			)
				break;
		}	
		switch ( f ) {
			case "d": {
				thisField.focusTo = "#thisField.id#_day";
				break;
			}
			case "m": {
				thisField.focusTo = "#thisField.id#_month";
				break;
			}
			case "y": {
				thisField.focusTo = "#thisField.id#_year";
				break;
			}
		}
		
		for ( i = 1; i lte 3; i = i + 1 ) 
			switch ( listGetAt(request.terraform.locale.dateOrder, i) ) {
				case "d": {
						htmlBlock = listAppend(htmlBlock, dayBlock, " ");
						break;
					}
				case "m": {
						htmlBlock = listAppend(htmlBlock, monthBlock, " ");
						break;
					}
				case "y": {
						htmlBlock = listAppend(htmlBlock, yearBlock, " ");
						break;
					}
			}
		
		if ( thisField.showDay and thisField.showMonth and thisField.showYear and thisField.calendar ) {
		
			if ( not structkeyexists(request.terraform.flags, "Calendar") ) {
				request.terraform.flags.Calendar = true;
				request.terraform.HTMLHead = request.terraform.HTMLHead & "<script src=""#Attributes.ResourcePath#formats/calendar/CalendarPopup.js"" type=""text/javascript""></script><script  type=""text/javascript"">document.write(getCalendarStyles());</script>";
			}
			
			if ( not thisField.popupWindow )
				divIdentifier = "'div#calendarId#'";
			
			for ( i = 1; i lte listLen(thisField.disabledWeekdays); i = i + 1 ) {
				jsDisabledWeekdays = listAppend(jsDisabledWeekdays, (listGetAt(thisField.disabledWeekdays, i) - thisField.firstDayOfWeek + 7) mod 7);
			}
			
			for ( i = 1; i lte arrayLen(thisField.disabledDates); i = i + 1 ) {
				if ( isSimpleValue(thisField.disabledDates[i]) )
					disabledDatesBlock = disabledDatesBlock & "cal#calendarId#.addDisabledDates(""#dateFormat(thisField.disabledDates[i], "yyyy-m-d")#"");"; 
				else
					disabledDatesBlock = disabledDatesBlock & "cal#calendarId#.addDisabledDates(""#dateFormat(thisField.disabledDates[i].from, "yyyy-m-d")#"", ""#dateFormat(thisField.disabledDates[i].to, "yyyy-m-d")#"");"; 
			}

			htmlBlock = htmlBlock & "<A HREF=""##"" onClick=""var theDate = terraFormApi.fields['#thisField.id#'].getValue(); if ( theDate == null ) theDate = new Date(); var dateString = theDate.getFullYear() + '-' + (theDate.getMonth() + 1) + '-' + theDate.getDate(); cal#calendarId#.showCalendar('anchor#calendarId#', dateString); return false;"" NAME=""anchor#calendarId#"" ID=""anchor#calendarId#""><img src=""#Attributes.ResourcePath#formats/calendar/calendar.gif"" width=""16"" height=""15"" style=""margin:1px 3px;border-width:0"" alt=""Calendar""#closeTag#</A>";
			if ( isBoolean(thisField.inline) and thisField.inline )
				inline = true;
			calBlock = "<div id=""div#calendarId#"" style=""background-color : white;position:absolute;visibility:hidden;z-index : 999;""> </div>
<script src=""#Attributes.ResourcePath#formats/calendar/CalendarPopup.js"" type=""text/javascript""></script><script type=""text/javascript"">
				var cal#calendarId# = new CalendarPopup(#divIdentifier#);
				cal#calendarId#.setMonthNames(#listQualify(thisField.months, """")#);
				cal#calendarId#.setDayHeaders(#listQualify(thisField.days, """")#);
				cal#calendarId#.setWeekStartDay(#thisField.firstDayOfWeek#-1);
				cal#calendarId#.offsetX = #thisField.offsetX#;
				cal#calendarId#.offsetY = #thisField.offsetY#;
				cal#calendarId#.setDisabledWeekDays(#jsDisabledWeekdays#);
				cal#calendarId#.todayText = '#thisField.today#';
				cal#calendarId#.inline = #inline#;
				cal#calendarId#.yearCharacter = '#thisField.yearCharacter#';
				cal#calendarId#.setReturnFunction(""updateDatePicker#calendarId#"");
				cal#calendarId#.addDisabledDates(null,""#dateFormat(dateAdd("d", -1, thisField.min), "yyyy-m-d")#""); 
				cal#calendarId#.addDisabledDates(""#dateFormat(dateAdd("d", 1, thisField.max), "yyyy-m-d")#"",null);
				#disabledDatesBlock#
				document.write(cal#calendarId#.getStyles());";
			
			if ( thisField.showYearNavigation )
				calBlock = calBlock & "cal#calendarId#.showYearNavigation();";
			calBlock = calBlock & "
				function updateDatePicker#calendarId#(y,m,d) {
					var newDate = new Date(y, m-1, d);
					terraFormApi.fields['#thisField.id#'].setValue(newDate);
					#onChange#
				}
				</script>";
		}
		
		if ( isBoolean(thisField.inline) and thisField.inline ){
			htmlBlock = htmlBlock & calBlock;
		}
		else {
			request.terraform.FormFoot = request.terraform.FormFoot & calBlock;
		}
        htmlBlock = listAppend(htmlBlock, "#ESC#*", " ");

        if ( NOT structkeyexists(request.terraform.flags, "datePicker") ) {
            request.terraform.flags.datePicker = true;
            request.terraform.HTMLHead = request.terraform.HTMLHead & "<script src=""#Attributes.ResourcePath#formats/datepicker/datepicker.js?#request.eswsoftware.terraform#"" type=""text/javascript""></script>";
        } 

        return htmlBlock;     
    }

    function datePickerFormatClean(value) {
        var y = 2000;
        var m = 1;
        var d = 1;
        var result = "";
        var thescope = evaluate(scope);

        if ( structKeyExists(thescope, thisfield.name) )
            return thescope[thisfield.name];
        if ( 
			( structKeyExists(thescope, "#thisfield.name#_year") or not thisField.showYear )
			and ( structKeyExists(thescope, "#thisfield.name#_month") or not thisField.showMonth )
			and ( structKeyExists(thescope, "#thisfield.name#_day") or not thisField.showDay )
		) {
            if ( thisField.showYear )
				y = thescope["#thisfield.name#_year"];	
            if ( thisField.showMonth )
				m = thescope["#thisfield.name#_month"];
            if ( thisField.showDay )
				 d = thescope["#thisfield.name#_day"];
            if ( val(y) and val(m) and val(d) )
				result = "#y#-#m#-#d#";
            return result;
        }

        return value;
    }         

    // End Format: DATEPICKER 
    // *************************************************************************





	// *************************************************************************
	// Format:		 DIVSELECT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20100107
	// Dependencies: 
	// Notes:-
	//
	// History:		20100107 Created
	// Customisations:-
	//
	
	Preset("Format_divSelect", "Datatype", "Default");
	Preset("Format_divSelect", "Multiple", "No");
	
	function divSelectFormatDegrade() {
		return "divselect";
	}
	
	function divSelectFormat(ThisField) {
		var markup = "";
		var arrValue = "";
		var arrDisplay = "";
		var i = 0;
		var selected = false;
		var selectedBlock = "";
		
		if ( structKeyExists(variables.ThisField, "ValueList") and len(ThisField.ValueList) ) {
			arrValue = listtoarray(ThisField.ValueList, ThisField.Delimiters, true);
			arrDisplay = listtoarray(ThisField.DisplayList, ThisField.Delimiters, true);
			for ( i = 1; i lte arrayLen(arrValue); i++ ) {
				if ( ThisField.Multiple )
					selected = ListFindNoCase(ThisField.Value, arrValue[i]);	
				else
					selected = ThisField.Value EQ arrValue[i];
				if ( selected ) 
					selectedBlock = " class=""divSelectSelected""";	
				else
					selectedBlock = "";
				markup &= "<div id=""#thisField.id#_option_#i#"" onclick=""divSelectChoose('#thisField.id#', #i#, #iif(ThisField.Multiple, "true", "false")#)""#selectedBlock#>#arrDisplay[i]#<div id=""#thisField.id#_value_#i#"" style=""display:none"">#arrValue[i]#</div></div>";
			}
		}
		return "<input value=""#ThisField.Value#"" type=""hidden"" id=""#thisField.id#"" name=""#thisfield.name#""#closeTag#<div class=""divSelect"" id=""#thisField.id#_wrapper"">#markup#</div>";
	}
	
	// End Format: DIVSELECT 
	// *************************************************************************
	
	
	
	
	
	// *************************************************************************
	// Format:		 EMAIL 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20130105
	// Dependencies: TEXT
	// Notes:-
	// EMAIL builds a email text field.
	// History:		20130105 Created
	// Customisations:-
	//
	
	Preset("Format_Email", "Datatype", "email");
	
	function emailFormatDegrade() {
		return "email";
	}
	
	EmailFormat = TextFormat;

	// End Format: EMAIL 
	// *************************************************************************
	




	// *************************************************************************
	// Format:		 FCKEDITOR 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20050808
	// Dependencies: 
	// Notes:-
	// FCKEDITOR implements the FCKEditor 2.0 WYSIWYG editor field . 
	// See http://www.fckeditor.net/ for more information.
	// FCKEditor is not distributed with TerraForm. You must download it
	// separately. Unpack the FCKEditor download into
	// [ResourcePath]/formats/fckeditor/ where [ResourcePath] is the 
	// ResourcePath attribute supplied in your cf_terraform tag or 
	// set up in your settings file. The folder [ResourcePath]/formats/fckeditor/
	// should thus contain a variety of files and folders. 
	// Copy the file, [ResourcePath]/formats/fckeditor/fckeditor.cfm to
	// [customtags]/terraform/formats/includes/fckeditor/ .
	// Some attributes like height and width are added as normal. But most 
	// FCKeditor attributes arr added as part of a structure. Create a structure 
	// called config and add attributes (such as toolbarset and defaultlanguage) to 
	// that, then supply the structure as an attribute: 
	// <cf_terrafield 
	//   name="myField"
	//   format="fckeditor" 
	//   width="500" 
	//   height="400" 
	//   config="#config#" 
	//   ...
	//  />
	// History:		20050808 added toolbarSet attribute
	//				20050428 Created
	// Customisations:-
	//
	
	Preset("Format_fckEditor", "Datatype", "string");
	Preset("Format_fckEditor", "width", "100%");
	Preset("Format_fckEditor", "height", 300);
	Preset("Format_fckEditor", "config", structNew());
	Preset("Format_fckEditor", "toolbarSet", "Default");
	
	function fckEditorFormatDegrade() {		
		return "fckEditor";
	}
	
	// End Format: FCKEDITOR 
	// *************************************************************************
	
	

	// *************************************************************************
	// Format:		 FILE 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031102
	// Dependencies: 
	// Notes:-
	// FILE builds a file upload field. As there is no way to pre-fill a value
	// into this HTML field, the field forgets its value after each
	// submission. This field will automatically reset the form EncType to
	// "multipart/form-data". If you want to use this field simply to select a
	// file rather than upload it, then add the attribute 
	// enctype="application/x-www-form-urlencoded" to your opening cf_terraform tag.
	// History:		20031102 made XHTML tag closure dependent on XHTML attribute
	//				20031031 switched from attributesList to ignoreAttributes
	//				20030511 added fileFormatDegrade()
	//				20020820 now allows "application/x-www-form-urlencoded"
	//				20020607 bugfix for CFMX
	//				20020318
	// Customisations:-
	//

	Preset("Format_File", "Datatype", "Default");
	Preset("Format_File", "NoProcessing", "Yes");
	
	function fileFormatDegrade() {
		return "file";
	}
	
	function FileFormat(ThisField) {
		var HTMLBlock = "<input #PASSTHROUGH# type=""file"" name=""#ThisField.Name#""#closeTag#";
		
		ignoreAttributes.NoProcessing = "";
		
		if ( Attributes.EncType eq "" )
			Attributes.EncType = "multipart/form-data";
		return HTMLBlock;
	}

	// End Format: FILE 
	// *************************************************************************



	// *************************************************************************
	// Format:		 HIDDEN 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030511
	// Dependencies: TEXT
	// Notes:-
	// HIDDEN builds a hidden field.
	// History:		20030511 added hiddenFormatDegrade()
	//				20020620 added default datatype
	//				20020318 Created
	// Customisations:-
	//
	
	Preset("Format_Hidden", "Datatype", "Default");
	
	function hiddenFormatDegrade() {
		return "hidden";
	}
	
	HiddenFormat = TextFormat;

	// End Format: HIDDEN 
	// *************************************************************************
	

	
	// *************************************************************************
	// Format:		 HTMLAREA 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20081212
	// Dependencies: 
	// Notes:-
	// HTMLAREA implements the HTMLArea field created by interactivetools.com . 
	// See http://www.interactivetools.com/products/htmlarea/ for more information.
	// History:		20081212 removed language="Javascript"
	//				20031119 bugfix for MSIE/Mac -- display:inline not supported
	//				20031031 switched from attributesList to ignoreAttributes
	//				20030807 added required flag placeholder
	//				20030624 Created
	// Customisations:-
	//
	
	Preset("Format_htmlArea", "Datatype", "string");

	function htmlAreaFormatDegrade() {
		return "htmlArea";
	}
			
	function htmlAreaFormat(ThisField) {
		var HTMLBlock = "";
		var config = "";
		var tableStyle = "";
		var defer = " defer";
		
		if ( attributes.xhtml ) 
			defer = " defer=""defer""";
		
		ignoreAttributes.height = "";
		ignoreAttributes.width = "";
		ignoreAttributes.bodyStyle = "";
		ignoreAttributes.debug = "";
		ignoreAttributes.toolbar = "";
		ignoreAttributes.fontnames = "";
		ignoreAttributes.fontsizes = "";
		ignoreAttributes.fontstyles = "";
		ignoreAttributes.stylesheet = "";
		
		if ( NOT structkeyexists(request.terraform.flags, "htmlArea") ) {
			request.terraform.flags.htmlArea = true;
			htmlBlock = "
				<script type=""text/javascript""><!-- // load htmlarea
					_editor_url = ""#Attributes.ResourcePath#formats/htmlarea/"";                     // URL to htmlarea files
					var win_ie_ver = parseFloat(navigator.appVersion.split(""MSIE"")[1]);
					if (navigator.userAgent.indexOf('Mac')        >= 0) { win_ie_ver = 0; }
					if (navigator.userAgent.indexOf('Windows CE') >= 0) { win_ie_ver = 0; }
					if (navigator.userAgent.indexOf('Opera')      >= 0) { win_ie_ver = 0; }
					if (win_ie_ver >= 5.5) {
					 document.write(String.fromCharCode(60) + 'script src=""' +_editor_url+ 'editor.js""');
					 document.write(' language=""Javascript1.2""' + String.fromCharCode(62) + String.fromCharCode(60) + '/script' + String.fromCharCode(62));  
					} else { document.write(String.fromCharCode(60) + 'script' + String.fromCharCode(62) + 'function editor_generate() { return false; }' + String.fromCharCode(60) + '/script' + String.fromCharCode(62)); }
				// --></script>
			";	
		}
		
		if ( structKeyExists(ThisField, "height") )
			config = config & "config.height = '#thisfield.height#';";
		if ( structKeyExists(ThisField, "width") )
			config = config & "config.width = '#thisfield.width#';";
		if ( structKeyExists(ThisField, "bodyStyle") )
			config = config & "config.bodyStyle = '#thisfield.bodyStyle#';";
		if ( structKeyExists(ThisField, "debug") )
			if ( ThisField.debug )
				config = config & "config.debug = 1;";
			else 
				config = config & "config.debug = 0;";
		if ( structKeyExists(ThisField, "toolbar") )
			config = config & "config.toolbar = #thisfield.toolbar#;";
		if ( structKeyExists(ThisField, "fontnames") )
			config = config & "config.fontnames = #thisfield.fontnames#;";
		if ( structKeyExists(ThisField, "fontsizes") )
			config = config & "config.fontsizes = #thisfield.fontsizes#;";
		if ( structKeyExists(ThisField, "fontstyles") )
			config = config & "config.fontstyles = #thisfield.fontstyles#;";
		if ( structKeyExists(ThisField, "stylesheet") )
			config = config & "config.stylesheet = '#thisfield.stylesheet#';";
	
		if ( not ( (not compareNoCase(request.terraform.browser.platform, "Mac")) and (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) ) )
			tableStyle = "display: inline;";	
		htmlBlock = htmlBlock & "
			<table class=""terraFormLayoutElement"" style=""#tableStyle#"" border=""0"" cellspacing=""0"" cellpadding=""0""><tr valign=""top""><td><textarea #PASSTHROUGH# name=""#ThisField.Name#"">#InputFieldFormat(ThisField.RawValue)#</textarea></td><td>#ESC#*</td></tr></table>
			<script type=""text/javascript""#defer#>
				var config = new Object(); // create new config object
				#config#
				editor_generate('#ThisField.Name#' , config);
			</script>
		";

		return HTMLBlock;
	}
	
	// End Format: HTMLAREA 
	// *************************************************************************
	
	

	// *************************************************************************
	// Format:		 IMAGE 
	// Author:  	 Matthew Walker ( esw@eswsoftware.com )
	// Version:		 20031102
	// Dependencies: 
	// Notes:-
	// IMAGE provides an HTML image submit button based on <input type="image">.
	// Supply the image path using the src attribute along with any other 
	// attributes, e.g. border="0". You can also supply hoversrc, downsrc,
	// clicksrc. These are the paths to different images for different states.
	// History:  	 20031102 made XHTML tag closure dependent on XHTML attribute
	//				 20031031 switched from attributesList to ignoreAttributes
	//				 20030825 Created
	// Customisations:-
	//
 
	Preset("Format_Image", "Datatype", "Default");
	Preset("Format_Image", "src", "");
	
	function imageFormatDegrade() {
		return "image";
	}
	 
	function imageFormat(ThisField) {
		var suffix = replace(createuuid(), "-", "", "all");
		var image1 = "image1" & suffix;
		var image2 = "image2" & suffix;
		var image3 = "image3" & suffix;
		var image4 = "image4" & suffix;
		var js = "var #image1# = new Image();#image1#.src = ""#thisfield.src#"";";
		
		ignoreAttributes.hoversrc = "";
		ignoreAttributes.downsrc = "";
		ignoreAttributes.clicksrc = "";
		
		if ( not structkeyexists(thisfield, "onmouseout") )
			thisfield.onmouseout = "";
		if ( not structkeyexists(thisfield, "onmouseover") )
			thisfield.onmouseover = "";
		if ( not structkeyexists(thisfield, "onmousedown") )
			thisfield.onmousedown = "";
		if ( not structkeyexists(thisfield, "onclick") )
			thisfield.onclick = "";			
		thisfield.onmouseout = listappend(thisfield.onmouseout, "this.src = '#thisfield.src#'", ";");	
		if ( structkeyexists(thisfield, "hoversrc") and len(thisfield.hoversrc) ) {
			thisfield.onmouseover = listappend(thisfield.onmouseover, "this.src = '#thisfield.hoversrc#'", ";");	
			js = js & "var #image2# = new Image();#image2#.src = ""#thisfield.hoversrc#"";";
		}	
		if ( structkeyexists(thisfield, "downsrc") and len(thisfield.downsrc) ) {
			thisfield.onmousedown = listappend(thisfield.onmousedown, "this.src = '#thisfield.downsrc#'", ";");	
			js = js & "var #image3# = new Image();#image3#.src = ""#thisfield.downsrc#"";";
		}
		if ( structkeyexists(thisfield, "clicksrc") and len(thisfield.clicksrc) ) {
			thisfield.onclick = listappend(thisfield.onclick, "this.disabled = true;this.src = '#thisfield.clicksrc#';this.form.submit()", ";");	
			js = js & "var #image4# = new Image();#image4#.src = ""#thisfield.clicksrc#"";";
		}

		request.terraform.htmlhead = request.terraform.htmlhead & "
			<script type=""text/javascript"">
				<!--
					#js#
				//-->
			</script>
		";
		return "<input #PASSTHROUGH# type=""image""#closeTag#";
	}
 
	// End Format: IMAGE 
	// ************************************************************************* 
	
	

	// *************************************************************************
	// Format:		 MOVESELECT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20091106
	// Dependencies: Select
	// Notes:-
	// MOVESELECT is designed to allow selection of multiple items from a list.
	// Two boxes are displayed with the left box containing a list of
	// options. By selecting items in this list and pushing buttons, the user
	// compiles the desired list in the right box. The advantage of this
	// interface is that the user is presented with two lists, one being the
	// chosen items, and one being the items not chosen. To use this format,
	// either supply valuelist and optional displaylist OR supply query,
	// valuecolumn and optional displaycolumn.
	// History:		 20091106 modified to respect the ordering of default values
	//				 20060502 added version suffix to script path
	//				 20060420 added id to ignore list
	//				 20040812 added focusTo
	//				 20040529 Added IDs
	//				 20031119 bugfix for MSIE/Mac -- display:inline not supported
	//						  added Safari support
	//				 20031102 made XHTML tag closure dependent on XHTML attribute
	//						  moved script into external file
	//				 20031031 switched from attributesList to ignoreAttributes
	//				 20030807 added required flag placeholder
	//				 20030704 added disabled support
	//				 20030526 tweaked for Opera 
	//				 20030515 added ondblclick handling code by Stuart Morgan
	//				 20030511 added moveSelectFormatDegrade()
	//				 20030129 added buttonPassthrough
	//				 20020901 degrades better in Netscape
	//				 20020818 Created
	// Customisations:-
	//
	
	Preset("Format_MoveSelect", "Datatype", "Default");
	Preset("Format_MoveSelect", "Multiple", true);
	Preset("Format_MoveSelect", "size", 8);
	Preset("Format_MoveSelect", "style", "width : 120px;");
	Preset("Format_MoveSelect", "heading1", "Available:");
	Preset("Format_MoveSelect", "heading2", "Used:");

	if ( request.terraform.browser.engine eq "Opera" ) 
		Preset("Format_MoveSelect", "ButtonPassthrough", "style=""width : 2em;padding: 5px;""");
	else 
		Preset("Format_MoveSelect", "ButtonPassthrough", "style=""width : 2em;""");
		
	function moveSelectFormatDegrade() {
		if ( 
			( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
			or ( request.terraform.browser.engine eq "Opera" )
			or ( request.terraform.browser.engine eq "Gecko" )
			or ( request.terraform.browser.engine eq "Konqueror" )
		)
			return "moveSelect";
		thisfield.multiple=true;	
		structDelete(thisField, "heading1");
		structDelete(thisField, "heading2");
		return selectFormatDegrade();
	}
	
	function MoveSelectFormat(ThisField) {
		var HTMLBlock = "";
		var Column1 = "";
		var Column2 = "";
		var pos = 0;
		var Option = "";
		var Options = structNew();
		var OptionValue = "";
		var OptionDisplay = "";
		var headingPassthrough = "";
		var ListLength = ListLen(ThisField.ValueList, ThisField.Delimiters);
		var tableStyle = "";
		
		ignoreAttributes.heading1 = "";
		ignoreAttributes.heading2 = "";
		ignoreAttributes.id = "";
		ignoreAttributes.ButtonPassthrough = "";

		thisField.focusTo = "#thisField.id#_left";
		
		if ( structkeyexists(thisfield, "disabled") ) {
			thisField.ButtonPassthrough = thisField.ButtonPassthrough & " disabled=""disabled""";
			headingPassthrough = " disabled=""disabled""";
		}	
		if ( Len(ThisField.ValueList) ) {
			for ( pos = 1; pos LTE ListLength; Pos = pos + 1 ) {
				OptionValue=ListGetAt(ThisField.ValueList, pos, ThisField.Delimiters);
				OptionDisplay=ListGetAt(ThisField.DisplayList, pos, ThisField.Delimiters);
				Options[OptionValue] = "<option value=""#HTMLEditFormat(OptionValue)#"">#HTMLEditFormat(OptionDisplay)#</option>";
				if ( not ListFindNoCase(ThisField.Value,OptionValue) )
					Column1 = Column1 & Options[OptionValue];
			}
			for ( pos = 1; pos LTE listLen(ThisField.Value); Pos = pos + 1 )
				Column2 = Column2 & Options[listGetAt(ThisField.Value, pos)];
		}	
			
		if ( NOT structkeyexists(request.terraform.flags, "MoveSelect") ) {
			request.terraform.flags.MoveSelect = true;
			request.terraform.HTMLHead = request.terraform.HTMLHead & "<script src=""#Attributes.ResourcePath#formats/moveselect/moveselect.js?#request.eswsoftware.terraform#"" type=""text/javascript""></script>";
		} 
		if ( not ( (not compareNoCase(request.terraform.browser.platform, "Mac")) and (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) ) )
			tableStyle = "display: inline;";	
		HTMLBlock = "
			<table class=""terraFormLayoutElement"" style=""#tableStyle#"">
				<tr>
					<td align=""right""#headingPassthrough#>
						<div id=""#ThisField.id#_heading1"">#ThisField.Heading1#</div>
						<select id=""#ThisField.id#_left"" name=""#ThisField.Name#1"" #Passthrough# multiple onDblClick=""addOne('#thisField.id#')"">
							#Column1#
						</select>
					</td>
					<td align=""center"">
						<button id=""#ThisField.id#_button1"" #thisField.ButtonPassthrough# type=""button"" onclick=""addOne('#thisField.id#')"">&nbsp;&gt;&nbsp;</button><br#closeTag#
						<button id=""#ThisField.id#_button2"" #thisField.ButtonPassthrough# type=""button"" onclick=""addAll('#thisField.id#')"">&gt;&gt;</button><br#closeTag#
						<button id=""#ThisField.id#_button3"" #thisField.ButtonPassthrough# type=""button"" onclick=""removeOne('#thisField.id#')"">&nbsp;&lt;&nbsp;</button><br#closeTag#
						<button id=""#ThisField.id#_button4"" #thisField.ButtonPassthrough# type=""button"" onclick=""removeAll('#thisField.id#')"">&lt;&lt;</button>
					</td>
					<td#headingPassthrough#>
						<div id=""#ThisField.id#_heading2"">#ThisField.Heading2#</div>
						<select id=""#ThisField.id#_right"" name=""#ThisField.Name#2"" #Passthrough# multiple onDblClick=""removeOne('#thisField.id#')"">
							#Column2#
						</select>
						<input id=""#thisField.id#"" type=""hidden"" value=""#ThisField.Value#"" name=""#ThisField.Name#"">
					</td>
					<td valign=""top"">
						#ESC#*
					</td>
				</tr>
			</table>
		";
		return HTMLBlock;
	}

	// End Format: MOVESELECT 
	// *************************************************************************



	// *************************************************************************
	// Format:		 MSSLIDER 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version: 	 20040526
	// Dependencies: numeric
	// Notes:-
	// MSSLIDER implements a slider control as a dHTML element behaviour for 
	// MS Internet Explorer. For further information:
	// http://msdn.microsoft.com/workshop/author/behaviors/library/slider/slider.asp
	// History:		 20040526 now uses supplied id
	//				 20031119 bugfix for MSIE/Mac -- display:inline not supported
	//				 20031031 switched from attributesList to ignoreAttributes
	//				 20030807 added required flag placeholder
	//				 20030511 added msSliderFormatDegrade()
	//				 20030123 Added InputFieldFormat()
	//				 20020902 Created
	// Customisations:-
	//
	
	Preset("Format_MSSlider", "Datatype", "Real");
	Preset("Format_MSSlider", "tickInterval", 1);
	Preset("Format_MSSlider", "min", 0);
	Preset("Format_MSSlider", "max", 10);
	Preset("Format_MSSlider", "dynamic", false);
	Preset("Format_MSSlider", "orientation", "horizontal");
	Preset("Format_MSSlider", "snap", true);
	Preset("Format_MSSlider", "style", "");
	Preset("Format_MSSlider", "tickStyle", "bottomRight"); // bottomRight | topLeft | both | none 
	Preset("Format_MSSlider", "tickColor", "black");
	Preset("Format_MSSlider", "barColor", "menu");

	function msSliderFormatDegrade() {
		if ( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) and request.terraform.browser.platform eq "Windows")
			return "msSlider";
		return spinEditFormatDegrade();	
	}
	
	function MSSliderFormat(ThisField) {
		var style = "behavior : url(#Attributes.ResourcePath#formats/msslider/slider.htc);sl--orientation : #thisField.orientation#;sl--tick-color  : #thisField.tickColor#;sl--tick-style : #thisField.tickStyle#;sl--bar-color : #thisField.barColor#";
		var HTMLBlock = "";
		var tableStyle = "";
		var scripts = "
			thisSlider = document.getElementById('#thisField.id#');
			thisSlider.min=#thisField.min#;
			thisSlider.max=#thisField.max#;
		";
		if ( structKeyExists(thisField, "tickNumber") )
			scripts = listAppend(scripts, "thisSlider.tickNumber=#thisField.tickNumber#", ";");
		else
			scripts = listAppend(scripts, "thisSlider.tickInterval=#thisField.tickInterval#", ";");		
		if ( not thisField.snap )
			style = listAppend(style, "sl--snap : 0", ";");
		thisField.style = listAppend(thisField.style, style, ";");
		if ( thisField.dynamic ) 
			scripts = scripts & "thisSlider.dynamic=1;";
		
		ignoreAttributes.tickInterval = "";
		ignoreAttributes.dynamic = "";
		ignoreAttributes.orientation = "";
		ignoreAttributes.snap = "";
		ignoreAttributes.tickNumber = "";
		ignoreAttributes.tickStyle = "";
		ignoreAttributes.barColor = "";
		ignoreAttributes.tickColor = "";
		
		if ( not ( (not compareNoCase(request.terraform.browser.platform, "Mac")) and (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) ) )
			tableStyle = "display: inline;";	
			
		HTMLBlock = "<table class=""terraFormLayoutElement"" border=""0"" cellspacing=""0"" cellpadding=""0"" style=""#tableStyle#""><tr valign=""top""><td><span #PASSTHROUGH# name=""#ThisField.Name#"" value=""#InputFieldFormat(ThisField.RawValue)#""></span></td><td>#ESC#*</tr></table><script type=""text/javascript"">#scripts#</script>";
		return HTMLBlock;
	}

	// End Format: MSSLIDER 
	// *************************************************************************
	

		
	// *************************************************************************
	// Format:		 NULL 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030511
	// Dependencies: 
	// Notes:-
	// NULL creates no field at all. Use it where you want to apply TerraForm 
	// validation to a form field you are generating by other means (e.g. a 
	// regular HTML field).
	// History:		 20030511 added nullFormatDegrade()
	//				 20030206 Created	
	// Customisations:-
	//
	
	Preset("Format_Null", "Datatype", "String");

	function nullFormatDegrade() {
		return "null";
	}
	
	function NullFormat(ThisField) {
		return "";
	}
	
	// End Format: NULL 
	// *************************************************************************


	
	// *************************************************************************
	// Format:		 NUMERIC
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20040625
	// Dependencies: 
	// Notes:-
	// NUMERIC builds a text field with data entry right-aligned as
	// appropriate for numbers.
	// History:		20040625 key filtering now ignores ENTER
	//			    20031102 made XHTML tag closure dependent on XHTML attribute
	//				20031031 switched from attributesList to ignoreAttributes
	//				20030526 added key filter
	//				20030511 added numericFormatDegrade()
	//				20030123 Added InputFieldFormat()
	//				20020623 Added AutoAdvanceTo
	//				20020613 bug wrong type being declared
	// 				20020607 bugfix for CFMX
	//				20020328 Created
	// Customisations:-
	//
	
	Preset("Format_Numeric", "Datatype", "Integer");
	
	function numericFormatDegrade() {
		if ( request.terraform.browser.engine eq "Netscape" ) 
			return textFormatDegrade();	
		return "numeric";	
	}
		
	function NumericFormat(ThisField) {
		var HTMLBlock = "#PASSTHROUGH# type=""text"" name=""#ThisField.Name#"" value=""#InputFieldFormat(ThisField.RawValue)#""";
		
		// key filter
		if ( structKeyExists(thisField, "jsMask") and len(thisField.jsMask) ) {
			if ( not structKeyExists(thisField, "onkeypress") )
				thisfield.onKeyPress = "";
			thisfield.onKeyPress = listAppend(thisfield.onKeyPress, "if (window.event.keyCode != 13) return ( String.fromCharCode(window.event.keyCode).search(#jsStringFormat(thisField.jsMask)#) > -1 )", ";");
		}
		
		ignoreAttributes.STYLE = "";

		if ( StructKeyExists(ThisField, "Style") )
			ThisField.Style = "text-align : right;" & ThisField.Style;
		else
			ThisField.Style = "text-align : right;";
		if ( StructKeyExists(ThisField, "AutoAdvanceTo") AND StructKeyExists(ThisField, "MaxLength") AND Len(ThisField.AutoAdvanceTo) AND Len(ThisField.MaxLength) ) {
			if ( NOT StructKeyExists(ThisField, "OnKeyUp") )
				ThisField.OnKeyUp = "";
			ThisField.OnKeyUp = ListAppend(ThisField.OnKeyUp, "if ( this.value.length == #ThisField.MaxLength# ) document.#Attributes.FormName#['#ThisField.AutoAdvanceTo#'].focus()", ";");	
		}	
		if ( ThisField.MaxLength )
			HTMLBlock = HTMLBlock & " maxlength=""#ThisField.MaxLength#""";
		HTMLBlock = HTMLBlock & " style=""#ThisField.Style#""";
		HTMLBlock = "<input #HTMLBlock##closeTag#";
		return HTMLBlock;
	}

	// End Format: NUMERIC 
	// *************************************************************************
	


	// *************************************************************************
	// Format:		 PASSWORD 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030511
	// Dependencies: TEXT
	// Notes:-
	// PASSWORD builds a password text field.
	// History:		20030511 added passwordFormatDegrade()
	//				20020620 added default datatype
	//				20020318 Created
	// Customisations:-
	//
	
	Preset("Format_Password", "Datatype", "String");
	
	function passwordFormatDegrade() {
		return "password";
	}
	
	PasswordFormat = TextFormat;

	// End Format: PASSWORD 
	// *************************************************************************
	


	// *************************************************************************
	// Format:		 RADIO 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031126
	// Dependencies: CHECKBOX
	// Notes:-
	// RADIO is a synonym for CHECKBOX, although Multiple defaults to true for
	// checkbox, false for radio.
	// History:		20031126 legend now highlights if error
	//				20031119 minor tweaks
	//				20030601 now specify cols=0 to have just one row
	//						 added gridStart/gridEnd
	//						 - note these override tablePassthrough
	//						 added cellStart/cellEnd 
	//						 - note these override tdPassthrough
	//						 added rowStart/rowEnd 	
	//				20030511 added radioFormatDegrade()
	//				20030129 added tablePassthrough and tdPassthrough
	//				20020903 added group attribute
	//				20020620 added default datatype
	//				20020318 Created
	// Customisations:-
	//
	
	Preset("Format_Radio", "Datatype", "Default");
	Preset("Format_Radio", "Group", "no");
	Preset("Format_Radio", "tablePassthrough", "class=""terraFormLayoutElement""");
	Preset("Format_Radio", "tdPassthrough", "");	
	Preset("Format_Radio", "gridEnd", "</table></td><td>#ESC#*</td></tr></table>");
	Preset("Format_Radio", "rowStart", "<tr>");
	Preset("Format_Radio", "rowEnd", "</tr>");
	Preset("Format_Radio", "cellEnd", "");
	
	function radioFormatDegrade() {
		return "radio";
	}
	
	RadioFormat = CheckboxFormat;
	
	// End Format: RADIO 
	// *************************************************************************
	

	
	// *************************************************************************
	// Format:		 RECAPTCHA
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20080717
	// Dependencies: 
	// Notes:-		 
	// History:		 20080717 Created
	// Customisations:-
	//

	// End Format: RECAPTCHA
	// *************************************************************************
	
	
	
	// *************************************************************************
	// Format:		 SEARCH 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20130629
	// Dependencies: TEXT
	// Notes:-
	// SEARCH builds an HTML5 search text field.
	// History:		20130629 Created
	// Customisations:-
	//
	
	Preset("Format_Search", "Datatype", "String");
	
	function searchFormatDegrade() {
		return "search";
	}
	
	SearchFormat = TextFormat;

	// End Format: PASSWORD 
	// *************************************************************************
	


	// *************************************************************************
	// Format:		 SELECT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20091006
	// Dependencies: 
	// Notes:-
	// SELECT builds an HTML select box from the ValueList and DisplayList
	// attributes (which may be generated internally from a Query attribute). 
	// The optional Prompt attribute will generate a valueless first option in
	// the select box (the value of this option may be adjusted using
	// PromptValue.
	// History:		 20091006 handle empty items better
	//				 20040316 added support of option groups - the HTML optgroup element
	//				 20030511 added selectFormatDegrade()
	//				 20030321 switched lists to arrays for performance gain
	//				 20030307 added qformsrelatedselect attribute for qForms n-related selects 
	//				 20020620 added default datatype
	//				 20020616 Should be using comma delimiter when asssessing value
	// 				 20020607 bugfix for CFMX
	//				 20020318 Created
	// Customisations:-
	//
	
	Preset("Format_Select", "Datatype", "Default");
	
	function selectFormatDegrade() {
		return "select";
	}
	
	function SelectFormat(ThisField) {
		var HTMLBlock = "";
		var Prompt = "";
		var MultipleFlag = "";
		var pos = 0;
		var arrValue = "";
		var arrDisplay = "";
		var arrGroup = "";
		var ThisFieldHTMLTag = "";
		
		ignoreAttributes.qformsrelatedselect = "";
		ignoreAttributes.escapeChars = "";
		
		if ( IsDefined("variables.ThisField.Prompt") AND Len(ThisField.Prompt) ) {
			if ( NOT IsDefined("variables.ThisField.PromptValue") )
				ThisField.PromptValue = "";
			Prompt = "<option value=""#ThisField.PromptValue#"">#ThisField.Prompt#</option><option value=""#ThisField.PromptValue#"">#RepeatString("-", Len(ThisField.Prompt))#</option>";
		}
		if ( Len(ThisField.ValueList) ) {
		
			arrValue = listtoarray(ThisField.ValueList, ThisField.Delimiters, true);
			arrDisplay = listtoarray(ThisField.DisplayList, ThisField.Delimiters, true);
			arrGroup = listtoarray(ThisField.groupList, ThisField.Delimiters, true);
			
			for ( pos = 1; pos LTE arraylen(arrValue); Pos = pos + 1 ) {
				ThisFieldHTMLTag = " value=""#HTMLEditFormat(arrValue[pos])#""";
				if ( ListFindNoCase(ThisField.Value,arrValue[pos]) )
					if ( attributes.xhtml )
						ThisFieldHTMLTag = "#ThisFieldHTMLTag# selected=""selected""";
					else
						ThisFieldHTMLTag = "#ThisFieldHTMLTag# selected";
				ThisFieldHTMLTag = "<option #ThisFieldHTMLTag#>#HTMLEditFormat(arrDisplay[pos])#</option>";
				
				// handle optgroup functionality
				if ( len(ThisField.groupList) ) {
					if ( (pos eq 1) or (compareNoCase(arrGroup[pos], arrGroup[pos - 1])) ) 
						ThisFieldHTMLTag = "<optgroup label=""#HTMLEditFormat(arrGroup[pos])#"">" & ThisFieldHTMLTag;
					if ( (pos eq arraylen(arrValue)) or (compareNoCase(arrGroup[pos], arrGroup[pos + 1])) )
						ThisFieldHTMLTag = ThisFieldHTMLTag & "</optgroup>";
				}
								
				HTMLBlock = HTMLBlock & ThisFieldHTMLTag;
			}
		}	
		
		if ( ThisField.Multiple )
			if ( attributes.xhtml )
				MultipleFlag = " multiple=""multiple""";
			else
				MultipleFlag = " multiple";
			
		HTMLBlock = "<select name=""#ThisField.Name#""#MultipleFlag# #Passthrough#>#Prompt##HTMLBlock#</select>";
					
		if ( structKeyExists(thisfield, "qformsrelatedselect") and isBoolean(thisfield.qformsrelatedselect) and thisfield.qformsrelatedselect )
			qFormsScript = listAppend(qFormsScript, "objForm.#thisField.name#.setValue(""#ThisField.Value#"")", ";");
		return HTMLBlock;
	}

	// End Format: SELECT 
	// *************************************************************************


	
	// *************************************************************************
	// Format:		 SOEDITOR 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030806
	// Dependencies: 
	// Notes:-
	// The SOEDITOR format provides support for the soEditor WYSIWYG editor. 
	// This format supports both soEditor Lite and soEditor Pro. soEditor must 
	// be downloaded separately from siteobjects.com. For more information about
	// soEditor, see
	//     http://www.siteobjects.com/pages/soeditor.cfm
	
	// Install soEditor Lite or Pro into [ResourcePath]/formats/ where
	// [ResourcePath] is the ResourcePath attribute supplied in your
	// cf_terraform tag or set up in your settings file. So you should have 
	// the following folder:
	//     [ResourcePath]/formats/siteobjects/soeditor/lite/ 
	// or 
	//     [ResourcePath]/formats/siteobjects/soeditor/pro/ 	
	// (depending on your version) containing a variety of .htc, .txt,
	// .htm, .cfm files. Copy soeditor_lite.cfm or soeditor_pro.cfm into 
	// [customtags]\terraform\formats\includes\soeditor\.
	//
	// If you are using soEditor Pro, be sure to pass the attribute
	// version="pro" in your cf_terrafield tag or ideally set it as a preset
	// in your settings file:
	//     Preset("format_soEditor", "version", "Pro");
	//
	// Regular soEditor attributes apply, although you cannot use the soEditor
	// attribute "format" as that is already a TerraField attribute.
	// Instead, use "soEditor_Format". See 
	//     http://www.siteobjects.com/pages/soeditordocs.cfm 
	// for soEditor docs.
	//
	// You do not need the ScriptPath, form, field, and HTML attributes as they are
	// generated for you by TerraForm. 
	//
	// History:		 20030806 added required flag placeholder
	//				 20030724 added soeditorformatdegrade()
	//				 20030320 improved Pro support by adding version attribute
	// 				 20030128 changed this to thisField
	//				 20020914 changed this.value to this.rawvalue
	//				 20020813 removed ScriptPath
	//				 20020405 fixed bug - format attribute clash
	// Customisations:-
	//
	
	function soeditorFormatDegrade() {
		if ( 
			( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
		)
			return "soeditor";
		return textareaFormatDegrade();
	}
	
	// End Format: SOEDITOR
	// *************************************************************************
	

	
	// *************************************************************************
	// Format:		 SPINEDIT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20091221
	// Dependencies: numeric
	// Notes:-
	// SPINEDIT builds a text field with superimposed buttons that allow a
	// visitor to alter a number by clicking "increase" and "decrease" buttons or
	// by rolling the mouse wheel. Specify a Step attribute to control the 
	// step size (default is 1). 
	//
	// This format is based on code by Jon Hall (jon@ozline.net).
	// History:		 20091221 onchange event now works
	//				 20080704 replaced layout table with div tags
	//                        added noStyle attribute
	//				 20060502 added version suffix to script path
	//				 20040805 rewritten for disabled compatibility
	//				 20040527 now uses supplied id
	//				 20031119 added Safari support
	//						  bugfix for MSIE/Mac -- display:inline not supported
	//				 20031102 made XHTML tag closure dependent on XHTML attribute
	//				 20031031 switched from attributesList to ignoreAttributes
	//				 20030903 added position:relative to terraFormLayoutElement table
	//				 20030807 added required flag placeholder
	//		 		 20030806 added a blur() and focus() so that onchange events are fired
	//				 20030610 converted buttons to images - much better cross platform performance
	//				 20030511 added spinEditFormatDegrade()
	//				 20030424 added id attribute for Netscape support
	//				 20030123 Added InputFieldFormat()
	//				 20020625 Added wrap and improved button positioning
	//				 20020607 bugfix for CFMX
	//				 20020605 Created
	// Customisations:-
	//
	
	Preset("Format_SpinEdit", "Datatype", "Integer");
	Preset("Format_SpinEdit", "Size", "4");

	function spinEditFormatDegrade() {
		if ( 
			( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
			or ( request.terraform.browser.engine eq "Opera" )
			or ( request.terraform.browser.engine eq "Gecko" )
			or ( request.terraform.browser.engine eq "Konqueror" )
		)
			return "spinedit";
		structDelete(thisField, "wrap");	
		return numericFormatDegrade();	
	}
	
	function SpinEditFormat(ThisField) {
		var HTMLBlock = "";
		var onClickAdd = "";
		var onClickSub = "";
		var onMouseWheel = "";
		var Min = ThisField.Min;
		var Max = ThisField.Max;
		var Wrap = "";
		var tableStyle = "";
		var disabledButtonsDisplay = "none";
		var enabledButtonsDisplay = "inline";
		var styles = structNew();
		
		if ( StructKeyExists(ThisField, "Wrap") AND ThisField.Wrap AND IsNumeric(ThisField.Max) AND IsNumeric(ThisField.Min) ) 
			Wrap = "true";
		else
			Wrap = "false";
		
		if ( StructKeyExists(ThisField, "noStyle") AND ThisField.noStyle ) {
			styles.terraform_spinedit			 = "";
			styles.terraform_spinedit_widget 	 = "";
			styles.terraform_spinedit_up	 	 = "";
			styles.terraform_spinedit_down	 	 = "";
		}
		else {
			styles.terraform_spinedit			 = " style=""display : inline-block""";
			styles.terraform_spinedit_widget	 = " style=""float : left; position : relative""";
			styles.terraform_spinedit_up	 	 = " style=""position : absolute; right : 2px; top : 3px""";
			styles.terraform_spinedit_down	 	 = " style=""position : absolute; right : 2px; bottom : 3px""";
		}
			
		/*
		if ( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
			if ( (not compareNoCase(request.terraform.browser.platform, "Mac")) ) {
				toppadding = 0;
				inputWidth = inputWidth - 4;
			}
			else
				toppadding = 0;	
		if ( ((not compareNoCase(request.terraform.browser.platform, "Mac"))) and (request.terraform.browser.engine eq "Gecko") )	
			toppadding = 1;
		
		if ( attributes.xhtml and ((not compareNoCase(request.terraform.browser.platform, "Windows"))) and (request.terraform.browser.engine eq "Gecko") )	{
			toppadding = -1;
			inputWidth = inputWidth - 3;
		}
		
		if ( NOT StructKeyExists(ThisField, "style") ) 
			ThisField.style = "";
		ThisField.style = listappend(ThisField.style, "width: #inputWidth#px; margin: 0px;", ";");
		*/
		
		ignoreAttributes.Step = "";
		ignoreAttributes.onchange = "";
		ignoreAttributes.Wrap = "";
		ignoreAttributes.noStyle = "";

		if ( NOT StructKeyExists(ThisField, "onchange") ) 
			ThisField.onchange = "";
		else {
			thisfield.onchange = trim(ThisField.onchange);
			if ( len(thisField.onchange) and (right(thisfield.onchange, 1) neq ";") )
				thisField.onchange = thisField.onchange & ";";
		}
		if ( NOT StructKeyExists(ThisField, "Step") ) 
			ThisField.Step = 1;
		if ( NOT Len(Min) )
			Min = "NaN";
		if ( NOT Len(Max) )
			Max = "NaN";
		onClickAdd = "terraform_spinedit_addVal('#ThisField.id#', #ThisField.Step#, #Min#, #Max#, #Wrap#); #ThisField.onchange# return false;";
		onClickSub = "terraform_spinedit_addVal('#ThisField.id#', -(#ThisField.Step#), #Min#, #Max#, #Wrap#); #ThisField.onchange# return false;";
		onMouseWheel = "terraform_spinedit_addVal('#ThisField.id#', event.wheelDelta * #ThisField.Step# / 120, #Min#, #Max#, #Wrap#);#ThisField.onchange#";		
		
		//if ( not ( (not compareNoCase(request.terraform.browser.platform, "Mac")) and (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) ) )
		//	tableStyle = "display: inline;";	
			
		if ( structkeyexists(thisfield, "disabled") ) {
			disabledButtonsDisplay = "inline";
			enabledButtonsDisplay = "none";
		}			
		
		htmlblock="<div class=""terraform_spinedit""#styles.terraform_spinedit#><div class=""styles.terraform_spinedit_widget""#styles.terraform_spinedit_widget#><input #PASSTHROUGH# onchange=""#ThisField.onchange#"" type=""text"" name=""#ThisField.Name#"" value=""#InputFieldFormat(ThisField.RawValue)#""#closeTag#<div class=""styles.terraform_spinedit_up""#styles.terraform_spinedit_up#><img id=""#ThisField.id#_upDisabled"" style=""margin:0;border-width:0;display:#disabledButtonsDisplay#"" src=""#Attributes.ResourcePath#formats/spinedit/up_disabled.gif"" width=""15"" height=""8"" alt=""""#closeTag#<a onmousedown=""document.getElementById('#thisField.id#_up').src='#Attributes.ResourcePath#formats/spinedit/up_down.gif'; return false"" onmouseover=""document.getElementById('#thisField.id#_up').src='#Attributes.ResourcePath#formats/spinedit/up_over.gif'; return false"" onmouseout=""document.getElementById('#thisField.id#_up').src='#Attributes.ResourcePath#formats/spinedit/up.gif'; return false""  onmouseup=""document.getElementById('#thisField.id#_up').src='#Attributes.ResourcePath#formats/spinedit/up_over.gif'; return false"" onmousewheel=""#onMouseWheel#"" ondblclick=""#onClickAdd#"" onclick=""#onClickAdd#"" href=""##""><img id=""#ThisField.id#_up"" style=""margin:0;border-width:0;display:#enabledButtonsDisplay#"" src=""#Attributes.ResourcePath#formats/spinedit/up.gif"" width=""15"" height=""8"" alt=""""#closeTag#</a></div><div class=""styles.terraform_spinedit_down""#styles.terraform_spinedit_down#><img id=""#ThisField.id#_downDisabled"" style=""margin:0;border-width:0;display:#disabledButtonsDisplay#"" src=""#Attributes.ResourcePath#formats/spinedit/down_disabled.gif"" width=""15"" height=""8"" alt=""""#closeTag#<a onmousedown=""document.getElementById('#thisField.id#_down').src='#Attributes.ResourcePath#formats/spinedit/down_down.gif'; return false"" onmouseover=""document.getElementById('#thisField.id#_down').src='#Attributes.ResourcePath#formats/spinedit/down_over.gif'; return false"" onmouseout=""document.getElementById('#thisField.id#_down').src='#Attributes.ResourcePath#formats/spinedit/down.gif'; return false""  onmouseup=""document.getElementById('#thisField.id#_down').src='#Attributes.ResourcePath#formats/spinedit/down_over.gif'; return false"" onmousewheel=""#onMouseWheel#"" ondblclick=""#onClickSub#"" onclick=""#onClickSub#"" href=""##""><img id=""#ThisField.id#_down"" style=""margin:0;border-width:0;display:#enabledButtonsDisplay#"" src=""#Attributes.ResourcePath#formats/spinedit/down.gif"" width=""15"" height=""8"" alt=""""#closeTag#</a></div></div><div>#ESC#*</div></div>";

		if ( NOT structkeyexists(request.terraform.flags, "SpinEdit") ) {
			request.terraform.flags.SpinEdit = true;
			request.terraform.HTMLHead = request.terraform.HTMLHead & "<script src=""#Attributes.ResourcePath#formats/spinedit/spinedit.js?#request.eswsoftware.terraform#"" type=""text/javascript""></script>";
		} 
		
		return HTMLBlock;
	}

	// End Format: SPINEDIT 
	// *************************************************************************	
	


	// *************************************************************************
	// Format:		 STARS 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version: 	 20090209
	// Dependencies: radio
	// Notes:-
	// Stars creates a row of stars that you can click to generate a number in a
	// range. This is intended to be used in situations where you might be
	// expected to supply a star rating for something such as a movie.
	// History:		 20090209 removed hspace, vspace and border for HTML strict support
	//				 20090127 added src attributes: brightsrc, darksrc, brightsrc_disabled, darksrc_disabled
	//				 20060502 added version suffix to script path
	//				 20060228 was not resetting properly when nothing selected
	//				 20040806 modified to allow disabling
	//			     20040527 now uses supplied id
	//				 20040301 Created
	// Customisations:-
	//
	
	Preset("Format_stars", "Datatype", "Integer");
	Preset("Format_stars", "min", 1);
	Preset("Format_stars", "max", 5);
	Preset("Format_stars", "brightsrc", "");
	Preset("Format_stars", "darksrc", "");
	Preset("Format_stars", "brightsrc_disabled", "");
	Preset("Format_stars", "darksrc_disabled", "");

	function starsFormatDegrade() {
		return "stars";
	}
	
	function starsFormat(ThisField) {
		var HTMLBlock = "";
		var i = 0;
		var path = "#Attributes.ResourcePath#formats/stars/";
		var src = "";
		var disabledStarsDisplay = "none";
		var enabledStarsDisplay = "inline";
		var disabledBlock = "";
		var brightSrc = path & "starhighlight.gif";
		var darkSrc = path & "star.gif";
		var brightSrc_disabled = path & "starhighlight_disabled.gif";
		var darkSrc_disabled = path & "star_disabled.gif";
		
		ignoreAttributes.brightsrc = "";
		ignoreAttributes.darksrc = "";
		ignoreAttributes.brightsrc_disabled = "";
		ignoreAttributes.darksrc_disabled = "";
		
		if ( len(thisField.brightsrc) )
			brightSrc = thisField.brightsrc;
		if ( len(thisField.darkSrc) )
			darkSrc = thisField.darkSrc;
		if ( len(thisField.brightSrc_disabled) )
			brightSrc_disabled = thisField.brightSrc_disabled;
		if ( len(thisField.darkSrc_disabled) )
			darkSrc_disabled = thisField.darkSrc_disabled;

		if ( structkeyexists(thisfield, "disabled") ) {
			disabledStarsDisplay = "inline";
			enabledStarsDisplay = "none";
		}	
		
		thisField.focusTo = "#thisField.id#_link_1";
		
		for ( i = 1; i lte thisField.max; i = i + 1 ) {
			if ( i lte thisField.value ) {
				HTMLBlock = HTMLBlock & "<a id=""#ThisField.id#_link_#i#"" href=""##"" onmouseout=""terraform_stars_reset('#thisField.id#', #thisField.max#, '#brightsrc#', '#darksrc#', '#brightsrc_disabled#', '#darksrc_disabled#');return false;"" onclick=""terraform_stars_update('#thisField.id#', #i#, #thisField.max#, '#brightsrc#', '#darksrc#', '#brightsrc_disabled#', '#darksrc_disabled#');return false;"" onmouseover=""terraform_stars_pointTo('#thisField.id#', #i#, #thisField.max#, '#brightsrc#', '#darksrc#', '#brightsrc_disabled#', '#darksrc_disabled#');return false;""><img src=""#brightSrc#"" alt="""" id=""#ThisField.id#_#i#"" style=""border-width:0;margin:0""#closeTag#</a>";
				disabledBlock = disabledBlock & "<img id=""#ThisField.id#_disabled_#i#"" src=""#brightSrc_disabled#"" alt="""" style=""border-width:0;margin:0""#closeTag#";
			}
			else {
				HTMLBlock = HTMLBlock & "<a id=""#ThisField.id#_link_#i#"" href=""##"" onmouseout=""terraform_stars_reset('#thisField.id#', #thisField.max#, '#brightsrc#', '#darksrc#', '#brightsrc_disabled#', '#darksrc_disabled#');return false;"" onclick=""terraform_stars_update('#thisField.id#', #i#, #thisField.max#, '#brightsrc#', '#darksrc#', '#brightsrc_disabled#', '#darksrc_disabled#');return false;"" onmouseover=""terraform_stars_pointTo('#thisField.id#', #i#, #thisField.max#, '#brightsrc#', '#darksrc#', '#brightsrc_disabled#', '#darksrc_disabled#');return false;""><img src=""#darkSrc#"" alt="""" id=""#ThisField.id#_#i#"" style=""border-width:0;margin:0""#closeTag#</a>";
				disabledBlock = disabledBlock & "<img id=""#ThisField.id#_disabled_#i#"" src=""#darkSrc_disabled#"" alt="""" style=""border-width:0;margin:0""#closeTag#";
			}

		}
		HTMLBlock = "<input type=""hidden"" name=""#ThisField.Name#"" #PASSTHROUGH# value=""#InputFieldFormat(ThisField.RawValue)#""#closeTag#<span id=""#ThisField.id#_enabledStars"" style=""display:#enabledStarsDisplay#"">#HTMLBlock#</span><span id=""#ThisField.id#_disabledStars"" style=""display:#disabledStarsDisplay#"">#disabledBlock#</span>";
		
		if ( NOT structkeyexists(request.terraform.flags, "stars") ) {
			request.terraform.flags.stars = true;
			request.terraform.HTMLHead = request.terraform.HTMLHead & "<script src=""#Attributes.ResourcePath#formats/stars/stars.js?#request.eswsoftware.terraform#"" type=""text/javascript""></script>";
		}
		
		return HTMLBlock;
	}

	// End Format: STARS 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// Format:		 SUBMIT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20060313
	// Dependencies: 
	// Notes:-
	// SUBMIT creates a submission button. You can specify a "WaitMessage" to
	// display once the form is submitted.
	//
	// History:		 20060313 switched to caption_label
	//				 20031127 removed name.value behaviour - buggy
	//				 20031119 small bugfix for MSIE/Mac
	//				 20031102 made XHTML tag closure dependent on XHTML attribute
	//				 20031031 switched from attributesList to ignoreAttributes
	//				 20030511 added submitFormatDegrade()
	//				 20030123 Added InputFieldFormat()
	//				 20020620 added default datatype
	//				 20020607 bugfix for CFMX
	// 				 20020524 Created
	// Customisations:-
	//
	
	Preset("Format_Submit", "Datatype", "string");
	Preset("Format_Submit", "WaitMessage", "Please wait . . .");
		
	function submitFormatDegrade() {
		return "submit";
	}
			
	function SubmitFormat(ThisField) {
		var HTMLBlock = "#PASSTHROUGH# type=""#ThisField.Format#"" value=""#ThisField.caption_label#""";
		ignoreAttributes.WaitMessage = "";
		
		HTMLBlock = HTMLBlock & " name=""#ThisField.Name#""";
		HTMLBlock = "<input #HTMLBlock##closeTag#";
		if ( 
			Attributes.HideButtonsOnSubmit and (
				( 
					( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) ) 
					and ( request.terraform.browser.platform eq "Windows" )
				) or ( request.terraform.browser.engine eq "Gecko" )
			)
		)
			HTMLBlock = HTMLBlock & "<input type=""button"" name=""#ThisField.Name#_hidden"" disabled=""disabled"" value=""#ThisField.WaitMessage#"" style=""display:none""#closeTag#";

		return HTMLBlock;
	}

	// End Format: SUBMIT 
	// *************************************************************************	
	
	

	// *************************************************************************
	// Format:		 SUBMITBUTTON 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20090209
	// Dependencies: submit
	// Notes:-
	// SUBMITBUTTON creates a submission button using <button type="submit"> 
	// rather than the regular <input type="submit">. 
	// <button> is more powerful than <input>, allowing you to use HTML markup
	// in the caption, including, for example, font styles and images. 
	//
	// You can specify a "WaitMessage" to display once the form is submitted.
	//
	// History:		 20090209 duplicate IDs were being generated
	//				 20090209 removed hourglass from wait button
	//				 20060313 switched to caption_label
	//				 20050324 noprocessing attribute added to ignoreAttributes
	//			     20040527 now uses supplied id
	//			     20031127 substantial rewrite to better manage MSIE all buttons submitted bug
	//				 20031119 small bugfix for MSIE/Mac
	//				 20031102 made XHTML tag closure dependent on XHTML attribute
	//				 20031031 switched from attributesList to ignoreAttributes
	//				 20030511 added submitFormatDegrade()
	//				 20030123 Added InputFieldFormat()
	//				 20020629 Added icon to button waitmessage
	// 				 20020620 added default datatype
	//				 20020610 tweaked
	//				 20020607 bugfix for CFMX
	// 				 20020604 Created
	// Customisations:-
	//
	
	Preset("Format_SubmitButton", "Datatype", "Default");
	Preset("Format_SubmitButton", "NoProcessing", true);
	
	Preset("Format_SubmitButton", "WaitMessage", "Please wait . . .");

	function submitButtonFormatDegrade() {
		if ( 
			( (not compareNoCase(request.terraform.browser.engine, "Internet Explorer")) )
			or ( request.terraform.browser.engine eq "Gecko" )
		)
			return "submitbutton";
		ThisField.caption_label = stripHtml(ThisField.caption_label);	
		return submitFormatDegrade();
	}
		
	function SubmitButtonFormat(ThisField) {
		var HTMLBlock = "#PASSTHROUGH# type=""submit""";
		
		ignoreAttributes.WaitMessage = "";
		ignoreAttributes.NoProcessing = "";
		ignoreAttributes.id = "";
		
		if ( not structKeyExists(thisField, "onClick") )
			thisField.onClick = "";
		thisField.onClick = listAppend(thisField.onClick, "document.getElementById('#thisField.id#_hidden').name = '#thisField.name#'; document.getElementById('#thisField.id#_hidden').value = '#jsStringFormat(thisField.value)#'", ";");

		HTMLBlock = "<button id=""#thisField.id#"" #HTMLBlock#>#ThisField.caption_label#</button><input type=""hidden"" id=""#ThisField.id#_hidden"">";

		if ( 
			Attributes.HideButtonsOnSubmit and (
				request.terraform.browser.engine neq "Internet Explorer" 
				or request.terraform.browser.platform eq "Windows"
			)
		)
			HTMLBlock = HTMLBlock & "<button type=""button"" #PASSTHROUGH# name=""#ThisField.Name#_hidden"" id=""#ThisField.id#_disabled"" disabled=""disabled"" style=""display:none"">#ThisField.WaitMessage#</button>";
		
		return HTMLBlock;
	}

	// End Format: SUBMITBUTTON 
	// *************************************************************************	
	

		
	// *************************************************************************
	// Format:		 TEXT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20040803
	// Dependencies: 
	// Notes:-
	// TEXT builds a regular text field. Now supports the Prompt attribute - 
	// a message that appears when the field is empty and disappears when the
	// field receives focus. The code appends the variable thisField.OnFocus (creating
	// it if necessary) so does not disrupt any other JS that might be attached to
	// this event. 
	// History:		 20040803 added disabled support
	//				 20040625 key filtering now ignores ENTER
	// 				 20031102 made XHTML tag closure dependent on XHTML attribute
	//				 20030526 added key filter
	//				 20030511 added textFormatDegrade()
	//				 20030123 Added InputFieldFormat()
	//				 20020623 Added AutoAdvanceTo
	//				 20020620 added default datatype
	//				 20020616 bugfix for CFMX
	// 				 20020531 Added Prompt attribute
	//				 20020318 Created	
	// Customisations:-
	//
	
	Preset("Format_Text", "Datatype", "String");
	
	function textFormatDegrade() {
		return "text";
	}
	
	function TextFormat(ThisField) {
		var HTMLBlock = "#PASSTHROUGH# type=""#ThisField.Format#"" name=""#ThisField.Name#""";
					
		// key filter
		if ( structKeyExists(thisField, "jsMask") and len(thisField.jsMask) ) {
			if ( not structKeyExists(thisField, "onkeypress") )
				thisfield.onKeyPress = "";
			thisfield.onKeyPress = listAppend(thisfield.onKeyPress, "if (window.event.keyCode != 13) return ( String.fromCharCode(window.event.keyCode).search(#jsStringFormat(thisField.jsMask)#) > -1 )", ";");
		}
		
		if ( Len(ThisField.RawValue) )
			HTMLBlock = HTMLBlock & " value=""#InputFieldFormat(ThisField.RawValue)#""";
		else
			if ( StructKeyExists(ThisField, "Prompt") ) 
				HTMLBlock = HTMLBlock & " value=""#HTMLEditFormat(ThisField.Prompt)#""";			
		if ( StructKeyExists(ThisField, "Prompt") ) {
			if ( NOT StructKeyExists(ThisField, "OnFocus") )
				ThisField.OnFocus = "";
			ThisField.OnFocus = ListAppend(ThisField.OnFocus, "if ( this.value == '#JSStringFormat(ThisField.Prompt)#' ) this.value=''", ";");	
		}
		if ( StructKeyExists(ThisField, "AutoAdvanceTo") AND StructKeyExists(ThisField, "MaxLength") AND Len(ThisField.AutoAdvanceTo) AND Len(ThisField.MaxLength) ) {
			if ( NOT StructKeyExists(ThisField, "OnKeyUp") )
				ThisField.OnKeyUp = "";
			ThisField.OnKeyUp = ListAppend(ThisField.OnKeyUp, "if ( this.value.length == #ThisField.MaxLength# ) document.#Attributes.FormName#['#ThisField.AutoAdvanceTo#'].focus()", ";");	
		}					
		if ( ThisField.MaxLength )
			HTMLBlock = HTMLBlock & " maxlength=""#ThisField.MaxLength#""";
		HTMLBlock = "<input #HTMLBlock##closeTag#";
		return HTMLBlock;
	}
	
	// End Format: TEXT 
	// *************************************************************************


	
	// *************************************************************************
	// Format:		 TEXTAREA 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20091204
	// Dependencies: 
	// Notes:-
	// TEXTAREA builds a textarea field. Now supports the Prompt attribute - 
	// a message that appears when the field is empty and disappears when the
	// field receives focus. Also supports an optional character counter,
	// counting down from the  maximum number of characters (maxlength). 
	// History:		 20091204 catches onMouseMove as it was possible to drag and paste large content in
	//				 20040527 now uses supplied id
	//				 20031102 made XHTML tag closure dependent on XHTML attribute
	//				 20031031 switched from attributesList to ignoreAttributes
	//				 20031015 removed table used for layout
	//				 20030908 bugfix with counter where multiple fields 
	//				 	 	  (thanks Greg Walker)
	//				 20030807 added required flag placeholder
	//				 20030716 altered to improve positioning of required asterisk
	//				 20030522 changed this.innerText to this.value
	//						  - prompt now disappears on Opera and Netscape
	//						  added id to counter and remove right align style for netscape
	//						  - seems to screw it up
	//				 20030511 added textFormatDegrade()
	//				 20030319 bugfix - this.form was thisfield.form
	//				 20030123 Added InputFieldFormat()
	//				 20021205 CSE HTML validator compliance tweaks
	//				 20020829 bugfix - was overwriting request.terraform.HTMLHead
	//				 20020819 added character counter
	//				 20020620 added default datatype
	//				 20020616 bugfix for CFMX
	//				 20020531 Added Prompt attribute
	//				 20020318 Created	
	// Customisations:-
	//
	
	Preset("Format_TextArea", "Datatype", "String");
	Preset("Format_TextArea", "Counter", true);
	Preset("Format_TextArea", "CounterCaption", "<span style=""color : ##808080; font-size : 10px"">Characters remaining: </span>");
	Preset("Format_TextArea", "style", "vertical-align : top");
	Preset("Format_TextArea", "rows", 4);
	Preset("Format_TextArea", "cols", 20);
	
	function textAreaFormatDegrade() {
		if ( request.terraform.browser.engine eq "Netscape" ) 
			thisField.counter = false;
		return "textArea";
	}
		
	function TextAreaFormat(ThisField) {
		var HTMLBlock = "<textarea #PASSTHROUGH# name=""#ThisField.Name#"">";
		var InitialValue = 0;
		var counterFlags = " disabled readonly";
		
		if ( attributes.xhtml ) 
			counterFlags = " disabled=""disabled"" readonly=""readonly""";
		
		ignoreAttributes.Counter = "";
		ignoreAttributes.CounterCaption = "";

		if ( Len(ThisField.RawValue) )
			HTMLBlock = HTMLBlock & InputFieldFormat(ThisField.RawValue);
		else
			if ( StructKeyExists(ThisField, "Prompt") ) 
				HTMLBlock = HTMLBlock & HTMLEditFormat(ThisField.Prompt);			
		if ( StructKeyExists(ThisField, "Prompt") ) {
			if ( NOT StructKeyExists(ThisField, "OnFocus") )
				ThisField.OnFocus = "";
			ThisField.OnFocus = ListAppend(ThisField.OnFocus, "if ( this.value == '#JSStringFormat(thisField.Prompt)#' ) this.value=''", ";");	
		}	
				
		HTMLBlock = HTMLBlock & "</textarea>#ESC#*";
		if ( ThisField.Counter and Val(ThisField.MaxLength) ) {
			InitialValue = ThisField.MaxLength - len(ThisField.RawValue);
			if ( not StructKeyExists(ThisField, "onMouseMove") )
				ThisField.onMouseMove = "";
			ThisField.onMouseMove=ListAppend(ThisField.onMouseMove, "if ( this.value.length > #thisField.maxLength# ) this.value = this.value.substring(0, #thisField.maxLength#); document.getElementById('#thisField.id#_count').value = #thisField.maxLength# - this.value.length;", ";");
			if ( not StructKeyExists(ThisField, "onKeyDown") )
				ThisField.onKeyDown = "";
			ThisField.onKeyDown=ListAppend(ThisField.onKeyDown, "if ( this.value.length > #thisField.maxLength# ) this.value = this.value.substring(0, #thisField.maxLength#); document.getElementById('#thisField.id#_count').value = #thisField.maxLength# - this.value.length;", ";");
			if ( not StructKeyExists(ThisField, "onKeyUp") )
				ThisField.onKeyUp = "";
			ThisField.onKeyUp=ListAppend(ThisField.onKeyUp, "if ( this.value.length > #thisField.maxLength# ) this.value = this.value.substring(0, #thisField.maxLength#); document.getElementById('#thisField.id#_count').value = #thisField.maxLength# - this.value.length;", ";");
			HTMLBlock = HTMLBlock & "<br#closeTag##ThisField.CounterCaption#<input style=""font-size : 10px";
			if ( request.terraform.browser.engine neq "Gecko" )
				HTMLBlock = HTMLBlock & "; text-align : right";
			HTMLBlock = HTMLBlock & """ value=""#InitialValue#"" size=""3"" type=""text"" name=""#ThisField.Name#_count"" class=""terraForm_textarea_count"" id=""#ThisField.id#_count""#counterFlags##closeTag#";
		}	
		return HTMLBlock;
	}

	// End Format: TEXTAREA
	// *************************************************************************			


		
	// *************************************************************************
	// Format:		 CKEDITOR 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 
	// Dependencies: 
	// Notes:-
	// Customisations:-
	//
	
	Preset("Format_ckeditor", "Datatype", "String");
	
	function ckeditorFormatDegrade() {
		return "ckeditor";
	}
		
	function ckeditorFormat(ThisField) {
		var HTMLBlock = "<textarea #PASSTHROUGH# name=""#ThisField.Name#"">";
		addToHTMLHead('<script src="/whio/helpers/ckeditor/ckeditor.js"></script>');
		if ( Len(ThisField.RawValue) )
			HTMLBlock = HTMLBlock & InputFieldFormat(ThisField.RawValue);		
		HTMLBlock &= "</textarea>#ESC#*<script>CKEDITOR.replace( '#ThisField.Name#', {toolbar: [
		    { name: 'document',    items : [ 'Maximize', 'ShowBlocks','Source' ] },
		    { name: 'clipboard',   items : [ 'SelectAll','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		    { name: 'editing',     items : [ 'Find','Replace','-','SpellChecker', 'Scayt' ] },
		    { name: 'basicstyles', items : [ 'Format','RemoveFormat','-','Bold','Italic','Strike','Subscript','Superscript' ] },
		    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote' ] },
		    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
		    { name: 'insert',      items : [ 'Table','HorizontalRule','SpecialChar' ] }
		],scayt_autoStartup:true,width:400,height:600});</script>";
			
		return HTMLBlock;
	}

	// End Format: TEXTAREA
	// *************************************************************************		// *************************************************************************
	// Format:		 TIMEPICKER 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20050809
	// Dependencies: 
	// Notes:-
	// History:		 20050809 added disabled support
	//				 20040205 added values to options to enable qForms to function
	//				 20031125 now rounds to nearest interval rather than rounding down
	//				 20031031 switched from attributesList to ignoreAttributes
	//				 20030928 fixed typo secondprompt -> secondsprompt
	//				 20030716 updated to support null value
	//				 20030511 added timePickerFormatDegrade()
	//				 20030205 Created	
	// Customisations:-
	//
	
	Preset("Format_TimePicker", "Datatype", "Time");
	Preset("Format_TimePicker", "minuteInterval", 1);
	Preset("Format_TimePicker", "minutes", true);
	Preset("Format_TimePicker", "hoursPrompt", "H");
	Preset("Format_TimePicker", "minutesPrompt", "M");
	Preset("Format_TimePicker", "secondsPrompt", "S"); 
	Preset("Format_TimePicker", "seconds", false);
	Preset("Format_TimePicker", "secondInterval", 1);
	Preset("Format_TimePicker", "twentyfourhour", ""); 
	// twentyfourhour can be true, false or blank (""). If blank, 
	// TerraForm will choose 12 hour or 24 hour based on the locale.
	
	function timePickerFormatDegrade() {
		return "timePicker";
	}
	
	function timePickerFormat(ThisField) {
	
		var htmlBlock = "";
		var i = 0;
		var section = "";
		var minHour = 1;
		var maxHour = 12;
		var hourFormat = "__";
		var valueHours = "";
		var valueMinutes = 0;
		var valueSeconds = 0;
		var valueAMPM = "AM";
		var option = "";
		var nearestAvailableMinute = "";
		var nearestAvailableSecond = "";
		var internationalisedValue = internationaliseTime(thisField.value);
		var thisLocale = request.terraform.locale;
		
		if ( isDate(internationalisedValue) ) {
			valueHours = hour(internationalisedValue);
			valueMinutes = minute(internationalisedValue);
			nearestAvailableMinute = round(valueMinutes / thisField.minuteInterval) * thisField.minuteInterval;
			valueSeconds = second(internationalisedValue);
			nearestAvailableSecond = round(valueSeconds / thisField.secondInterval) * thisField.secondInterval;
			if ( valueHours gte 12 )
				valueAMPM = "PM";
		}
		
		if ( not isBoolean(thisField.twentyFourHour) )
			thisField.twentyFourHour = request.terraform.locale.twentyFourHour;
		
		ignoreAttributes.id = "";
		ignoreAttributes.minuteInterval = "";
		ignoreAttributes.minutes = "";
		ignoreAttributes.seconds = "";
		ignoreAttributes.secondInterval = "";
		ignoreAttributes.twentyfourhour = "";
		ignoreAttributes.hoursPrompt = "";
		ignoreAttributes.minutesPrompt = "";
		ignoreAttributes.secondsPrompt = "";

		HTMLBlock = HTMLBlock & "<span id=""#ThisField.id#""></span>";
		
		thisField.focusTo = "#thisField.id#_hours";
		
		if ( thisField.twentyfourhour ) {
			minHour = 0;
			maxHour = 23;
			hourFormat="00";
		}
		else
			if ( len(valuehours) )
				valueHours = ((valueHours + 11) mod 12) + 1;

		// build hours
		for ( i = minHour ; i lte maxHour; i = i + 1 ) {
			if ( valueHours eq i )
				if ( attributes.xhtml )
					option = " selected=""selected""";
				else
					option = " selected";
			else
				option = "";
			option = "<option value=""#i#""#option#>#numberFormat(i, hourFormat)#</option>";
			section = section & option;
		}	
		htmlBlock = htmlBlock & "<select id=""#ThisField.id#_hours"" #PASSTHROUGH# name=""#thisfield.name#_hours"">";
		if ( len(thisfield.hoursPrompt) )
			htmlBlock = htmlBlock & "<option value="""">#thisfield.hoursPrompt#</option>";
		htmlBlock = htmlBlock & "#section#</select>";
		
		if ( thisField.minutes ) {
			// build minutes
			section = "";
			for ( i = 0 ; i lte 59; i = i + thisField.minuteInterval ) {
				if ( nearestAvailableMinute eq i )
					if ( attributes.xhtml )
						option = " selected=""selected""";
					else
						option = " selected";
				else
					option = "";
				option = "<option value=""#i#""#option#>#numberFormat(i, "00")#</option>";					
				section = section & option;
			}	
			htmlBlock = htmlBlock & " <strong>:</strong> <select id=""#ThisField.id#_minutes"" #PASSTHROUGH# name=""#thisfield.name#_minutes"">";
			if ( len(thisField.minutesPrompt) )
				htmlBlock = htmlBlock & "<option value="""">#thisfield.minutesPrompt#</option>";
			htmlBlock = htmlBlock & "#section#</select>";
		}

		if ( thisField.minutes and thisField.seconds ) {
			// build seconds
			section = "";
			for ( i = 0 ; i lte 59; i = i + thisField.secondInterval ) {
				if ( nearestAvailableSecond eq i )
					if ( attributes.xhtml )
						option = " selected=""selected""";
					else
						option = " selected";
				else
					option = "";
				option = "<option value=""#i#""#option#>#numberFormat(i, "00")#</option>";
				section = section & option;
			}	
			htmlBlock = htmlBlock & " <strong>:</strong> <select id=""#ThisField.id#_seconds"" #PASSTHROUGH# name=""#thisfield.name#_seconds"">";
			if ( len(thisField.secondsPrompt) )	
				htmlBlock = htmlBlock & "<option value="""">#thisfield.secondsprompt#</option>";
			htmlBlock = htmlBlock & "#section#</select>";
		}
		
		if ( not thisField.twentyfourhour ) {
			// build am/pm
			section = "<option value=""#thisLocale.am#"">#thisLocale.am#</option>";
			if ( valueAMPM eq "PM" )
				if ( attributes.xhtml )
					option = " selected=""selected""";
				else
					option = " selected";
			else
				option = "";
			section = section & "<option value=""#thisLocale.pm#""#option#>#thisLocale.pm#</option>";
			htmlBlock = htmlBlock & " <select id=""#ThisField.id#_ampm"" #PASSTHROUGH# onfocus=""this.selectedIndex = Math.abs(1 - this.selectedIndex);"" name=""#thisfield.name#_ampm"">#section#</select>";
		}
		
		return htmlBlock;	
	}

	function TimePickerFormatClean(value) {
		var h = "";
		var m = "";
		var s = "";
		var t = "";
		var result = "";
		var thescope = evaluate(scope);
		if ( structKeyExists(thescope, thisfield.name) )
			return thescope[thisfield.name];
		if ( structKeyExists(thescope, "#thisfield.name#_hours") )
			h = thescope["#thisfield.name#_hours"];
		if ( structKeyExists(thescope, "#thisfield.name#_minutes") )
			m = thescope["#thisfield.name#_minutes"];
		if ( structKeyExists(thescope, "#thisfield.name#_seconds") )
			s = thescope["#thisfield.name#_seconds"];
		if ( structKeyExists(thescope, "#thisfield.name#_ampm") )
			t = thescope["#thisfield.name#_ampm"];
		if ( len(h & m & s) )
			result = "#val(h)#:#val(m)#:#val(s)# #t#";
		return result;
	}		
	
	// End Format: TIMEPICKER 
	// *************************************************************************

	
	
	// *************************************************************************
	// Format:					  TIMESPANPICKER 
	// Author:					  Matthew Walker (esw@eswsoftware.com)
	// Version:					  20090730
	// Dependencies: 
	// Notes:-
	// History:					  20090730 defaults were not working
	//							  20060418 Created	
	// Customisations:-
	//
	
	Preset("Format_TimeSpanPicker", "Datatype", "real");
	Preset("Format_TimeSpanPicker", "days", false);
	Preset("Format_TimeSpanPicker", "daysPrompt", "D");
	Preset("Format_TimeSpanPicker", "daysMax", 99);
	Preset("Format_TimeSpanPicker", "hours", true);
	Preset("Format_TimeSpanPicker", "hoursPrompt", "H");
	Preset("Format_TimeSpanPicker", "hoursMax", 99);
	Preset("Format_TimeSpanPicker", "minutes", true);
	Preset("Format_TimeSpanPicker", "minutesPrompt", "M");
	Preset("Format_TimeSpanPicker", "minutesMax", 59);
	Preset("Format_TimeSpanPicker", "secondsPrompt", "S"); 
	Preset("Format_TimeSpanPicker", "seconds", false);
	Preset("Format_TimeSpanPicker", "secondsMax", 59);
	Preset("Format_TimeSpanPicker", "baseUnit", "d"); // d | h | n | s
	
	function timeSpanPickerFormatDegrade() {
		return "timeSpanPicker";
	}
	
	function timeSpanPickerFormat(ThisField) {
			
		var htmlBlock = "";	
		var valueDays = "";
		var valueHours = "";
		var valueMinutes = "";
		var valueSeconds = "";
		var section = "";
		var option = "";
		var i = 0;
		var rawValue = val(thisField.value);
		var totalSeconds = 0;
		var first = true;
			 
		if ( thisField.days )
			thisField.focusTo = "#thisField.id#_days";					   
		else
			if ( thisField.hours )
				thisField.focusTo = "#thisField.id#_hours";					  
			else
				if ( thisField.minutes ) 
					thisField.focusTo = "#thisField.id#_minutes";	   
				else
					thisField.focusTo = "#thisField.id#_seconds";	
						
		if ( len(thisField.value) ) {
			switch(thisField.baseUnit){
				case "d": {
					totalSeconds = round(rawValue * 86400);
					break;
				}
			
				case "h": {
					totalSeconds = round(rawValue * 3600);
					break;
				}		   
			
				case "n": {
					totalSeconds = round(rawValue * 60);
					break;
				}
			
				case "s": {
					totalSeconds = round(rawValue);
					break;
				}
			}			 
			
			if ( thisField.days ) {
				valueDays = min(totalSeconds \ 86400, thisField.daysMax);
				totalSeconds = totalSeconds - valueDays * 86400;
			}
			
			if ( thisField.hours ) {
				valueHours = min(totalSeconds \ 3600, thisField.hoursMax);
				totalSeconds = totalSeconds - valueHours * 3600;
			}
			
			if ( thisField.minutes ) {
				valueMinutes = min(totalSeconds \ 60, thisField.minutesMax);
				totalSeconds = totalSeconds - valueMinutes * 60;
			}
			
			if ( thisField.seconds ) {
				valueSeconds = min(totalSeconds, thisField.secondsMax);
			}
 		}
		else {
			valueDays = "";
			valueHours = "";
			valueMinutes = "";
			valueSeconds = "";
		}
		
		ignoreAttributes.days = "";	
		ignoreAttributes.daysPrompt = "";	
		ignoreAttributes.daysMax = "";
		ignoreAttributes.hours = "";
		ignoreAttributes.hoursPrompt = "";
		ignoreAttributes.hoursMax = "";
		ignoreAttributes.id = "";
		ignoreAttributes.minutes = "";
		ignoreAttributes.minutesPrompt = "";
		ignoreAttributes.minutesMax = "";
		ignoreAttributes.seconds = "";
		ignoreAttributes.secondsPrompt = "";
		ignoreAttributes.secondsMax = "";
		ignoreAttributes.baseUnit = "";
 
		htmlBlock = "<span id=""#ThisField.id#""></span><input style=""display:none"" id=""#ThisField.id#_baseUnit"" value=""#thisField.baseUnit#""#closeTag#";
 
		if ( thisField.days ) {
			// build days
			section = "";
			for ( i = 0 ; i lte thisField.daysMax; i = i + 1 ) {
				if ( valueDays eq i )
					if ( attributes.xhtml )
						option = " selected=""selected""";
					else
						option = " selected";
				else
					option = "";
				option = "<option value=""#i#""#option#>#numberFormat(i, "00")#</option>";													  
				section = section & option;
			}		   
			if ( len(thisField.daysPrompt) )
				section = "<option value="""">#thisfield.daysPrompt#</option>" & section;
			section = "<select id=""#ThisField.id#_days"" #PASSTHROUGH# name=""#thisfield.name#_days"">" & section & "</select>";
			if ( not first )
				htmlBlock = htmlBlock & " <strong>:</strong> ";
			htmlBlock = htmlBlock & section;
			first = false;
		}
		
		if ( thisField.hours ) {
			// build hours
			section = "";
			for ( i = 0 ; i lte thisField.hoursMax; i = i + 1 ) {
				if ( valueHours eq i )
					if ( attributes.xhtml )
						option = " selected=""selected""";
					else
						option = " selected";
				else
					option = "";
				option = "<option value=""#i#""#option#>#numberFormat(i, "00")#</option>";													  
				section = section & option;
			}		   
			if ( len(thisField.hoursPrompt) )
				section = "<option value="""">#thisfield.hoursPrompt#</option>" & section;
			section = "<select id=""#ThisField.id#_hours"" #PASSTHROUGH# name=""#thisfield.name#_hours"">" & section & "</select>";
			if ( not first )
				htmlBlock = htmlBlock & " <strong>:</strong> ";
			htmlBlock = htmlBlock & section;
			first = false;
		}

		if ( thisField.minutes ) {
			// build minutes
			section = "";
			for ( i = 0 ; i lte thisField.minutesMax; i = i + 1 ) {
				if ( valueMinutes eq i )
					if ( attributes.xhtml )
						option = " selected=""selected""";
					else
						option = " selected";
				else
					option = "";
				option = "<option value=""#i#""#option#>#numberFormat(i, "00")#</option>";													  
				section = section & option;
			}		   
			if ( len(thisField.minutesPrompt) )
				section = "<option value="""">#thisfield.minutesPrompt#</option>" & section;
			section = "<select id=""#ThisField.id#_minutes"" #PASSTHROUGH# name=""#thisfield.name#_minutes"">" & section & "</select>";
			if ( not first )
				htmlBlock = htmlBlock & " <strong>:</strong> ";
			htmlBlock = htmlBlock & section;
			first = false;
		}

		if ( thisField.seconds ) {
			// build seconds
			section = "";
			for ( i = 0 ; i lte thisField.secondsMax; i = i + 1 ) {
				if ( valueSeconds eq i )
					if ( attributes.xhtml )
						option = " selected=""selected""";
					else
						option = " selected";
				else
					option = "";
				option = "<option value=""#i#""#option#>#numberFormat(i, "00")#</option>";													  
				section = section & option;
			}		   
			if ( len(thisField.secondsPrompt) )
				section = "<option value="""">#thisfield.secondsPrompt#</option>" & section;
			section = "<select id=""#ThisField.id#_seconds"" #PASSTHROUGH# name=""#thisfield.name#_seconds"">" & section & "</select>";
			if ( not first )
				htmlBlock = htmlBlock & " <strong>:</strong> ";
			htmlBlock = htmlBlock & section;
			first = false;
		}		
			
		return htmlBlock;		   
	}
 
	function TimeSpanPickerFormatClean(value) {	
		var d = "";
		var h = "";
		var m = "";
		var s = "";
		var totalSeconds = 0;
		var result = "";
		var thescope = evaluate(scope);	
		
		if ( structKeyExists(thescope, "#thisfield.name#_days") )
			totalSeconds = totalSeconds + val(thescope["#thisfield.name#_days"]) * 86400;
		if ( structKeyExists(thescope, "#thisfield.name#_hours") )
			totalSeconds = totalSeconds + val(thescope["#thisfield.name#_hours"]) * 3600;
		if ( structKeyExists(thescope, "#thisfield.name#_minutes") )
			totalSeconds = totalSeconds + val(thescope["#thisfield.name#_minutes"]) * 60;
		if ( structKeyExists(thescope, "#thisfield.name#_seconds") )
			totalSeconds = totalSeconds + val(thescope["#thisfield.name#_seconds"]);

		switch(thisField.baseUnit){
			case "d": {
				return totalSeconds / 86400;
			}
		
			case "h": {
				return totalSeconds / 3600;
			}		   
		
			case "n": {
				return totalSeconds / 60;
			}
		
			case "s": {
				return totalSeconds;
			}
		}
	}					   
	
	// End Format: TIMESPANPICKER 
	// *************************************************************************
	
	

	// *************************************************************************
	// Format:   URL 
	// Author:   Matthew Walker (esw@eswsoftware.com)
	// Version:   20060123
	// Dependencies: text
	// Notes:-
	// URL builds a text field designed to receive a URL. A button appears
	// alongside so you can test the URL.
	// History:   20060123 addProtocol attribute added
	//			  20050603 test button no longer inserts http:// into url
	//			  20040527 now uses supplied id
	//			  20031203 switched to getElementByUId()
	//			  20031102 made XHTML tag closure dependent on XHTML attribute
	//			  20031031 switched from attributesList to ignoreAttributes
	//			  20030604 fixed type attribute, tweaked for Opera
	//			  20030511 added urlFormatDegrade()
	//			  20030129 added buttonPassthrough
	//			  20030123 Added InputFieldFormat()
	//			  20020710 Created 
	// Customisations:-
	//
 
	Preset("Format_URL", "Datatype", "String");
	Preset("Format_URL", "ButtonCaption", "Test");
	Preset("Format_URL", "AddProtocol", false);
	if ( request.terraform.browser.engine eq "Opera" ) 
		Preset("Format_URL", "ButtonPassthrough", "style=""font-family: verdana,arial,geneva,sans-serif; font-size : 9px; padding: 5px;""");
	else 
		Preset("Format_URL", "ButtonPassthrough", "style=""font-size : 9px;""");

	function urlFormatDegrade() {
		return "text";
	}
	
	function URLFormat(ThisField) {
		var HTMLBlock = "#PASSTHROUGH# type=""text"" name=""#ThisField.Name#""";
		var buttonAttributes = "";
		ignoreAttributes.ButtonCaption = "";
		ignoreAttributes.ButtonPassthrough = "";
		ignoreAttributes.AddProtocol = "";
		if ( Len(ThisField.RawValue) )
			HTMLBlock = HTMLBlock & " value=""#InputFieldFormat(ThisField.RawValue)#""";
		else
			if ( StructKeyExists(ThisField, "Prompt") ) 
				HTMLBlock = HTMLBlock & " value=""#HTMLEditFormat(ThisField.Prompt)#""";   
		if ( StructKeyExists(ThisField, "Prompt") ) {
			if ( NOT StructKeyExists(ThisField, "OnFocus") )
				ThisField.OnFocus = "";
			ThisField.OnFocus = ListAppend(ThisField.OnFocus, "if ( this.value == '#JSStringFormat(ThisField.Prompt)#' ) this.value=''", ";"); 
  		}
  		if ( ThisField.MaxLength )
   			HTMLBlock = HTMLBlock & " maxlength=""#ThisField.MaxLength#""";
		
		HTMLBlock = "<input #HTMLBlock##closeTag#&nbsp;";
		buttonAttributes = "#thisfield.buttonPassthrough# class=""terraForm_URL_button"" id=""#ThisField.id#_button"" type=""button"" 
title=""Test this URL"" onclick=""var theUrl = document.getElementById('#thisField.id#').value;if ( theUrl.search(/^[a-z0-9]+:/i) != 0 ) if ( theUrl.search(/@/) > -1 ) theUrl = 'mailto:' + theUrl; else theUrl = 'http://' + theUrl; window.open(theUrl,'_blank','top=25,left=25,width=500,height=400,dependent,location,resizable,scrollbars');return false""";
		if ( request.terraform.browser.engine eq "Netscape" ) 
			HTMLBlock = HTMLBlock & "<a href=""##"" #buttonAttributes#>#ThisField.ButtonCaption#</a>";
		else
			HTMLBlock = HTMLBlock & "<button #buttonAttributes#>#ThisField.ButtonCaption#</button>";
		return HTMLBlock;
	}

	// End Format: URL
	// ************************************************************************* 

</cfscript>

 

<cffunction name="AddToHTMLHead" output="false" returnType="void">
    <cfargument name="text" type="string" required="yes">
    <cfhtmlhead text="#text#">
</cffunction>