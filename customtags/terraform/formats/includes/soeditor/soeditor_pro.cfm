<cfif thistag.executionmode eq "start">
	<cfoutput>
		<table border="1" cellspacing="0" cellpadding="10" bordercolor="##FF0000">
		
			<tr>
				<td>
					soEditor Pro is not installed properly. If you actually have soEditor Lite installed, use the <code>version="lite"</code> attribute, or alter the line <code>&lt;cfparam name="thisField.version" default="pro"&gt;</code> in <code>#listdeleteat(getdirectoryfrompath(getcurrenttemplatepath()), listlen(getdirectoryfrompath(getcurrenttemplatepath()), "\/"), "\/")#\soeditor.cfm</code> to read: <code>&lt;cfparam name="thisField.version" default="lite"&gt;</code>. Otherwise follow these instructions:
					<ol>
						<li>soEditor must be downloaded separately from <a href="http://www.SiteObjects.com">SiteObjects.com</a>.</li>
						<li>
							Copy the <code>\pro\</code> folder from your soEditor download so that it is visible at the web path: <code>#attributes.scriptpath#</code>.
						</li>
						<li>Copy the file <code>soeditor_pro.cfm</code> included in that download to this address: <code>#getcurrenttemplatepath()#</code>. Notes: 
							<ul>
								<li>You will see a file called <code>soeditor.cfm</code> in <code>#listdeleteat(getdirectoryfrompath(getcurrenttemplatepath()), listlen(getdirectoryfrompath(getcurrenttemplatepath()), "\/"), "\/")#\</code>. Leave that file as is!</li>
								<li>You will also see a file called <code>soeditor_pro.cfm</code> in <code>#getdirectoryfrompath(getcurrenttemplatepath())#</code>. That is the file displaying this message. Copy your downloaded <code>soeditor_pro.cfm</code> over the top of this file!</li>
							</ul>
						</li>
					</ol>
					 
				</td>
			</tr>
		
		</table>
	</cfoutput>
</cfif>