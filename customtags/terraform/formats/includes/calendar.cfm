<!---
	// *************************************************************************
	// Format:		 CALENDAR
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20090522
	// Dependencies: 
	// Notes:-		 CALENDAR generates a popup calendar based on the 
	//				 current locale setting. This format is supported
	//		 		 via the external file: calendar.cfm
	//				 along with some images and Javascript files.
	//				 See readme.txt.
	// History:		 20090522 added inline attribute to disable positioning of popup calendar
	//				 20090209 removed align, hspace, vspace and border for HTML strict support
	//				 20050720 calendar not working in FireFox due to 
	//						  id error
	//				20050407 was not using LS parsing of dates when choosing which 
	//						  month in the calendar to pop open
	//				 20040730 added disabled support
	//				 20040528 now uses supplied id
	//				 20031125 Updated to Matt Kruse's 19 Aug 2003
	//						  release. Now selects default date.
	//				 20031119 added Safari support	
	//				 20031102 made XHTML tag closure dependent on XHTML attribute
	//				 20031031 switched from attributesList to ignoreAttributes
	//				 20030806 added required flag placeholder
	//				 20030704 added disabled support
	//				 20030625 replaced poupcalendarnumber with calendarid - a uuid based id
	//						  to resolve bug with two forms on one page
	//				 20030527 tweaked for Netscape and Opera
	//				 20030521 added styles to tweak alignment in Netscape and Opera
	//				 20030519 implemented updated code from mattkruse.com. dates outside
	//						  min and max now disabled. Added disabledDates attribute.
	//				 20030511 added calendarFormatDegrade()
	//				 20030129 added buttonPassthrough
	//				 20030128 changed this to thisField
	//				 20030123 Added InputFieldFormat()
	//				 20021205 CSE HTML validator compliance tweaks
	//				 20021130 added support for the undocumented locales: Chinese (China), 
	// 				 Chinese (Hong Kong), Chinese (Taiwan)
	//				 20021020 degrades gracefully in Netscape 3
	//				 20020914 changed this.value to this.rawvalue
	//				 20020627 Added support for Japanese and Korean, the new CFMX locales
	//				 20020415 Created
	// Customisations:-
	//
--->

	<cfscript>
		ignoreAttributes.ButtonCaption = "";
		ignoreAttributes.ButtonPassthrough = "";
		ignoreAttributes.PopupWindow = "";
		ignoreAttributes.ShowYearNavigation = "";
		ignoreAttributes.DisabledWeekdays = "";
		ignoreAttributes.disabledDates = "";
		ignoreAttributes.offsetx = "";
		ignoreAttributes.offsety = "";
		ignoreAttributes.inline = "";
	</cfscript>
	
	<cfif structkeyexists(thisfield, "disabled")>
		<cfset thisField.buttonPassthrough = thisField.buttonPassthrough & " disabled=""disabled""">
	</cfif>
			
	<!--- Convert the date mask into Javascript --->
	<cfscript>
		DateMask = ShortDateMask();
		YearMask = REFindNoCase("y+", DateMask, 1, "TRUE");
		YearLen = YearMask.Len[1];
		MonthMask = REFindNoCase("m+", DateMask, 1, "TRUE");
		MonthLen = MonthMask.Len[1];
		DayMask = REFindNoCase("d+", DateMask, 1, "TRUE");
		DayLen = DayMask.Len[1];
		JSDateBlock = "#DateMask#";
		JSDateBlock = REReplaceNoCase(JSDateBlock, "y+", """ + y + """, "ALL");
		JSDateBlock = REReplaceNoCase(JSDateBlock, "m+", """ + m + """, "ALL");
		JSDateBlock = REReplaceNoCase(JSDateBlock, "d+", """ + d + """, "ALL");
		YearCharacter = "";
	</cfscript>
	
	<cfif request.terraform.browser.engine eq "Opera">
		<!--- popup window form doesn't seem to work in Opera 7.11 --->
		<cfset thisField.PopupWindow = false>
	</cfif>
	
	<cfswitch expression="#GetLocale()#">

		<cfcase value="Chinese (China),Chinese (Taiwan)">
			<cfset Today = "&##20170;&##22825;">
			<cfset DayList = "&##26085;,&##19968;,&##20108;,&##19977;,&##22235;,&##20116;,&##20845;">
			<cfset MonthList = "&##19968;&##26376;,&##20108;&##26376;,&##19977;&##26376;,&##22235;&##26376;,&##20116;&##26376;,&##20845;&##26376;,&##19971;&##26376;,&##20843;&##26376;,&##20061;&##26376;,&##21313;&##26376;,&##21313;&##19968;&##26376;,&##21313;&##20108;&##26376;">
			<cfset YearCharacter = "&##24180;">
		</cfcase>

		<cfcase value="Chinese (Hong Kong)">
			<cfset Today = "&##20170;&##22825;">
			<cfset DayList = "S,M,T,W,T,F,S">
			<cfset MonthList = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec">
		</cfcase>
		
		<cfcase value="Dutch (Belgian),Dutch (Standard)">
			<cfset Today = "Vandaag">
			<cfset DayList = "Z,M,D,W,D,V,Z">
  			<cfset MonthList = "jan,feb,mrt,apr,mei,jun,jul,aug,sep,okt,nov,dec">
		</cfcase>

		<cfcase value="French (Belgian),French (Canadian),French (Standard),French (Swiss)">
			<cfset Today = "Aujourd'hui">
			<cfset DayList = "D,L,M,M,J,V,S">
  			<cfset MonthList = "jan,f&eacute;v,mar,avr,mai,juin,jui,ao&ucirc;t,sep,oct,nov,d&eacute;c">
		</cfcase>
		
		<cfcase value="German (Austrian)">
			<cfset Today = "Heute">
			<cfset DayList = "S,M,D,M,D,F,S">
			<cfset MonthList = "J&auml;n,Feb,M&auml;r,Apr,Mai,Jun,Jul,Aug,Sep,Okt,Nov,Dez">
		</cfcase>

		<cfcase value="German (Standard),German (Swiss)">
			<cfset Today = "Heute">
			<cfset DayList = "S,M,D,M,D,F,S">
			<cfset MonthList = "Jan,Feb,M&auml;r,Apr,Mai,Jun,Jul,Aug,Sep,Okt,Nov,Dez">
		</cfcase>
		
		<cfcase value="Italian (Standard),Italian (Swiss)">
			<cfset Today = "Oggi">
			<cfset DayList = "D,L,M,M,G,V,S">
			<cfset MonthList = "gen,feb,mar,apr,mag,giu,lug,ago,set,ott,nov,dic">
		</cfcase>
		
		<cfcase value="Japanese">
			<cfset Today = "&##26412;&##26085;">
			<cfset DayList = "&##26085;,&##26376;,&##28779;,&##27700;,&##26408;,&##37329;,&##22303;">
			<cfset MonthList = "1&##26376;,2&##26376;,3&##26376;,4&##26376;,5&##26376;,6&##26376;,7&##26376;,8&##26376;,9&##26376;,10&##26376;,11&##26376;,12&##26376;">
			<cfset YearCharacter = "&##24180;">
		</cfcase>

		<cfcase value="Korean">
			<cfset Today = "&##50724;&##45720;">
			<cfset DayList = "&##51068;,&##50900;,&##54868;,&##49688;,&##47785;,&##44552;,&##53664;">
			<cfset MonthList = "1&##50900;,2&##50900;,3&##50900;,4&##50900;,5&##50900;,6&##50900;,7&##50900;,8&##50900;,9&##50900;,10&##50900;,11&##50900;,12&##50900;">
			<cfset YearCharacter = "&##45380;">
		</cfcase>
		
		<cfcase value="Norwegian (Bokmal),Norwegian (Nynorsk)">
			<cfset Today = "I dag">
			<cfset DayList = "S,M,T,O,T,F,L">
			<cfset MonthList = "jan,feb,mar,apr,mai,jun,jul,aug,sep,okt,nov,des">
		</cfcase>
		
		<cfcase value="Portuguese (Brazilian)">
			<cfset Today = "Hoje">
			<cfset DayList = "D,S,T,Q,Q,S,S">
			<cfset MonthList = "jan,fev,mar,abr,mai,jun,jul,ago,set,out,nov,dez">
		</cfcase>
		
		<cfcase value="Portuguese (Standard)">
			<cfset Today = "Hoje">
			<cfset DayList = "D,S,T,Q,Q,S,S">
			<cfset MonthList = "Jan,Fev,Mar,Abr,Mai,Jun,Jul,Ago,Set,Out,Nov,Dez">
		</cfcase>
		
		<cfcase value="Spanish (Mexican)">
			<cfset Today = "Hoy">
			<cfset DayList = "D,L,M,M,J,V,S">
			<cfset MonthList = "Ene,Feb,Mar,Abr,May,Jun,Jul,Ago,Sep,Oct,Nov,Dic">
		</cfcase>
		
		<cfcase value="Spanish (Modern),Spanish (Standard)">
			<cfset Today = "Hoy">
			<cfset DayList = "D,L,M,M,J,V,S">
			<cfset MonthList = "ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic">
		</cfcase>
		
		<cfcase value="Swedish">
			<cfset Today = "Idag">
			<cfset DayList = "S,M,T,O,T,F,L">
			<cfset MonthList = "jan,feb,mar,apr,maj,jun,jul,aug,sep,okt,nov,dec">
		</cfcase>

		<cfdefaultcase>
			<cfset Today = "Today">
			<cfset DayList = "S,M,T,W,T,F,S">
			<cfset MonthList = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec">
		</cfdefaultcase>
		
	</cfswitch>
	<cfset DayList = ListQualify(DayList, """")>
	<cfset MonthList = ListQualify(MonthList, """")>

				
	<cfoutput>
	
		<cfif NOT structkeyexists(request.terraform.flags, "Calendar")>
			<cfset request.terraform.flags.Calendar = true>
			<cfsavecontent variable="HTMLHead">
				<script src="#Attributes.ResourcePath#formats/calendar/CalendarPopup.js" type="text/javascript"></script>
				<script  type="text/javascript">document.write(getCalendarStyles());</script>
			</cfsavecontent>	
		<cfelse>
			<cfset HTMLHead = "">
		</cfif>
		<cfset calendarId=replace(createuuid(), "-", "", "all")>
		
		<cfif thisField.PopupWindow>
			<cfset DIVID = "">
		<cfelse>
			<cfset DIVID = "testdiv#calendarId#">
		</cfif>

		<script type="text/javascript">
		<!--
			// Create CalendarPopup object
			<cfif thisField.PopupWindow>
				var cal#calendarId# = new CalendarPopup();
			<cfelse>
				var cal#calendarId# = new CalendarPopup("testdiv#calendarId#");
			</cfif>
			<cfif thisField.ShowYearNavigation>
				cal#calendarId#.showYearNavigation();
			</cfif>
			cal#calendarId#.setDisabledWeekDays(#thisField.DisabledWeekdays#);
				cal#calendarId#.setReturnFunction("showDate#calendarId#");
			document.write(cal#calendarId#.getStyles());
			cal#calendarId#.setMonthNames(#MonthList#);
			cal#calendarId#.setDayHeaders(#DayList#);
			cal#calendarId#.setWeekStartDay(1); // Monday
			cal#calendarId#.offsetX = #thisField.offsetX#;
			cal#calendarId#.offsetY = #thisField.offsetY#;
			cal#calendarId#.todayText = "#Today#";
			<cfif isBoolean(thisField.inline) and thisField.inline>
				cal#calendarId#.inline = true;
			<cfelse>
				cal#calendarId#.inline = false;
			</cfif>
			cal#calendarId#.yearCharacter = "#YearCharacter#";
			<cfif lsIsDate(thisField.value)>
				<cftry>
					<cfset temp = lsParseDateTime(thisField.value)>
					<cfcatch>
						<cfset temp = parseDateTime(thisField.value)>
					</cfcatch>
				</cftry>
				cal#calendarId#.currentDate=new Date(#year(temp)#, #month(temp)#-1,#day(temp)#); 
			<cfelseif isDate(thisField.value)>
				cal#calendarId#.currentDate=new Date(#year(thisField.value)#, #month(thisField.value)#-1,#day(thisField.value)#); 
			</cfif>
			
			<cfif len(thisField.min)>
				cal#calendarId#.addDisabledDates(null, "#dateformat(thisField.min - 1, "yyyy-mm-dd")#");
			</cfif>
			<cfif len(thisField.max)>
				cal#calendarId#.addDisabledDates("#dateformat(thisField.max + 1, "yyyy-mm-dd")#", null);
			</cfif>
			
			<!--- process disabled date ranges --->
			<cfif structKeyExists(thisField, "disabledDates") and len(thisField.disabledDates)>
				<cfloop index="disabledDate" list="#thisField.disabledDates#">
					<cfif find(":", disabledDate)>
						<!--- it's a date range --->
						cal#calendarId#.addDisabledDates("#dateformat(listfirst(disabledDate, ":"), "yyyy-mm-dd")#","#dateformat(listlast(disabledDate, ":"), "yyyy-mm-dd")#");
					<cfelse>
						<!--- it's a single date --->
						cal#calendarId#.addDisabledDates("#dateformat(listfirst(disabledDate, ":"), "yyyy-mm-dd")#");
					</cfif>
				</cfloop>
			</cfif>


			// Function to get input back from calendar popup
			function showDate#calendarId#(y,m,d) {
				y = y.toString();
				for ( i = y.length; i < #YearLen#; i++ )
					y = "0" + y;	
				m = m.toString();
				for ( i = m.length; i < #MonthLen#; i++ )
					m = "0" + m;
				d = d.toString();
				for ( i = d.length; i < #DayLen#; i++ )
					d = "0" + d;
				document.forms['#Attributes.Name#']['#thisField.id#'].value = "#JSDateBlock#"
			}
		-->	
		</script>

		<input #Passthrough# value="#InputFieldFormat(thisField.RawValue)#" name="#thisField.Name#"#closeTag#
		<cfset request.terraform.HTMLHead = request.terraform.HTMLHead & HTMLHead>
		<a name="calposition#calendarId#" id="calposition#calendarId#"> </a>
		<cfif (request.terraform.browser.engine eq "Opera") or (request.terraform.browser.engine eq "Gecko")>
			<!--- Looks better in Opera and Gecko to go without a button. --->
			<a title="#jsstringFormat(thisField.ButtonCaption)#" onclick="cal#calendarId#.showCalendar('#thisField.id#_anchor'); return false;" name="anchor#calendarId#" id="#thisField.id#_anchor" href="##"><img alt="#jsstringFormat(thisField.ButtonCaption)#" src="#Attributes.ResourcePath#formats/calendar/calendar.gif" width="16" height="15" style="vertical-align:middle;border-width:0;margin:1px" alt="Calendar"#closeTag#</a>#ESC#*
		<cfelse>
			<button #thisField.buttonPassthrough# type="button" onclick="cal#calendarId#.showCalendar('anchor#calendarId#'); return false;" name="anchor#calendarId#" id="#thisField.id#_anchor"><img src="#Attributes.ResourcePath#formats/calendar/calendar.gif" width="16" height="15" style="vertical-align:middle;border-width:0;margin:1px" alt="Calendar"#closeTag##thisField.ButtonCaption#</button>#ESC#*
		</cfif>
		<cfif NOT thisField.PopupWindow>
			<div id="testdiv#calendarId#" style="background-color : white;position:absolute;visibility:hidden;z-index : 999;"> </div>
		</cfif>
		
			
	</cfoutput>

<!---
	// End Format: CALENDAR 
	// *************************************************************************
--->

