<!---
	// *************************************************************************
	// Format:		 SOEDITOR 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030806
	// Dependencies: 
	// Notes:-
	// The SOEDITOR format provides support for the soEditor WYSIWYG editor. 
	// This format supports both soEditor Lite and soEditor Pro. soEditor must 
	// be downloaded separately from siteobjects.com. For more information about
	// soEditor, see
	//     http://www.siteobjects.com/pages/soeditor.cfm
	
	// Install soEditor Lite or Pro into [ResourcePath]/formats/ where
	// [ResourcePath] is the ResourcePath attribute supplied in your
	// cf_terraform tag or set up in your settings file. So you should have 
	// the following folder:
	//     [ResourcePath]/formats/siteobjects/soeditor/lite/ 
	// or 
	//     [ResourcePath]/formats/siteobjects/soeditor/pro/ 	
	// (depending on your version) containing a variety of .htc, .txt,
	// .htm, .cfm files. Copy soeditor_lite.cfm or soeditor_pro.cfm into 
	// [customtags]\terraform\formats\includes\soeditor\.
	//
	// If you are using soEditor Pro, be sure to pass the attribute
	// version="pro" in your cf_terrafield tag or ideally set it as a preset
	// in your settings file:
	//     Preset("format_soEditor", "version", "Pro");
	//
	// Regular soEditor attributes apply, although you cannot use the soEditor
	// attribute "format" as that is already a TerraField attribute.
	// Instead, use "soEditor_Format". See 
	//     http://www.siteobjects.com/pages/soeditordocs.cfm 
	// for soEditor docs.
	//
	// You do not need the ScriptPath, form, field, and HTML attributes as they are
	// generated for you by TerraForm. 
	//
	// History:		 20030806 added required flag placeholder
	//				 20030724 added soeditorformatdegrade()
	//				 20030320 improved Pro support by adding version attribute
	// 				 20030128 changed this to thisField
	//				 20020914 changed this.value to this.rawvalue
	//				 20020813 removed ScriptPath
	//				 20020405 fixed bug - format attribute clash
	// Customisations:-
	//
--->
	<cfparam name="thisField.version" default="lite">
	<cfparam name="thisField.soEditor_Format" default="TRUE">
	<cfif StructKeyExists(thisField, "form")>
		<cfabort showerror="TerraForm implementation error: the soEditor ""form"" attribute is set automatically by TerraForm. Please delete it from your cf_terrafield tag.">
	</cfif>
	<cfif StructKeyExists(thisField, "field")>
		<cfabort showerror="TerraForm implementation error: the soEditor ""field"" attribute is set automatically by TerraForm. Please delete it from your cf_terrafield tag.">
	</cfif>
	<cfif StructKeyExists(thisField, "html")>
		<cfabort showerror="TerraForm implementation error: the soEditor ""HTML"" attribute is set automatically by TerraForm. Please delete it from your cf_terrafield tag.">
	</cfif>
	<cfif StructKeyExists(thisField, "ScriptPath")>
		<cfabort showerror="TerraForm implementation error: the soEditor ""ScriptPath"" attribute is set automatically by TerraForm to ""[ResourcePath]formats/siteobjects/soeditor/#thisfield.version#/"" where [ResourcePath] is the ResourcePath attribute supplied in your cf_terraform tag or set up in your settings file. Please delete it from your cf_terrafield tag.">
	</cfif>
	
	<!--- Move the TerraForm attribute out and the soEditor attribute in --->
	<cfset thisField.Format = thisField.soEditor_Format>
	<cfmodule template="soeditor/soEditor_#thisfield.version#.cfm"
		form="#Attributes.Name#"
		field="#thisField.Name#"
		AttributeCollection="#thisField#"
		ScriptPath="/siteobjects/soeditor/#thisfield.version#/"
		HTML="#thisField.RawValue#"
	><cfoutput>#ESC#*</cfoutput>
	<!--- Move the soEditor attribute out and the TerraForm attribute back in --->
	<cfset thisField.Format = "soEditor">
<!---
	// End Format: SOEDITOR 
	// *************************************************************************
--->