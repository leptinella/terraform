<!---
	// *************************************************************************
	// Format:		 FCKEDITOR 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20050808
	// Dependencies: 
	// Notes:-
	// FCKEDITOR implements the FCKEditor 2.0 WYSIWYG editor field . 
	// See http://www.fckeditor.net/ for more information.
	// FCKEditor is not distributed with TerraForm. You must download it
	// separately. Unpack the FCKEditor download into
	// [ResourcePath]/formats/fckeditor/ where [ResourcePath] is the 
	// ResourcePath attribute supplied in your cf_terraform tag or 
	// set up in your settings file. The folder [ResourcePath]/formats/fckeditor/
	// should thus contain a variety of files and folders. 
	// Copy the file, [ResourcePath]/formats/fckeditor/fckeditor.cfm to
	// [customtags]/terraform/formats/includes/fckeditor/ .
	// Some attributes like height and width are added as normal. But most 
	// FCKeditor attributes are added as part of a structure. Create a structure 
	// called config and add attributes (such as defaultlanguage) to 
	// that, then supply the structure as an attribute: 
	// <cf_terrafield 
	//   name="myField"
	//   format="fckeditor" 
	//   width="500" 
	//   height="400" 
	//   config="#config#" 
	//   ...
	//  />
	// History:		20050808 added toolbarSet attribute
	//				20050428 Created
	// Customisations:-
	//
--->
	<cfset ignoreAttributes.config = "">
	<cfset attribs = structNew()>
	<cfloop collection="#thisField#" item="name">
		<cfif not structKeyExists(ignoreAttributes, name)>
			<cfif listLen(name, ".") eq 2>
				<cfset left = listFirst(name, ".")>
				<cfset right = listRest(name, ".")>
				<cfif not structKeyExists(attribs, left)>
					<cfset attribs[left] = structNew()>
				</cfif>
				<cfset attribs[left][right] = thisField[name]>
			<cfelse>
				<cfset attribs[name] = thisField[name]>
			</cfif>
		</cfif>
	</cfloop>
	<cfmodule 
		template="fckeditor/fckeditor.cfm"
		basePath="#Attributes.ResourcePath#formats/fckeditor/"
		instanceName="#thisField.name#"
		value="#thisField.RawValue#"
		attributecollection="#attribs#"
		config="#thisField.config#"
		toolbarSet="#thisField.toolBarSet#"
	>	
<!---
	// End Format: FCKEDITOR 
	// *************************************************************************
--->