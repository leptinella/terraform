<cfif thistag.executionmode eq "start">
	<cfoutput>
		<table border="1" cellspacing="0" cellpadding="10" bordercolor="##FF0000">
		
			<tr>
				<td>
					ActivEdit is not installed properly.
					<ol>
						<li>ActivEdit must be downloaded separately from <a href="http://www.CFDev.com">CFDev.com</a>.</li>
						<li>
							Copy the <code>inc</code> folder from your ActivEdit download so that it is visible at the web path: <code>#attributes.inc#</code>.
						</li>
						<li>Copy the file <code>activedit.cfm</code> included in that download to this address: <code>#getcurrenttemplatepath()#</code>. Notes: 
							<ul>
								<li>You will see a file called <code>activedit.cfm</code> in <code>#listdeleteat(getdirectoryfrompath(getcurrenttemplatepath()), listlen(getdirectoryfrompath(getcurrenttemplatepath()), "\/"), "\/")#\</code>. Leave that file as is!</li>
								<li>You will also see a file called <code>activedit.cfm</code> in <code>#getdirectoryfrompath(getcurrenttemplatepath())#</code>. That is the file displaying this message. Copy your downloaded <code>activedit.cfm</code> over the top of this file!</li>
							</ul>
						</li>
					</ol>
					 
				</td>
			</tr>
		
		</table>
	</cfoutput>
</cfif>