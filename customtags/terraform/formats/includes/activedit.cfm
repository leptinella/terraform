<!---
	// *************************************************************************
	// Format:		 ACTIVEDIT 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20031031
	// Dependencies: 
	// Notes:-
	// ACTIVEDIT creates an ActivEdit WYSIWYG editor field. Place ActivEdit.cfm in
	// [customtags]\terraform\formats\includes\activedit\ .
	// Place the ActivEdit inc folder in [ResourcePath]/formats/activedit/
	// where [ResourcePath] is the ResourcePath attribute supplied in your cf_terraform tag or 
	// set up in your settings file. Regular ActivEdit attributes apply although the inc 
	// attribute is not used. See cfdev.com for details.
	// History:		 20031031 switched from attributesList to ignoreAttributes
	//				 20030806 added required flag placeholder
	//				 20030724 added activeditformatdegrade()
	//				 20030611 added cfsetting tags
	//				 20030331 changed path to activedit.cfm
	//				 20030128 changed this to thisField
	//				 20020914 changed this.value to this.rawvalue
	//				 20020813 shifted inc files
	// 				 20020319 Created
	// Customisations:-
	//
--->

 	<cfif StructKeyExists(thisField, "Inc")>
		<cfabort showerror="TerraForm implementation error: the ActivEdit ""inc"" attribute is set automatically by TerraForm to ""[ResourcePath]formats/activedit/inc/"" where [ResourcePath] is the ResourcePath attribute supplied in your cf_terraform tag or set up in your settings file. Please delete it from your cf_terrafield tag.">
	</cfif>	
	
	<cfsetting enablecfoutputonly="no">
	<cfmodule
		template="activedit/activedit.cfm"
		inc="#Attributes.ResourcePath#formats/activedit/inc/"
		AttributeCollection="#thisField#"
	><cfoutput>#thisField.RawValue#</cfoutput></cfmodule>#ESC#*
	<cfsetting enablecfoutputonly="yes"> 

	<cfscript>
		ignoreAttributes.IMAGE = "";
		ignoreAttributes.IMAGEPATH = "";
		ignoreAttributes.IMAGEURL = "";
		ignoreAttributes.BASEURL = "";
		ignoreAttributes.UPLOAD = "";
		ignoreAttributes.HEIGHT = "";
		ignoreAttributes.WIDTH = "";
		ignoreAttributes.TOOLBAR = "";
		ignoreAttributes.ALLOWTABLE = "";
		ignoreAttributes.CUTCOPYPASTE = "";
		ignoreAttributes.REDOUNDO = "";
		ignoreAttributes.FIND = "";
		ignoreAttributes.BUTTONCOLOR = "";
		ignoreAttributes.BORDER = "";
		ignoreAttributes.ALLOWEDITSOURCE = "";
		ignoreAttributes.BREAKONENTER = "";
		ignoreAttributes.DEFAULTFONT = "";
		ignoreAttributes.TABVIEW = "";
	</cfscript>

<!---
	// End Format: ACTIVEDIT 
	// *************************************************************************
--->