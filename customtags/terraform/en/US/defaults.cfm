<!--- Note that Attributes.DynamicLocale is now cfparamed directly in the settings.cfm file so that it may be used to decide which language pack to incorporate. --->

<cfparam name="Attributes.Action"					default="#cgi.request_uri#?#cgi.query_string#"
																														type="string">
<cfparam name="Attributes.AlwaysDisplayForm"		default="NO"														type="boolean">
<cfparam name="Attributes.BuildFormTags"			default="YES"														type="boolean">
<cfparam name="Attributes.Debug"					default="NO"														type="boolean">
<cfparam name="Attributes.EncType"					default="">
<cfparam name="Attributes.flagLabels"				default="no"														type="boolean">
<cfparam name="Attributes.flagRequiredFields"		default="yes"														type="boolean">
<cfparam name="Attributes.Focus"					default="NO"														type="boolean">
<cfparam name="Attributes.HideButtonsOnSubmit"		default="yes"														type="boolean">
<cfparam name="Attributes.Highlight"				default="style=""color : red; font-weight : bold""" 				type="string">
<cfparam name="Attributes.HighlightLabelTagStart"	default="<span #Attributes.Highlight#>" 							type="string">
<cfparam name="Attributes.HighlightLabelTagEnd"		default="</span>" 													type="string">
<cfparam name="Attributes.InputScopeList"			default="">
<cfparam name="Attributes.LanguageList"				default="">
<cfparam name="Attributes.onLoad"					default="">
<cfparam name="Attributes.onSubmit"					default="">
<cfparam name="Attributes.PassthroughFieldList"		default="">
<cfparam name="Attributes.qForms"					default="NO"														type="boolean">
<cfparam name="Attributes.qFormsPath"				default="/lib/">
<cfparam name="Attributes.requiredFlag"				default="<span style=""color : ##cc0000; font-size : 150%"">&nbsp;* </span>">
<cfparam name="Attributes.requiredFieldsMessage"	default="<p>Fields marked with #ESC#REQUIREDFLAG are required.</p>">
<cfparam name="Attributes.ResourcePath"				default="/terraformresources/">
<cfparam name="Attributes.Scope"					default="FORM">
<cfparam name="Attributes.smartDegrade"				default="yes"														type="boolean">
<cfparam name="Attributes.xhtml"					default="no"														type="boolean">
<cfparam name="Attributes.labelTransformRegex"		default="(.*)"														type="string">
<cfparam name="Attributes.labelTransformSubstring"	default="\u\1:"														type="string">
<cfparam name="Attributes.inlineErrors"				default="NO"														type="boolean">
<cfparam name="Attributes.inlineErrorsTagStart"		default="<div style=""color : red; font-weight : bold; font-size : xx-small"">" type="string">
<cfparam name="Attributes.inlineErrorsTagEnd"		default="</div>" 													type="string">