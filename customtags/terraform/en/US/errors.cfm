<cfscript>

	// rules-based validation errors

	DeclareError("OK","OK");
	
	DeclareError("InlineRequiredFieldMissing","#ESC#InitialCapsCAPTION is required.");
	DeclareError("RequiredFieldMissing","Please enter #ESC#REQUIREDFIELDS.");
	DeclareError("RequiredFieldsMissing","Please complete the following fields: #ESC#REQUIREDFIELDS.");
	
	DeclareError("NotInList","""#ESC#VALUE"" is not a valid choice for #ESC#CAPTION.");

	DeclareError("TooShort_Exact","#ESC#InitialCapsCAPTION is too short. It must be exactly #ESC#MINLENGTH characters.");
	DeclareError("TooLong_Exact","#ESC#InitialCapsCAPTION is too long. It must be exactly #ESC#MINLENGTH characters.");
	DeclareError("TooShort_MinMax","#ESC#InitialCapsCAPTION is too short. It must be between #ESC#MINLENGTH and #ESC#MAXLENGTH characters.");
	DeclareError("TooLong_MinMax","#ESC#InitialCapsCAPTION is too long. It must be between #ESC#MINLENGTH and #ESC#MAXLENGTH characters.");
	DeclareError("TooShort_Min","#ESC#InitialCapsCAPTION is too short. It must be at least #ESC#MINLENGTH characters.");
	DeclareError("TooLong_Max","#ESC#InitialCapsCAPTION is too long. It must be no more than  #ESC#MAXLENGTH characters.");

	DeclareError("OutOfRange_MinMax","#ESC#InitialCapsCAPTION must be in the range: #ESC#MIN to #ESC#MAX.");
	DeclareError("OutOfRange_Min","#ESC#InitialCapsCAPTION must be #ESC#MIN or more.");
	DeclareError("OutOfRange_Max","#ESC#InitialCapsCAPTION must be #ESC#MAX or less.");
	DeclareError("OutOfRange_Exact","#ESC#InitialCapsCAPTION must be #ESC#MIN.");

	DeclareError("OutOfRange_MinMax_Date","#ESC#InitialCapsCAPTION must be between the dates: #ESC#MINDATE and #ESC#MAXDATE.");
	DeclareError("OutOfRange_Min_Date","#ESC#InitialCapsCAPTION must be #ESC#MINDATE or later.");
	DeclareError("OutOfRange_Max_Date","#ESC#InitialCapsCAPTION must be #ESC#MAXDATE or earlier.");
	DeclareError("OutOfRange_Exact_Date","#ESC#InitialCapsCAPTION must be exactly #ESC#MINDATE.");

	DeclareError("OutOfRange_MinMax_Time","#ESC#InitialCapsCAPTION must be between #ESC#MINTIME and #ESC#MAXTIME.");
	DeclareError("OutOfRange_Min_Time","#ESC#InitialCapsCAPTION must be #ESC#MINTIME or later.");
	DeclareError("OutOfRange_Max_Time","#ESC#InitialCapsCAPTION must be #ESC#MAXTIME or earlier.");
	DeclareError("OutOfRange_Exact_Time","#ESC#InitialCapsCAPTION must be exactly #ESC#MINTIME.");

	DeclareError("Pattern","""#ESC#VALUE"" is not a valid entry for #ESC#CAPTION.");

	DeclareError("ProhibitedWords","The words: #ESC#PROHIBITEDWORDS are not allowed. Please rephrase.");
	DeclareError("ProhibitedWord","The word: ""#ESC#PROHIBITEDWORDS"" is not allowed. Please rephrase.");

	// datatype mismatch errors
	
	DeclareError("NotBoolean", "#ESC#InitialCapsCAPTION must be either YES or NO.");

	DeclareError("CreditCardNumber_InvalidCharacters", """#ESC#VALUE"" is not a valid credit card number (digits only please). Please re-enter #ESC#CAPTION.");
	DeclareError("CreditCardNumber_InvalidNumber", """#ESC#VALUE"" is not a valid credit card number. Please check the digits and re-enter #ESC#CAPTION.");

	DeclareError("NotADate", "#ESC#VALUE #ESC#InitialCapsCAPTION doesn't seem to be a valid date. Please enter a date in the following format: #ESC#DATEHINT .");

	DeclareError("NotAnEmailAddress", "#ESC#InitialCapsCAPTION does not seem to be valid.");
	DeclareError("NotAnEmailAddress_MissingAt", "#ESC#InitialCapsCAPTION does not seem to be valid. Your address must contain an @ symbol following your username.");
	DeclareError("NotAnEmailAddress_MissingDomain", "#ESC#InitialCapsCAPTION does not seem to be valid. Your address must contain two or more blocks separated by dots (.) after the @ symbol, for example: me@hotmail.com , me@yahoo.com .");

	DeclareError("NotANumber", "#ESC#InitialCapsCAPTION should be a number.");
	DeclareError("NotAnInteger", "#ESC#InitialCapsCAPTION must be an integer (no decimal part).");

	DeclareError("NotAPrice", "#ESC#InitialCapsCAPTION doesn't seem to be a valid price.");

	DeclareError("NotAColorTriplet", "#ESC#InitialCapsCAPTION does not seem to be a color triplet. A color triplet looks like this: ""##3366CC"" or ""##F80010"".");
	DeclareError("NotAWebsafeColor", "#ESC#InitialCapsCAPTION is not a ""web-safe"" color triplet. A web-safe color must be built from pairs of the following letters and digits: ""00, 33, 66, 99, CC, FF"". For example, ""##00ccff"" or ""##660099"".");

	DeclareError("NotATime", "#ESC#InitialCapsCAPTION doesn't seem to be a valid time. Please enter a time in the following format: #ESC#TIMEHINT .");

	DeclareError("NotAUUID", "#ESC#InitialCapsCAPTION does not seem to be a universally unique identifier (UUID). UUIDs are of the form: xxxxxxxx-xxxx-xxxx-xxxxxxxxxxxxxxxx where x is a hexadecimal digit.");

	DeclareError("ENHANCEDFILE_Upload", "Sorry, there was an error uploading #ESC#CAPTION. Check that you have the correct file.");
	DeclareError("ENHANCEDFILE_ExtensionNotAllowed", "Sorry, there was an error uploading #ESC#CAPTION. That file type is not allowed. Allowed file extensions are: #ESC#EXTENSIONLIST .");
	DeclareError("ENHANCEDFILE_MimetypeNotAllowed", "Sorry, there was an error uploading #ESC#CAPTION. That media type (#ESC#MIMETYPE) is not allowed. Allowed media types are: #ESC#MIMETYPELIST .");
	DeclareError("ENHANCEDFILE_FileTooLarge", "Sorry, there was an error uploading #ESC#CAPTION. That file is too large (#ESC#SIZE). Your file must be no larger than #ESC#MAXSIZE .");

	DeclareError("dateNotAvailable", "That #ESC#CAPTION is unavailable.");
	DeclareError("ProhibitedHtml", "Please do not insert HTML markup into the #ESC#CAPTION field.");
	
	DeclareError("CAPTCHAIncorrect", "Sorry, you did not enter the characters in the image correctly. Please try again.");
	
</cfscript>