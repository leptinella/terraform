<!--- 
*****************************************************************
** PRESETS  *****************************************************
*****************************************************************

Presets are sets of default attributes, declared as follows:

Preset(SetName, AttributeName, AttributeValue);

If you specify a preset for a field, the set of default
attributes in that set will be applied to the field. In this way,
you can potentially build a field with just a couple of
attributes, e.g.:

<cf_terrafield preset="SelectCountry" name="CountryID"/>

Datatypes and formats can also have presets that will be applied
automatically. When a given datatype "xxxx" is specified, TerraForm
will look for a corresponding preset set called "Datatype_xxxx".
Similarly, for a given format "yyyy", TerraForm will look for a
corresponding preset set called "Format_yyyy". 

Note that the custom presets are applied before the format and
datatype presets. There is the possibility for unpredictable
results due to contradictory defaults from the datatype and
format for the same attribute. If in doubt, declare attributes
explicitly. Use this section to override implications declared
below and to declare custom presets. It is recommended you
declare presets here rather than changing or overwriting
those below. The first preset for a given set and attribute will
be used. Others will be ignored.

Examples: 

Preset("Format_text", "onmouseover", "focus()");

Preset("Format_Boolean", "Datatype", "sqBit");

Preset("Format_Password", "MinLength", "6");
Preset("Format_Password", "MaxLength", "12");

Preset("Datatype_Date", "format", "Calendar");

Preset("Format_Calendar", "ButtonCaption", "&nbsp;Calendar");

Preset("Title", "Datatype", "String");
Preset("Title", "Format", "Select");
Preset("Title", "Prompt", "&gt; Select &lt;");
Preset("Title", "ValueList", "Ms.,Mr.,Mrs.,Miss,Dr.,Professor");

Preset("Format_Select", "class", "Select");
Preset("Format_Select", "onfocus", "this.style.backgroundColor='white';select()");
Preset("Format_Select", "onblur", "this.style.backgroundColor='##eeeeee'"); 

Preset("Format_Text", "class", "Text");
Preset("Format_Text", "onfocus", "this.style.backgroundColor='white';select()");
Preset("Format_Text", "onblur", "this.style.backgroundColor='##eeeeee'");

Preset("Format_TextArea", "class", "TextArea");
Preset("Format_TextArea", "onfocus", "this.style.backgroundColor='white';select()");
Preset("Format_TextArea", "onblur", "this.style.backgroundColor='##eeeeee'");

Preset("Format_URL", "class", "URL");
Preset("Format_URL", "onfocus", "this.style.backgroundColor='white';select()");
Preset("Format_URL", "onblur", "this.style.backgroundColor='##eeeeee'"); --->
	
<cfscript> 




	// *************************************************************************
	// Preset:		 GENDER
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030615
	// Dependencies: 
	// History:		 20030615 Created	
	// Customisations:-
	//	
	
	Preset("Gender", "caption", "Your gender");
	Preset("Gender", "Datatype", "string");
	Preset("Gender", "Format", "radio");
	Preset("Gender", "valuelist", "F,M");
	Preset("Gender", "displaylist", "Female,Male");
	
	// End Preset: GENDER
	// *************************************************************************
	
	

	// *************************************************************************
	// Preset:		 UKPOSTCODE
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20021013
	// Dependencies: 
	// Notes:-
	// A UK postcode of the form ## ###, ### ###, or #### ### . Adapted from
	// http://www.spot-light.virgin-lands.co.uk/postcode.php
	// cf. http://www.wikipedia.org/wiki/Postcode/
	
	// Examples: a1 2bd, a12 3bc, ab12 3cd, ab1c 2de 
	// History:		 20021013 Created	
	// Customisations:-
	//	
	
	Preset("UKPostCode", "caption", "Your postcode");
	Preset("UKPostCode", "Datatype", "string");
	Preset("UKPostCode", "Format", "text");
	Preset("UKPostCode", "size", 8);
	Preset("UKPostCode", "pattern", "[A-Z]{1,2}[0-9]{1,2}[A-Z]? [0-9][A-Z]{2}");
	Preset("UKPostCode", "error", "#ESC#CAPTION does not seem to be valid.");
	
	// End Preset: UKPOSTCODE
	// *************************************************************************
	
	
	
	// *************************************************************************
	// Preset:		 SOCIALSECURITYNUMBER
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20021013
	// Dependencies: 
	// Notes:-
	// A US social security number of the form: ###-##-#### (matching
	// the cfinput social_security_number validation).
	// History:		 20021013 Created	
	// Customisations:-
	//	
	
	Preset("SocialSecurityNumber", "caption", "Your social security number");
	Preset("SocialSecurityNumber", "Datatype", "string");
	Preset("SocialSecurityNumber", "Format", "text");
	Preset("SocialSecurityNumber", "size", 11);
	Preset("SocialSecurityNumber", "prompt", "######-####-########");
	Preset("SocialSecurityNumber", "pattern", "[0-9]{3}[ -][0-9]{2}[ -][0-9]{4}");
	Preset("SocialSecurityNumber", "error", "#ESC#CAPTION does not seem to be valid. Please write it in this form: ######-####-########");
	
	// End Preset: SOCIALSECURITYNUMBER
	// *************************************************************************



	// *************************************************************************
	// Preset:		 ZIPCODE 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20021013
	// Dependencies: 
	// Notes:-
	// A US-style ZIP code of the form: ##### or #####-#### (matching
	// the cfinput zipcode validation). Note that international ZIP and
	// postal codes use different formats.
	// History:		 20021013 Created	
	// Customisations:-
	//	
	
	Preset("Zipcode", "caption", "Your ZIP code");
	Preset("Zipcode", "Datatype", "string");
	Preset("Zipcode", "Format", "text");
	Preset("Zipcode", "size", 10);
	Preset("Zipcode", "pattern", "[0-9]{5}([ -][0-9]{4})?");
	Preset("Zipcode", "error", "#ESC#CAPTION does not seem to be valid. Please write it in this form: ########## or ##########-########");
	
	// End Preset: ZIPCODE
	// *************************************************************************
	
	
	
	// *************************************************************************
	// Preset:		 TELEPHONE 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20021013
	// Dependencies: 
	// Notes:-
	// A US-style telephone number of the form: ###-###-#### (matching
	// the cfinput telephone validation). Note that international numbers use
	// different formats.
	// History:		 20021013 Created	
	// Customisations:-
	//	
	
	Preset("Telephone", "caption", "Your telephone number");
	Preset("Telephone", "Datatype", "string");
	Preset("Telephone", "Format", "text");
	Preset("Telephone", "size", 12);
	Preset("Telephone", "prompt", "######-######-########");
	Preset("Telephone", "pattern", "[1-9][0-9]{2}[ -][1-9][0-9]{2}[ -][0-9]{4}");
	Preset("Telephone", "error", "#ESC#CAPTION does not seem to be valid. Please write it in this form: ######-######-########");
	
	// End Preset: TELEPHONE
	// *************************************************************************
	
		

	// *************************************************************************
	// Preset:		 NEXT5YEARS 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020522
	// Dependencies: 
	// Notes:-
	// A select box of the next five years.
	// History:		 20020522 Created	
	// Customisations:-
	//	
	
	Preset("Next5Years", "Datatype", "Integer");
	Preset("Next5Years", "Format", "Select");
	Preset("Next5Years", "Prompt", "&gt; Year &lt;");
	YearList = "";
	for ( i = 0; i LT 5; i = i + 1 )
		YearList=ListAppend(YearList,Year(Now()) + i);	
	Preset("Next5Years", "ValueList", YearList);
	
	// End Preset: NEXT5YEARS
	// *************************************************************************
	
	
	
	// *************************************************************************
	// Preset:		 MONTH 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20020522
	// Dependencies: 
	// Notes:-
	// A select box of months.
	// History:		 20020522 Created	
	// Customisations:-
	//	
	
	Preset("Month", "Datatype", "Integer");
	Preset("Month", "Format", "Select");
	Preset("Month", "Prompt", "&gt; Month &lt;");
	Preset("Month", "ValueList", "1,2,3,4,5,6,7,8,9,10,11,12");
	Preset("Month", "DisplayList", "January,February,March,April,May,June,July,August,September,October,November,December");

	// End Preset: MONTH
	// *************************************************************************
	
	

	// *************************************************************************
	// Preset:		 TITLE 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20040629
	// Dependencies: 
	// Notes:-
	// A select box of titles.
	// History:		 20040629 Removed some options
	//				 20020414 Created	
	// Customisations:-
	//	
	
	Preset("Title", "Datatype", "String");
	Preset("Title", "Format", "Select");
	Preset("Title", "Prompt", "&gt; Title &lt;");
	Preset("Title", "ValueList", "Ms.,Mr.,Mrs.,Miss");
	
	// End Preset: TITLE
	// *************************************************************************

	
	
	// *************************************************************************
	// Preset:		 COUNTRY 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20081128
	// Dependencies: 
	// Notes:-
	// A select box of countries based on the ISO 3166-1 Country codes. If you want to use the
	// full country name as the field value instead of the code, comment out the block:
	// 		Preset("Country", "ValueList", "...");
	// and change "DisplayList" in the other preset function to "valueList".
	// cf. http://www.iso.org/iso/en/prods-services/iso3166ma/02iso-3166-code-lists/index.html
	// See also http://sites.encompassnow.com/idude/articles/web_forms_and_iso_3166.asp for an
	// interesting critique of ISO-3166-1 .
	//
	// Also interesting -- the UN "Countries or areas, codes and abbreviations":
	// http://unstats.un.org/unsd/methods/m49/m49alpha.htm
	//
	// History:		 20081128 Split "Serbia and Montenegro" into two entries, ISO 3166-1 Newsletter V-12. Added Saint Barth�lemy, ISO 3166-1 Newsletter VI-1. Moldova amendment, ISO 3166-1 Newsletter VI-2.
	//				 20060428 added Jersey, Guernsey, Isle of Man in accordance with ISO 3166-1 Newsletter V-11
	//				 20050426 changed "Taiwan, Province of China" to "Taiwan". This
	//						  is a deviation from ISO-3166, but many people take issue
	//						  with this suffix, and there's no need for TerraForm to express
	//						  an opinion about this contentious issue. See 
	//                        http://www.iso.org/iso/en/prods-services/iso3166ma/10faq/frequently-asked-questions.html?printable=true#QS03
	//				 20030329 updated from ISO 3166-1 Newsletter V-9 - added
	//						  �land Islands
	//				 20031209 updated data from newsletters up to and including 
	//						  ISO 3166-1 Newsletter V-8
	//				 		  http://www.iso.ch/iso/en/prods-services/iso3166ma/03updates-on-iso-3166/index.html
	//						  Kazakstan -> Kazakhstan
	//						  Delete Yugoslavia|YU
	//						  Add Serbia and Montenegro|CS
	//				 20030702 added caption
	//				 20030531 Added ISO country codes
	//				 20020414 Created
	// Customisations:-
	//
	
	Preset("Country", "caption", "your country");
	Preset("Country", "Datatype", "string");
	Preset("Country", "Format", "SELECT");
	Preset("Country", "Prompt", "&gt; Country &lt;");
	Preset("Country", "Delimiters", "|");
	Preset("Country", "default", request.terraform.locale.countryCode);
	
	Preset("Country", "ValueList", "AF|AX|AL|DZ|AS|AD|AO|AI|AQ|AG|AR|AM|AW|AU|AT|AZ|BS|BH|BD|BB|BY|BE|BZ|BJ|BM|BT|BO|BA|BW|BV|BR|IO|BN|BG|BF|BI|KH|CM|CA|CV|KY|CF|TD|CL|CN|CX|CC|CO|KM|CG|CD|CK|CR|CI|HR|CU|CY|CZ|DK|DJ|DM|DO|EC|EG|SV|GQ|ER|EE|ET|FK|FO|FJ|FI|FR|GF|PF|TF|GA|GM|GE|DE|GH|GI|GR|GL|GD|GP|GU|GT|GG|GN|GW|GY|HT|HM|VA|HN|HK|HU|IS|IN|ID|IR|IQ|IE|IM|IL|IT|JM|JP|JE|JO|KZ|KE|KI|KP|KR|KW|KG|LA|LV|LB|LS|LR|LY|LI|LT|LU|MO|MK|MG|MW|MY|MV|ML|MT|MH|MQ|MR|MU|YT|MX|FM|MD|MC|MN|ME|MS|MA|MZ|MM|NA|NR|NP|NL|AN|NC|NZ|NI|NE|NG|NU|NF|MP|NO|OM|PK|PW|PS|PA|PG|PY|PE|PH|PN|PL|PT|PR|QA|RE|RO|RU|RW|BL|SH|KN|LC|MF|PM|VC|WS|SM|ST|SA|SN|RS|SC|SL|SG|SK|SI|SB|SO|ZA|GS|ES|LK|SD|SR|SJ|SZ|SE|CH|SY|TW|TJ|TZ|TH|TL|TG|TK|TO|TT|TN|TR|TM|TC|TV|UG|UA|AE|GB|US|UM|UY|UZ|VU|VE|VN|VG|VI|WF|EH|YE|ZM|ZW");

	Preset("Country", "DisplayList", "Afghanistan|�land Islands|Albania|Algeria|American Samoa|Andorra|Angola|Anguilla|Antarctica|Antigua and Barbuda|Argentina|Armenia|Aruba|Australia|Austria|Azerbaijan|Bahamas|Bahrain|Bangladesh|Barbados|Belarus|Belgium|Belize|Benin|Bermuda|Bhutan|Bolivia|Bosnia and Herzegovina|Botswana|Bouvet Island|Brazil|British Indian Ocean Territory|Brunei Darussalam|Bulgaria|Burkina Faso|Burundi|Cambodia|Cameroon|Canada|Cape Verde|Cayman Islands|Central African Republic|Chad|Chile|China|Christmas Island|Cocos (Keeling) Islands|Colombia|Comoros|Congo|Congo, The Democratic Republic of the|Cook Islands|Costa Rica|C�te d'Ivoire|Croatia|Cuba|Cyprus|Czech Republic|Denmark|Djibouti|Dominica|Dominican Republic|Ecuador|Egypt|El Salvador|Equatorial Guinea|Eritrea|Estonia|Ethiopia|Falkland Islands (Malvinas)|Faroe Islands|Fiji|Finland|France|French Guiana|French Polynesia|French Southern Territories|Gabon|Gambia|Georgia|Germany|Ghana|Gibraltar|Greece|Greenland|Grenada|Guadeloupe|Guam|Guatemala|Guernsey|Guinea|Guinea-Bissau|Guyana|Haiti|Heard Island and McDonald Islands|Holy See (Vatican City State)|Honduras|Hong Kong|Hungary|Iceland|India|Indonesia|Iran, Islamic Republic of|Iraq|Ireland|Isle of Man|Israel|Italy|Jamaica|Japan|Jersey|Jordan|Kazakhstan|Kenya|Kiribati|Korea, Democratic People's Republic of|Korea, Republic of|Kuwait|Kyrgyzstan|Lao People's Democratic Republic|Latvia|Lebanon|Lesotho|Liberia|Libyan Arab Jamahiriya|Liechtenstein|Lithuania|Luxembourg|Macao|Macedonia, The Former Yugoslav Republic of|Madagascar|Malawi|Malaysia|Maldives|Mali|Malta|Marshall Islands|Martinique|Mauritania|Mauritius|Mayotte|Mexico|Micronesia, Federated States of|Moldova|Monaco|Mongolia|Montenegro|Montserrat|Morocco|Mozambique|Myanmar|Namibia|Nauru|Nepal|Netherlands|Netherlands Antilles|New Caledonia|New Zealand|Nicaragua|Niger|Nigeria|Niue|Norfolk Island|Northern Mariana Islands|Norway|Oman|Pakistan|Palau|Palestinian Territory, Occupied|Panama|Papua New Guinea|Paraguay|Peru|Philippines|Pitcairn|Poland|Portugal|Puerto Rico|Qatar|R�union|Romania|Russian Federation|Rwanda|Saint
Barth�lemy|Saint Helena|Saint Kitts and Nevis|Saint Lucia|Saint Martin (French part)|Saint Pierre and Miquelon|Saint Vincent and the Grenadines|Samoa|San Marino|S�o Tom� and Pr�ncipe|Saudi Arabia|Senegal|Serbia|Seychelles|Sierra Leone|Singapore|Slovakia|Slovenia|Solomon Islands|Somalia|South Africa|South Georgia and the South Sandwich Islands|Spain|Sri Lanka|Sudan|Suriname|Svalbard and Jan Mayen|Swaziland|Sweden|Switzerland|Syrian Arab Republic|Taiwan|Tajikistan|Tanzania, United Republic of|Thailand|Timor-Leste|Togo|Tokelau|Tonga|Trinidad and Tobago|Tunisia|Turkey|Turkmenistan|Turks and Caicos Islands|Tuvalu|Uganda|Ukraine|United Arab Emirates|United Kingdom|United States|United States Minor Outlying Islands|Uruguay|Uzbekistan|Vanuatu|Venezuela|Viet Nam|Virgin Islands, British|Virgin Islands, U.S.|Wallis and Futuna|Western Sahara|Yemen|Zambia|Zimbabwe");
		
	// End Preset: COUNTRY 
	// *************************************************************************
		

	
	// *************************************************************************
	// Preset:		 USSTATE 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030305
	// Dependencies: 
	// Notes:-
	// A select box of US states. The full state name is displayed, while the 
	// two-letter abbreviation is stored. If you wish to store the full state
	// name, remove the valuelist preset and rename the displaylistpreset to 
	// valuelist.
	// History:		 20030305 Created
	// Customisations:-
	//
	
	Preset("usstate", "Datatype", "string");
	Preset("usstate", "Format", "SELECT");
	Preset("usstate", "Prompt", "&gt; State &lt;");
	
	Preset("usstate", "ValueList", "AL,AK,AS,AZ,AR,CA,CO,CT,DE,DC,FM,FL,GA,GU,HI,ID,IL,IN,IA,KS,KY,LA,ME,MH,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,MP,OH,OK,OR,PW,PA,PR,RI,SC,SD,TN,TX,UT,VT,VI,VA,WA,WV,WI,WY");
	Preset("usstate", "DisplayList", "Alabama,Alaska,American Samoa,Arizona,Arkansas,California,Colorado,Connecticut,Delaware,District of Columbia,Federated States of Micronesia,Florida,Georgia,Guam,Hawaii,Idaho,Illinois,Indiana,Iowa,Kansas,Kentucky,Louisiana,Maine,Marshall Islands,Maryland,Massachusetts,Michigan,Minnesota,Mississippi,Missouri,Montana,Nebraska,Nevada,New Hampshire,New Jersey,New Mexico,New York,North Carolina,North Dakota,Northern Mariana Islands,Ohio,Oklahoma,Oregon,Palau,Pennsylvania,Puerto Rico,Rhode Island,South Carolina,South Dakota,Tennessee,Texas,Utah,Vermont,Virgin Islands,Virginia,Washington,West Virginia,Wisconsin,Wyoming");

	// End Preset: USSTATE 
	// *************************************************************************
	
	
	
	// *************************************************************************
	// Preset:		 TIMEZONE 
	// Author:		 Matthew Walker (esw@eswsoftware.com)
	// Version:		 20030625
	// Dependencies: 
	// Notes:-
	// A list of world time zones.  If you wish to store the full state
	// name, remove the valuelist preset and rename the displaylistpreset to 
	// valuelist.
	// History:		 20030625 Created
	// Customisations:-
	//
	
	Preset("timezone", "Datatype", "string");
	Preset("timezone", "Format", "SELECT");
	Preset("timezone", "Prompt", "&gt; Time zone &lt;");
	Preset("timezone", "Delimiters", "|");
	
	Preset("timezone", "DisplayList", "(GMT-12:00) Eniwetok, Kwajalein|(GMT-11:00) Midway Island, Samoa|(GMT-10:00) Hawaii|(GMT-09:00) Alaska|(GMT-08:00) Pacific Time (US & Canada); Tijuana|(GMT-07:00) Mountain Time (US & Canada)|(GMT-07:00) Arizona|(GMT-06:00) Saskatchewan|(GMT-06:00) Central America|(GMT-06:00) Central Time (US & Canada)|(GMT-06:00) Mexico City|(GMT-05:00) Eastern Time (US & Canada)|(GMT-05:00) Bogota, Lima, Quito|(GMT-05:00) Indiana (East)|(GMT-04:00) Atlantic Time (Canada)|(GMT-04:00) Santiago|(GMT-04:00) Caracas, La Paz|(GMT-03:30) Newfoundland|(GMT-03:00) Brasilia|(GMT-03:00) Greenland|(GMT-03:00) Buenos Aires, Georgetown|(GMT-02:00) Mid-Atlantic|(GMT-01:00) Azores|(GMT-01:00) Cape Verde Is.|(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London|(GMT) Casablanca, Monrovia|(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague|(GMT+01:00) Sarajevo, Skopje, Sofija, Vilnius, Warsaw, Zagreb|(GMT+01:00) Brussels, Copenhagen, Madrid, Paris|(GMT+01:00) West Central Africa|(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna|(GMT+02:00) Bucharest|(GMT+02:00) Cairo|(GMT+02:00) Helsinki, Riga, Tallinn|(GMT+02:00) Athens, Istanbul, Minsk|(GMT+02:00) Jerusalem|(GMT+02:00) Harare, Pretoria|(GMT+03:00) Kuwait, Riyadh|(GMT+03:00) Baghdad|(GMT+03:00) Nairobi|(GMT+03:00) Moscow, St. Petersburg, Volgograd|(GMT+03:30) Tehran|(GMT+04:00) Abu Dhabi, Muscat|(GMT+04:00) Baku, Tbilisi, Yerevan|(GMT+04:30) Kabul|(GMT+05:00) Ekaterinburg|(GMT+05:00) Islamabad, Karachi, Tashkent|(GMT+05:30) Calcutta, Chennai, Mumbai, New Delhi|(GMT+05:45) Kathmandu|(GMT+06:00) Astana, Dhaka|(GMT+06:00) Almaty, Novosibirsk|(GMT+06:00) Sri Jayawardenepura|(GMT+06:30) Rangoon|(GMT+07:00) Krasnoyarsk|(GMT+07:00) Bangkok, Hanoi, Jakarta|(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi|(GMT+08:00) Irkutsk, Ulaan Bataar|(GMT+08:00) Kuala Lumpur, Singapore|(GMT+08:00) Taipei|(GMT+08:00) Perth|(GMT+09:00) Seoul|(GMT+09:00) Osaka, Sapporo, Tokyo|(GMT+09:00) Yakutsk|(GMT+09:30) Darwin|(GMT+09:30) Adelaide|(GMT+10:00) Canberra, Melbourne, Sydney|(GMT+10:00) Brisbane|(GMT+10:00) Hobart|(GMT+10:00) Vladivostok|(GMT+10:00) Guam, Port Moresby|(GMT+11:00) Magadan, Solomon Is., New Caledonia|(GMT+12:00) Fiji, Kamchatka, Marshall Is.|(GMT+12:00) Auckland, Wellington|(GMT+13:00) Nuku'alofa");

	// here's an alternative display list:
	// Preset("timezone", "DisplayList", "Dateline Standard Time|Samoa Standard Time|Hawaiian Standard Time|Alaskan Standard Time|Pacific Standard Time|Mountain Standard Time|US Mountain Standard Time|Canada Central Standard Time|Central America Standard Time|Central Standard Time|Mexico Standard Time|Eastern Standard Time|SA Pacific Standard Time|US Eastern Standard Time|Atlantic Standard Time|Pacific SA Standard Time|SA Western Standard Time|Newfoundland Standard Time|E. South America Standard Time|Greenland Standard Time|SA Eastern Standard Time|Mid-Atlantic Standard Time|Azores Standard Time|Cape Verde Standard Time|GMT Standard Time|Greenwich Standard Time|Central Europe Standard Time|Central European Standard Time|Romance Standard Time|W. Central Africa Standard Time|W. Europe Standard Time|E. Europe Standard Time|Egypt Standard Time|FLE Standard Time|GTB Standard Time|Israel Standard Time|South Africa Standard Time|Arab Standard Time|Arabic Standard Time|E. Africa Standard Time|Russian Standard Time|Iran Standard Time|Arabian Standard Time|Caucasus Standard Time|Afghanistan Standard Time|Ekaterinburg Standard Time|West Asia Standard Time|India Standard Time|Nepal Standard Time|Central Asia Standard Time|N. Central Asia Standard Time|Sri Lanka Standard Time|Myanmar Standard Time|North Asia Standard Time|SE Asia Standard Time|China Standard Time|North Asia East Standard Time|Singapore Standard Time|Taipei Standard Time|W. Australia Standard Time|Korea Standard Time|Tokyo Standard Time|Yakutsk Standard Time|AUS Central Standard Time|Cen. Australia Standard Time|AUS Eastern Standard Time|E. Australia Standard Time|Tasmania Standard Time|Vladivostok Standard Time|West Pacific Standard Time|Central Pacific Standard Time|Fiji Standard Time|New Zealand Standard Time|Tonga Standard Time");

	Preset("timezone", "ValueList", "-12:00|-11:00|-10:00|-09:00|-08:00|-07:00|-07:00|-06:00|-06:00|-06:00|-06:00|-05:00|-05:00|-05:00|-04:00|-04:00|-04:00|-03:30|-03:00|-03:00|-03:00|-02:00|-01:00|-01:00|00:00|00:00|+01:00|+01:00|+01:00|+01:00|+01:00|+02:00|+02:00|+02:00|+02:00|+02:00|+02:00|+03:00|+03:00|+03:00|+03:00|+03:30|+04:00|+04:00|+04:30|+05:00|+05:00|+05:30|+05:45|+06:00|+06:00|+06:00|+06:30|+07:00|+07:00|+08:00|+08:00|+08:00|+08:00|+08:00|+09:00|+09:00|+09:00|+09:30|+09:30|+10:00|+10:00|+10:00|+10:00|+10:00|+11:00|+12:00|+12:00|+13:00");


	// End Preset: TIMEZONE 
	// *************************************************************************
	









	

</cfscript>	