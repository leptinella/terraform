<!--- 
*****************************************************************
** PRESETS  *****************************************************
*****************************************************************

Presets are sets of default attributes, declared as follows:

Preset(SetName, AttributeName, AttributeValue);

If you specify a preset for a field, the set of default
attributes in that set will be applied to the field. In this way,
you can potentially build a field with just a couple of
attributes, e.g.:

<cf_terrafield preset="SelectCountry" name="CountryID"/>

Datatypes and formats can also have presets that will be applied
automatically. When a given datatype "xxxx" is specified, TerraForm
will look for a corresponding preset set called "Datatype_xxxx".
Similarly, for a given format "yyyy", TerraForm will look for a
corresponding preset set called "Format_yyyy". 

Note that the custom presets are applied before the format and
datatype presets. There is the possibility for unpredictable
results due to contradictory defaults from the datatype and
format for the same attribute. If in doubt, declare attributes
explicitly. Use this section to override implications declared
below and to declare custom presets. It is recommended you
declare presets here rather than changing or overwriting
those below. The first preset for a given set and attribute will
be used. Others will be ignored.

Examples: 

Preset("Format_text", "onmouseover", "focus()");

Preset("Format_Boolean", "Datatype", "sqBit");

Preset("Format_Password", "MinLength", "6");
Preset("Format_Password", "MaxLength", "12");

Preset("Datatype_Date", "format", "Calendar");

Preset("Format_Calendar", "ButtonCaption", "&nbsp;Calendar");

Preset("Title", "Datatype", "String");
Preset("Title", "Format", "Select");
Preset("Title", "Prompt", "&gt; Select &lt;");
Preset("Title", "ValueList", "Ms.,Mr.,Mrs.,Miss,Dr.,Professor");

Preset("Format_Select", "class", "Select");
Preset("Format_Select", "onfocus", "this.style.backgroundColor='white';select()");
Preset("Format_Select", "onblur", "this.style.backgroundColor='##eeeeee'"); 

Preset("Format_Text", "class", "Text");
Preset("Format_Text", "onfocus", "this.style.backgroundColor='white';select()");
Preset("Format_Text", "onblur", "this.style.backgroundColor='##eeeeee'");

Preset("Format_TextArea", "class", "TextArea");
Preset("Format_TextArea", "onfocus", "this.style.backgroundColor='white';select()");
Preset("Format_TextArea", "onblur", "this.style.backgroundColor='##eeeeee'");

Preset("Format_URL", "class", "URL");
Preset("Format_URL", "onfocus", "this.style.backgroundColor='white';select()");
Preset("Format_URL", "onblur", "this.style.backgroundColor='##eeeeee'"); --->
	
<cfscript> 	
	
	// *************************************************************************
	// Datatype / format presets
	// *************************************************************************
	
	Preset("Format_ColorPicker", "buttoncaption", "Colour Picker");

</cfscript>	

<!--- Most of the presets are the same as US English --->
<cfinclude template="../US/presets.cfm">