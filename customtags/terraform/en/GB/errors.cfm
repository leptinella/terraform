<cfscript>

	DeclareError("NotAColorTriplet", "#ESC#CAPTION does not seem to be a colour triplet. A colour triplet looks like this: ""##3366CC"" or ""##F80010"".");
	DeclareError("NotAWebsafeColor", "#ESC#CAPTION is not a ""web-safe"" colour triplet. A web-safe colour must be built from pairs of the following letters and digits: ""00, 33, 66, 99, CC, FF"". For example, ""##00ccff"" or ""##660099"".");

</cfscript>

<!--- Most of the errors are the same as US English --->
<cfinclude template="../US/errors.cfm">