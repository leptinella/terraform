<cfsilent>
	<cfswitch expression="#ThisTag.ExecutionMode#">
		
		<cfcase value="start">
		
			<!--- correct tag usage checks --->
			<cfif NOT ListFind(GetBaseTagList(),"CF_TERRAFORM")>
				<cfabort showerror="TerraForm implementation error: CF_terrafield must be nested inside a CF_terraform tag pair.">
			</cfif>
			<cfif NOT ThisTag.HasEndTag>
				<cfabort showerror="TerraForm implementation error: CF_terrafield must have an end tag.">
			</cfif>
			
			<!--- shift this tag's attributes into the parent CF_TERRAFORM tag --->
			<cfassociate basetag="CF_terraform" datacollection="Fields">
			
		</cfcase>
		
		<cfcase value="end">
		
			<!--- The default attribute for this field is the content between
			the start and end CF_terrafield tags, if not already defined as an attribute --->
			<cfif len(ThisTag.GeneratedContent)>
				<cfparam name="Attributes.Default" default="#Trim(ThisTag.GeneratedContent)#">
			</cfif>
			
			<!--- insert a placeholder so we know where to substitute the HTML
			block for this field --->
			<cfset request.terraform.CurrentFieldNum = request.terraform.CurrentFieldNum + 1>

			<cfset ThisTag.GeneratedContent = "#chr(27)#terrafield #request.terraform.CurrentFieldNum##chr(27)#">
			
		</cfcase>
		
	</cfswitch>
</cfsilent>