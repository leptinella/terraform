

// Original:  Ronnie T. Moore
// Web Site:  The JavaScript Source
// Dynamic 'fix' by: Nannette Thacker
// Web Site: http://www.shiningstar.net
// This script and many more are available free online at
// The JavaScript Source!! http://javascript.internet.com
function terraform_textarea_counter(field, countfield, maxlimit) {
	if (field.value.length > maxlimit) // if too long...trim it!
		field.value = field.value.substring(0, maxlimit);
	// otherwise, update 'characters left' counter
	else 
		countfield.value = maxlimit - field.value.length;
}


var terraFormApi = new Object();
terraFormApi.fields = new Object();				
terraFormApi.forms = new Object();
terraFormApi.formatPrototypes = new Object();

function getDisabled() {
	return document.getElementById(this.id).disabled;
}

function getValue() {
	return document.getElementById(this.id).value;
}

function setDisabled(disabled) {
	document.getElementById(this.id).disabled = disabled;
	
	if ( this.suffixes ) {
		var arrSuffixes = this.suffixes.split(',');
		if ( arrSuffixes.length )
			for ( var i in arrSuffixes )
				if ( document.getElementById(this.id + '.' + arrSuffixes[i]) )
					document.getElementById(this.id + '.' + arrSuffixes[i]).disabled = disabled;
	}
	return disabled;
}

function setValue(value) {
	document.getElementById(this.id).value = value;
	return value;
}

function toggleDisabled() {
	return this.setDisabled(!this.getDisabled());
}

function doNothing() {}





function booleanGetValue() {
	return document.getElementById(this.id).checked;
}

terraFormApi.formatPrototypes['boolean'] = function(id) {
	this.id					 = id;
	this.suffixes			 = "label";
	this.getDisabled		 = getDisabled;
	this.getValue			 = booleanGetValue;
	this.setDisabled		 = setDisabled;
	this.setValue 			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
};





terraFormApi.formatPrototypes.button = function(id) {
	this.id					 = id;
	this.suffixes			 = "";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = setDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
};





terraFormApi.formatPrototypes.calendar = function(id) {
	this.id					 = id;
	this.suffixes			 = "anchor";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = setDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
};





function checkboxGetValue() {
	var length = document.getElementById(this.id).title;
	var values = new Array();
	var thisCheckBox;
	for ( var i = 0; i < length; i++ ) {
		thisCheckBox = document.getElementById(this.id + '.value.' + i)
		if ( thisCheckBox.checked )
			values.push(thisCheckBox.value)
	}
	return values;
}

function checkboxSetDisabled(disabled) {
	var length = document.getElementById(this.id).title;
	for ( var i = 0; i < length; i++ )
		 {
			document.getElementById(this.id + '.value.' + i).disabled = disabled;
			document.getElementById(this.id + '.label.' + i).disabled = disabled;
		}
	document.getElementById(this.id + '.fieldset').disabled = disabled;
	return disabled;
}

terraFormApi.formatPrototypes.checkbox = function(id) {
	this.id					 = id;
	this.getDisabled		 = getDisabled;
	this.getValue			 = checkboxGetValue;
	this.setDisabled		 = checkboxSetDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
};





terraFormApi.formatPrototypes.colorpicker = function(id) {
	this.id					 = id;
	this.suffixes			 = "anchor";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = setDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
};





terraFormApi.formatPrototypes.combo = function(id) {
	this.id					 = id;
	this.suffixes			 = "select";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = setDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
};







function datepickerGetValue() {
	var y = document.getElementById(this.id + '.year').value;
	var m = document.getElementById(this.id + '.month').value;
	var d = document.getElementById(this.id + '.day').value
	if ( ( y > 0 ) && ( m > 0 ) && ( d > 0 ) )
		return new Date(y, m - 1, d);
	else
		return null;
}
function datepickerSetValue(value) {
	document.getElementById(this.id + '.year').value = value.getFullYear();
	document.getElementById(this.id + '.month').value = value.getMonth() + 1;
	document.getElementById(this.id + '.day').value = value.getDate();
}

terraFormApi.formatPrototypes.datepicker = function(id) {
	this.id					 = id;
	this.suffixes			 = "day,month,year";
	this.getDisabled		 = getDisabled;
	this.getValue			 = datepickerGetValue;
	this.setDisabled		 = setDisabled;
	this.setValue			 = datepickerSetValue;
	this.toggleDisabled		 = toggleDisabled;
};





terraFormApi.formatPrototypes.email = terraFormApi.formatPrototypes.button;




terraFormApi.formatPrototypes.enhancedfile = terraFormApi.formatPrototypes.button;





terraFormApi.formatPrototypes.fckeditor = terraFormApi.formatPrototypes.button;
terraFormApi.formatPrototypes.ckeditor = terraFormApi.formatPrototypes.button;





terraFormApi.formatPrototypes.file = terraFormApi.formatPrototypes.button;





terraFormApi.formatPrototypes.hidden = terraFormApi.formatPrototypes.button;





terraFormApi.formatPrototypes.htmlarea = function(id) {
	this.id					 = id;
	this.suffixes			 = "";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = doNothing;
	this.setValue			 = setValue;
	this.toggleDisabled		 = doNothing;
};





terraFormApi.formatPrototypes.image = terraFormApi.formatPrototypes.button;





terraFormApi.formatPrototypes.moveselect = function(id) {
	this.id					 = id;
	this.suffixes			 = "left,right,heading1,heading2,button1,button2,button3,button4";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = setDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
};





terraFormApi.formatPrototypes.msslider = terraFormApi.formatPrototypes.button;





terraFormApi.formatPrototypes["null"] = function(id) {
	this.id					 = id;
	this.suffixes			 = "";
	this.getDisabled		 = doNothing;
	this.getValue			 = doNothing;
	this.setDisabled		 = doNothing;
	this.setValue			 = doNothing;
	this.toggleDisabled		 = doNothing;
};





terraFormApi.formatPrototypes.numeric 
	= terraFormApi.formatPrototypes.password
	= terraFormApi.formatPrototypes.button
	= terraFormApi.formatPrototypes.recaptcha
	= terraFormApi.formatPrototypes.search 
	= terraFormApi.formatPrototypes.button;



terraFormApi.formatPrototypes.radio = terraFormApi.formatPrototypes.checkbox;





function selectSetDisabled(disabled) {
	document.getElementById(this.id).disabled = disabled;
	
	if ( this.suffixes ) {
		var arrSuffixes = this.suffixes.split(',');
		if ( arrSuffixes.length )
			for ( var i in arrSuffixes )
				if ( document.getElementById(this.id + '.' + arrSuffixes[i]) )
					document.getElementById(this.id + '.' + arrSuffixes[i]).disabled = disabled;
	}
	return disabled;
}

terraFormApi.formatPrototypes.select = function(id) {
	this.id					 = id;
	this.suffixes			 = "";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = selectSetDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
}





function spineditSetDisabled(disabled) {
	document.getElementById(this.id).disabled = disabled;
	if ( disabled ) {
		document.getElementById(this.id + '.up').style.display = 'none';
		document.getElementById(this.id + '.down').style.display = 'none';
		document.getElementById(this.id + '.upDisabled').style.display = 'inline';
		document.getElementById(this.id + '.downDisabled').style.display = 'inline';
	}
	else {
		document.getElementById(this.id + '.up').style.display = 'inline';
		document.getElementById(this.id + '.down').style.display = 'inline';
		document.getElementById(this.id + '.upDisabled').style.display = 'none';
		document.getElementById(this.id + '.downDisabled').style.display = 'none';
	}
	return disabled;
}

terraFormApi.formatPrototypes.spinedit = function(id) {
	this.id					 = id;
	this.suffixes			 = "";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = spineditSetDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
}





function starsSetDisabled(disabled) {
	document.getElementById(this.id).disabled = disabled;
	if ( disabled ) {
		document.getElementById(this.id + '.disabledStars').style.display = 'inline';
		document.getElementById(this.id + '.enabledStars').style.display = 'none';
	}
	else {
		document.getElementById(this.id + '.disabledStars').style.display = 'none';
		document.getElementById(this.id + '.enabledStars').style.display = 'inline';
	}
	return disabled;
}

terraFormApi.formatPrototypes.stars = function(id) {
	this.id					 = id;
	this.suffixes			 = "";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = starsSetDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
}





function submitSetDisabled(disabled) {
	document.getElementById(this.id).disabled = disabled;
	return disabled;
}

terraFormApi.formatPrototypes.submit = function(id) {
	this.id					 = id;
	this.suffixes			 = "";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = submitSetDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
}





terraFormApi.formatPrototypes.submitbutton = terraFormApi.formatPrototypes.submit;





terraFormApi.formatPrototypes.text = terraFormApi.formatPrototypes.button;





terraFormApi.formatPrototypes.textarea = terraFormApi.formatPrototypes.button;





function timepickerGetValue() {
	var result = document.getElementById(this.id + '.hours').value;
	if ( document.getElementById(this.id + '.minutes') )
		result = result + ':' + document.getElementById(this.id + '.minutes').value;
	if ( document.getElementById(this.id + '.seconds') )
		result = result + ':' + document.getElementById(this.id + '.seconds').value;
	if ( document.getElementById(this.id + '.ampm') )
		result = result + ' ' + document.getElementById(this.id + '.ampm').value;
	return result;
}

terraFormApi.formatPrototypes.timepicker = function(id) {
	this.id					 = id;
	this.suffixes			 = "hours,minutes,seconds,ampm";
	this.getDisabled		 = getDisabled;
	this.getValue			 = timepickerGetValue;
	this.setDisabled		 = setDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
};





function timespanpickerGetValue() {
	var totalSeconds = 0;
	var baseUnit = document.getElementById(this.id + '.baseUnit').value;
	
	if ( document.getElementById(this.id + '.days') )
		totalSeconds += parseInt(document.getElementById(this.id + '.days').value * 86400);
	if ( document.getElementById(this.id + '.hours') )
		totalSeconds += parseInt(document.getElementById(this.id + '.hours').value * 3600);
	if ( document.getElementById(this.id + '.minutes') )	
		totalSeconds += parseInt(document.getElementById(this.id + '.minutes').value * 60);
	if ( document.getElementById(this.id + '.seconds') )	
		totalSeconds += parseInt(document.getElementById(this.id + '.seconds').value);
 
	switch ( baseUnit ) {
		case "d": return totalSeconds / 86400;
		case "h": return totalSeconds / 3600;
		case "n": return totalSeconds / 60;
		case "s": return totalSeconds;
	}
}
 
terraFormApi.formatPrototypes.timespanpicker = function(id) {
	this.id							   	= id;
	this.suffixes					   	= "days,hours,minutes,seconds";
	this.getDisabled					= getDisabled;
	this.getValue						= timespanpickerGetValue;
	this.setDisabled					= setDisabled;
	this.setValue						= setValue;
	this.toggleDisabled				   	= toggleDisabled;
};
 
 
 


terraFormApi.formatPrototypes.url = function(id) {
	this.id					 = id;
	this.suffixes			 = "button";
	this.getDisabled		 = getDisabled;
	this.getValue			 = getValue;
	this.setDisabled		 = setDisabled;
	this.setValue			 = setValue;
	this.toggleDisabled		 = toggleDisabled;
};





	
function terraFormRefresh(e) {
	var sourceForm = (e.target) ? e.target.form : e.srcElement.form;
	sourceForm.elements[sourceForm.name].value = 'refresh';
	sourceForm.submit();
}








// Browser Onload script 
// From http://javascript.about.com/library/scripts/blsafeonload.htm

// Browser Detection
isMac = (navigator.appVersion.indexOf("Mac")!=-1) ? true : false;
NS4 = (document.layers) ? true : false;
IEmac = ((document.all)&&(isMac)) ? true : false;
IE4plus = (document.all) ? true : false;
IE4 = ((document.all)&&(navigator.appVersion.indexOf("MSIE 4.")!=-1)) ? true : false;
IE5 = ((document.all)&&(navigator.appVersion.indexOf("MSIE 5.")!=-1)) ? true : false;
ver4 = (NS4 || IE4plus) ? true : false;
NS6 = (!document.layers) && (navigator.userAgent.indexOf('Netscape')!=-1)?true:false;

// Body onload utility (supports multiple onload functions)
var gSafeOnload = new Array();
function SafeAddOnload(f)
{
	if (IEmac && IE4)  // IE 4.5 blows out on testing window.onload
	{
		window.onload = SafeOnload;
		gSafeOnload[gSafeOnload.length] = f;
	}
	else if  (window.onload)
	{
		if (window.onload != SafeOnload)
		{
			gSafeOnload[0] = window.onload;
			window.onload = SafeOnload;
		}		
		gSafeOnload[gSafeOnload.length] = f;
	}
	else
		window.onload = f;
}
function SafeOnload()
{
	for (var i=0;i<gSafeOnload.length;i++)
		gSafeOnload[i]();
}

// Call the following with your function as the argument
// SafeAddOnload(yourfunctioname);





/*
Adapted from...
Submit Once form validation- 
� Dynamic Drive (www.dynamicdrive.com)
For full source code, usage terms, and 100's more DHTML scripts, visit http://dynamicdrive.com
*/

function hidebuttons(theform){
	document.body.style.cursor = "wait";
	//if IE 4+ or NS 6+
	if (document.all||document.getElementById){
	//screen thru every element in the form, and hunt down "submit" and "reset"
		for (i=0;i<theform.length;i++){
			var tempobj=theform.elements[i]
			try {
				if(tempobj.type.toLowerCase()=="submit"||tempobj.type.toLowerCase()=="button") {
					var found = tempobj.name.match(/_hidden$/);
					if (found)
						tempobj.style.display="inline";
					else
						tempobj.style.display="none";
				}
			}
			catch(errorObject) {}	
		}
	}
}




function comboLookup(id) {
	var textBox = document.getElementById(id);
	var selectBox = document.getElementById(id + '.select');
	selectBox.selectedIndex = -1;
	if ( textBox.createTextRange ) {
		var e = window.event;
		if ( e.keyCode != 8 && e.keyCode != 46 ) {
			var key = textBox.value.toLowerCase();
			if ( key.length ) 
				for ( var i = 0; i < selectBox.options.length; i++ ) 
					if ( key == selectBox.options[i].innerHTML.substr(0, key.length).toLowerCase() ) {
						selectBox.selectedIndex = i;
						textBox.value = selectBox.options[i].innerHTML;
						var tr = textBox.createTextRange();
						tr.moveStart('character', key.length);
						tr.select();
					}
		}	
		else {
			var key = textBox.value.toLowerCase();
			if ( key.length ) 
				for ( var i = 0; i < selectBox.options.length; i++ ) 
					if ( key == selectBox.options[i].innerHTML.toLowerCase() ) {
						selectBox.selectedIndex = i;
						textBox.value = selectBox.options[i].innerHTML;
						var tr = textBox.createTextRange();
						tr.moveStart('character', key.length);
						tr.select();
					}
		}		
	}
}




terraFormApi.formatPrototypes.soeditor = terraFormApi.formatPrototypes.button;
terraFormApi.formatPrototypes.tinymce = terraFormApi.formatPrototypes.button;
