<!--- 
<fusedoc fuse="colorpicker.cfm" language="ColdFusion" specification="2.0">
	<responsibilities>
		I display color preview/select utility.
		When a color is selected I return the selected hex color value to a form field (for whatever formname and fieldname are passed in via the querystring) in the window.opener.
		I then close the popup window.
	</responsibilities>
	<properties>
		<history type="Modify" date="20020815" author="Matthew Walker" email="esw@eswsoftware.com" comments="Modified for the TerraForm environment with Brent's permission. Changed this file name from dsp_colorpicker.cfm to colorpicker.cfm . The file formerly called colorpicker.cfm is not needed here as its functions are handled by the TerraForm settings file. Also changed the <select name="cboColorGrid"> tag as it seemed to be doubling up the colourgrid in the URL."/>
		<history type="Create" date="8.27.2001" author="Brent V. Ertz" email="brent80301@yahoo.com" comments="email me for help, to complain, or to suggest enhancements"/>
	</properties>	
	<io>
		<in>
			<string name="URL.formname" optional="No" comments="The name of your form in which this button appears"/>		
			<string name="URL.fieldname" optional="No" comments="The name of your formfield that will contain the return color value"/>
			<string name="URL.inc" optional="No" comments="The relative (preferable) or full HTTP path to the directory of your website that the colorpicker files were extracted to"/>			
			<string name="URL.colorgrid" optional="Yes" comments="The name of the color grid that will serve as the default (colorcubes, continuous, windows, mac, grayscale)" default="colorcubes"/>						
			<list name="URL.disabledgrids" optional="Yes" comments="A comma delimited list of any color grid names that you would like to be disabled for this colorpicker (colorcubes, continuous, windows, mac, grayscale)" default="0"/>									
		</in>
	</io>
	<note>
		It is amazingly easy to add custom color grids.  There are 2 steps.
			1.) Simply find the code for the select box in this page and add another selection (mimic the existing format)
			2.) Find the switch/case statements below the select box and add another case (mimic the existing format)
	</note>
</fusedoc> 
--->
			<!--- Set the default color grid to be used --->
			<cfparam name="URL.colorgrid" default="colorcubes">
			<cfparam name="URL.disabledgrids" default="0">			
			
			<!--- Detect Browser and version for proper display --->
			<cfscript>
			browserName="Unknown";
			browserVersion="0";
			if (Find("MSIE",CGI.HTTP_USER_AGENT)) { 
				browserName="MSIE";
				browserVersion=Val(RemoveChars(CGI.HTTP_USER_AGENT,1,Find("MSIE",CGI.HTTP_USER_AGENT)+4));
			} else if (Find("Opera",CGI.HTTP_USER_AGENT)) { 		
				browserName="Opera";
				browserVersion=Val(RemoveChars(CGI.HTTP_USER_AGENT,1,Find("Opera",CGI.HTTP_USER_AGENT)+4));			
			} else {
				if (Find("Mozilla",CGI.HTTP_USER_AGENT)) { // it's a Netscape compatible browser
					if (not Find("compatible",CGI.HTTP_USER_AGENT) AND not Find("Gecko",CGI.HTTP_USER_AGENT) OR Find("Netscape",CGI.HTTP_USER_AGENT)) { 
						browserName="Netscape";
						browserVersion=Val(RemoveChars(CGI.HTTP_USER_AGENT,1,Find("/",CGI.HTTP_USER_AGENT)));
					} else {
						browserName="compatible";
					}
				}
			}
			</cfscript>			
			<cfif variables.browserName IS "MSIE" and variables.browserVersion GTE 4>
				<!--- IE 4+ --->
				<cfset textBoxSize="8">
			<cfelseif variables.browserName IS "Opera">
				<!--- NS/compatible >=4<6 --->
				<cfset textBoxSize="12">				
			<cfelseif (variables.browserName IS "Netscape") AND variables.browserVersion GTE 4 AND variables.browserVersion LT 5>
				<!--- NS/compatible >=4<6 --->
				<cfset textBoxSize="7">
			<cfelseif (variables.browserName IS "Netscape") AND variables.browserVersion GTE 5>
				<cfset textBoxSize="5"><!--- 5 for Netscape 6 and 12 for Mozilla --->
			<cfelseif (variables.browserName IS "compatible")>
				<cfset textBoxSize="12"><!--- 5 for Netscape 6 and 12 for Mozilla --->
			<cfelse>
				<cfset textBoxSize="7">
			</cfif>			
			<html>
			<head>
			<title><cfparam name="url.title" default="Color Picker"><cfoutput>#url.title#</cfoutput></title>
			<meta http-equiv="Content-Type" content="text/html;">
			<script type="text/javascript">
			<!--
			function showColor(newcolor) {
			//when a color is moused over
			//show the hex color value in the textbox
			document.frmColorPicker.txtColor.value=newcolor;
			
			//show the color in the toolbar rectangle
				if(document.layers) { //browser="NN4";     
					document.layers["divColor"].bgColor=newcolor;         
				} else if(document.all) { //browser="ie"          
				   document.all.divColor.style.backgroundColor=newcolor;  
				} else if(!document.all && document.getElementById) { //browser="NN6";          
				   document.getElementById("divColor").style.backgroundColor=newcolor;           
				}
			}
				
			function selectColor(newcolor) {
				//when a color is clicked
				if(!window.opener.closed) {
					<!--- // window.opener.document.<cfoutput>#URL.formName#</cfoutput>.<cfoutput>#URL.fieldName#</cfoutput>.value=newcolor; --->
					
					window.opener.document.getElementById("<cfoutput>#url.id#</cfoutput>").value=newcolor;           
					window.opener.focus();
				}
				window.close();
			}
			
			function customColor() {
				//used when a color value is typed in the textbox
				var cColor = document.frmColorPicker.txtColor.value;
				if(!window.opener.closed){
					<!--- // window.opener.document.<cfoutput>#URL.formName#</cfoutput>.<cfoutput>#URL.fieldName#</cfoutput>.value=cColor; --->
					window.opener.document.getElementById("<cfoutput>#url.id#</cfoutput>").value=cColor; 
					window.opener.focus();
				}
				window.close();			
			}
			
			function defaultColor() {
				//when the colorpicker is opened
				<!--- //if(window.opener.document.<cfoutput>#URL.formName#</cfoutput>.<cfoutput>#URL.fieldName#</cfoutput>.value != "") {
				//	showColor(window.opener.document.<cfoutput>#URL.formName#</cfoutput>.<cfoutput>#URL.fieldName#</cfoutput>.value);
				//} --->
				if(window.opener.document.getElementById("<cfoutput>#url.id#</cfoutput>").value != "") {
					showColor(window.opener.document.getElementById("<cfoutput>#url.id#</cfoutput>").value);
				}
			}
			//-->
			</script>
			<style type="text/css">
			<!--
			.textbox {  font-family: Arial, Helvetica, sans-serif; font-size: 8pt; background-color: #FFFFFF;}
			.combobox {  font-family: Arial, Helvetica, sans-serif; font-size: 8pt; background-color: #CCCCCC;}
			body {  background-image: url(loading.gif); background-repeat: no-repeat}			
			-->
			</style>
			</head>
			<body bgcolor="#000000" marginheight="0" marginwidth="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" onLoad="self.focus();defaultColor();">
			<cfoutput><form action="#cgi.script_name#" method="get" name="frmColorPicker" id="frmColorPicker" onSubmit="return customColor();"><input type="hidden" name="title" value="#url.title#" /></cfoutput>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#000000">
			  <tr>
			      <td>
				  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  			<tr>
			    			<td bgcolor="#CCCCCC">  
						        <table border="0" cellpadding="1" cellspacing="0" width="100%">
						          <tr>
								      <td width="40">
				  					  <!--- Color Cube Table --->
									  	<table width="42" border="0" cellspacing="0" cellpadding="1" bordercolor="#000000" bgcolor="#000000">
											<tr>
												<td><div id="divColor" style="position:relative; width:40px; height:18px; z-index:1; background-color: #000000; layer-background-color: #000000; border: 1px none #000000"></div></td>
											</tr>
										</table>	  
									  </td>
								      <td align="right" bgcolor="#CCCCCC"><cfoutput><input type="text" name="txtColor" size="#textBoxSize#" maxlength="7" class="textbox" onChange="showColor(this.value);" /></cfoutput></td>
									  <td align="right" bgcolor="#CCCCCC"><cfoutput>
									  <div class="combobox">
									  <select name="cboColorGrid" class="combobox" onChange="parent.location='#CGI.SCRIPT_NAME#?id=#URL.id#&inc=#URL.inc#&disabledgrids=#URL.DisabledGrids#&colorgrid='+this.options[this.selectedIndex].value">
										<cfif NOT ListFindNoCase(URL.disabledgrids, "colorcubes")><option value="colorcubes" <cfif url.colorgrid is "colorcubes">selected</cfif>>Color Cubes</option></cfif>
										<cfif NOT ListFindNoCase(URL.disabledgrids, "continuous")><option value="continuous" <cfif url.colorgrid is "continuous">selected</cfif>>Continuous</option></cfif>
										<cfif NOT ListFindNoCase(URL.disabledgrids, "windows")><option value="windows" <cfif url.colorgrid is "windows">selected</cfif>>Windows OS</option></cfif>
										<cfif NOT ListFindNoCase(URL.disabledgrids, "mac")><option value="mac" <cfif url.colorgrid is "mac">selected</cfif>>Mac OS</option></cfif>
										<cfif NOT ListFindNoCase(URL.disabledgrids, "grayscale")><option value="grayscale" <cfif url.colorgrid is "grayscale">selected</cfif>>Grayscale</option></cfif>										
										<!--- Add your custom color grid option here.  Simply mimic the above selections --->
									  </select></div></cfoutput></td>
								 </tr>
								</table>
							</td>
					  </tr>
				  </table>
				</td>
			  </tr>
			  <tr>
				   <td align="center">
					   <cfswitch expression="#URL.colorgrid#">
						   <cfcase value="colorcubes">
								<cfset lGrid ="000000,NULL,000000,003300,006600,009900,00CC00,00FF00,330000,333300,336600,339900,33CC00,33FF00,660000,663300,666600,669900,66CC00,66FF00,333333,NULL,000033,003333,006633,009933,00CC33,00FF33,330033,333333,336633,339933,33CC33,33FF33,660033,663333,666633,669933,66CC33,66FF33,666666,NULL,000066,003366,006666,009966,00CC66,00FF66,330066,333366,336666,339966,33CC66,33FF66,660066,663366,666666,669966,66CC66,66FF66,999999,NULL,000099,003399,006699,009999,00CC99,00FF99,330099,333399,336699,339999,33CC99,33FF99,660099,663399,666699,669999,66CC99,66FF99,CCCCCC,NULL,0000CC,0033CC,0066CC,0099CC,00CCCC,00FFCC,3300CC,3333CC,3366CC,3399CC,33CCCC,33FFCC,6600CC,6633CC,6666CC,6699CC,66CCCC,66FFCC,FFFFFF,NULL,0000FF,0033FF,0066FF,0099FF,00CCFF,00FFFF,3300FF,3333FF,3366FF,3399FF,33CCFF,33FFFF,6600FF,6633FF,6666FF,6699FF,66CCFF,66FFFF,FF0000,NULL,990000,993300,996600,999900,99CC00,99FF00,CC0000,CC3300,CC6600,CC9900,CCCC00,CCFF00,FF0000,FF3300,FF6600,FF9900,FFCC00,FFFF00,00FF00,NULL,990033,993333,996633,999933,99CC33,99FF33,CC0033,CC3333,CC6633,CC9933,CCCC33,CCFF33,FF0033,FF3333,FF6633,FF9933,FFCC33,FFFF33,0000FF,NULL,990066,993366,996666,999966,99CC66,99FF66,CC0066,CC3366,CC6666,CC9966,CCCC66,CCFF66,FF0066,FF3366,FF6666,FF9966,FFCC66,FFFF66,FFFF00,NULL,990099,993399,996699,999999,99CC99,99FF99,CC0099,CC3399,CC6699,CC9999,CCCC99,CCFF99,FF0099,FF3399,FF6699,FF9999,FFCC99,FFFF99,0000FF,NULL,9900CC,9933CC,9966CC,9999CC,99CCCC,99FFCC,CC00CC,CC33CC,CC66CC,CC99CC,CCCCCC,CCFFCC,FF00CC,FF33CC,FF66CC,FF99CC,FFCCCC,FFFFCC,FF00FF,NULL,9900FF,9933FF,9966FF,9999FF,99CCFF,99FFFF,CC00FF,CC33FF,CC66FF,CC99FF,CCCCFF,CCFFFF,FF00FF,FF33FF,FF66FF,FF99FF,FFCCFF,FFFFFF">
						   </cfcase>
						   <cfcase value="continuous">
						   		<cfset lGrid = "000000,NULL,CCFFFF,CCFFCC,CCFF99,CCFF66,CCFF33,CCFF00,66FF00,66FF33,66FF66,66FF99,66FFCC,66FFFF,00FFFF,00FFCC,00FF99,00FF66,00FF33,00FF00,333333,NULL,CCCCFF,CCCCCC,CCCC99,CCCC66,CCCC33,CCCC00,66CC00,66CC33,66CC66,66CC99,66CCCC,66CCFF,00CCFF,00CCCC,00CC99,00CC66,00CC33,00CC00,666666,NULL,CC99FF,CC99CC,CC9999,CC9966,CC9933,CC9900,669900,669933,669966,669999,6699CC,6699FF,0099FF,0099CC,009999,009966,009933,009900,999999,NULL,CC66FF,CC66CC,CC6699,CC6666,CC6633,CC6600,666600,666633,666666,666699,6666CC,6666FF,0066FF,0066CC,006699,006666,006633,006600,CCCCCC,NULL,CC33FF,CC33CC,CC3399,CC3366,CC3333,CC3300,663300,663333,663366,663399,6633CC,6633FF,0033FF,0033CC,003399,003366,003333,003300,FFFFFF,NULL,CC00FF,CC00CC,CC0099,CC0066,CC0033,CC0000,660000,660033,660066,660099,6600CC,6600FF,0000FF,0000CC,000099,000066,000033,000000,FF0000,NULL,FF00FF,FF00CC,FF0099,FF0066,FF0033,FF0000,990000,990033,990066,990099,9900CC,9900FF,3300FF,3300CC,330099,330066,330033,330000,00FF00,NULL,FF33FF,FF33CC,FF3399,FF3366,FF3333,FF3300,993300,993333,993366,993399,9933CC,9933FF,3333FF,3333CC,333399,333366,333333,333300,0000FF,NULL,FF66FF,FF66CC,FF6699,FF6666,FF6633,FF6600,996600,996633,996666,996699,9966CC,9966FF,3366FF,3366CC,336699,336666,336633,336600,FFFF00,NULL,FF99FF,FF99CC,FF9999,FF9966,FF9933,FF9900,999900,999933,999966,999999,9999CC,9999FF,3399FF,3399CC,339999,339966,339933,339900,00FFFF,NULL,FFCCFF,FFCCCC,FFCC99,FFCC66,FFCC33,FFCC00,99CC00,99CC33,99CC66,99CC99,99CCCC,99CCFF,33CCFF,33CCCC,33CC99,33CC66,33CC33,33CC00,FF00FF,NULL,FFFFFF,FFFFCC,FFFF99,FFFF66,FFFF33,FFFF00,99FF00,99FF33,99FF66,99FF99,99FFCC,99FFFF,33FFFF,33FFCC,33FF99,33FF66,33FF33,33FF00">
						   </cfcase>
						   <cfcase value="windows">
								<cfset lGrid = "FFFFFF,00FFFF,FF00FF,00FFFF,FFFF00,00FF00,FF0000,808080,A0A0A4,FFFBF0,2A3F55,2A3F00,2A1FFF,2A1FAA,2A1F55,2A1F00,2A00FF,2A00AA,2A0055,2A0000,00FFAA,00FF55,00DFFF,00DFAA,00DF55,00DF00,00BFFF,00BFAA,00BF55,00BF00,009FFF,009FAA,009F55,009F00,007FFF,007FAA,007F55,007F00,CCFFFF,99FFFF,66FFFF,33FFFF,FFCCFF,CCCCFF,FFFFAA,FFFF55,FFDFFF,FFDFAA,FFDF55,FFDF00,FFBFFF,FFBFAA,FFBF55,FFBF00,FF9FFF,FF9FAA,FF9F55,FF9F00,FF7FFF,FF7FAA,FF7F55,FF7F00,FF5FFF,FF5FAA,FF5F55,FF5F00,FF3FFF,FF3FAA,FF3F55,FF3F00,FF1FFF,FF1FAA,FF1F55,FF1F00,FF00AA,FF0055,D4FFFF,D4FFAA,D4FF55,D4FF00,D4DFFF,D4DFAA,D4DF55,D4DF00,D4BFFF,D4BFAA,D4BF55,D4BF00,D49FFF,D49FAA,D49F55,D49F00,D47FFF,D47FAA,D47F55,D47F00,D45FFF,D45FAA,D45F55,D45F00,D43FFF,D43FAA,D43F55,D43F00,D41FFF,D41FAA,D41F55,D41F00,D400FF,D400AA,D40055,D40000,AAFFFF,AAFFAA,AAFF55,AAFF00,AADFFF,AADFAA,AADF55,AADF00,AABFFF,AABFAA,AABF55,AABF00,AA9FFF,AA9FAA,AA9F55,AA9F00,AA7FFF,AA7FAA,AA7F55,AA7F00,AA5FFF,AA5FAA,AA5F55,AA5F00,AA3FFF,AA3FAA,AA3F55,AA3F00,AA1FFF,AA1FAA,AA1F55,AA1F00,AA00FF,AA00FF,AA0055,AA0000,7FFFFF,7FFFAA,7FFF55,7FFF00,7FDFFF,7FDFAA,7FDF55,7FDF00,7FBFFF,7FBFAA,7FBF55,7FBF00,7F9FFF,7F9FAA,7F9F55,7F9F00,7F7FFF,7F7FAA,7F7F55,7F7F00,7F5FFF,7F5FAA,7F5F55,7F5F00,7F3FFF,7F3FAA,7F3F55,7F3F00,7F1FFF,7F1FAA,7F1F55,7F1F00,7F00FF,7F00AA,7F0055,7F0000,55FFFF,55FFAA,55FF55,55FF00,55DFFF,55DFAA,55DF55,55DF00,55BFFF,55BFAA,55BF55,55BF00,559FFF,559FAA,559F55,559F00,557FFF,557FAA,557F55,557F00,555FFF,555FAA,555F55,555F00,553FFF,553FAA,553F55,553F00,551FFF,551FAA,551F55,551F00,5500FF,5500AA,550055,550000,2AFFFF,2AFFAA,2AFF55,2AFF00,2ADFFF,2ADFAA,2ADF55,2ADF00,2ABFFF,2ABFAA,2ABF55,2ABF00,2A9FFF,2A9FAA,2A9F55,2A9F00,2A7FFF,2A7FAA,2A7F55,2A7F00,2A5FFF,2A5FAA,2A5F55,2A5F00,2A3FFF,2A3FAA,2A3F55,2A3F00,A6CAF0,C0DCC0,808080,008080,800080,000080,808000,008000,800000,000000,000000,000000">
						   </cfcase>
						   <cfcase value="mac">
								<cfset lGrid ="000000,111111,222222,444444,555555,777777,888888,AAAAAA,BBBBBB,DDDDDD,EEEEEE,000000,000011,000022,000044,000055,000077,000088,0000AA,0000BB,0000DD,0000EE,001100,002200,004400,005500,007700,008800,00AA00,00BB00,00DD00,00EE00,110000,220000,440000,550000,770000,880000,AA0000,BB0000,DD0000,EE0000,000033,000066,000099,0000CC,0000FF,003300,003333,003366,003399,0033CC,0033FF,006600,006633,006699,0066CC,0066FF,009900,009933,009966,009999,0099CC,0099FF,00CC00,00CC33,00CC66,00CC99,00CCCC,00CCFF,00FF00,00FF33,00FF66,00FF99,00FFCC,00FFFF,330000,330033,330066,330099,3300CC,3300FF,333300,333333,333366,333399,3333CC,3333FF,336600,336633,336666,336699,3366CC,3366FF,339900,339933,339966,3399CC,3399FF,33CC00,33CC33,33CC66,33CC99,33CCCC,33CCFF,33FF00,33FF33,33FF66,33FF99,33FFCC,33FFFF,660000,660033,660066,660099,6600CC,6600FF,663300,663333,663366,663399,6633CC,6633FF,666600,666633,666666,666699,6666CC,6666FF,669900,669933,669966,669999,6699CC,6699FF,66CC00,66CC33,66CC66,66CC99,66CCCC,66CCFF,66FF00,66FF33,66FF66,66FF99,66FFCC,66FFFF,990000,990033,990066,990099,9900CC,9900FF,993300,993333,993366,993399,9933CC,9933FF,996600,996633,996666,996699,9966CC,9966FF,999900,999933,999966,999999,9999CC,9999FF,99CC00,99CC33,99CC66,99CC99,99CCCC,99CCFF,99FF00,99FF33,99FF66,99FF99,99FFCC,99FFFF,CC0000,CC0033,CC0066,CC0099,CC00CC,CC00FF,CC3300,CC3333,CC3366,CC3399,CC33CC,CC33FF,CC6600,CC6633,CC6666,CC6699,CC66CC,CC66FF,CC9900,CC9933,CC9966,CC9999,CC99CC,CC99FF,CCCC00,CCCC33,CCCC66,CCCC99,CCCCCC,CCCCFF,CCFF00,CCFF33,CCFF66,CCFF99,CCFFCC,CCFFFF,FF0000,FF0033,FF0066,FF0099,FF00CC,FF00FF,FF3300,FF3333,FF3366,FF3399,FF33CC,FF33FF,FF6600,FF6633,FF6666,FF6699,FF66CC,FF66FF,FF9900,FF9933,FF9966,FF9999,FF99CC,FF99FF,FFFF00,FFFF33,FFFF66,FFFF99,FFFFCC,FFFFFF">
						   </cfcase>			
						   <cfcase value="grayscale">
								<cfset lGrid="FFFFFF,FEFEFE,FDFDFD,FCFCFC,FBFBFB,FAFAFA,F9F9F9,F8F8F8,F7F7F7,F6F6F6,F5F5F5,F4F4F4,F3F3F3,F2F2F2,F1F1F1,F0F0F0,EFEFEF,EEEEEE,EDEDED,ECECEC,EBEBEB,EAEAEA,E9E9E9,E8E8E8,E7E7E7,E6E6E6,E5E5E5,E4E4E4,E3E3E3,E2E2E2,E1E1E1,E0E0E0,DFDFDF,DEDEDE,DDDDDD,DCDCDC,DBDBDB,DADADA,D9D9D9,D8D8D8,D7D7D7,D6D6D6,D5D5D5,D4D4D4,D3D3D3,D2D2D2,D1D1D1,D0D0D0,CFCFCF,CECECE,CDCDCD,CCCCCC,CBCBCB,CACACA,C9C9C9,C8C8C8,C7C7C7,C6C6C6,C5C5C5,C4C4C4,C3C3C3,C2C2C2,C1C1C1,C0C0C0,BFBFBF,BEBEBE,BDBDBD,BCBCBC,BBBBBB,BABABA,B9B9B9,B8B8B8,B7B7B7,B6B6B6,B5B5B5,B4B4B4,B3B3B3,B2B2B2,B1B1B1,B0B0B0,AFAFAF,AEAEAE,ADADAD,ACACAC,ABABAB,AAAAAA,A9A9A9,A8A8A8,A7A7A7,A6A6A6,A5A5A5,A4A4A4,A3A3A3,A2A2A2,A1A1A1,A0A0A0,9F9F9F,9E9E9E,9D9D9D,9C9C9C,9B9B9B,9A9A9A,999999,989898,979797,969696,959595,949494,939393,929292,919191,909090,8F8F8F,8E8E8E,8D8D8D,8C8C8C,8B8B8B,8A8A8A,898989,888888,878787,868686,858585,848484,838383,828282,818181,808080,7F7F7F,7E7E7E,7D7D7D,7C7C7C,7B7B7B,7A7A7A,797979,787878,777777,767676,757575,747474,737373,727272,717171,707070,6F6F6F,6E6E6E,6D6D6D,6C6C6C,6B6B6B,6A6A6A,696969,686868,676767,666666,656565,646464,636363,626262,616161,606060,5F5F5F,5E5E5E,5D5D5D,5C5C5C,5B5B5B,5A5A5A,595959,585858,575757,565656,555555,545454,535353,525252,515151,505050,4F4F4F,4E4E4E,4D4D4D,4C4C4C,4B4B4B,4A4A4A,494949,484848,474747,464646,454545,434343,424242,414141,404040,3F3F3F,3E3E3E,3D3D3D,3C3C3C,3B3B3B,3A3A3A,393939,383838,373737,363636,353535,343434,333333,323232,313131,303030,2F2F2F,2E2E2E,2D2D2D,2C2C2C,2B2B2B,2A2A2A,292929,282828,272727,262626,252525,242424,232323,222222,212121,202020,1F1F1F,1E1E1E,1D1D1D,1C1C1C,1B1B1B,1A1A1A,191919,181818,171717,161616,151515,141414,131313,121212,111111,101010,0F0F0F,0E0E0E,0D0D0D,0C0C0C,0B0B0B,0A0A0A,090909,080808,070707,060606,050505,040404,030303,020202,010101,000000,000000,000000,000000,000000,000000"> 
						   </cfcase>
						   <!--- Add your custom color grid values here.  Simply mimic the above case statements --->
					   </cfswitch>
					
					<!--- OUTPUT THE COLOR GRID --->
					<table cellpadding="0" cellspacing="1" border="0">
						<tr>
						<cfset cellCount = 1>
						<cfset lenLGrid = listLen(lGrid)>
						<cfloop list="#lGrid#" index="color" delimiters=",">
							<cfoutput>
								<cfif color IS NOT "NULL">
								<td bgcolor="###color#"><a href="javascript:void(0);" onMouseOver="showColor('###color#');" onClick="selectColor('###color#');"><img src="transparent.gif" width="9" height="9" alt="###color#" style="border-width:0;margin:0"></a></td>
								<cfelse>
								<td bgcolor="##000000"><img src="transparent.gif" width="9" height="9" alt="" style="border-width:0;margin:1px"></td>		
								</cfif>
								<cfif cellCount MOD 20 IS 0>
									</tr>
									<cfif cellCount IS NOT lenLGrid>
										<tr>
									</cfif>
								<cfelseif cellCount IS lenLGrid>
									</tr>
								</cfif> 
								
							</cfoutput>	
							<cfset cellCount = cellCount + 1>
						</cfloop>
					</table>	
					<br />
					<br />
					<br />
					<br />
					<br />
					<br />
					<br />
			   </td>
			   </tr>
			</table>
			
			<cfoutput>
			<input type="hidden" name="id" value="#URL.id#">	
			<input type="hidden" name="inc" value="#URL.inc#">			
			</cfoutput>		
			</form>
			</body>
			</html>	





